package com.jtv.android.utils;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;

public abstract class SimpleCallback<T> implements Callback<T> {

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e("Retrofit", "onFailure: ", t);
    }
}
