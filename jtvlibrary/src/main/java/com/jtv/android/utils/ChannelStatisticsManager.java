package com.jtv.android.utils;

import android.util.Log;
import android.util.SparseArray;

import com.activeandroid.ActiveAndroid;
import com.jtv.android.models.Channel;
import com.jtv.android.models.ChannelStats;

import java.util.Calendar;

public class ChannelStatisticsManager {

    public static final String TAG = "ChannelStatistics";

    private SparseArray<Channel> activeChannels;
    private SparseArray<Long> startTime;

    public ChannelStatisticsManager() {
        this.activeChannels = new SparseArray<>();
        this.startTime = new SparseArray<>();
    }

    public void onChannelStarted(Channel channel){
        if(channel == null){
            return;
        }
        if (activeChannels.get(channel.getRemoteId()) != null) {
            Log.w(TAG, "onChannelStarted: Channel already registered. overwriting");
        }
        activeChannels.put(channel.getRemoteId(), channel);
        startTime.put(channel.getRemoteId(), getTime());
    }

    public void onChannelError(Channel channel){
        if(channel == null){
            return;
        }
        if (activeChannels.get(channel.getRemoteId()) == null) {
            Log.w(TAG, "onChannelStarted: Channel doesnt exist");
            return;
        }
        updateStats(channel, 1);
        activeChannels.remove(channel.getRemoteId());
        startTime.remove(channel.getRemoteId());
    }

    public void onChannelStopped(Channel channel){
        if(channel == null){
            return;
        }
        if (activeChannels.get(channel.getRemoteId()) == null) {
            Log.w(TAG, "onChannelStarted: Channel doesnt exist");
            return;
        }
        updateStats(channel, 0);
        activeChannels.remove(channel.getRemoteId());
        startTime.remove(channel.getRemoteId());
    }

    private long getTime(){
        return Calendar.getInstance().getTimeInMillis();
    }

    private void updateStats(Channel channel, int error){
        ActiveAndroid.beginTransaction();
        try{
            long currentTime = getTime();
            long watchedTime = currentTime - startTime.get(channel.getRemoteId());

            ChannelStats channelStatistic = Database.getChannelStatistic(channel.getRemoteId());
            if(channelStatistic == null){
                channelStatistic = new ChannelStats(channel.getRemoteId(), watchedTime, currentTime);
                channelStatistic.save();
            } else {
                channelStatistic.setLastWatchedAt(currentTime);
                channelStatistic.setWatchedTime(channelStatistic.getWatchedTime() + watchedTime);
                channelStatistic.setErrorCount(channelStatistic.getErrorCount() + error);
                channelStatistic.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

}
