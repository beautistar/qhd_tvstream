package com.jtv.android.utils;

public class QHDParser {

    public static final String TAG = "QHDParser";

    public static final int ERROR_LOGIN = 0;
    public static final int ERROR_INCORRECT_PASSWORD = 2;
    public static final int ERROR_USER_NOT_ACTIVATED = 3;
    public static final int ERROR_BAD_CODE = 4;
    public static final int ERROR_EMPTY = 5;
    public static final int ERROR_BAD_MAC = 6;
    public static final int ERROR_USER_ACTIVATED = 7;
    public static final int ERROR_CODE_ACTIVE = 8;


}
