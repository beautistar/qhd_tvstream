package com.jtv.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.jtv.android.models.LoginResult;

import java.security.MessageDigest;

/**
 * Handles Shared preferences
 *
 * @see SharedPreferences
 */
public class SettingsManager {

    public static final String PREF_PLAYER = "player";
    public static final String PREF_PLAYER_RATIO = "player_ratio";
    public static final String PREF_CAPTION_STYLE = "caption_style";
    public static final String PREF_CAPTION_SIZE = "caption_size";
    private static final String PREF_PASSWORD = "password";
    private static final String PREF_LOGIN = "login";
    private static final String PREF_AUTOBOOT = "autoboot";
    private static final String PREF_LAST_CATEGORY = "last_category";
    private static final String PREF_LAST_CHANNEL = "last_channel";
    private static final String PREF_LOCALE = "locale";
    private static final String PREF_THEME = "theme";
    private static final String PREF_LAST_UPDATED_AT = "last_updated_at";
    public static final String PREF_TOKEN_EXPIRE = "token_expire";
    public static final String PREF_LOGIN_TOKEN = "login_token";
    public static final String PREF_LAST_LOGIN = "last_login";
    private static final String PREF_NORMAL_EXIT = "normal_exit";
    private static final String PREF_SUBSCRIBE_NEWS = "subscribe_to_news";
    private static final String PREF_MEMO_CAST_ENABLE = "memo_cast";
    public static final String PREF_COUNTRY_PRIORITY = "priority_country";

    private final SharedPreferences preferences;

    public SettingsManager(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    /**
     * Encodes text using SHA1 algorithm
     *
     * @param text text to encode
     * @return encoded text
     */
    public static String SHA1(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] textBytes = text.getBytes("UTF-8");
            md.update(textBytes, 0, textBytes.length);
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Gets player id from preferences
     * Native player - 0
     * Exo player - 1
     *
     * @return media player id
     */
    public int getPlayer() {
        return Integer.parseInt(preferences.getString(PREF_PLAYER, "1"));
    }

    /**
     * Gets last category position (not id)
     *
     * @return category position
     */
    public int getLastCategory() {
        return preferences.getInt(PREF_LAST_CATEGORY, 0);
    }

    /**
     * Set last category position
     *
     * @param category category position
     */
    public void setLastCategory(int category) {
        preferences.edit().putInt(PREF_LAST_CATEGORY, category).apply();
    }

    /**
     * Gets last channel position (not id) in last category.
     *
     * @return channel position
     */
    public int getLastChannel() {
        return preferences.getInt(PREF_LAST_CHANNEL, -1);
    }

    /**
     * Sets last channel position
     *
     * @param channel channel position
     */
    public void setLastChannel(int channel) {
        preferences.edit().putInt(PREF_LAST_CHANNEL, channel).apply();
    }

    private String getLogin() {
        return preferences.getString(PREF_LOGIN, null);
    }

    public void setLogin(String login) {
        if (login != null) {
            preferences.edit()
                    .putString(PREF_LOGIN, login)
                    .putString(PREF_LAST_LOGIN, login)
                    .apply();
        }
        if (login == null && preferences.contains(PREF_LOGIN)) {
            preferences.edit().remove(PREF_LOGIN).apply();
        }
    }

    /**
     * Gets application's language
     *
     * @return locale name
     */
    public String getLocale() {
        return preferences.getString(PREF_LOCALE, "en");
    }

    public void setLocale(String lang) {
        preferences.edit().putString(PREF_LOCALE, lang).apply();
    }

    public int getCaptionStyle(){
        return Integer.parseInt(preferences.getString(PREF_CAPTION_STYLE, "0"));
    }

    public void setCaptionStyle(int captionStyle){
        preferences.edit().putString(PREF_CAPTION_STYLE, String.valueOf(captionStyle)).apply();
    }

    public int getCaptionSize(){
        return Integer.parseInt(preferences.getString(PREF_CAPTION_SIZE, "14"));
    }

    public void setCaptionSize(int size){
        preferences.edit().putString(PREF_CAPTION_SIZE, String.valueOf(size)).apply();
    }

    /**
     * Gets encoded parent control password
     *
     * @return encoded password
     */
    public String getPassword() {
        String encodedPassword = SHA1("0000");
        return preferences.getString(PREF_PASSWORD + getLogin(), encodedPassword);
    }

    public void setPassword(String password) {
        String encodedPassword = SHA1(password);
        preferences.edit().putString(PREF_PASSWORD + getLogin(), encodedPassword).apply();
    }

    public boolean getAutoBoot() {
        return preferences.getBoolean(PREF_AUTOBOOT, false);
    }

    /**
     * Gets movie's last watched position
     *
     * @param movie movie url
     * @return position, -1 if not set
     */
    public long getMovieTime(String movie) {
        return preferences.getLong(movie, -1);
    }

    /**
     * Sets movies last position
     *
     * @param movie movie url
     * @param time  movie position
     */
    public void setMovieTime(String movie, long time) {
        preferences.edit().putLong(movie, time).apply();
    }

    /**
     * Gets aspect ratio mode
     * <p>
     * 0 - Auto
     * 1 - 4:3
     * 2 - 16:9
     * 3 - 21:9
     *
     * @return aspect ratio mode
     */
    public int getAspectRatio() {
        return Integer.parseInt(preferences.getString(PREF_PLAYER_RATIO, "2"));
    }

    public int getTheme() {
        return Integer.parseInt(preferences.getString(PREF_THEME, "1"));
    }

    public long getLastUpdatedAt(){
        return preferences.getLong(PREF_LAST_UPDATED_AT, 0);
    }

    public void setLastUpdatedAt(long updatedAt){
        preferences.edit().putLong(PREF_LAST_UPDATED_AT, updatedAt).apply();
    }

    public void setLoginInfo(LoginResult result){
        preferences.edit()
                .putLong(PREF_TOKEN_EXPIRE, result.getTokenExpires())
                .putString(PREF_LOGIN_TOKEN, result.getLoginToken())
                .apply();
    }

    public long getTokenExpirationTime(){
        return preferences.getLong(PREF_TOKEN_EXPIRE, 0);
    }

    public String getLoginToken(){
        return preferences.getString(PREF_LOGIN_TOKEN, null);
    }

    public String getLastLogin(){
        return preferences.getString(PREF_LAST_LOGIN, "");
    }

    public void setNormalExit(boolean exitConfirm) {
        preferences.edit().putBoolean(PREF_NORMAL_EXIT, exitConfirm).apply();
    }

    public boolean wasNormalExit() {
        return preferences.getBoolean(PREF_NORMAL_EXIT, true);
    }

    public boolean checkSubscribtion(){
        return !preferences.contains(PREF_SUBSCRIBE_NEWS);
    }

    public boolean isSubscribed(){
        return preferences.getBoolean(PREF_SUBSCRIBE_NEWS, false);
    }

    public void setSubscribed(boolean subscribed){
        preferences.edit().putBoolean(PREF_SUBSCRIBE_NEWS, subscribed).apply();
    }

    public boolean isMemoCastEnabled(){
        return preferences.getBoolean(PREF_MEMO_CAST_ENABLE, false);
    }

    public void setMemoCastEnabled(boolean enabled){
        preferences.edit().putBoolean(PREF_MEMO_CAST_ENABLE, enabled).apply();
    }

    public String getPriorityCountry() {
        return preferences.getString(PREF_COUNTRY_PRIORITY, null);
    }

    public void setPriorityCountry(String priority){
        if(priority == null){
            preferences.edit().remove(PREF_COUNTRY_PRIORITY).apply();
            return;
        }
        preferences.edit().putString(PREF_COUNTRY_PRIORITY, priority).apply();
    }
}
