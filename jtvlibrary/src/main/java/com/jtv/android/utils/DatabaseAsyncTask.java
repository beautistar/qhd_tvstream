package com.jtv.android.utils;

import android.os.AsyncTask;

import com.jtv.android.listeners.OnCompleteListener;


public class DatabaseAsyncTask<T> extends AsyncTask<T, Float, Void> {


    private final DbTask<T> task;

    public DatabaseAsyncTask(DbTask<T> task) {
        this.task = task;
    }

    @Override
    protected Void doInBackground(T... params) {
        if(task != null){
            task.run(params);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(task != null) {
            task.onComplete();
        }
    }

    public interface DbTask<T> extends OnCompleteListener {

        void run(T... params);

    }

}