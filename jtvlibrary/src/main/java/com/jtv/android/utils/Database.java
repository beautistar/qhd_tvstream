package com.jtv.android.utils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.models.ChannelStats;
import com.jtv.android.models.Reminder;
import com.jtv.android.models.Server;

import java.util.ArrayList;
import java.util.List;

public class Database {

    public static Server selectServer(int remoteId) {
        ActiveAndroid.beginTransaction();
        try {
            new Update(Server.class).set(Server.COL_SELECTED + " = 0").where(Server.COL_SELECTED + " = 1").execute();
            new Update(Server.class).set(Server.COL_SELECTED + " = 1").where(Server.COL_REMOTE_ID + " = ?", remoteId).execute();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return getSelectedServer();
    }

    public static List<Server> getServerList(boolean orderByName) {
        From where = new Select().from(Server.class);
        if (orderByName) {
            where = where.orderBy(Server.COL_NAME);
        }
        return where.execute();
    }

    public static Server getSelectedServer() {
        return new Select().from(Server.class).orderBy(Server.COL_SELECTED + " DESC").executeSingle();
    }

    public static List<Channel> getFavorites() {
        return new Select().from(Channel.class).where("favorite=?", true).execute();
    }

    public static Category getCategory(int id) {
        return new Select().from(Category.class).where("remoteId=?", id).executeSingle();
    }

    public static List<Category> getCategories(boolean ignoreAudited) {
        From query = new Select().from(Category.class);
        if (ignoreAudited) {
            query = query.where("auditing=?", false);
        }
        query.orderBy("order_id ASC");
        return query.execute();
    }

    public static List<Channel> searchChannels(String query) {
        query = "%" + query + "%";
        return new Select().from(Channel.class).where("name LIKE ?", query).execute();
    }

    public static List<Channel> searchChannels(String query, long category) {
        query = "%" + query + "%";
        return new Select().from(Channel.class).where("name LIKE ?", query).where("Category = ?", category).execute();
    }

    public static ChannelStats getChannelStatistic(int channelRemoteId) {
        return new Select().from(ChannelStats.class).where(ChannelStats.COL_CHANNEL_ID + " = ?", channelRemoteId).executeSingle();
    }

    public static List<Channel> getRecentlyWatched(int count) {
        return new Select().from(Channel.class)
                .innerJoin(ChannelStats.class)
                    .on("Channels." + Channel.COL_REMOTE_ID + " = " + ChannelStats.COL_CHANNEL_ID)
                .innerJoin(Category.class)
                .on("Categories.Id=Category")
                .where("auditing=0")
                .orderBy(ChannelStats.COL_LAST_WATCHED_AT + " DESC")
                .limit(count)
                .execute();
    }

    public static List<Channel> getMostWatched(int count){
        return new Select().from(Channel.class)
                .innerJoin(ChannelStats.class)
                .on("Channels." + Channel.COL_REMOTE_ID + " = " + ChannelStats.COL_CHANNEL_ID)
                .innerJoin(Category.class)
                .on("Categories.Id=Category")
                .where("auditing=0")
                .orderBy(ChannelStats.COL_WATCHED_TIME + " DESC")
                .limit(count)
                .execute();
    }

    public static Channel getChannel(long id){
        return new Select().from(Channel.class).where("remoteId=?", id).executeSingle();
    }

    public static List<String> getCountryList(){
        ArrayList<String> countries = new ArrayList<>();
        List<Channel> country = new Select().from(Channel.class).groupBy("country").execute();
        for (Channel channel : country) {
            if (channel.getCountry() != null && !channel.getCountry().isEmpty()) {
                countries.add(channel.getCountry());
            }
        }
        return countries;
    }

    public static List<String> getCountryList(Category category){
        ArrayList<String> countries = new ArrayList<>();
        List<Channel> country = new Select().from(Channel.class).where("Category=?", category.getId()).groupBy("country").execute();
        for (Channel channel : country) {
            if (channel.getCountry() != null && !channel.getCountry().isEmpty()) {
                countries.add(channel.getCountry());
            }
        }
        return countries;
    }

    public static List<Reminder> getReminders(){
        return new Select().from(Reminder.class).execute();
    }

    public static List<Reminder> getReminders(long channelId){
        return new Select().from(Reminder.class).where("channel=?", channelId).execute();
    }

    public static Reminder getReminder(long id){
        return Reminder.load(Reminder.class, id);
    }

    public static List<Channel> getChannels() {
        return new Select().from(Channel.class)
                .innerJoin(Category.class)
                .on("Categories.Id=Category")
                .where("auditing=0")
                .execute();
    }
}
