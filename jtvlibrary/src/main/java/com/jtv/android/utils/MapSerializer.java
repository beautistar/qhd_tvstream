package com.jtv.android.utils;

import com.activeandroid.serializer.TypeSerializer;
import com.google.gson.Gson;

import java.util.Map;

public class MapSerializer extends TypeSerializer {
    @Override
    public Class<?> getDeserializedType() {
        return Map.class;
    }

    @Override
    public Class<?> getSerializedType() {
        return String.class;
    }

    @Override
    public String serialize(Object data) {
        if (data == null) {
            return null;
        }

        // Transform a Map<String, Object> to JSON and then to String
        return new Gson().toJson(data, Map.class);
    }

    @Override
    public Map<String, Object> deserialize(Object data) {
        if (data == null) {
            return null;
        }

        return new Gson().fromJson((String) data, Map.class);
    }
}
