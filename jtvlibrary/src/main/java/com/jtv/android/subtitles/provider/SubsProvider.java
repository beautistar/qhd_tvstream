package com.jtv.android.subtitles.provider;

import android.content.Context;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;

public abstract class SubsProvider extends BaseProvider {
    public static final String SUBTITLE_LANGUAGE_NONE = "no-subs";
    public static final String TAG = SubsProvider.class.getSimpleName();

    private static List<String> SUB_EXTENSIONS = Arrays.asList("srt", "ssa", "ass");

    private final Context context;
    private final OkHttpClient client;

    public SubsProvider(Context context, OkHttpClient client) {
        super(client);
        this.context = context;
        this.client = client;
    }

    public static File getStorageLocation(Context context) {
        return new File(context.getCacheDir() + File.separator + "subtitles");
    }

    /**
     * Test if file is subtitle format
     *
     * @param filename Name of file
     * @return is subtitle?
     */
    private static boolean isSubFormat(String filename) {
        for (String ext : SUB_EXTENSIONS) {
            if (filename.contains("." + ext)) {
                return true;
            }
        }
        return false;
    }


    public abstract void getList(String imdbId, retrofit2.Callback<Map<String, String>> callback);

    public interface Callback<T> {
        void onSuccess(T result);

        void onFailure(Exception e);
    }
}