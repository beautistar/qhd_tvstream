package com.jtv.android.subtitles;

public class Caption {

    public Style style;
    public Time start;
    public Time end;
    public String content = "";

}