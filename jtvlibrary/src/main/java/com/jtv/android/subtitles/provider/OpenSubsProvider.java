package com.jtv.android.subtitles.provider;

import android.content.Context;
import android.util.Log;

import com.jtv.android.BuildConfig;
import com.jtv.android.network.NetworkManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;

public class OpenSubsProvider extends SubsProvider {

    static final String API_URL = "http://api.opensubtitles.org/xml-rpc";
    static final String USER_AGENT = "Popcorn Time v1";//"Popcorn Time Android v1";

    private final XMLRPCClient client;
    private final NetworkManager networkManager;

    private final ArrayList<Long> ongoingCalls = new ArrayList<>();

    public static XMLRPCClient buildClient() {
        try {
            return new XMLRPCClient(new URL(API_URL), USER_AGENT);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public OpenSubsProvider(Context context, NetworkManager networkManager, OkHttpClient client, XMLRPCClient xmlClient) {
        super(context, client);
        this.client = xmlClient;
        this.networkManager = networkManager;
    }

    @Override
    public void getList(final String imdbId, final retrofit2.Callback<Map<String, String>> callback) {
        networkManager.getSubtitles(imdbId, new retrofit2.Callback<Map<String, String>>() {
            @Override
            public void onResponse(final Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if (response.isSuccessful() && !response.body().isEmpty()) {
                    callback.onResponse(call, response);
                } else {

                    login(new XMLRPCCallback() {
                        @Override
                        public void onResponse(long id, Object result) {
                            Map<String, Object> response = (Map<String, Object>) result;
                            String token = (String) response.get("token");
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "opensubtitles token: " + token);
                            }
                            if (!token.isEmpty()) {
                                networkManager.downloadAndGetSubtitles(imdbId, token, callback);
                            } else {
                                callback.onFailure(call, new XMLRPCException("Token not correct"));
                            }
                        }

                        @Override
                        public void onError(long id, XMLRPCException error) {
                            callback.onFailure(call, error);
                            removeCall(id);
                        }

                        @Override
                        public void onServerError(long id, XMLRPCServerException error) {
                            callback.onFailure(call, error);
                            removeCall(id);
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                callback.onFailure(call, new Exception(t));
            }
        });
    }

    @Override
    public void cancel() {
        super.cancel();

        synchronized (ongoingCalls) {
            for (Long ongoingCall : ongoingCalls) {
                client.cancel(ongoingCall);
            }
            ongoingCalls.clear();
        }
    }

    private void removeCall(long callId) {
        synchronized (ongoingCalls) {
            ongoingCalls.remove(callId);
        }
    }

    /**
     * Login to server and get token
     *
     * @param callback XML RPC callback
     */
    private void login(XMLRPCCallback callback) {
        long callId = client.callAsync(callback, "LogIn", "", "", "en", USER_AGENT);
        synchronized (ongoingCalls) {
            ongoingCalls.add(callId);
        }
    }

    /**
     * @param imdbId   imdb id
     * @param token    Login token
     * @param callback XML RPC callback callback
     */
    private void search(String imdbId, String token, XMLRPCCallback callback) {
        Map<String, String> option = new HashMap<>();
        imdbId = imdbId.replace("tt", "");
        option.put("imdbid", imdbId);
        option.put("sublanguageid", "all");
        long callId = client.callAsync(callback, "SearchSubtitles", token, new Object[]{option});
        synchronized (ongoingCalls) {
            ongoingCalls.add(callId);
        }
    }

}