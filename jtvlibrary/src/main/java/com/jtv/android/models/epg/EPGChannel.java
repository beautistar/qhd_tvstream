package com.jtv.android.models.epg;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EPGChannel {

    @SerializedName("display-name")
    private String name;
    @SerializedName("url")
    private String url;
    @SerializedName("id")
    private String id;

    @SerializedName("programme")
    private List<EPGProgramme> programme;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public List<EPGProgramme> getProgramme() {
        return programme;
    }

    @Override
    public String toString() {
        return "EPGChannel{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", id='" + id + '\'' +
                ", programme=" + programme +
                '}';
    }
}
