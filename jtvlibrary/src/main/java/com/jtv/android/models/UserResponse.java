package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class UserResponse {

    @SerializedName("Packages")
    private List<Package> packages;

    @SerializedName("User")
    private User user;

    @SerializedName("server_timezone")
    private String serverTimezone;

    public List<Package> getPackages() {
        return packages;
    }

    public User getUser() {
        return user;
    }

    public String getServerTimezone() {
        return serverTimezone;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "packages=" + packages +
                ", user=" + user +
                '}';
    }
}
