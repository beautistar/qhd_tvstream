package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

public class LoginResult {

    public static class Status{
        @SerializedName("code")
        private int code;
        @SerializedName("error")
        private String error;

        public Status() {
            this(0, "");
        }

        public Status(int code, String error) {
            this.code = code;
            this.error = error;
        }

        public int getCode() {
            return code;
        }

        public String getError() {
            return error;
        }
    }

    @SerializedName("status")
    private Status status;

    @SerializedName("token")
    private String token;

    @SerializedName("tokenExpires")
    private long tokenExpires;

    @SerializedName("login_token")
    private String loginToken;

    public LoginResult() {
    }

    public LoginResult(long tokenExpires, String loginToken) {
        this.tokenExpires = tokenExpires;
        this.loginToken = loginToken;
    }

    public Status getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public long getTokenExpires() {
        return tokenExpires;
    }

    public String getLoginToken() {
        return loginToken;
    }
}
