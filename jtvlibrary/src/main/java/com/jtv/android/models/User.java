package com.jtv.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class User {

    @SerializedName("RegistTime")
    private String registTime;
    @SerializedName("Name")
    private String name;
    @SerializedName("Mail")
    private String mail;
    @SerializedName("CurrentBalance")
    private float currentBalance;
    @SerializedName("TrueName")
    @Expose
    private String trueName;
    @SerializedName("Movies")
    private int movies;
    @SerializedName("BeginTime")
    private String beginTime;
    @SerializedName("EndTime")
    private String endTime;

    public User() {
    }

    public String getRegistTime() {
        return registTime;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public float getCurrentBalance() {
        return currentBalance;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public int getMovies() {
        return movies;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "registTime='" + registTime + '\'' +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", currentBalance=" + currentBalance +
                ", trueName='" + trueName + '\'' +
                ", movies=" + movies +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}
