package com.jtv.android.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "Channels")
@SuppressWarnings("unused")
public class Channel extends Model {

    public static final String COL_REMOTE_ID = "remoteId";

    @Column(name = COL_REMOTE_ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("ID")
    private int remoteId;
    @Column(name = "name")
    @SerializedName("Name")
    private String name;
    @Column(name = "image")
    @SerializedName("KeyPicture")
    private String imgURL;
    @Column(name = "stream_url")
    @SerializedName("stream_name")
    private String streamName;
    @Column(name = "favorite", index = true)
    @SerializedName("fav")
    private boolean favorite = false;
    @Column(name = "country")
    @SerializedName("Country")
    private String country;

    @Column(name = "Category", index = true, onUpdate = Column.ForeignKeyAction.NO_ACTION)
    private Category category;

    private long availableTimeshift = -1;

    public Channel() {
        super();
    }

    public Channel(final int remoteId){
        this.remoteId = remoteId;
    }

    public Channel(final int remoteId, final String name, final String imgURL, final String streamURL) {
        this.remoteId = remoteId;
        this.name = name;
        this.imgURL = imgURL;
        this.streamName = streamURL;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name.trim();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "remoteId=" + remoteId +
                ", name='" + name + '\'' +
                ", imgURL='" + imgURL + '\'' +
                ", streamName='" + streamName + '\'' +
                ", favorite=" + favorite +
                ", country='" + country + '\'' +
                ", category=" + category +
                ", availableTimeshift=" + availableTimeshift +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
//        if (!super.equals(o)) return false;

        Channel channel = (Channel) o;

        return remoteId == channel.remoteId;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + remoteId;
        return result;
    }

    public void setAvailableTimeshift(long availableTimeshift) {
        this.availableTimeshift = availableTimeshift;
    }

    public long getAvailableTimeshift() {
        return availableTimeshift;
    }
}
