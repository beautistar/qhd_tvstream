package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class OrderPackage {

    @SerializedName("ID")
    @Deprecated
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("Description")
    private String description;
    @SerializedName("Duration")
    private List<Duration> durations;

    /**
     * @deprecated not used anymore.
     */
    @Deprecated
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Duration> getDurations() {
        return durations;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "OrderPackage{" +
                "name='" + name + '\'' +
                ", durations=" + durations +
                '}';
    }
}
