package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

public class FavoriteStatus {

    @SerializedName("code")
    private int code;

    @SerializedName("status")
    private String status;

    public FavoriteStatus() {
    }

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "FavoriteStatus{" +
                "code=" + code +
                ", status='" + status + '\'' +
                '}';
    }
}
