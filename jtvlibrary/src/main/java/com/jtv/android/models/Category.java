package com.jtv.android.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Table(name = "Categories")
@SuppressWarnings("unused")
public class Category extends Model {

    public static final String COL_REMOTE_ID = "remoteId";
    public static final int CATEGORY_FAVORITE = -100;

    @Column(name = COL_REMOTE_ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("ID")
    private int remoteId;
    @Column(name = "name")
    @SerializedName("Name")
    private String name;
    @Column(name = "ImageUrl")
    @SerializedName("ColumnPic")
    private String imageUrl;

    @Column(name = "auditing")
    @SerializedName("Auditing")
    private int auditing;

    @Column(name = "order_id")
    private int orderId = Integer.MAX_VALUE;

    @SerializedName("channels")
    private List<Channel> channels;

    public Category() {
        super();
    }

    public int getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(int remoteId) {
        this.remoteId = remoteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageUrl;
    }

    public void setImageURL(String imgURL) {
        this.imageUrl = imgURL;
    }

    public boolean isAuditing() {
        return auditing == 1;
    }

    public void setAuditing(boolean auditing) {
        this.auditing = auditing ? 1 : 0;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * If called first time gets categories' channels by foreign key
     * Second time takes channels from field
     *
     * @return channel list
     */
    public List<Channel> getChannels() {
        if (channels == null) {
            channels = getMany(Channel.class, "Category");
        }
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Category category = (Category) o;

        return remoteId == category.remoteId;

    }

    @Override
    public String toString() {
        return "Category{" +
                "remoteId=" + remoteId +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", auditing=" + auditing +
                '}';
    }
}
