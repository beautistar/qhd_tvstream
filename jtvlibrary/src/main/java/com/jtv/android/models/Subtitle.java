package com.jtv.android.models;

import java.util.Map;

public class Subtitle {

    private long id;
    private String zipDownloadLink;
    private String format;
    private String language;
    private String languageId;
    private float rating;
    private int downloadCount;
    private long movieId;
    private String fileName;
    private String fileId;

    public Subtitle(Map<String, String> result) {
        id = Long.parseLong(result.get("IDSubtitle"));
        zipDownloadLink = result.get("ZipDownloadLink");
        format = result.get("SubFormat");
        rating = Float.parseFloat(result.get("SubRating"));
        downloadCount = Integer.parseInt(result.get("SubDownloadsCnt"));
        movieId = Long.parseLong(result.get("IDMovie"));
        language = result.get("LanguageName");
        languageId = result.get("SubLanguageID");
        fileName = result.get("SubFileName");
        fileId = result.get("IDSubtitleFile");
    }

    public long getId() {
        return id;
    }

    public String getZipDownloadLink() {
        return zipDownloadLink;
    }

    public String getFormat() {
        return format;
    }

    public String getLanguage() {
        return language;
    }

    public String getLanguageId() {
        return languageId;
    }

    public float getRating() {
        return rating;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public long getMovieId() {
        return movieId;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileId() {
        return fileId;
    }

    @Override
    public String toString() {
        return "Subtitle{" +
                "id=" + id +
                ", zipDownloadLink='" + zipDownloadLink + '\'' +
                ", format='" + format + '\'' +
                ", language='" + language + '\'' +
                ", languageId='" + languageId + '\'' +
                ", rating=" + rating +
                ", downloadCount=" + downloadCount +
                ", movieId=" + movieId +
                ", fileName='" + fileName + '\'' +
                ", fileId='" + fileId + '\'' +
                '}';
    }
}
