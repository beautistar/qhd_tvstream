package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("code")
    public int code;

    @SerializedName("error")
    public String error;

    @Override
    public String toString() {
        return "RegisterResponse{" +
                "code=" + code +
                ", error='" + error + '\'' +
                '}';
    }
}
