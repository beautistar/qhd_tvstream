package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Package {

    public static final int PACK_EXPIRED = 0;
    public static final int PACK_ACTIVE = 1;
    public static final int PACK_NOT_ACTIVE = 2;

    @SerializedName("AddedTime")
    private String addedTime;
    @SerializedName("BeginTime")
    private String beginTime;
    @SerializedName("EndTime")
    private String endTime;
    @SerializedName("Name")
    private String name;
    @SerializedName("Description")
    private String description;
    @SerializedName("State")
    private int state;

    public Package() {
    }

    public String getAddedTime() {
        return addedTime;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Package{" +
                "addedTime='" + addedTime + '\'' +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
