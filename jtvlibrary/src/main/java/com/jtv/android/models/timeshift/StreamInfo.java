package com.jtv.android.models.timeshift;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StreamInfo {

    @SerializedName("stream")
    private String stream;
    @SerializedName("warning")
    private String warning;
    @SerializedName("error")
    private String error;

    @SerializedName("ranges")
    private List<Range> ranges;

    public StreamInfo() {
    }

    public StreamInfo(String stream, String warning, String error, List<Range> ranges) {
        this.stream = stream;
        this.warning = warning;
        this.error = error;
        this.ranges = ranges;
    }

    public String getStream() {
        return stream;
    }

    public String getWarning() {
        return warning;
    }

    public String getError() {
        return error;
    }

    public List<Range> getRanges() {
        return ranges;
    }

    public long getMissingTimeTo(Range range) {
        long missingTime = range.getMissingTime();
        if (missingTime != -1) {
            return missingTime;
        }
        long begin = ranges.get(0).getFrom();
        long targetBegin = range.getFrom();
        long realTime = targetBegin - begin;
        long existingTime = 0;
        for (int i = 0; ranges.get(i) != range && i < ranges.size(); i++) {
            existingTime += ranges.get(i).getDuration();
        }
        missingTime = realTime - existingTime;
        range.setMissingTime(missingTime);
        return missingTime;
    }

    public Range getRangeByTime(long timeFromStart) {
        timeFromStart /= 1000;
        long begin = ranges.get(0).getFrom();
        Range last = ranges.get(ranges.size() - 1);
        if (timeFromStart > last.getEnd()) {
            return last;
        }
        for (Range range : ranges) {
            long rangeStart = range.getFrom() - begin;
            if (timeFromStart >= rangeStart && timeFromStart <= rangeStart + range.getDuration()) {
                return range;
            }
        }
        return null;
    }

    public Range getRangeIfAtMissing(long timeFromStart){
        timeFromStart /= 1000;
        long begin = ranges.get(0).getFrom();
        for (Range range : ranges) {
            long rangeStart = range.getFrom() - begin;
            if (timeFromStart < rangeStart) {
                return range;
            }
        }
        return ranges.get(ranges.size() - 1);
    }

    public Range getRangeByPlayerPosition(long currentTime){
        long time = 0;
        for (Range range : ranges) {
            time += range.getDuration();
            if(time > currentTime){
                return range;
            }
        }
        return ranges.get(ranges.size() - 1);
    }

    @Override
    public String toString() {
        return "StreamInfo{" +
                "stream='" + stream + '\'' +
                ", warning='" + warning + '\'' +
                ", error='" + error + '\'' +
                ", ranges=" + ranges +
                '}';
    }
}
