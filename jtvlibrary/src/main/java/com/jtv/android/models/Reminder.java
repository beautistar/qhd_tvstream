package com.jtv.android.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.jtv.android.utils.Database;

@Table(name = "Reminders")
public class Reminder extends Model {

    @Column(name = "hash", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int hash;

    @Column(name = "name")
    private String name;
    @Column(name = "timestamp")
    private long timestamp;

    @Column(name = "channel")
    private long channelId;

    public Reminder() {
        super();
    }

    public Reminder(String name, long timestamp, long channelId) {
        this.name = name;
        this.timestamp = timestamp;
        this.channelId = channelId;
        this.hash = hashCode();
    }

    public String getName() {
        return name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Channel getChannel() {
        return Database.getChannel(channelId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Reminder reminder = (Reminder) o;

        if (timestamp != reminder.timestamp) return false;
        return name.equals(reminder.name);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Reminder{" +
                "name='" + name + '\'' +
                ", timestamp=" + timestamp +
                ", channelId=" + channelId +
                '}';
    }
}
