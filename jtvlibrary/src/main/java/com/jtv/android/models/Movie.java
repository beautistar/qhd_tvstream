package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Movie implements Serializable {

    @SerializedName("Response")
    public String response;
    @SerializedName("imdbID")
    private String imdbID;
    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("Rated")
    private String rated;
    @SerializedName("Released")
    private String released;
    @SerializedName("Runtime")
    private String runtime;
    @SerializedName("Genre")
    private String genre;
    @SerializedName("Director")
    private String director;
    @SerializedName("Writer")
    private String writer;
    @SerializedName("Actors")
    private String actors;
    @SerializedName("Plot")
    private String plot;
    @SerializedName("Language")
    private String language;
    @SerializedName("Country")
    private String country;
    @SerializedName("Awards")
    private String awards;
    @SerializedName("Poster")
    private String poster;
    @SerializedName("Metascore")
    private String metascore;
    @SerializedName("imdbRating")
    private String imdbRating;
    @SerializedName("imdbVotes")
    private String imdbVotes;
    @SerializedName("Type")
    private String type;
    @SerializedName("url")
    private String streamUrl;
    @SerializedName("trailer")
    private String trailerUrl;
    @SerializedName("image")
    private String poster2;
    @SerializedName("tickets")
    private int tickets;
    @SerializedName("p2pid")
    private String p2pId;
    @SerializedName("canWatch")
    private boolean canWatch;

    public Movie() {
    }

    public int getTickets() {
        return tickets;
    }

    public boolean canWatch() {
        return canWatch;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getRated() {
        return rated;
    }

    public String getReleased() {
        return released;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getWriter() {
        return writer;
    }

    public String getActors() {
        return actors;
    }

    public String getPlot() {
        return plot;
    }

    public String getLanguage() {
        return language;
    }

    public String getCountries() {
        return country;
    }

    public String getAwards() {
        return awards;
    }

    public String getPoster() {
        return poster;
    }

    public String getPoster2() {
        return poster2;
    }

    public String getMetascore() {
        return metascore;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public String getType() {
        return type;
    }

    public String getResponse() {
        return response;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public String getP2pId() {
        return p2pId;
    }
}