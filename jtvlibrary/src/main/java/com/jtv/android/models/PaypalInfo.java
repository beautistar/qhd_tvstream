package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

public class PaypalInfo {

    @SerializedName("ID")
    private String clientID;

    public PaypalInfo() {
    }

    public String getClientID() {
        return clientID;
    }
}
