package com.jtv.android.models;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * data:
 * save - true/false - save to database or just temporary
 * type - normal/alert
 * duration - if normal message duration in seconds (max 60 seconds)
 *
 * icon:
 * info, warning, danger
 *
 */
@Table(name = "Notification")
public class Notification extends Model implements Parcelable {

    public static final String COL_MESSAGE_ID = "messageId";

    @Column(name = COL_MESSAGE_ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String messageId;

    @Column(name = "title")
    private String title;
    @Column(name = "body")
    private String body;

    @Column(name = "clickAction")
    private String clickAction;
    @Column(name = "url_link")
    private Uri link;
    @Column(name = "icon")
    private String icon;

    @Column(name = "sentTime")
    private long sentTime;
    @Column(name = "receivedTime")
    private long receivedTime;

    @Column(name = "data")
    private Map<String, String> data;

    public Notification() {
    }

    public Notification(String messageId,
                        String title,
                        String body,
                        String clickAction,
                        Uri link,
                        String icon,
                        long sentTime,
                        long receivedTime,
                        Map<String, String> data) {
        this.messageId = messageId;
        this.title = title;
        this.body = body;
        this.clickAction = clickAction;
        this.link = link;
        this.icon = icon;
        if(this.icon == null){
            this.icon = "";
        }
        this.sentTime = sentTime;
        this.receivedTime = receivedTime;
        this.data = data;
    }

    public Notification(String messageId, long sentTime, long receivedTime, Map<String, String> data) {
        this.messageId = messageId;
        this.sentTime = sentTime;
        if(this.icon == null){
            this.icon = "";
        }
        this.receivedTime = receivedTime;
        this.data = data;
    }

    public Notification(Parcel parcel) {
        messageId = parcel.readString();

        title = parcel.readString();
        body = parcel.readString();
        clickAction = parcel.readString();
        link = parcel.readParcelable(Uri.class.getClassLoader());
        icon = parcel.readString();

        sentTime = parcel.readLong();
        receivedTime = parcel.readLong();


        data = new HashMap<>();
        int i = parcel.readInt();
        for (int j = 0; j < i; j++) {
            data.put(parcel.readString(), parcel.readString());
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getClickAction() {
        return clickAction;
    }

    public Uri getLink() {
        return link;
    }

    public String getIcon() {
        return icon;
    }

    public long getSentTime() {
        return sentTime;
    }

    public long getReceivedTime() {
        return receivedTime;
    }

    public Map<String, String> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "messageId='" + messageId + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", clickAction='" + clickAction + '\'' +
                ", link=" + link +
                ", icon='" + icon + '\'' +
                ", sentTime=" + sentTime +
                ", receivedTime=" + receivedTime +
                ", data=" + data +
                '}';
    }

    public static Intent getIntent(Notification notification, String action) {
        Intent intent = new Intent(action);
        intent.putExtra("notification", notification);
        return intent;
    }

    public static Notification parseIntent(Intent intent){
        return ((Notification) intent.getParcelableExtra("notification"));
    }

    public static final Parcelable.Creator<Notification> CREATOR
            = new Parcelable.Creator<Notification>() {
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(messageId);

        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(clickAction);
        dest.writeParcelable(link, PARCELABLE_WRITE_RETURN_VALUE);
        dest.writeString(icon);

        dest.writeLong(sentTime);
        dest.writeLong(receivedTime);

        dest.writeInt(data.size());
        for (Map.Entry<String, String> entry : data.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }
}
