package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MovieCategory {

    @SerializedName("ID")
    private int id;
    @SerializedName("Name")
    private String name;
    @SerializedName("ColumnPic")
    private String thumbnail;
    @SerializedName("type")
    private String type;
    @SerializedName("Auditing")
    private int auditing = 0;

    public MovieCategory() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getType() {
        return type;
    }

    public boolean isAuditing(){
        return auditing == 1;
    }

    @Override
    public String toString() {
        return "MovieCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
