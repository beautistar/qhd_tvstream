package com.jtv.android.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "ChannelStats")
public class ChannelStats extends Model {

    public static final String COL_CHANNEL_ID = "channelId";
    public static final String COL_WATCHED_TIME = "watchedTime";
    public static final String COL_LAST_WATCHED_AT = "lastWatchedAt";
    public static final String COL_ERROR_COUNT = "errorCount";

    @Column(name = COL_CHANNEL_ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int channelRemoteId;

    @Column(name = COL_WATCHED_TIME)
    private long watchedTime;

    @Column(name = COL_LAST_WATCHED_AT)
    private long lastWatchedAt;

    @Column(name = COL_ERROR_COUNT)
    private long errorCount;

    public ChannelStats() {
    }

    public ChannelStats(int channelRemoteId, long watchedTime, long lastWatchedAt) {
        this.channelRemoteId = channelRemoteId;
        this.watchedTime = watchedTime;
        this.lastWatchedAt = lastWatchedAt;
        this.errorCount = 0;
    }

    public int getChannelRemoteId() {
        return channelRemoteId;
    }

    public long getWatchedTime() {
        return watchedTime;
    }

    public long getLastWatchedAt() {
        return lastWatchedAt;
    }

    public void setWatchedTime(long watchedTime) {
        this.watchedTime = watchedTime;
    }

    public void setLastWatchedAt(long lastWatchedAt) {
        this.lastWatchedAt = lastWatchedAt;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }
}
