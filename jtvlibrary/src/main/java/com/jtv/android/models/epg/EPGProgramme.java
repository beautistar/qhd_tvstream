package com.jtv.android.models.epg;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public class EPGProgramme {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
    static{
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @SerializedName("title")
    private String title;
    @SerializedName("desc")
    private String description;
    @SerializedName("start")
    private String start;
    @SerializedName("stop")
    private String stop;
    @SerializedName("icon")
    private Object icon;

    private float weight = -1f;
    private String formattedTime = null;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getStart() {
        return start;
    }

    public String getStop() {
        return stop;
    }

    public String getIcon() {
        return icon.toString();
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getFormattedTime() {
        return formattedTime;
    }

    public void setFormattedTime(String formattedTime) {
        this.formattedTime = formattedTime;
    }

    public long getStartTimestamp(){
        try {
            return DATE_FORMAT.parse(getStart()).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "EPGProgramme{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", start=" + start +
                ", stop=" + stop +
                ", icon=" + icon +
                '}';
    }
}
