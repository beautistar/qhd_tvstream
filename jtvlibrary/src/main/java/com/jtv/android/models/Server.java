package com.jtv.android.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "Servers")
public class Server extends Model {

    public static final String COL_REMOTE_ID = "remoteId";
    public static final String COL_IP = "IP";
    public static final String COL_NAME = "name";
    public static final String COL_SELECTED = "selected";

    @Column(name = COL_REMOTE_ID, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("ID")
    private int remoteId;
    @Column(name = COL_IP)
    @SerializedName("IP")
    private String IP;
    @Column(name = COL_NAME)
    @SerializedName("name")
    private String name;
    @Column(name = COL_SELECTED, index = true)
    private boolean selected = false;

    public Server() {
        super();
    }

    public String getIP() {
        return IP;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Server server = (Server) o;

        if (remoteId != server.remoteId) return false;
        return IP.equals(server.IP);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + remoteId;
        result = 31 * result + IP.hashCode();
        return result;
    }
}
