package com.jtv.android.models.timeshift;

import com.google.gson.annotations.SerializedName;

public class Range {

    @SerializedName("from")
    private long from;
    @SerializedName("duration")
    private long duration;

    private long missingTime = -1;

    public Range() {
    }

    public Range(long from, long duration) {
        this.from = from;
        this.duration = duration;
    }

    public long getFrom() {
        return from;
    }

    public long getDuration() {
        return duration;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getEnd() {
        return from + duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getMissingTime() {
        return missingTime;
    }

    public void setMissingTime(long missingTime) {
        this.missingTime = missingTime;
    }

    @Override
    public String toString() {
        return "Range{" +
                "from=" + from +
                ", duration=" + duration +
                '}';
    }
}



