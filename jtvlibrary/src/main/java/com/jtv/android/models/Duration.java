package com.jtv.android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Duration implements Serializable {

    @SerializedName("ID")
    private int id;
    @SerializedName("Price")
    private double price;
    @SerializedName("TimePeriod")
    private int timePeriod;
    @SerializedName("Tax")
    private String tax;
    @SerializedName("Discount")
    private String discount;
    @SerializedName("Total")
    private double total;
    @SerializedName("Currency")
    private String currency;

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public String getTax() {
        return tax;
    }

    public String getDiscount() {
        return discount;
    }

    public double getTotal() {
        return total;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Duration{" +
                "id=" + id +
                ", price=" + price +
                ", timePeriod=" + timePeriod +
                ", currency=" + currency +
                ", tax='" + tax + '\'' +
                ", discount='" + discount + '\'' +
                ", total=" + total +
                '}';
    }
}
