package com.jtv.android.network;

import java.util.TimerTask;

class QHDPresenceTimerTask extends TimerTask {

    private final NetworkManager manager;

    QHDPresenceTimerTask(NetworkManager manager) {
        this.manager = manager;
    }

    @Override
    public void run() {
        manager.login();
    }
}
