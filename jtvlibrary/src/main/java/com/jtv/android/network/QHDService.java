package com.jtv.android.network;


import com.forcetech.android.ForceTV;
import com.jtv.android.models.Category;
import com.jtv.android.models.FavoriteStatus;
import com.jtv.android.models.LoginResult;
import com.jtv.android.models.Movie;
import com.jtv.android.models.MovieCategory;
import com.jtv.android.models.OrderPackage;
import com.jtv.android.models.PaypalInfo;
import com.jtv.android.models.RegisterResponse;
import com.jtv.android.models.Server;
import com.jtv.android.models.UserResponse;
import com.jtv.android.models.epg.EPGChannel;
import com.jtv.android.models.epg.EPGProgramme;
import com.jtv.android.models.timeshift.StreamInfo;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

interface QHDService {

    /**
     * login using mac
     *
     * @param mac network mac adress
     * @return call to server
     */
    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginMacV2(@Field("mac") String mac);


    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginV2(@Field("mac") String mac,
                              @Field("name") String name,
                              @Field("pwd") String password);

    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginV2(@Field("name") String name,
                              @Field("pwd") String password);

    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginCodeV2(@Field("mac") String mac,
                                  @Field("code") String code);

    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginCodeV2(@Field("code") String code);

    @FormUrlEncoded
    @POST("/app/v2/login.php")
    Call<LoginResult> loginTokenV2(@Field("login_token") String token);

    /**
     * checks if theres was application update
     *
     * @param appId   apllication id
     * @param version current application version
     * @return call to server
     */
    @GET("/app/update.php")
    Call<String> getAppUpdate(@Query("package") String appId, @Query("ver") int version);

    /**
     * @param token - session token
     * @return call to server
     */
    @GET("/app/v2/member.php")
    Call<UserResponse> getUser(@Query("token") String token);

    @FormUrlEncoded
    @POST("/app/v2/member.php")
    Call<UserResponse> updateUser(@Field("TrueName") String fullName, @Field("token") String token);


    /**
     * Registers new user.
     *
     * @param mac      - network adapter's mc address
     * @param mail     - user email used for login < 50
     * @param password - user password < 50
     * @param fullName - user full name
     * @param countryCode
     * @return call from server
     */
    @FormUrlEncoded
    @POST("/app/v2/register.php")
    Call<RegisterResponse> register(@Field("Mac") String mac,
                                    @Field("Mail") String mail,
                                    @Field("Password") String password,
                                    @Field("TrueName") String fullName,
                                    @Field("cc") String countryCode,
                                    @Field("Phone") String phone);

    /**
     * Gets available server list
     *
     * @param token - session token
     * @return call to server
     */
    @GET("/app/v2/getServers.php")
    Call<List<Server>> getServerList(@Query("token") String token);

    /**
     * Gets all categories and channels in one request
     *
     * @param token - session token
     * @return call to server
     */
    @GET("/app/v2/playlist.php?noip")
    Call<List<Category>> getCategoriesAll(@Query("token") String token);

    @GET("/app/v2/fav.php")
    Call<FavoriteStatus> addFavorite(@Query("id") int id, @Query("token") String token);

    @GET("/app/v2/unfav.php")
    Call<FavoriteStatus> removeFavorite(@Query("id") int id, @Query("token") String token);

    /**
     * Logouts from session
     *
     * @param token - session token
     * @return call to server
     */
    @FormUrlEncoded
    @POST("/app/v2/logout.php")
    Call<String> logout(@Field("token") String token);

    /**
     * Gets list of available packages
     *
     * @param token - session token
     * @return call to server
     */
    @GET("/app/v2/orderList.php")
    Call<List<OrderPackage>> getOrderPackages(@Query("token") String token);


    /**
     * Gets movies categories or movies in category
     *
     * @param id    category id
     * @param token auth token
     * @return category or movie list
     */
    @GET("/app/v2/getVOD.php")
    Call<List<MovieCategory>> getMovieCategory(
            @Query("catid") int id, @Query("token") String token);

    /**
     * Gets movie data
     *
     * @param id    movie id
     * @param token auth token
     * @return movie data
     */
    @GET("/app/v2/getVOD.php")
    Call<Movie> getMovie(@Query("movid") int id, @Query("token") String token);

    /**
     * Send's payment information to back-end to verify
     *
     * @param id    paypal payment id
     * @param token auth token
     * @param path  for debug or release versions
     * @return result
     */
    @FormUrlEncoded
    @POST(NetworkManager.PAYMENT_URL + "/{path}")
    Call<String> purchaseConfirmation(@Field("purchase") String id,
                                      @Field("token") String token,
                                      @Path("path") String path);

    @GET(NetworkManager.SUBTITLE_URL)
    Call<Map<String, String>> getSubtitles(@Query("imdbid") String imdbid);


    @GET(NetworkManager.SUBTITLE_URL)
    Call<Map<String, String>> downloadAndGetSubtitles(
            @Query("imdbid") String imdbid, @Query("token") String token);

    @GET("/app/v2/clientID.php")
    Call<PaypalInfo> getPaypalClientID(@Query("token") String token);

    @FormUrlEncoded
    @POST("/app/v2/firebaseToken.php")
    Call<String> registerFirebaseToken(
            @Field("token") String token, @Field("ftoken") String firebaseToken);

    @FormUrlEncoded
    @POST("/app/v2/firebaseToken.php")
    Call<String> registerFirebaseToken(@Field("ftoken") String firebaseToken);

    @GET("/app/v2/epg.php")
    Call<Map<String, EPGChannel>> getCategoryEPG(
            @Query("cat") int catId,
            @Query("from") String from,
            @Query("to") String to, @Query("fixed_titles") String fixedTitles);

    @GET("/app/v2/epg.php")
    Call<Map<String, EPGChannel>> getCategoryEPG(
            @Query("cat") int catId,
            @Query("from") String from, @Query("fixed_titles") String fixedTitles);

    @GET("/app/v2/epg.php")
    Call<EPGChannel> getChannelEPG(@Query("chid") long channelId);

    @GET("/app/v2/epg.php")
    Call<EPGProgramme> getCurrentProgramme(
            @Query("category") int catId,
            @Query("channel") int channelId
    );

    @GET("http://0.0.0.0:" + ForceTV.FORCE_PORT + "/cmd.xml")
    Call<String> forceStartCommand(
            @Query("cmd") String cmd, @Query("id") String id, @Query("server") String server);

    @GET
    Call<List<StreamInfo>> getAvailableTimeshift(@Url String url);

}
