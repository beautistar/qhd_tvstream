package com.jtv.android.network.listeners;

public interface OnTokenChangeListener {

    void onTokenChange(String token);

}
