package com.jtv.android.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.forcetech.android.ForceTV;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.models.FavoriteStatus;
import com.jtv.android.models.LoginResult;
import com.jtv.android.models.Movie;
import com.jtv.android.models.MovieCategory;
import com.jtv.android.models.OrderPackage;
import com.jtv.android.models.PaypalInfo;
import com.jtv.android.models.RegisterResponse;
import com.jtv.android.models.Server;
import com.jtv.android.models.UserResponse;
import com.jtv.android.models.epg.EPGChannel;
import com.jtv.android.models.epg.EPGProgramme;
import com.jtv.android.models.timeshift.StreamInfo;
import com.jtv.android.network.listeners.OnLoginListener;
import com.jtv.android.network.listeners.OnTokenChangeListener;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.DatabaseAsyncTask;
import com.jtv.android.utils.SettingsManager;
import com.jtv.android.utils.SimpleCallback;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Handles all requests to remote servers,
 * stores authentication information.
 */
@SuppressWarnings("unused")
public class NetworkManager implements Callback<LoginResult> {

    private static final String TAG = NetworkManager.class.getSimpleName();

    public final static String PAYMENT_URL = "http://payments.q-hd.net:88";
    public static final String SUBTITLE_URL = "http://subtitles.q-hd.net:82";
    private static final String HEADER_CACHE_CONTROL = "Cache-Control";
    private static final int LOGIN_PERIOD = 600000; //Login period in milliseconds
    private final Cache okhhtpCache;
    private final Context context;

    private String token;
    private String selectedServer;
    private String mac;

    private WifiManager wifiManager;
    private QHDService service;

    /**
     * Listeners
     */
    private OnLoginListener loginListener;
    private OnTokenChangeListener tokenListener;

    /**
     * Timer executing {@linkplain #task timer task} to login again every {@link #LOGIN_PERIOD}
     */
    private Timer timer;
    private QHDPresenceTimerTask task;

    private SettingsManager settingsManager;
    private int timeout = 30;
    private String origin;
    private String baseUrl;

    private boolean debug;

    public NetworkManager(Context applicationContext, String baseUrl, String origin, SettingsManager settingsManager, WifiManager manager, Cache okhhtpCache, boolean debug) {
        wifiManager = manager;
        timer = new Timer("QHDPresenceTimer");
        this.settingsManager = settingsManager;
        this.baseUrl = baseUrl;
        this.origin = origin;
        this.debug = debug;
        this.okhhtpCache = okhhtpCache;
        this.context = applicationContext;
    }

    private boolean isConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Build Retrofit service {@link QHDService}
     *
     * @return QHDService
     */
    private QHDService getService() {
        if (service == null) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(timeout, TimeUnit.SECONDS)
                    .readTimeout(timeout * 2, TimeUnit.SECONDS)
                    .writeTimeout(timeout * 2, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .addInterceptor(new OriginInterceptor());
            if (okhhtpCache != null) {
                builder.cache(okhhtpCache);
                builder.addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        okhttp3.Response originalResponse = chain.proceed(request);
                        if (request.method().equals("GET")) {
                            CacheControl cacheControl = new CacheControl.Builder()
                                    .maxAge(1, TimeUnit.MINUTES)
                                    .build();
                            return originalResponse.newBuilder()
                                    .removeHeader("Pragma")
                                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                                    .build();
                        }
                        return originalResponse;
                    }
                });
                builder.addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (request.method().equals("GET") && !isConnected()) {
                            CacheControl cacheControl = new CacheControl.Builder()
                                    .maxStale(24, TimeUnit.DAYS) // 4 weeks
                                    .build();
                            request = request.newBuilder().cacheControl(cacheControl).build();
                        }

                        return chain.proceed(request);
                    }
                });
            }
            if (debug) {
                builder.addInterceptor(httpLoggingInterceptor);
            }
            OkHttpClient client = builder.build();

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(baseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            service = retrofit.create(QHDService.class);
        }
        return service;
    }

    public boolean increaseTimeout() {
        if (timeout < 50) {
            timeout += 10;
            return true;
        }
        return false;
    }

    public void rebuildService() {
        service = null;
    }

    /**
     * Login using mac address
     * Results returned thru {@link OnLoginListener}
     * You can set it using {@link #setLoginListener(OnLoginListener)}
     */
    public void login() {
        Log.d(TAG, "Attempting to login...");
        String loginToken = settingsManager.getLoginToken();
        if (loginToken != null) {
            Log.i(TAG, "Logging in using login token address");
            getService().loginTokenV2(loginToken).enqueue(this);
        } else {
            Log.i(TAG, "Login Token bit set. Trying to login using mac address");
            try {
                getService().loginMacV2(getMac()).enqueue(this);
            } catch (Exception e) {
                onLoginError(new LoginResult.Status(-999, "Couldn't get mac address"));
            }
//            onLoginError(new LoginResult.Status(-1000, "Login Token Expired expire at:" + tokenExpirationTime+ "   current Time: " + timeInMillis));
        }
    }

    /**
     * Login using user's credentials.
     * Results returned thru {@link OnLoginListener}
     * You can set it using {@link #setLoginListener(OnLoginListener)}
     *
     * @param name     username
     * @param password password
     */
    public void login(String name, String password) {
        settingsManager.setLogin(name);
        Log.d(TAG, "Logging in using username(" + name + ") and password");
        String mac;
        try {
            mac = getMac();
            getService().loginV2(mac, name, password).enqueue(this);
        } catch (Exception ignored) {
            getService().loginV2(name, password).enqueue(this);
        }
    }

    /**
     * Login using pre-generated code.
     * Results returned thru {@link OnLoginListener}
     * You can set it using {@link #setLoginListener(OnLoginListener)}
     *
     * @param code pre-generated code.
     */
    public void login(String code) {
        settingsManager.setLogin(code);
        Log.d(TAG, "Logging in using code(" + code + ")");
        String mac;
        try {
            mac = getMac();
            getService().loginCodeV2(mac, code).enqueue(this);
        } catch (Exception ignored) {
            getService().loginCodeV2(code).enqueue(this);
        }
    }

    /**
     * Get user's information: username, fullname, email, packages
     *
     * @param callback retrofit callback
     */
    public void getUser(Callback<UserResponse> callback) {
        getService().getUser(getToken()).enqueue(callback);
    }

    /**
     * Update user's full name
     *
     * @param name     new full name
     * @param callback retrofit callback
     */
    public void updateUser(String name, Callback<UserResponse> callback) {
        getService().updateUser(name, getToken()).enqueue(callback);
    }

    /**
     * Register new user.
     *
     * @param mail     email
     * @param password password
     * @param fullName full name
     * @param callback retrofit callback
     */
    public void register(String mail, String password, String fullName, String countryCode, String phone, Callback<RegisterResponse> callback) {
        String mac = null;
        try {
            mac = getMac();
        } catch (Exception ignored) {
        }
        getService().register(mac, mail, password, fullName, countryCode, phone).enqueue(callback);
    }

    /**
     * Builds stream url for channel
     * and append token to url.
     *
     * @param channelName channel name
     * @return formatted url
     */
    public String getStreamUrl(String channelName) {
        boolean http = channelName.startsWith("http");
        if (http) {
            return Uri.parse(channelName).buildUpon().appendQueryParameter("token", getToken()).toString();
        }
        if (channelName.contains("{TOKEN}")) {
            channelName = channelName.substring(0, channelName.indexOf('?'));
            String[] split = channelName.split("/");
            return getStreamUrl(split[0], split[1]);
        }
        return getStreamUrl(channelName, "index.m3u8");
    }

    public String getStreamUrl(String channelName, String lastSegment) {
        return getStreamBaseUrl()
                .appendPath(channelName)
                .appendPath(lastSegment)
                .appendQueryParameter("token", getToken())
                .build().toString();

    }

    private Uri.Builder getStreamBaseUrl() {
        return new Uri.Builder()
                .scheme("http")
                .encodedAuthority(getSelectedServer());
    }

    /**
     * Get auth token
     *
     * @return token
     */
    private String getToken() {
        return token;
    }

    /**
     * Set new auth token
     *
     * @param token token
     */
    private void setToken(String token) {
        if (this.token == null || !this.token.equals(token)) {
            if (debug) {
                Log.d(TAG, "Setting token: " + token);
            }
            this.token = token;
            if (tokenListener != null) {
                tokenListener.onTokenChange(token);
            }
        }
    }

    /**
     * Get current server's IP address
     *
     * @return IP address
     */
    public String getSelectedServer() {
        if (selectedServer == null) {
            Server selectedServer = Database.getSelectedServer();
            if (selectedServer != null) {
                this.selectedServer = selectedServer.getIP();
            }
        }
        return selectedServer;
    }

    /**
     * Check if there's an update available from remote server
     *
     * @param callback retrofit callback
     */
    public void getAppUpdate(String appid, int versionCode, Callback<String> callback) {
        getService().getAppUpdate(appid, versionCode).enqueue(callback);
    }

    /**
     * Get server list assigned to your location
     *
     * @param callback retrofit callback
     */
    public void getServerList(Callback<List<Server>> callback) {
        getService().getServerList(getToken()).enqueue(callback);
    }

    /**
     * Get all categories with their channels
     *
     * @param callback retrofit callback
     */
    public void getCategoriesAll(Callback<List<Category>> callback) {
        getService().getCategoriesAll(getToken()).enqueue(callback);
    }

    /**
     * Logout from server and clear token
     */
    public void logout(final Callback<String> callback) {
        settingsManager.setLogin(null);
        getService().logout(getToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                NetworkManager.this.token = null;
                settingsManager.setLoginInfo(new LoginResult(0, null));
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }

    /**
     * Get list of availabla packages and their prices
     *
     * @param callback retrofit callback
     */
    public void getOrderPackages(Callback<List<OrderPackage>> callback) {
        getService().getOrderPackages(getToken()).enqueue(callback);
    }

    /**
     * Gets categorie's content
     * use id=0 for top category
     *
     * @param id       category id
     * @param callback retrofit callback
     */
    public void getMovieCategories(int id, Callback<List<MovieCategory>> callback) {
        getService().getMovieCategory(id, getToken()).enqueue(callback);
    }

    /**
     * Get movie info
     *
     * @param id       movie id
     * @param callback retrofit callback
     */
    public void getMovie(int id, Callback<Movie> callback) {
        getService().getMovie(id, getToken()).enqueue(callback);
    }

    public void addFavorite(int id) {
        getService().addFavorite(id, getToken()).enqueue(new SimpleCallback<FavoriteStatus>() {
            @Override
            public void onResponse(Call<FavoriteStatus> call, Response<FavoriteStatus> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() != 1) {
                        Log.e(TAG, "addFavorite: " + response.body().getStatus());
                    }
                }
            }
        });
    }

    public void removeFavorite(int id) {
        getService().removeFavorite(id, getToken()).enqueue(new SimpleCallback<FavoriteStatus>() {
            @Override
            public void onResponse(Call<FavoriteStatus> call, Response<FavoriteStatus> response) {
                if (response.isSuccessful()) {
                    if (response.body().getCode() != 1) {
                        Log.e(TAG, "removeFavorite: " + response.body().getStatus());
                    }
                }
            }
        });
    }

    /**
     * Send payment confirmation to remote server
     *
     * @param callback retrofit callback
     * @param id       paypal payment id
     */
    public void purchaseConfirmation(Callback<String> callback, String id) {
        Log.d(TAG, token);
        String path = debug ? "debug.php" : "";
        getService().purchaseConfirmation(id, getToken(), path).enqueue(callback);
    }

    public void forceStartCommand(String id, Callback<String> callback) {
        getService().forceStartCommand("start_chan", id, ForceTV.FORCE_SERVER).enqueue(callback);
    }

    /**
     * Get device's mac address
     *
     * @return mac address
     */
    @SuppressWarnings("all")
    public String getMac() {
        if (mac == null) {
            mac = getMACAddress("eth0"); // Try to get ethernet mac
        }
        //Android 6.0+ doesn't let you get mac address.
        //Always returns 02:00:00:00:00:00
        //So just skip it
        if (mac == null && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mac = wifiManager.getConnectionInfo().getMacAddress(); // Try to get wifi mac
        }
        if (mac == null || mac.isEmpty() || mac.equalsIgnoreCase("02:00:00:00:00:00")) {
            mac = getMACAddress("wlan0"); //Try to get wifi mac
        }
        if (mac == null || mac.isEmpty()) {
            Log.e(TAG, "Couldn't get device's mac adress");
            throw new RuntimeException("Couldn't get device's mac adress");
        } else {
            mac = mac.toUpperCase();
        }
        if (debug) {
            Log.d("LoginMac", "Mac adress: " + mac);
        }
        return mac;
    }

    public void getSubtitles(String imdbId, Callback<Map<String, String>> callback) {
        getService().getSubtitles(imdbId).enqueue(callback);
    }

    public void downloadAndGetSubtitles(String imdbId, String token, Callback<Map<String, String>> callback) {
        getService().downloadAndGetSubtitles(imdbId, token).enqueue(callback);
    }

    public void getPaypalClientID(Callback<PaypalInfo> callback) {
        getService().getPaypalClientID(getToken()).enqueue(callback);
    }

    public void registerFirebaseToken(String firebaseToken, Callback<String> callback) {
        if (getToken() == null) {
            getService().registerFirebaseToken(firebaseToken).enqueue(callback);
        } else {
            getService().registerFirebaseToken(getToken(), firebaseToken).enqueue(callback);
        }
    }

    public void replaceFirebaseToken(String firebaseToken, Callback<String> callback) {
        getService().registerFirebaseToken(getToken(), firebaseToken).enqueue(callback);
    }

    public void getCategoryEPG(int categoryId, String from, String to, Callback<Map<String, EPGChannel>> callback) {
        getService().getCategoryEPG(categoryId, from, to, "").enqueue(callback);
    }

    public void getChannelEPG(long channelId, Callback<EPGChannel> callback) {
        getService().getChannelEPG(channelId).enqueue(callback);
    }

    public void getCategoryEPG(int categoryId, String from, Callback<Map<String, EPGChannel>> callback) {
        getService().getCategoryEPG(categoryId, from, "").enqueue(callback);
    }

    public void getCurrentProgramme(int categoryId, int channelId, Callback<EPGProgramme> callback) {
        getService().getCurrentProgramme(categoryId, channelId).enqueue(callback);
    }

    public void getAvailableTimeshift(String channel, Callback<List<StreamInfo>> callback) {
        getAvailableTimeshift(channel, "0", callback);
    }

    public void getAvailableTimeshift(String channel, String from, Callback<List<StreamInfo>> callback) {
        String server = getSelectedServer();
        String url = getStreamBaseUrl()
                .appendPath(channel)//channel name
                .appendPath("recording_status.json")
                .appendQueryParameter("from", from)
                .appendQueryParameter("to", "now")
                .appendQueryParameter("token", getToken()).build().toString();
        getService().getAvailableTimeshift(url).enqueue(callback);
    }


    public String appendTokenToUrl(String url) {
        if (url == null) {
            return null;
        }
        return Uri.parse(url).buildUpon().appendQueryParameter("token", getToken()).build().toString();
    }

    public boolean clearCache() {
        if (okhhtpCache != null) {
            try {
                okhhtpCache.evictAll();
                return true;
            } catch (IOException e) {
                Log.e(TAG, "clearCache: failed to clear cache", e);
                return false;
            }
        }
        return false;
    }

    @Override
    public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {
        if (response.isSuccessful()) {
            LoginResult result = response.body();
            if (result.getStatus().getCode() == 1) {
                setToken(result.getToken());
                settingsManager.setLoginInfo(result);
                onLoginSuccess();
            } else if (result.getStatus().getCode() == 0) {
                settingsManager.setLoginInfo(new LoginResult(0, null));
                try {
                    getService().loginMacV2(getMac()).enqueue(this);
                } catch (Exception e) {
                    onLoginError(new LoginResult.Status(-999, "Couldn't get mac address"));
                }
            } else {
                onLoginError(result.getStatus());
            }
        } else {
            onLoginError(new LoginResult.Status(response.code(), "Bad response code"));
        }
    }

    /**
     * Try to get interface's mac address
     *
     * @param interfaceName interface's name
     * @return mac address of interface
     */
    private String getMACAddress(String interfaceName) {
        try {
            NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
            byte[] mac = networkInterface.getHardwareAddress();
            if (mac == null) return "";
            StringBuilder buf = new StringBuilder();
            for (byte part : mac) buf.append(String.format("%02X:", part));
            if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
            return buf.toString();
        } catch (Exception ignored) {
        }
        return null;
    }

    private void onLoginError(LoginResult.Status status) {
        if (loginListener != null) {
            loginListener.onLoginError(status.getCode(), status.getError());
        }
    }

    private void onLoginSuccess() {
        if (task == null) {
            task = new QHDPresenceTimerTask(this);
            timer.scheduleAtFixedRate(task, LOGIN_PERIOD, LOGIN_PERIOD); //Delay and run every 10 minutes  600000
        }
        if (loginListener != null) {
            loginListener.onLoginSuccess();
        }
    }

    @Override
    public void onFailure(Call<LoginResult> call, Throwable t) {
        onLoginError(new LoginResult.Status(-1000, "Network problems: " + t.getMessage()));
        Log.e(TAG, "onFailure: " + t.toString());
    }

    public void setLoginListener(OnLoginListener loginListener) {
        this.loginListener = loginListener;
    }

    public void setTokenListener(OnTokenChangeListener tokenListener) {
        this.tokenListener = tokenListener;
    }

    public void setServer(String server) {
        this.selectedServer = server;
    }

    public boolean isLoggedIn() {
        return token != null;
    }

    public void updateChannels(final OnChannelUpdateCallback listener) {
        getCategoriesAll(new SimpleCallback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                final List<Category> categories = response.body();
                if (categories != null) {
                    new DatabaseAsyncTask<>(new DatabaseAsyncTask.DbTask<Category>() {

                        @Override
                        public void run(Category... params) {
                            ActiveAndroid.beginTransaction();
                            try {
                                new Delete().from(Channel.class).where("1=1").execute();
                                new Delete().from(Category.class).where("1=1").execute();
                                int i = 0;
                                for (Category category : params) {
                                    category.setOrderId(i++);
                                    category.save();
                                    for (Channel channel : category.getChannels()) {
                                        channel.setCategory(category);
                                        channel.save();
                                    }
                                }
                                ActiveAndroid.setTransactionSuccessful();
                            } finally {
                                ActiveAndroid.endTransaction();
                            }
                        }

                        @Override
                        public void onComplete() {
                            Log.i(TAG, "Channels updated...");
                            long timeInMillis = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis();
                            settingsManager.setLastUpdatedAt(timeInMillis);
                            listener.onUpdated();
                        }
                    }).execute(categories.toArray(new Category[0]));
                } else {
                    Log.e(TAG, "Failed to load categories");
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                listener.onFailed();
            }
        });
    }

    public void saveInstance(Bundle outState) {
        if (outState != null) {
            outState.putBoolean("networkSaved", true);
            outState.putString("token", getToken());
            outState.putString("selectedServer", getSelectedServer());
            outState.putString("origin", origin);
            outState.putString("baseUrl", baseUrl);
            outState.putBoolean("debug", debug);
        }
    }

    public void restoreInstance(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getBoolean("networkSaved", false)) {
                token = bundle.getString("token");
                selectedServer = bundle.getString("selectedServer");
                origin = bundle.getString("origin");
                baseUrl = bundle.getString("baseUrl");
                debug = bundle.getBoolean("debug", false);
            }
        }
    }

    private class OriginInterceptor implements Interceptor {

        private static final String HEADER_ORIGIN = "Origin";

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            request = request.newBuilder().addHeader(HEADER_ORIGIN, origin).build();
            okhttp3.Response response = chain.proceed(request);
            if (response.code() == 403) {
                login();
            }
            return response;
        }
    }

    public interface OnChannelUpdateCallback{
        void onUpdated();

        void onFailed();
    }

}
