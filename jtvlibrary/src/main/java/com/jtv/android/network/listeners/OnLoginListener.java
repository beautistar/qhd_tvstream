package com.jtv.android.network.listeners;

public interface OnLoginListener {

    void onLoginSuccess();

    void onLoginError(int errorMessage, String body);

}
