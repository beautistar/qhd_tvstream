package com.forcetech.android;

import android.util.Log;

import com.jtv.android.BuildConfig;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ForceTV {

    public static final String FORCE_SERVER = "195.12.170.154:9906";

    public static final int FORCE_PORT = 9096;

    private static boolean isStarted = false;


    /*
    *"http://"+ ForceConstant.ForceP2pIp+":"+ ForceConstant.ForceP2pPort+"/cmd.xml?cmd=start_chan&id=p2p_id&server=fccs_ip_and_port
    *
    * florin_m(2598401131) 16:34:56
        * so
        * first command u send to android library is:
    * florin_m(2598401131) 16:36:08
        * http://127.0.0.1:9906/cmd.xml?cmd=start_chan&id=58988852000be90700545eeb54d5004a&server=195.12.170.154:9906
    * florin_m(2598401131) 16:36:10
        * then
    * florin_m(2598401131) 16:36:14
        * get
        * this file:
    * florin_m(2598401131) 16:36:18
        * http://127.0.0.1:9906/58988852000be90700545eeb54d5004a.txt
    *
    *
    */
    public static void initForceClient() {
        //System.out.println("start Force P2P.........");
        Log.d("ForceLib", "initForceClient: starting");
        isStarted = isRunning(FORCE_PORT);
        if (BuildConfig.DEBUG) {
            Log.d("ForceLib", "Try to load library and p2pisStart is " + String.valueOf(isStarted));
        }
        if (!isStarted) {
            int result = start(FORCE_PORT, 16 * 1024 * 1024);
            Log.d("ForceLib", String.valueOf(result));
            // String path = Environment.getExternalStorageDirectory().getPath();
            // Log.d("jni", String.valueOf(startWithLog(9906, 20 * 1024 * 1024, path.getBytes())));
            // Log.d("jni", String.valueOf(start()));
        } else {
            Log.w("ForceLib", "Seems lib  is running already!!!");
        }

    }

    public static boolean isRunning() {
        return isRunning(FORCE_PORT);
    }

    private static boolean isRunning(int port) {
        boolean answer = false;
        try {
            Process process = Runtime.getRuntime().exec("netstat");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()), 1024);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("0.0.0.0:" + port))
                    answer = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    public static native int start(int port, int size);

    public static native int startWithLog(int port, int size, byte[] pathbytes);

    public static native int stop();

    // public native int start();

    static {
        System.loadLibrary("forcetv");
    }

}
