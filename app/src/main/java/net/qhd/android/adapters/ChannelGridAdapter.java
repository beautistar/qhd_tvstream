package net.qhd.android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.qhd.android.R;

import java.util.List;

public class ChannelGridAdapter extends BaseAdapter {

    public static final DisplayImageOptions DISPLAY_IMAGE_OPTIONS = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(500))
            .resetViewBeforeLoading(true)
            .build();
    private List<Channel> channels;
    private LayoutInflater inflater;

    public ChannelGridAdapter(LayoutInflater inflater, List<Channel> channels) {
        this.channels = channels;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return channels.size();
    }

    @Override
    public Object getItem(int position) {
        return channels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_channel_grid_item, null);
        }
        TextView title = (TextView) convertView.findViewById(R.id.title);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
        View favorite = convertView.findViewById(R.id.button_favorite_icon);
        final Channel channel = channels.get(position);
        title.setText(channel.getName());

        ImageLoader loader = ImageLoader.getInstance();
        loader.displayImage(channel.getImgURL(), imageView, DISPLAY_IMAGE_OPTIONS);
        favorite.setVisibility(channel.isFavorite() ? View.VISIBLE : View.GONE);
        return convertView;
    }

}