package net.qhd.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;

import net.qhd.android.R;
import net.qhd.android.filter.ChannelFilter;
import net.qhd.android.filter.ChannelSorter;
import net.qhd.android.filter.FilterUtils;
import net.qhd.android.utils.CustomCategories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChannelAdapter extends BaseAdapter {

    private int selected;
    private int focused;
    private int selectedCategory;
    private int currentCategory;
    private Context context;


    private List<Channel> channels;
    private Category category;
    private List<Channel> sorted = new ArrayList<>();
    private ChannelSorter sort = null;
    private ChannelFilter filter = null;

    public ChannelAdapter(Context context) {
        this.context = context;
        selected = -1;
    }

    public void setChannels(List<Channel> channels, Category category) {
        this.channels = channels;
        this.category = category;
        applyFilters(channels);
        notifyDataSetChanged();
    }

    private void applyFilters(List<Channel> channels) {
        if (getCategoryId() == CustomCategories.CATEGORY_ALL_CHANNELS_ID) {
            this.sorted = FilterUtils.applyFilter(channels, filter);
        } else {
            this.sorted = FilterUtils.applyFilter(channels, null);
        }
        if (this.sorted != null && sort != null) {
            Collections.sort(this.sorted, sort);
        }
    }

    public void setSort(ChannelSorter sorter) {
        if (this.sort == sorter) {
            return;
        }
        this.sort = sorter;
        applyFilters(channels);
        notifyDataSetChanged();
    }

    public void setFilter(ChannelFilter filter) {
        this.filter = filter;
        applyFilters(channels);
        notifyDataSetChanged();
    }

    private int getCategoryId() {
        if (category != null) {
            return category.getRemoteId();
        }
        return -999;
    }

    public ChannelSorter getSort() {
        return sort;
    }

    public ChannelFilter getFilter() {
        return filter;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int position) {
        this.selected = position;
    }

    public int getFocused() {
        return focused;
    }

    public void setFocused(int focused) {
        this.focused = focused;
    }

    public void setSelectedCategory(int selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public void setCurrentCategory(int currentCategory) {
        this.currentCategory = currentCategory;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ViewGroup.inflate(context, R.layout.layout_channel, null);
        }

        // set number value
        TextView tvChannelNum = convertView.findViewById(R.id.tvChannelNumb);
        tvChannelNum.setText(String.valueOf(position + 1));

        // set text value
        TextView tvChannelName = convertView.findViewById(R.id.tvChannelName);
        Channel channel = getItem(position);
        tvChannelName.setText(channel.getName());

        ImageView tvFavorite = convertView.findViewById(R.id.tvChannelFavorite);
        tvFavorite.setVisibility(channel.isFavorite() ? View.VISIBLE : View.INVISIBLE);

        int textColor = Color.WHITE;

        if (position == focused) {
            textColor = ContextCompat.getColor(context, R.color.colorHover);
        }
        if (currentCategory == selectedCategory && position == selected) {
            textColor = Color.rgb(100, 100, 255);
            convertView.setSelected(true);
        }
        tvChannelName.setTextColor(textColor);

        return convertView;
    }

    @Override
    public int getCount() {
        if (sort != null || filter != null) {
            return sorted != null ? sorted.size() : 0;
        }
        return channels != null ? channels.size() : 0;
    }

    @Override
    public Channel getItem(int position) {
        if (position >= getCount() || position < 0) {
            Log.e("ChannelAdapter", "getItem: position out of bounds! pos:" + position);
            return null;
        }
        if (sort != null || filter != null) {
            return sorted.get(position);
        }
        return channels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}