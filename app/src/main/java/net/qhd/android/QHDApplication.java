package net.qhd.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.franmontiel.localechanger.LocaleChanger;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.SettingsManager;
import com.memo.sdk.MemoTVCastSDK;
import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.L;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.squareup.leakcanary.LeakCanary;

import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.utils.Foreground;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import okhttp3.Cache;

public class QHDApplication extends MultiDexApplication {

    public static final String TAG = QHDApplication.class.getSimpleName();

    public static final String APP_NAME = BuildConfig.APP_NAME + " " + BuildConfig.VERSION_NAME;
    public static final int CACHE_MAX_SIZE = 10_485_760;

    private NetworkManager networkManager;
    private SettingsManager settingsManager;

    private PayPalConfiguration payPalConfiguration;

    public static Locale[] SUPPORTED_LOCALES = new Locale[]{
            new Locale("en"),
            new Locale("lt"),
            new Locale("ru"),
            new Locale("fr")
    };

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...

        LocaleChanger.initialize(getApplicationContext(), Arrays.asList(SUPPORTED_LOCALES));

        ActiveAndroid.initialize(DatabaseContentProvider.getConfigurations(this));
        initImageLoader();
        payPalConfiguration = new PayPalConfiguration()
                .environment(BuildConfig.DEBUG ? PayPalConfiguration.ENVIRONMENT_SANDBOX : PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                .clientId(getString(BuildConfig.DEBUG ? R.string.paypal_client_id_sandbox : R.string.paypal_client_id_live))
                .merchantName("Q HD")
                .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"))
                .acceptCreditCards(true);
        settingsManager = new SettingsManager(this);
        networkManager = getNetworkManager();

        if (getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.init(this);
        }
        Foreground.init(this);

//        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        stopService(new Intent(this, ChannelUpdateService.class));
        if (getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.exit();
        }
    }

    /**
     * Configures Universal Image Loader
     *
     * @see ImageLoader#init(ImageLoaderConfiguration)
     */
    private void initImageLoader() {
        if (!ImageLoader.getInstance().isInited()) {
            ImageLoaderConfiguration imageLoaderConfiguration;
            File cacheDir = getExternalCacheDir();
            if (cacheDir == null) {
                cacheDir = getCacheDir();
            }
            DiskCache diskCache = null;
            try {
                //Create 'Least Recently Used' disk cache with size of 20mb
                diskCache = new LruDiskCache(cacheDir, new HashCodeFileNameGenerator(), 20971520);
            } catch (IOException ignored) {
            }
            imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(getApplicationContext())
                    .diskCache(diskCache)
                    .diskCacheFileCount(100)
                    .build();
            L.writeLogs(false);
            L.writeDebugLogs(false);
            ImageLoader.getInstance().init(imageLoaderConfiguration);
        }
    }

    /**
     * Gets system's {@linkplain WifiManager wifi manager}
     *
     * @return wifi manager
     */
    private WifiManager getWifiManager() {
        return (WifiManager) getSystemService(Context.WIFI_SERVICE);
    }

    private Cache getNetworkCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(getCacheDir(), "http-cache"), CACHE_MAX_SIZE);
        } catch (Exception e) {
            Log.e(TAG, "getNetworkCache: Failed to create cache");
        }
        return cache;
    }

    /**
     * Gets network manager
     *
     * @return {@linkplain NetworkManager IPHD Network Manager}
     */
    public NetworkManager getNetworkManager() {
        if (networkManager == null) {
            networkManager = new NetworkManager(getApplicationContext(), BuildConfig.BASE_URL, APP_NAME, getSettingsManager(), getWifiManager(), getNetworkCache(), BuildConfig.DEBUG);
        }
        return networkManager;
    }

    /**
     * Gets settings manager
     *
     * @return {@linkplain SettingsManager IPHD Settings Manager}
     */
    public SettingsManager getSettingsManager() {
        if (settingsManager == null) {
            settingsManager = new SettingsManager(this);
        }
        return settingsManager;
    }

    /**
     * Gets Paypal configuration
     *
     * @return paypal configuration
     */
    public PayPalConfiguration getPayPalConfiguration() {
        return payPalConfiguration;
    }

    public void setPaypalClientId(String clientId) {
        if (!BuildConfig.DEBUG) {
            getPayPalConfiguration().clientId(clientId);
        }
    }

}
