package net.qhd.android.remake;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.filter.ChannelFilter;
import net.qhd.android.filter.ChannelSorter;
import net.qhd.android.filter.FilterUtils;
import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.utils.ChangeChannelByNumberManager;
import net.qhd.android.utils.CustomCategories;
import net.qhd.android.views.WheelImageLoaderInvalidator;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelAdapter;

public class RemakeChannelsFragment extends SideListFragment {

    private static final String ARG_CATEGORY_ID = "categoryId";

    private Category category;

    @BindView(R.id.wheel) WheelView wheel;
    @BindView(R.id.message) TextView message;

    @BindView(R.id.icon_filter) View iconFilter;
    @BindView(R.id.icon_sort) View iconSort;

    public static final DisplayImageOptions DISPLAY_CHANNNEL_IMAGE = new DisplayImageOptions.Builder()
            .displayer(new RoundedBitmapDisplayer(10))
            .resetViewBeforeLoading(true)
            .cacheInMemory(true)
            .cacheOnDisk(false)
            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
            .build();

    private BroadcastReceiver channelUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle arguments = getArguments();
            if (arguments == null) arguments = new Bundle();
            category = getCategory(arguments);
            wheel.invalidateWheel(true);
        }
    };

    private FavoriteBroadcastReceiver channelFavoriteReceiver = new FavoriteBroadcastReceiver() {
        @Override
        public void onFavoriteChanged(int remoteId, boolean favorited) {
            wheel.invalidateWheel(true);
        }
    };

    private ChangeChannelByNumberManager changeChannelByNumberManager;
    private ChannelAdapter adapter;
    private BroadcastReceiver filterUpdateReceiver = new FilterBroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setFilter(getContract().getFilter());
            setSorter(getContract().getSorter());
        }
    };

    public static RemakeChannelsFragment instance(int categoryId) {
        RemakeChannelsFragment remakeChannelsFragment = new RemakeChannelsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_ID, categoryId);
        remakeChannelsFragment.setArguments(args);
        return remakeChannelsFragment;
    }

    public RemakeChannelsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null) arguments = new Bundle();
        category = getCategory(arguments);
        if (category == null) {
            throw new RuntimeException("Category not found");
        }
    }

    protected Category getCategory(Bundle arguments) {
        int categoryId = arguments.getInt(ARG_CATEGORY_ID, -1);
        return Database.getCategory(categoryId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_remake_channels, container, false);
        ButterKnife.bind(this, root);

        if (category.getChannels().size() == 0) {
            message.setVisibility(View.VISIBLE);
        }

        final TextView title = root.findViewById(R.id.category_name);
        title.setText(category.getName());

        changeChannelByNumberManager = new ChangeChannelByNumberManager(category.getChannels().size())
                .setOnChannelSelectedListener(new ChangeChannelByNumberManager.OnChannelSelected() {
                    @Override
                    public void onChannelSelected(int position) {
                        if (getContract() != null) {
                            wheel.setCurrentItem(position);
                            playChannel(position);
                        }
                    }

                    @Override
                    public void onNumberChanged(int number) {
                        if (getContract() != null) {
                            if (number == 0) {
                                getContract().showChannelInfo(wheel.getCurrentItem() + 1, getContract().getCurrentChannel().getName(), Color.WHITE);
                            } else {
                                getContract().showChannelInfo(number, adapter.getChannels().get(number - 1).getName(), Color.YELLOW);
                            }
                        }
                    }
                });

        wheel.setDrawShadows(WheelView.SHADOWS_NONE);
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 96, r.getDisplayMetrics());
        wheel.setItemOffset((int) px);
        adapter = new ChannelAdapter(category.getChannels());
        wheel.setViewAdapter(adapter);
        wheel.setFocusable(true);
        wheel.setOnTouchListener(new View.OnTouchListener() {

            GestureDetectorCompat detector = new GestureDetectorCompat(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public void onLongPress(MotionEvent e) {
                    int currentItem = wheel.getCurrentItem();
                    Channel channel = null;
                    if (currentItem >= 0 && currentItem < adapter.getRealItemCount()) {
                        channel = adapter.getChannels().get(currentItem);
                    }
                    if (channel != null) {
                        getContract().showPopupForChannel(channel, category);
                    }
                    wheel.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                }
            });

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return detector.onTouchEvent(motionEvent);
            }
        });
        wheel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                getContract().onInteraction();
                if (changeChannelByNumberManager.handleKeyEvent(event)) {
                    return true;
                }

                int currentItem = wheel.getCurrentItem();

                if (event.isLongPress()) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        Channel channel = null;
                        if (currentItem >= 0 && currentItem < adapter.getRealItemCount()) {
                            channel = adapter.getChannels().get(currentItem);
                        }
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                getContract().showPopupForChannel(channel, category);
                                return true;
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                                getContract().showPopupForChannel(channel, category);
                                return true;
                        }
                    }
                } else {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                                playChannel(currentItem);
                                return true;
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                                playChannel(currentItem);
                                return true;
                            case KeyEvent.KEYCODE_DPAD_LEFT:
                                getFragmentManager().popBackStack();
                                return true;
                            case KeyEvent.KEYCODE_DPAD_UP:
                                wheel.setCurrentItem(currentItem - 1, true);
                                return true;
                            case KeyEvent.KEYCODE_DPAD_DOWN:
                                wheel.setCurrentItem(currentItem + 1, true);
                                return true;
                        }
                    }
                }
                return false;
            }
        });
        wheel.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(WheelView wheel, int itemIndex) {
                getContract().onInteraction();
                while (itemIndex < 0) {
                    itemIndex += adapter.getItemsCount();
                }
                if (wheel.getCurrentItem() == itemIndex) {
                    playChannel(itemIndex);
                } else {
                    wheel.setCurrentItem(itemIndex, true);
                }
            }
        });
        wheel.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                getContract().onInteraction();
                playChannel(newValue);
            }
        });
        setFilter(getContract().getFilter());
        setSorter(getContract().getSorter());

        if (getContract().getCurrentChannel() != null) {
            Channel firstChannel = getFirstChannel();
            if (firstChannel != null) {
                int i = adapter.getChannels().indexOf(firstChannel);
                if (i > -1) {
                    wheel.setCurrentItem(i);
                }
            }
        }

        wheel.requestFocus();

        return root;
    }

    public void setFilter(ChannelFilter filter) {
        if (category.getRemoteId() == CustomCategories.CATEGORY_ALL_CHANNELS_ID) {
            adapter.setFilter(filter);
            adapter.updateFilter();
            changeChannelByNumberManager.setMax(adapter.getItemsCount());
            iconFilter.setVisibility(filter == null ? View.GONE : View.VISIBLE);
        }
        wheel.setCyclic(adapter.getRealItemCount() > 1);
    }

    public void setSorter(ChannelSorter sorter) {
        adapter.setSorter(sorter);
        adapter.updateFilter();
        iconSort.setVisibility(sorter == null ? View.GONE : View.VISIBLE);
    }

    protected Channel getFirstChannel() {
        return getContract().getCurrentChannel();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerChannelUpdateReceiver(channelUpdateReceiver);
        registerChannelFavoriteReceiver(channelFavoriteReceiver);
        registerFilterUpdateReceiver(filterUpdateReceiver);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(channelUpdateReceiver);
        getActivity().unregisterReceiver(channelFavoriteReceiver);
        getActivity().unregisterReceiver(filterUpdateReceiver);
    }

    private void registerChannelUpdateReceiver(BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter(ChannelUpdateService.ACTION_UPDATED);
        getActivity().registerReceiver(receiver, filter);
    }

    private void registerFilterUpdateReceiver(BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter(FilterBroadcastReceiver.ACTION_FILTER);
        getActivity().registerReceiver(receiver, filter);
    }

    /**
     * extras: "channel" : remoteChannelId, "favorite" : boolean
     *
     * @param receiver receiver to register
     */
    private void registerChannelFavoriteReceiver(BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter(FavoriteBroadcastReceiver.ACTION_FAVORITE);
        getActivity().registerReceiver(receiver, filter);
    }

    private void playChannel(int index) {
        if (category == null) return;
        if (index < 0 || index >= adapter.getRealItemCount()) return;
        Channel channel = adapter.getChannels().get(index);
        ((BaseActivity) getActivity()).getSettingsManager().setLastChannel(index);
        getContract().playChannel(channel);
        getContract().showChannelInfo(index + 1, channel.getName(), Color.WHITE);
    }

    private class ChannelAdapter extends AbstractWheelAdapter {

        private ChannelSorter sorter;
        private ChannelFilter filter;

        private List<Channel> original;
        private List<Channel> channels;

        ChannelAdapter(List<Channel> channels) {
            this.original = channels;
            this.channels = FilterUtils.applyFilter(channels, filter);
            if (sorter != null) {
                Collections.sort(this.channels, sorter);
            }
        }

        @Override
        public int getItemsCount() {
            return channels.size() == 0 ? 1 : channels.size();
        }

        public int getRealItemCount() {
            return channels.size();
        }

        public List<Channel> getChannels() {
            return channels;
        }

        public void setFilter(ChannelFilter filter) {
            this.filter = filter;
        }

        public void setSorter(ChannelSorter sorter) {
            this.sorter = sorter;
        }

        public void updateFilter() {
            this.channels = FilterUtils.applyFilter(original, filter);
            if (sorter != null) {
                Collections.sort(this.channels, sorter);
            }
            notifyDataChangedEvent();
        }

        @Override
        public View getItem(int index, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater(null).inflate(R.layout.layout_channel_item, parent, false);
            }
            TextView title = convertView.findViewById(R.id.item_title);
            TextView number = convertView.findViewById(R.id.item_number);
            ImageView icon = convertView.findViewById(R.id.item_icon);
            View favorite = convertView.findViewById(R.id.is_favorite);
            if (channels.size() == 0) {
                title.setText(R.string.error_list_empty);
                number.setText("");
                favorite.setVisibility(View.INVISIBLE);
                ImageLoader.getInstance().displayImage(
                        "",
                        icon,
                        new DisplayImageOptions.Builder().cloneFrom(DISPLAY_CHANNNEL_IMAGE)
                                .delayBeforeLoading(index * 50)
                                .build(),
                        new WheelImageLoaderInvalidator(wheel)
                );
            } else {
                Channel channel = channels.get(index);
                favorite.setVisibility(channel.isFavorite() ? View.VISIBLE : View.INVISIBLE);
                ImageLoader.getInstance().displayImage(
                        channel.getImgURL(),
                        icon,
                        new DisplayImageOptions.Builder().cloneFrom(DISPLAY_CHANNNEL_IMAGE)
                                .delayBeforeLoading(index * 50)
                                .build(),
                        new WheelImageLoaderInvalidator(wheel)
                );
                number.setText(String.valueOf(index + 1));
                title.setText(channel.getName());
            }

            return convertView;
        }

    }

}
