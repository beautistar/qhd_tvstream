package net.qhd.android.remake;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.jtv.android.models.Reminder;
import com.jtv.android.models.epg.EPGChannel;
import com.jtv.android.models.epg.EPGProgramme;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.Database;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.services.ReminderService;
import net.qhd.android.utils.RealtimeDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemakeEPGFragment extends Fragment implements Callback<EPGChannel> {
    private static final String ARG_CHANNEL_ID = "channel_id";
    public static final String TAG = "EPGFragment";

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
    public static final SimpleDateFormat DATE_HUMAN = new SimpleDateFormat("MM-dd EEEE", Locale.getDefault());
    public static final SimpleDateFormat DATE_HOUR = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final String ARG_AVAILABLE_TIMESHIFT = "availableTimeshift";

    @BindView(R.id.epg_title) TextView epgTitle;

    @BindView(R.id.epg_date) TextView epgDate;
    @BindView(R.id.epg_time) TextView epgTime;

    @BindView(R.id.epg_list) RecyclerView recycler;
    @BindView(R.id.epg_list_programme) RecyclerView recyclerProgramme;

    @BindView(R.id.progressBar) View progressBar;
    @BindView(R.id.progress_view) ProgressBar progress;
    @BindView(R.id.list_empty) TextView listEmpty;
    @BindView(R.id.epg_root) View epgRoot;

    private EPGContract contract;
    private Channel channel;

    private RealtimeDate date;
    private NetworkManager networkManager;
    private EPGChannel channelEPG;
    private EpgDayAdapter dayAdapter;
    private EpgAdapter programmeAdapter;

    private List<Reminder> reminders;

    public RemakeEPGFragment() {
        // Required empty public constructor
    }

    public static RemakeEPGFragment newInstance(long channelId) {
        RemakeEPGFragment fragment = new RemakeEPGFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CHANNEL_ID, channelId);
        fragment.setArguments(args);
        return fragment;
    }

    public static RemakeEPGFragment newInstance(long channelId, long availableTimeshift) {
        RemakeEPGFragment fragment = new RemakeEPGFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CHANNEL_ID, channelId);
        args.putLong(ARG_AVAILABLE_TIMESHIFT, availableTimeshift);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        if (getArguments() != null) {
            long channelId = getArguments().getLong(ARG_CHANNEL_ID);
            channel = Database.getChannel(channelId);
            networkManager.getChannelEPG(channelId, this);
            channel.setAvailableTimeshift(getArguments().getLong(ARG_AVAILABLE_TIMESHIFT, -1));
            reminders = Database.getReminders(channelId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_remake_epg, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setNestedScrollingEnabled(false);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerProgramme.setNestedScrollingEnabled(false);
        recyclerProgramme.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        if (channelEPG != null) {
            init();
        }
        date = new RealtimeDate(epgTime, epgDate);
        epgTitle.setText(channel.getName());

    }

    @Override
    public void onResume() {
        super.onResume();
        date.start();
        recycler.requestFocus();
        contract.setInterfaceActive(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        date.stop();
        contract.setInterfaceActive(true);
    }

    private void init() {
        if (channelEPG == null || recycler == null || channelEPG.getProgramme().size() == 0) {
            progress.setVisibility(View.GONE);
            listEmpty.setVisibility(View.VISIBLE);
            return;
        }
        Log.d(TAG, "init: " + channelEPG.toString());
        progressBar.setVisibility(View.GONE);
        epgRoot.setVisibility(View.VISIBLE);
        List<Long> days = new LinkedList<>();
        for (EPGProgramme programme : channelEPG.getProgramme()) {
            try {
                Calendar instance = Calendar.getInstance();
                instance.setTime(DATE_FORMAT.parse(programme.getStart()));
                instance.set(Calendar.HOUR_OF_DAY, 0);
                instance.set(Calendar.MINUTE, 0);
                instance.set(Calendar.SECOND, 0);
                instance.set(Calendar.MILLISECOND, 0);
                long timeInMillis = instance.getTimeInMillis();
                if (!days.contains(timeInMillis)) {
                    days.add(timeInMillis);
                }
            } catch (ParseException e) {
                Log.e(TAG, "init: failed to parse date", e);
            }
        }
        dayAdapter = new EpgDayAdapter(days);
        recycler.setAdapter(dayAdapter);
        Calendar dayCal = Calendar.getInstance();
        int current = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        long begin = 0;
        long end = Long.MAX_VALUE;
        for (int i = 0; i < days.size(); i++) {
            dayCal.setTimeInMillis(days.get(i));
            if (dayCal.get(Calendar.DAY_OF_YEAR) == current) {
                begin = days.get(i);
                if (i < days.size() - 1) {
                    end = days.get(i + 1);
                }
                dayAdapter.setSelected(i);
                break;
            }
        }
        programmeAdapter = new EpgAdapter(begin, end);
        programmeAdapter.setListener(new OnEPGClick() {
            @Override
            public void onEPGClick(final int position, View view, final EPGProgramme programme) {

                long currentTime = Calendar.getInstance().getTimeInMillis();
                final long startTime = programme.getStartTimestamp();

                PopupMenu popupMenu = new PopupMenu(getActivity(), view);
                Menu menu = popupMenu.getMenu();
                final Reminder reminderByTimestamp = findReminderByTimestamp(startTime);
                if (startTime < currentTime && channel.getAvailableTimeshift() > 0) {
                    menu.add(0, 0, 0, R.string.buttton_play);
                }
                if (currentTime < startTime && reminderByTimestamp == null) {
                    menu.add(1, 1, 1, R.string.add_reminder);
                }
                if (reminderByTimestamp != null) {
                    menu.add(2, 2, 2, R.string.remove_reminder);
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 0:
                                contract.openTimeshift(channel, startTime);
                                return true;
                            case 1:
                                Reminder reminder = new Reminder(programme.getTitle(), startTime, channel.getRemoteId());
                                reminder.save();
                                reminders = Database.getReminders(channel.getRemoteId());
                                ReminderService.add(getActivity(), reminder);
                                programmeAdapter.notifyItemChanged(position);
                                return true;
                            case 2:
                                ReminderService.remove(getActivity(), reminderByTimestamp);
                                reminders.remove(reminderByTimestamp);
                                programmeAdapter.notifyItemChanged(position);
                                return true;
                        }
                        return false;
                    }
                });
                if (menu.size() > 0) {
                    popupMenu.show();
                }
            }

        });
        recyclerProgramme.setAdapter(programmeAdapter);
        for (int i = 0; i < programmeAdapter.getItemCount(); i++) {
            EPGProgramme programme = programmeAdapter.getItem(i);
            long cal = Calendar.getInstance().getTimeInMillis();
            Calendar start = getProgrammeStartCalendar(programme);
            Calendar stop = getProgrammeStopCalendar(programme);

            if (cal >= start.getTimeInMillis() && cal < stop.getTimeInMillis()) {
                recyclerProgramme.scrollToPosition(i);
                break;
            }
        }
    }

    private Reminder findReminderByTimestamp(long timestamp) {
        for (Reminder reminder : reminders) {
            if (reminder.getTimestamp() == timestamp) {
                return reminder;
            }
        }
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EPGContract) {
            contract = (EPGContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ChannelButtonPressed");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        contract = null;
    }

    @Override
    public void onResponse(Call<EPGChannel> call, Response<EPGChannel> response) {
        if (response.isSuccessful()) {
            channelEPG = response.body();
            if (recycler != null) {
                init();
            }
        }
    }

    @Override
    public void onFailure(Call<EPGChannel> call, Throwable t) {
        if (progressBar != null) {
            progress.setVisibility(View.GONE);
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    class EpgDayAdapter extends RecyclerView.Adapter<EpgDayAdapter.EpgViewHolder> {

        private List<Long> days;
        private int selected = -1;

        public EpgDayAdapter(List<Long> days) {
            this.days = days;
        }


        public void setSelected(int index) {
            int last = selected;
            if (last >= 0 && last < days.size()) {
                notifyItemChanged(last);
            }
            selected = index;
            if (index >= 0 || index < days.size()) {
                notifyItemChanged(selected);
            }
        }

        @Override
        public EpgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_epg_day, parent, false);
            // set the view's size, margins, paddings and layout parameters
            return new EpgViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final EpgViewHolder holder, int position) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(days.get(position));
            holder.textView.setText(DATE_HUMAN.format(instance.getTime()));
            holder.selected.setVisibility(selected == position ? View.VISIBLE : View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getAdapterPosition();
                    setSelected(pos);
                    long end = Long.MAX_VALUE;
                    long start = days.get(pos);
                    if (pos < days.size() - 1) {
                        end = days.get(pos + 1);
                    }
                    programmeAdapter.setRange(start, end);
                }
            });
        }

        @Override
        public int getItemCount() {
            return days == null ? 0 : days.size();
        }

        class EpgViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.day) TextView textView;
            @BindView(R.id.underscore) View selected;

            public EpgViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    class EpgAdapter extends RecyclerView.Adapter<EpgAdapter.EpgViewHolder> {

        private List<EPGProgramme> filtered = new ArrayList<>();
        private OnEPGClick listener;

        public EpgAdapter(long begin, long end) {
            setRange(begin, end);
        }

        public void setRange(long begin, long end) {
            filtered.clear();
            Calendar cal = Calendar.getInstance();
            for (EPGProgramme programme : channelEPG.getProgramme()) {
                try {
                    cal.setTime(DATE_FORMAT.parse(programme.getStart()));
                    if (cal.getTimeInMillis() >= begin && cal.getTimeInMillis() < end) {
                        filtered.add(programme);
                    }
                } catch (ParseException e) {
                    Log.e(TAG, "setRange: parse error", e);
                }
            }
            notifyDataSetChanged();
        }

        public EPGProgramme getItem(int position) {
            return filtered.get(position);
        }

        @Override
        public EpgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_epg_programme, parent, false);
            // set the view's size, margins, paddings and layout parameters
            return new EpgAdapter.EpgViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final EpgViewHolder holder, int position) {
            EPGProgramme programme = filtered.get(position);
            holder.setProgramme(programme);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onEPGClick(holder.getAdapterPosition(), holder.itemView, filtered.get(holder.getAdapterPosition()));
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return filtered.size();
        }

        public void setListener(OnEPGClick listener) {
            this.listener = listener;
        }

        class EpgViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.programme_title) TextView title;
            @BindView(R.id.programme_progress) ProgressBar progress;
            @BindView(R.id.programme_time) TextView time;
            @BindView(R.id.programme_description) TextView description;
            @BindView(R.id.programme_reminder) ImageView reminder;

            public EpgViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void setProgramme(EPGProgramme programme) {
                this.title.setText(programme.getTitle());
                this.description.setVisibility(programme.getDescription() == null || programme.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
                this.description.setText(programme.getDescription());
                progress.setVisibility(View.GONE);
                long current = Calendar.getInstance().getTimeInMillis();
                Calendar start = getProgrammeStartCalendar(programme);
                Calendar end = getProgrammeStopCalendar(programme);

                time.setText(String.format(Locale.getDefault(), "%s - %s", DATE_HOUR.format(start.getTime()), DATE_HOUR.format(end.getTime())));
                if (current >= start.getTimeInMillis() && current < end.getTimeInMillis()) {
                    progress.setMax((int) (end.getTimeInMillis() - start.getTimeInMillis()));
                    progress.setProgress((int) (current - start.getTimeInMillis()));
                    progress.setVisibility(View.VISIBLE);
                }

                reminder.setVisibility(View.GONE);
                for (Reminder rem : reminders) {
                    if (start.getTimeInMillis() == rem.getTimestamp()) {
                        reminder.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    public static Calendar getProgrammeStartCalendar(EPGProgramme programme) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(DATE_FORMAT.parse(programme.getStart()));
        } catch (ParseException ignored) {
        }
        return cal;
    }

    public static Calendar getProgrammeStopCalendar(EPGProgramme programme) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(DATE_FORMAT.parse(programme.getStop()));
        } catch (ParseException ignored) {
        }
        return cal;
    }

    public interface EPGContract {

        void setInterfaceActive(boolean active);

        void openTimeshift(Channel channel, long seekTo);

    }

    interface OnEPGClick {
        void onEPGClick(int position, View view, EPGProgramme programme);

    }

}
