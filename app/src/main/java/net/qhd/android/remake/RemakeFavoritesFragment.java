package net.qhd.android.remake;

import android.os.Bundle;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;

import net.qhd.android.utils.CustomCategories;

public class RemakeFavoritesFragment extends RemakeChannelsFragment {

    @Override
    protected Category getCategory(Bundle arguments) {
        return CustomCategories.getFavoriteCategory(getContext());
    }

    @Override
    protected Channel getFirstChannel() {
        return null;
    }
}
