package net.qhd.android.remake;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.SettingsManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.SettingsActivity;
import net.qhd.android.views.WheelImageLoaderInvalidator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RemakeCategoriesFragment extends SideListFragment {


    public static final String TAG = "CategoriesFragment";

    public static final DisplayImageOptions DISPLAY_CATEGORY_IMAGE = new DisplayImageOptions.Builder()
            .displayer(new RoundedBitmapDisplayer(10))
            .resetViewBeforeLoading(true)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
            .build();

    @BindView(R.id.wheel) WheelView wheel;
    @BindView(R.id.message) TextView message;

    private List<Category> categories;
    private int lastItem = -1;
    private SettingsManager settingsManager;

    public RemakeCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categories = Database.getCategories(false);
        settingsManager = ((BaseActivity) getActivity()).getSettingsManager();
        lastItem = settingsManager.getLastCategory();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_categories, container, false);
        ButterKnife.bind(this, root);

        if (categories.size() == 0) {
            message.setVisibility(View.VISIBLE);
        }

        wheel.setCyclic(true);
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 96, r.getDisplayMetrics());
        wheel.setItemOffset((int) px);

        wheel.setDrawShadows(WheelView.SHADOWS_NONE);
        wheel.setViewAdapter(new AbstractWheelAdapter() {
            @Override
            public int getItemsCount() {
                return categories.size();
            }

            @Override
            public View getItem(int index, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater(null).inflate(R.layout.layout_menu_item, parent, false);
                }
                TextView title = (TextView) convertView.findViewById(R.id.item_title);
                ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
                ImageLoader.getInstance().displayImage(
                        categories.get(index).getImageURL(),
                        icon,
                        new DisplayImageOptions.Builder().cloneFrom(DISPLAY_CATEGORY_IMAGE)
                                .delayBeforeLoading(index * 50)
                                .build(),
                        new WheelImageLoaderInvalidator(wheel)
                );
                title.setText(categories.get(index).getName());
                return convertView;
            }

        });
        wheel.setFocusable(true);
        wheel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                getContract().onInteraction();
                int currentItem = wheel.getCurrentItem();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            openCategory(wheel.getCurrentItem());
                            return true;
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            openCategory(wheel.getCurrentItem());
                            return true;
                        case KeyEvent.KEYCODE_DPAD_LEFT:
                            getFragmentManager().popBackStack();
                            return true;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            wheel.setCurrentItem(currentItem - 1, true);
                            return true;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            wheel.setCurrentItem(currentItem + 1, true);
                            return true;
                    }
                }
                return false;
            }
        });
        wheel.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(WheelView wheel, int itemIndex) {
                getContract().onInteraction();
                while (itemIndex < 0) {
                    itemIndex += categories.size();
                }
                openCategory(itemIndex % categories.size());

            }
        });
        wheel.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                getContract().onInteraction();
            }
        });

        Channel currentChannel = getContract().getCurrentChannel();
        if (currentChannel != null) {
            Category currentCategory = currentChannel.getCategory();
            int i = categories.indexOf(currentCategory);
            if (i > -1) {
                wheel.setCurrentItem(i);
            }
        }


        wheel.requestFocus();

        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        lastItem = wheel.getCurrentItem();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (lastItem != -1) {
            wheel.setCurrentItem(lastItem);
        }
    }

    private void openCategory(int i) {
        if (categories.size() == 0) {
            getActivity().startActivity(SettingsActivity.getIntent(getContext(), 5)); // open order package
            return;
        }
        if (i < 0 || i >= categories.size()) {
            return;
        }
        ((BaseActivity) getActivity()).getSettingsManager().setLastCategory(i);
        getContract().openCategory(categories.get(i));
    }


}
