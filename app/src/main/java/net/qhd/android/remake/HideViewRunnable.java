package net.qhd.android.remake;

import android.view.View;

public class HideViewRunnable implements Runnable {

    private View view;
    private int visibility = View.INVISIBLE;

    public HideViewRunnable(View view) {
        this.view = view;
    }

    public HideViewRunnable(View view, int visibility) {
        this.view = view;
        this.visibility = visibility;
    }

    @Override
    public void run() {
        view.setVisibility(visibility);
    }

    public boolean isVisible() {
        return view.getVisibility() == View.VISIBLE;
    }
}
