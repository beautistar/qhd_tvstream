package net.qhd.android.remake;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;

import net.qhd.android.filter.ChannelFilter;
import net.qhd.android.filter.ChannelSorter;

interface RemakeContract extends RemakeEPGFragment.EPGContract {

    void openLiveTV();

    void openVOD();

    void openApps();

    void openConfig();

    void openSettings();

    void openCategory(Category category);

    void openFavorites();

    void openRecents();

    void openMostWatched();

    void openSearch();

    void openTimeshift(Channel channel);

    void showPopupForChannel(Channel channel, Category category);

    void playChannel(Channel channel);

    void showChannelInfo(int number, String name, int color);

    ChannelSorter getSorter();

    ChannelFilter getFilter();

    Channel getCurrentChannel();

    void onInteraction();

    void openAllChannels();
}
