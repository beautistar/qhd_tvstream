package net.qhd.android.remake;

import android.os.Bundle;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;

import net.qhd.android.utils.CustomCategories;

public class RemakeAllChannelsFragment extends RemakeChannelsFragment {

    @Override
    protected Category getCategory(Bundle arguments) {
        return CustomCategories.getAllChannelsCategory(getContext());
    }

    @Override
    protected Channel getFirstChannel() {
        return null;
    }

}
