package net.qhd.android.remake;

import android.content.Context;
import android.support.v4.app.Fragment;

public class SideListFragment extends Fragment {

    private RemakeContract contract;

    public SideListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RemakeContract) {
            contract = (RemakeContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SideListContract");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        contract = null;
    }

    protected RemakeContract getContract() {
        return contract;
    }

}
