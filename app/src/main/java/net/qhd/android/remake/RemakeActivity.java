package net.qhd.android.remake;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnTokenChangeListener;
import com.jtv.android.utils.ChannelStatisticsManager;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.SettingsManager;
import com.jtv.player.BandwidthEstimateListener;
import com.jtv.player.JTVExoPlayer;
import com.jtv.player.JTVNativePlayer;
import com.jtv.player.JTVPlayer;
import com.jtv.player.exceptions.HttpException;
import com.jtv.player.trackselection.Track;

import net.qhd.android.BuildConfig;
import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.QHDPlayerActivity;
import net.qhd.android.activities.SettingsActivity;
import net.qhd.android.activities.TubiControllerActivity;
import net.qhd.android.activities.main.AppListActivity;
import net.qhd.android.activities.main.InitActivity;
import net.qhd.android.activities.movies.MovieCategoryActivity;
import net.qhd.android.filter.ChannelFilter;
import net.qhd.android.filter.ChannelSorter;
import net.qhd.android.filter.CountryFilter;
import net.qhd.android.filter.FilterUtils;
import net.qhd.android.fragments.ChannelButtonsFragment;
import net.qhd.android.fragments.ChannelInfoFragment;
import net.qhd.android.utils.Analytics;
import net.qhd.android.utils.CustomCategories;
import net.qhd.android.utils.RealtimeDate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;
import static net.qhd.android.activities.SettingsActivity.PROP_SUBSCRIBED;

public class RemakeActivity extends BaseActivity implements RemakeContract,
        JTVPlayer.QHDListener,
        OnTokenChangeListener,
        ChannelButtonsFragment.ChannelButtonPressed {

    public static final String TAG = "RemakeActivity";

    public static final int ERROR_COUNT_MAX = 6;

    private SettingsManager settingsManager;
    private NetworkManager networkManager;

    @BindView(R.id.activity_root) FrameLayout root;

    @BindView(R.id.surface_view) SurfaceView surfaceView;
    @BindView(R.id.aspect_ratio_layout) AspectRatioFrameLayout aspectRatioFrameLayout;

    @BindView(R.id.side_fragment) View sideFragment;
    @BindView(R.id.selected_channel_root) View selectedChannelRoot;
    @BindView(R.id.text_channel_number) TextView selectedChannelNumber;
    @BindView(R.id.text_channel_name) TextView selectedChannelName;

    @BindView(R.id.text_hours) TextView textHours;
    @BindView(R.id.text_date) TextView textDate;
    @BindView(R.id.icon_hd) ImageView iconHD;

    @BindView(R.id.ui_root) View uiRoot;
    private HideViewRunnable hideRootRunnable;
    private HideViewRunnable hideSelectedChannel;

    private JTVPlayer player;
    private int errorCount = 0;

    private Handler mainThreadHandler = new Handler();
    private ChangeChannelRunnable changeChannelRunnable = new ChangeChannelRunnable();
    private ChannelStatisticsManager statisticsManager;
    private int playerId;
    private int aspectRatio;
    private Channel currentChannel;

    private ChannelFilter filter;
    private ChannelSorter sorter;

    private RealtimeDate dateHandler;

    private FirebaseAnalytics mFirebaseAnalytics;

    private Runnable restartChannelRunnable = new Runnable() {
        @Override
        public void run() {
            if (currentChannel != null) {
                playChannel(currentChannel);
            }
        }
    };
    private ChannelInfoFragment channelInfoFragment;
    private ChannelButtonsFragment channelButtonsFragment;
    private boolean inputShowsUI = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getNetworkManager().isLoggedIn()) {
            Intent intent = new Intent(this, InitActivity.class);
            openActivity(intent, R.anim.fade_in, R.anim.fade_out);
            finish();
            return;
        }

        setContentView(R.layout.activity_remake);
        ButterKnife.bind(this);
        hideRootRunnable = new HideViewRunnable(uiRoot, View.GONE);
        hideSelectedChannel = new HideViewRunnable(selectedChannelRoot);

        settingsManager = getSettingsManager();
        networkManager = getNetworkManager();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, null, null);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.side_fragment, new RemakeMenuFragment(), "menu")
                .commit();

        statisticsManager = new ChannelStatisticsManager();

        channelInfoFragment = (ChannelInfoFragment) getSupportFragmentManager().findFragmentById(R.id.channel_info_fragment);
        channelButtonsFragment = (ChannelButtonsFragment) getSupportFragmentManager().findFragmentById(R.id.channel_button_fragment);

        if (getSettingsManager().wasNormalExit()) {
            List<Channel> recentlyWatched = Database.getRecentlyWatched(1);
            if (!recentlyWatched.isEmpty()) {
                playChannel(recentlyWatched.get(0));
            }
            getSettingsManager().setNormalExit(false);
        }

        dateHandler = new RealtimeDate(textHours, textDate);
        surfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUI();
            }
        });

        if (getSettingsManager().checkSubscribtion()) {
            new AlertDialog.Builder(this).setTitle(R.string.subscribe_title)
                    .setMessage(R.string.subscribe_message)
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getSettingsManager().setSubscribed(false);
                            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(RemakeActivity.this);
                            analytics.setUserProperty(PROP_SUBSCRIBED, Boolean.toString(false));
                        }
                    })
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseMessaging.getInstance().subscribeToTopic("news");
                            getSettingsManager().setSubscribed(true);
                            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(RemakeActivity.this);
                            analytics.setUserProperty(PROP_SUBSCRIBED, Boolean.toString(true));
                        }
                    }).show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onResume() {
        super.onResume();
        int last = playerId;
        playerId = settingsManager.getPlayer();
        if (last != playerId && player != null) {
            releasePlayer();
        }
        setAspectRatio();
        dateHandler.start();
        if (Build.VERSION.SDK_INT <= 23 && currentChannel != null) {
            playChannel(currentChannel);
        }
        keepScreenOn(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT > 23 && currentChannel != null) {
            playChannel(currentChannel);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        if (Build.VERSION.SDK_INT > 23) {
            releasePlayer();
        }
        getSettingsManager().setNormalExit(true);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");

        statisticsManager.onChannelStopped(currentChannel);
        dateHandler.stop();

        //Hide channel name
        selectedChannelRoot.setVisibility(View.INVISIBLE);
        mainThreadHandler.removeCallbacks(hideSelectedChannel);
        mainThreadHandler.removeCallbacks(changeChannelRunnable);
        mainThreadHandler.removeCallbacks(hideRootRunnable);

        if (Build.VERSION.SDK_INT <= 23) {
            releasePlayer();
        }
        keepScreenOn(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        currentChannel = null;
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (!isMyLauncherDefault() || getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        }
    }

    boolean isMyLauncherDefault() {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<>();
        filters.add(filter);

        final String myPackageName = getPackageName();
        List<ComponentName> activities = new ArrayList<>();
        final PackageManager packageManager = getPackageManager();

        // You can use name of your package here as third argument
        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    private void setAspectRatio() {
        aspectRatio = settingsManager.getAspectRatio();
        switch (aspectRatio) {
            case 0: //Auto
                break;
            case 1:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_4_3);
                break;
            case 2:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_16_9);
                break;
            case 3:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_21_9);
        }
    }

    @Override
    public void openLiveTV() {
        changeSideFragment(new RemakeCategoriesFragment(), "livetv");
    }

    private void changeSideFragment(Fragment fragment, String tag) {
        showUI();
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.side_fragment, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void openVOD() {
        startActivity(new Intent(this, MovieCategoryActivity.class));
    }

    @Override
    public void openApps() {
        startActivity(new Intent(this, AppListActivity.class));
    }

    @Override
    public void openConfig() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    public void openSettings() {
        startActivity(new Intent(Settings.ACTION_SETTINGS));
    }

    @Override
    public void openCategory(final Category category) {
        if (category.isAuditing()) {
            CustomDialogs.showPasswordInput(this, getSettingsManager(), new CustomDialogs.OnPasswordAcceptedListener() {
                @Override
                public void onPasswordAccepted() {
                    changeSideFragment(RemakeChannelsFragment.instance(category.getRemoteId()), "category");
                }
            });
        } else {
            changeSideFragment(RemakeChannelsFragment.instance(category.getRemoteId()), "category");
        }
    }

    @Override
    public void openFavorites() {
        changeSideFragment(new RemakeFavoritesFragment(), "favorites");
    }

    @Override
    public void openRecents() {
        changeSideFragment(new RemakeRecentsFragment(), "recents");
    }

    @Override
    public void openMostWatched() {
        changeSideFragment(new RemakeMostWatchedFragment(), "most_watched");
    }

    @Override
    public void openSearch() {
        CustomDialogs.showSearchInput(this, new CustomDialogs.OnSearchQueryListener() {
            @Override
            public void onSearchQuery(String query) {
                changeSideFragment(RemakeSearchFragment.instance(query), "search");
            }
        });
    }

    @Override
    public void openTimeshift(Channel channel) {
        onTimeshiftPressed(channel);
    }

    @Override
    public void showPopupForChannel(Channel channel, Category category) {
        showPopupForView(channelInfoFragment.getView(), channel, category);
    }

    private void showPopupForView(View view, final Channel channel, Category category) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(this, view);
        Menu menu = popup.getMenu();
        if (channel != null) {
            menu.add(0, 0, 0, R.string.buttton_play);
            if (category == null) {
                category = channel.getCategory();
            }
        }
        if (channel != null && !category.isAuditing()) {
            menu.add(0, 1, 1, channel.isFavorite() ? R.string.favorite_remove : R.string.favorite_add);
        }
        menu.add(0, 3, 3, R.string.list_sort);
        if (category.getRemoteId() == CustomCategories.CATEGORY_ALL_CHANNELS_ID) {
            menu.add(0, 4, 4, R.string.list_filter);
        }
        menu.add(0, 5, 5, R.string.search);
        menu.add(0, 6, 6, R.string.recents);
        if (channel != null && channel.getAvailableTimeshift() > 0) {
            menu.add(0, 7, 7, R.string.timeshift);
        }
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0: //Play pressed
                        playChannel(channel);
                        break;
                    case 1: //Fav unfav
                        onFavoritePressed(channel);
                        break;
                    case 3:
                        CustomDialogs.showListSelection(
                                RemakeActivity.this,
                                getString(R.string.title_select_sort),
                                FilterUtils.getSorters(RemakeActivity.this),
                                new CustomDialogs.OnItemSelected() {
                                    @Override
                                    public void onItemSelected(int which) {
                                        sorter = FilterUtils.getSorter(which);
                                        sendBroadcast(FilterBroadcastReceiver.getIntent());
                                    }
                                });
                        break;
                    case 4:
                        CustomDialogs.showListSelection(
                                RemakeActivity.this,
                                getString(R.string.title_select_filter),
                                FilterUtils.getFilters(RemakeActivity.this),
                                new CustomDialogs.OnItemSelected() {
                                    @Override
                                    public void onItemSelected(int which) {
                                        // TODO: 2017-07-14 set filter for adapter
                                        switch (which) {
                                            case 0:
                                                filter = null;
                                                sendBroadcast(FilterBroadcastReceiver.getIntent());
                                                break;
                                            case 1:
                                                final List<String> countryList = Database.getCountryList();
                                                CustomDialogs.showListSelection(
                                                        RemakeActivity.this,
                                                        getString(R.string.title_select_country),
                                                        countryList.toArray(new String[0]),
                                                        new CustomDialogs.OnItemSelected() {
                                                            @Override
                                                            public void onItemSelected(int which) {
                                                                filter = new CountryFilter(countryList.get(which));
                                                                sendBroadcast(FilterBroadcastReceiver.getIntent());
                                                            }
                                                        });
                                                break;
                                        }
                                    }
                                });
                        break;
                    case 5:
                        openSearch();
                        break;
                    case 6:
                        openRecents();
                        break;
                    case 7:
                        openTimeshift(channel);
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (inputShowsUI) {
            showUI();
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
                //noinspection StatementWithEmptyBody
                while (getSupportFragmentManager().popBackStackImmediate()) {
                }
                return true;
            case KeyEvent.KEYCODE_INFO:
                channelButtonsFragment.requestFocus();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void playChannel(final Channel channel) {
        showUI();

        iconHD.setVisibility(View.GONE);
        currentChannel = channel;
        Analytics.logChannel(mFirebaseAnalytics, channel);
        channelButtonsFragment.setChannel(channel);

        if (currentChannel != null) {
            statisticsManager.onChannelStopped(currentChannel);
        }
        String url = networkManager.getStreamUrl(channel.getStreamName());
        mainThreadHandler.removeCallbacks(changeChannelRunnable);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "playChannel: " + channel.getStreamName() + " - " + url);
        }
        changeChannelRunnable.setUrl(url);
        mainThreadHandler.postDelayed(changeChannelRunnable, 1000);
        channelInfoFragment.channelStarted(currentChannel);
        if (!channel.getCategory().isAuditing()) {
            statisticsManager.onChannelStarted(channel);
        }
    }

    @Override
    public void showChannelInfo(int number, String name, int color) {
        selectedChannelNumber.setTextColor(color);
        selectedChannelNumber.setText(String.valueOf(number));
        selectedChannelName.setText(name);
        selectedChannelRoot.setVisibility(VISIBLE);
        mainThreadHandler.removeCallbacks(hideSelectedChannel);
        mainThreadHandler.postDelayed(hideSelectedChannel, 2000);
    }

    @Override
    public ChannelSorter getSorter() {
        return sorter;
    }

    @Override
    public ChannelFilter getFilter() {
        return filter;
    }

    @Override
    public Channel getCurrentChannel() {
        return currentChannel;
    }

    @Override
    public void onInteraction() {
        showUI();
    }

    @Override
    public void openAllChannels() {
        changeSideFragment(new RemakeAllChannelsFragment(), "all_channels");
    }

    @Override
    public void setInterfaceActive(boolean active) {
        inputShowsUI = active;
        if (active) {
            showUI();
        } else {
            hideUI();
        }
    }

    @Override
    public void openTimeshift(Channel channel, long seekTo) {
        startActivity(QHDPlayerActivity.getTimeshiftIntent(this,
                channel.getStreamName(),
                networkManager.getStreamUrl(channel.getStreamName(), "index-" + channel.getAvailableTimeshift() + "-now.m3u8"),
                channel.getAvailableTimeshift(),
                seekTo));
    }

    private void showUI() {
        uiRoot.setVisibility(VISIBLE);
        mainThreadHandler.removeCallbacks(hideRootRunnable);
        mainThreadHandler.postDelayed(hideRootRunnable, 10000);
    }

    private void hideUI() {
        mainThreadHandler.removeCallbacks(hideRootRunnable);
        mainThreadHandler.post(hideRootRunnable);
    }

    private void preparePlayer() {
        if (player == null) {
            player = getPlayer();
            player.prepare();
            player.setSurface(surfaceView);
            player.setPlayWhenReady(true);
            player.setListener(this);
            player.setBandwidthEstimateListener(new BandwidthEstimateListener() {
                @Override
                public void onBandwidthEstimate(long bitrate) {
                    Log.i(TAG, "onBandwidthEstimate: " + bitrate + "mbps");
                }
            });
            player.setOnTrackChangedListener(channelButtonsFragment);
        }
    }

    @NonNull
    private JTVPlayer getPlayer() {
        switch (playerId) {
            case JTVPlayer.PLAYER_NATIVE:
                return new JTVNativePlayer(this, BuildConfig.DEBUG);
            case JTVPlayer.PLAYER_EXO:
                return new JTVExoPlayer(this, mainThreadHandler, BuildConfig.DEBUG);
            default:
                Log.e("JTVQHDPlayer", "Player with id=" + playerId + " doesnt exist. Loading native player.");
                return new JTVNativePlayer(this, BuildConfig.DEBUG);
        }
    }

    @Override
    public void onStateChanged(int state) {
        Log.d(TAG, "Player state: " + state);
        mainThreadHandler.removeCallbacks(restartChannelRunnable);
        switch (state) {
            case JTVPlayer.STATE_BUFFERING:
                mainThreadHandler.postDelayed(restartChannelRunnable, 10000);
                break;
            case JTVPlayer.STATE_ENDED:
                Log.e(TAG, "Stream ended.");
                if (currentChannel != null) {
                    if (errorCount < ERROR_COUNT_MAX) {
                        if (player instanceof JTVNativePlayer) {
                            mainThreadHandler.postDelayed(restartChannelRunnable, 2000);
                        } else {
                            playChannel(currentChannel);
                        }
                    }
                }
                channelInfoFragment.channelStopped();
                break;
            case JTVPlayer.STATE_IDLE:
                break;
            case JTVPlayer.STATE_READY:
                Log.d(TAG, "Player ready.");
                errorCount = 0;
                break;
        }
    }

    @Override
    public void onError(Exception exception) {
        statisticsManager.onChannelError(currentChannel);
        String selectedServer = networkManager.getSelectedServer();
        String code = null;
        if (exception instanceof HttpException) {
            HttpException httpException = (HttpException) exception;
            code = String.valueOf(httpException.getStatus());
            if (httpException.getStatus() == HttpException.STATUS_FORBIDDEN) {
                networkManager.setTokenListener(this);
                networkManager.login();
            }
        } else { //other
            if (errorCount < ERROR_COUNT_MAX) {
                mainThreadHandler.postDelayed(restartChannelRunnable, 2000);
            }
        }
        Analytics.logChannelError(mFirebaseAnalytics, currentChannel, selectedServer, code);
        errorCount++;
        Log.e(TAG, "Player rrror counter: " + errorCount);
    }

    @Override
    public void onVideoSizeChanged(int width, int height, float pixelWidthHeightRatio) {
        Log.d(TAG, "Video size changed : " + width + " " + height);
        if (aspectRatio == 0) {
            Log.d(TAG, "Aspect ratio set to auto. Changing aspect ratio of layout");
            aspectRatioFrameLayout.setAspectRatio(((float) width) / ((float) height));
        }
        if (width * height >= 921600) {
            Log.d(TAG, "onVideoSizeChanged: channel is HD");
            iconHD.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onTokenChange(String token) {
        if (currentChannel != null) {
            String url = networkManager.getStreamUrl(currentChannel.getStreamName());
            mainThreadHandler.removeCallbacks(changeChannelRunnable);
            changeChannelRunnable.setUrl(url);
            mainThreadHandler.postDelayed(changeChannelRunnable, 1000);
        }
    }


    @Override
    public void onFavoritePressed(Channel channel) {
        if (channel != null) {
            channel.setFavorite(!channel.isFavorite());
            channel.save();
            channelButtonsFragment.setFavorited(channel.isFavorite());
            if (channel.isFavorite()) {
                networkManager.addFavorite(channel.getRemoteId());
            } else {
                networkManager.removeFavorite(channel.getRemoteId());
            }
            sendBroadcast(FavoriteBroadcastReceiver.getIntent(channel));
        }
    }

    @Override
    public void onTimeshiftPressed(Channel channel) {
        if (channel != null && channel.getAvailableTimeshift() >= 0) {
            Intent simpleInstance = QHDPlayerActivity.getTimeshiftIntent(this,
                    channel.getStreamName(),
                    networkManager.getStreamUrl(channel.getStreamName(), "index-" + channel.getAvailableTimeshift() + "-now.m3u8"),
                    channel.getAvailableTimeshift());
            openActivity(simpleInstance, R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    public void onEpgPressed(Channel channel) {
        if (channel != null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_root, RemakeEPGFragment.newInstance(channel.getRemoteId(), channel.getAvailableTimeshift()))
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void onCast() {
        startActivity(TubiControllerActivity.castLiveTv(this, currentChannel));
    }


    @Override
    public void onFocusLost() {
        sideFragment.requestFocus();
    }

    @Override
    public void onTrackSelectPressed() {
        if (player != null) {
            final List<Track> tracksInfo = player.getTracksInfo();
            if (tracksInfo.size() <= 1) {
                Toast.makeText(this, "No tracks to select.", Toast.LENGTH_SHORT).show();
                return;
            }
            String[] names = new String[tracksInfo.size()];
            for (int i = 0; i < tracksInfo.size(); i++) {
                names[i] = tracksInfo.get(i).getName();
            }
            CustomDialogs.showListSelection(this, "Tracks", names, new CustomDialogs.OnItemSelected() {
                @Override
                public void onItemSelected(int which) {
                    player.selectTrack(tracksInfo.get(which));
                }
            });
        }
    }


    private class ChangeChannelRunnable implements Runnable {

        private String url;

        private void setUrl(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            preparePlayer();
            player.play(url);
        }
    }

}
