package net.qhd.android.remake;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelAdapter;

public class RemakeMenuFragment extends SideListFragment {

    public static final int[] ITEM_NAME = {
            R.string.live_tv,
            R.string.vod,
            R.string.apps,
            R.string.config_title,
            R.string.settings,
            R.string.search,
            R.string.most_watched,
            R.string.recents,
            R.string.title_favorites,
            R.string.category_all
    };
    public static final int[] ITEM_ICONS = {
            R.drawable.ic_live_tv,
            R.drawable.ic_movie,
            R.drawable.ic_apps,
            R.drawable.ic_settings_applications,
            R.drawable.ic_settings_small,
            R.drawable.ic_search,
            R.drawable.ic_most_watched,
            R.drawable.ic_recent,
            R.drawable.ic_favorite_menu,
            R.drawable.ic_live_tv
    };

    @BindView(R.id.wheel) WheelView wheel;
    @BindView(R.id.title) TextView title;

    private int lastItem = -1;

    public RemakeMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_side_menu, container, false);
        if (BuildConfig.DEBUG) {
            if (ITEM_NAME.length != ITEM_ICONS.length) {
                Log.e("WheelMenu", "onCreateView: ITEM NAME and ITEM ICONS lenght doesnt match", new Exception("ITEM NAME and ITEM ICONS lenght doesnt match"));
            }
        }
        ButterKnife.bind(this, root);

        if (BuildConfig.DEBUG) {
            title.setText(BuildConfig.VERSION_NAME);
        }

        wheel.setDrawShadows(WheelView.SHADOWS_NONE);
        wheel.setCyclic(true);
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 96, r.getDisplayMetrics());
        wheel.setItemOffset((int) px);

        wheel.setViewAdapter(new AbstractWheelAdapter() {
            @Override
            public int getItemsCount() {
                return ITEM_NAME.length;
            }

            @Override
            public View getItem(int index, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater(null).inflate(R.layout.layout_menu_item, parent, false);
                }
                TextView title = convertView.findViewById(R.id.item_title);
                ImageView icon = convertView.findViewById(R.id.item_icon);
                icon.setImageResource(ITEM_ICONS[index]);
                title.setText(getString(ITEM_NAME[index]));
                return convertView;
            }

        });
        wheel.setFocusable(true);
        wheel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                getContract().onInteraction();
                int currentItem = wheel.getCurrentItem();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            moveToActivity(wheel.getCurrentItem());
                            return true;
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            moveToActivity(wheel.getCurrentItem());
                            return true;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            wheel.setCurrentItem(currentItem - 1, true);
                            return true;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            wheel.setCurrentItem(currentItem + 1, true);
                            return true;
                    }
                }
                return false;
            }
        });
        wheel.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(WheelView wheel, int itemIndex) {
                getContract().onInteraction();
                while (itemIndex < 0) {
                    itemIndex += ITEM_NAME.length;
                }
                Log.d("WheelMenu", "onItemClicked: " + getString(ITEM_NAME[itemIndex % ITEM_NAME.length]));
                moveToActivity(itemIndex % ITEM_NAME.length);
            }
        });
        wheel.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                getContract().onInteraction();
            }
        });
        wheel.requestFocus();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (lastItem != -1) {
            wheel.setCurrentItem(lastItem);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        lastItem = wheel.getCurrentItem();
    }

    private void moveToActivity(int item) {
        RemakeContract contract = getContract();
        switch (item) {
            case 0:
                contract.openLiveTV();
                return;
            case 1:
                contract.openVOD();
                return;
            case 2:
                contract.openApps();
                return;
            case 3:
                contract.openConfig();
                return;
            case 4:
                contract.openSettings();
                return;
            case 5:
                contract.openSearch();
                return;
            case 6:
                contract.openMostWatched();
                return;
            case 7:
                contract.openRecents();
                return;
            case 8:
                contract.openFavorites();
                return;
            case 9:
                contract.openAllChannels();
        }
    }

}
