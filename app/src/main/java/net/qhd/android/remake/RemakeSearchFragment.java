package net.qhd.android.remake;

import android.os.Bundle;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;

public class RemakeSearchFragment extends RemakeChannelsFragment {

    public static final String ARG_QUERY = "searchQuery";

    public static RemakeSearchFragment instance(String query) {
        RemakeSearchFragment fragment = new RemakeSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Category getCategory(Bundle arguments) {
        String query = arguments.getString(ARG_QUERY, "").toLowerCase();
        Category category = new Category();
        category.setName(query);
        category.setChannels(Database.searchChannels(query));
        return category;
    }


    @Override
    protected Channel getFirstChannel() {
        return null;
    }


}
