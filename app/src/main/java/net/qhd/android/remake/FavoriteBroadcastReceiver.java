package net.qhd.android.remake;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.jtv.android.models.Channel;
import com.jtv.player.BuildConfig;

public abstract class FavoriteBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_FAVORITE = BuildConfig.APPLICATION_ID + ".broadcast.FAVORITE";

    public static final String EXTRA_ID = "channel";
    public static final String EXTRA_FAVORITE = "favorite";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            int remoteId = extras.getInt(EXTRA_ID);
            boolean favorited = extras.getBoolean(EXTRA_FAVORITE);
            onFavoriteChanged(remoteId, favorited);
        }
    }

    public abstract void onFavoriteChanged(int remoteId, boolean favorited);

    public static Intent getIntent(Channel channel) {
        return new Intent(ACTION_FAVORITE)
                .putExtra(EXTRA_ID, channel.getRemoteId())
                .putExtra(EXTRA_FAVORITE, channel.isFavorite());
    }
}
