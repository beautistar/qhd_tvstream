package net.qhd.android.remake;

import android.os.Bundle;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;

import net.qhd.android.R;

public class RemakeMostWatchedFragment extends RemakeChannelsFragment {

    public static final int COUNT = 30;

    @Override
    protected Category getCategory(Bundle arguments) {
        Category category = new Category();
        category.setName(getString(R.string.most_watched));
        category.setChannels(Database.getMostWatched(COUNT));
        return category;
    }


    @Override
    protected Channel getFirstChannel() {
        return null;
    }
}
