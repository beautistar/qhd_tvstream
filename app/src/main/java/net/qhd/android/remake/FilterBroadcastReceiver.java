package net.qhd.android.remake;

import android.content.BroadcastReceiver;
import android.content.Intent;

import com.jtv.player.BuildConfig;

public abstract class FilterBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_FILTER = BuildConfig.APPLICATION_ID + ".broadcast.FILTER";

    public static Intent getIntent() {
        return new Intent(ACTION_FILTER);
    }

}
