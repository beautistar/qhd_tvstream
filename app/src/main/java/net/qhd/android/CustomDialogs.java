package net.qhd.android;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jtv.android.utils.SettingsManager;

import net.qhd.android.activities.SettingsActivity;

public class CustomDialogs {

    public static void showSearchInput(final Context context, final OnSearchQueryListener listener) {
        final AlertDialog.Builder dialogBuilter = new AlertDialog.Builder(context);
        dialogBuilter.setTitle(R.string.search);
        dialogBuilter.setMessage(R.string.search_by_name);

        final EditText input = new EditText(context);
        input.setSingleLine();
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setImeActionLabel(context.getString(R.string.search), KeyEvent.KEYCODE_ENTER);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                return false;
            }
        });
        dialogBuilter.setView(input);
        dialogBuilter.setIcon(R.drawable.ic_search);

        dialogBuilter.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String query = input.getText().toString();
                        listener.onSearchQuery(query);
                    }
                });
        dialogBuilter.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alertDialog = dialogBuilter.create();

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                    return true;
                }
                return false;
            }
        });
        alertDialog.show();
    }

    public static void showPasswordInput(final Context context, final SettingsManager settingsManager, final OnPasswordAcceptedListener listener) {
        final AlertDialog.Builder dialogBuilter = new AlertDialog.Builder(context);
        dialogBuilter.setTitle(R.string.title_protected);
        dialogBuilter.setMessage(R.string.message_enter_password);

        final EditText input = new EditText(context);
        input.setSingleLine();
        input.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
//        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        input.setImeActionLabel(context.getString(R.string.unluck), KeyEvent.KEYCODE_ENTER);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                return false;
            }
        });
        dialogBuilter.setView(input);
        dialogBuilter.setIcon(R.drawable.ic_lock);

        dialogBuilter.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String password = SettingsManager.SHA1(input.getText().toString());
                        String savedPassword = settingsManager.getPassword();
                        if (savedPassword != null) {
                            if (savedPassword.equals(password)) {
                                if (listener != null) {
                                    listener.onPasswordAccepted();
                                }
                            } else {
                                Toast.makeText(context,
                                        R.string.error_bad_password, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.error_no_password_set, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        dialogBuilter.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alertDialog = dialogBuilter.create();

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                    return true;
                }
                return false;
            }
        });
        alertDialog.show();
    }

    public static AlertDialog showCodeInput(final Context context, final OnCodeSubmitted listener) {
        final AlertDialog.Builder dialogBuilter = new AlertDialog.Builder(context);
        dialogBuilter.setTitle(R.string.phone_title);
        dialogBuilter.setMessage(R.string.phone_message);

        final EditText input = new EditText(context);
        input.setSingleLine();
        input.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        input.setImeActionLabel(context.getString(R.string.button_submit), KeyEvent.KEYCODE_ENTER);
        dialogBuilter.setView(input);

        dialogBuilter.setPositiveButton(R.string.button_submit,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onCodeSubmitted(input.getText().toString().trim());
                    }
                });
        dialogBuilter.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                listener.onCancel();
            }
        });

        final AlertDialog alertDialog = dialogBuilter.create();

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                    return true;
                }
                return false;
            }
        });
        alertDialog.show();
        return alertDialog;
    }

    public static void showNoPackageDialog(final Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.no_package)
                .setMessage(R.string.no_package_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(SettingsActivity.getIntent(context, 5));
                    }
                }).setNegativeButton(R.string.no, null)
                .show();
    }

    public static void showListSelection(final Context context, String title, String[] items, final OnItemSelected listener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setAdapter(new ArrayAdapter<>(context,
                                android.R.layout.simple_list_item_1,
                                items)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (listener != null) {
                                    listener.onItemSelected(which);
                                }
                            }
                        })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    public interface OnPasswordAcceptedListener {
        void onPasswordAccepted();
    }

    public interface OnSearchQueryListener {
        void onSearchQuery(String query);
    }

    public interface OnItemSelected {
        void onItemSelected(int which);
    }

    public interface OnCodeSubmitted {
        void onCodeSubmitted(String code);
        void onCancel();
    }

}
