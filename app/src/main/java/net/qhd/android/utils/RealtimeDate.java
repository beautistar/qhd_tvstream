package net.qhd.android.utils;

import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class RealtimeDate {

    private SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm", Locale.US);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

    private final TextView hourView;
    private final TextView dateView;

    private Handler handler = new Handler();
    private Timer timer = new Timer();
    private TimerTask task;

    public RealtimeDate(TextView hourView, TextView dateView) {
        this.hourView = hourView;
        this.dateView = dateView;

    }

    public void stop() {
        task.cancel();
        task = null;
    }

    public void start() {
        task = new TimerUpdateTask();
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    public void setHourFormat(SimpleDateFormat format) {
        this.hourFormat = format;
    }

    public void setDateFormat(SimpleDateFormat format) {
        this.dateFormat = format;
    }

    private class TimerUpdateTask extends TimerTask {
        @Override
        public void run() {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                handler.post(this);
                return;
            }
            Date time = Calendar.getInstance().getTime();
            if (hourView != null) {
                hourView.setText(hourFormat.format(time));
            }
            if (dateView != null) {
                dateView.setText(dateFormat.format(time));
            }
        }
    }
}
