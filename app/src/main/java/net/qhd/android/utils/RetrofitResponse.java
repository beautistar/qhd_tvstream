package net.qhd.android.utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitResponse<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful() && response.body() != null) {
            onResult(response.body());
        } else {
            onFail(response.body(), response.code());
        }
    }

    /**
     * Result if response code in range of 2XX and body is not null
     *
     * @param body parsed object
     */
    public abstract void onResult(T body);

    /**
     * Response code not in range of 2XX
     *
     * @param body response body
     * @param code response code
     */
    public abstract void onFail(T body, int code);
}
