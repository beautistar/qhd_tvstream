package net.qhd.android.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.Task;

import net.qhd.android.R;

public class GoogleUtils {

    public static final int GOOGLE_SERVICES_RESOLUTION_REQUEST = 199;

    public static void checkAvailabillity(final Activity activity, final Result listener) {
        final GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
        int result = googleApi.isGooglePlayServicesAvailable(activity);
        if (result == ConnectionResult.SUCCESS) {
            listener.onComplete();
        } else {
            if (googleApi.isUserResolvableError(result)) {
                googleApi.showErrorDialogFragment(activity, result, GOOGLE_SERVICES_RESOLUTION_REQUEST,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                listener.onError();
                            }
                        });
            }else{
                new AlertDialog.Builder(activity, R.style.QHDAlertTheme)
                        .setTitle("Do you want to install GoogleUtils services?")
                        .setMessage("You need to have GoogleUtils services installed to use our application.")
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Task<Void> task = googleApi.makeGooglePlayServicesAvailable(activity);
                                task.addOnCompleteListener(new com.google.android.gms.tasks.OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isComplete() && task.isSuccessful()) {
                                            listener.onComplete();
                                        } else {
                                            Toast.makeText(activity, "Services failed to install", Toast.LENGTH_SHORT).show();
                                            listener.onError();
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                listener.onError();
                            }
                        }).create().show();
            }
        }
    }

    public interface Result {
        void onComplete();

        void onError();
    }

}
