package net.qhd.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {

    public static final Pattern CODE_PATTERN = Pattern.compile("\\AHD-\\d{6}");

    private OnCodeReceivedListener listener;

    public SmsReceiver() {
    }

    public SmsReceiver(OnCodeReceivedListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("SmsReceiver", "onReceive");
        if (intent.getAction() != null && intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();//---get the SMS message passed in---
            if (bundle != null) {
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    parseMessages(pdus);
                } catch (Exception ignored) {
                }
            }
        }
    }

    private void parseMessages(Object[] pdus) {
        if (pdus != null) {
            for (Object pdu : pdus) {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu);
                String msgFrom = msg.getOriginatingAddress();
                String msgBody = msg.getMessageBody();
                Matcher matcher = CODE_PATTERN.matcher(msgBody);
                Log.d("SmsReceiver", "onReceive: " + msgFrom + ": " + msgBody);
                if (matcher.find() && listener != null) {
                    Log.d("SmsReceiver", "Matched pattern.");
                    listener.onCodeReceived(matcher.group(1));
                }
            }
        }
    }

    public interface OnCodeReceivedListener {
        void onCodeReceived(String code);
    }

}
