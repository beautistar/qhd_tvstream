package net.qhd.android.utils;

import android.os.Handler;
import android.view.KeyEvent;

public class ChangeChannelByNumberManager {

    private int number = 0;
    private int max = 0;
    private OnChannelSelected listener;

    private Handler handler = new Handler();
    private Runnable runnable = new DelayedAction();

    public ChangeChannelByNumberManager(int max) {
        this.max = max;
    }

    public boolean handleKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        int action = event.getAction();
        if (action == KeyEvent.ACTION_DOWN) {
            if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
                return onNumberPressed(keyCode - KeyEvent.KEYCODE_0);
            }
            if (keyCode >= KeyEvent.KEYCODE_NUMPAD_0 && keyCode <= KeyEvent.KEYCODE_NUMPAD_9) {
                return onNumberPressed(keyCode - KeyEvent.KEYCODE_NUMPAD_0);
            }
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                return onNumberDeleted();
            }
            if ((keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) && number > 0) {
                handler.removeCallbacks(runnable);
                if (listener != null) {
                    listener.onChannelSelected(number - 1);
                    number = 0;
                }
                return true;
            }
        }
        return false;
    }

    public int getNumber() {
        return number;
    }

    public void setMax(int max) {
        this.max = max;
    }

    private boolean onNumberPressed(int number) {
        handler.removeCallbacks(runnable);
        int temp = this.number;
        temp *= 10;
        temp += number;
        if (temp > 0 && temp <= max) {
            handler.postDelayed(runnable, 2000);
            this.number = temp;
            if (listener != null) {
                listener.onNumberChanged(this.number);
            }
            return true;
        }
        return false;
    }

    private boolean onNumberDeleted() {
        handler.removeCallbacks(runnable);
        if (number > 0) {
            handler.postDelayed(runnable, 2000);
            number /= 10;
            if (listener != null) {
                listener.onNumberChanged(this.number);
            }
            return true;
        }
        return false;
    }


    public ChangeChannelByNumberManager setOnChannelSelectedListener(OnChannelSelected listener) {
        this.listener = listener;
        return this;
    }

    private class DelayedAction implements Runnable {

        @Override
        public void run() {
            if (listener != null && number > 0) {
                listener.onChannelSelected(number - 1);
                number = 0;
            }
        }
    }

    public interface OnChannelSelected {

        void onChannelSelected(int position);

        void onNumberChanged(int number);

    }

}
