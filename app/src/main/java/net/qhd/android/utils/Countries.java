package net.qhd.android.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Countries {

    private static final String TAG = Countries.class.getSimpleName();

    public static List<Country> getCountryList(Context context) {
        List<Country> countryList = new ArrayList<>();
        AssetManager assetManager = context.getAssets();

        try {
            InputStream csvStream = assetManager.open("country.csv");

            Scanner scanner = new Scanner(csvStream);
            scanner.useDelimiter(",|\\r\\n");

            while (scanner.hasNext()) {
                double lattitude = 0;
                double longitude = 0;
                String code = scanner.next();
                if (code.isEmpty()) {
                    break;
                }
                if (scanner.hasNextDouble()) {
                    lattitude = scanner.nextDouble();
                } else {
                    scanner.next();
                }
                if (scanner.hasNextDouble()) {
                    longitude = scanner.nextDouble();
                } else {
                    scanner.next();
                }
                String name = scanner.next();

                countryList.add(new Country(code, name, lattitude, longitude));
            }
            scanner.close();
            csvStream.close();

        } catch (Exception e) {
            Log.e(TAG, "getCountryList: Last read country: " + countryList.get(countryList.size() - 1).toString());
            Log.e(TAG, "getCountryList: failed reading csv", e);
//            e.printStackTrace();
        }
        return countryList;
    }


    public static class Country {

        private String code;
        private String name;

        private double lattitude;
        private double longitude;

        public Country(String code, String name, double lattitude, double longitude) {
            this.code = code;
            this.name = name;
            this.lattitude = lattitude;
            this.longitude = longitude;
        }


        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getLattitude() {
            return lattitude;
        }

        public void setLattitude(double lattitude) {
            this.lattitude = lattitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "code='" + code + '\'' +
                    ", name='" + name + '\'' +
                    ", lattitude=" + lattitude +
                    ", longitude=" + longitude +
                    '}';
        }
    }

}
