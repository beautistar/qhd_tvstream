package net.qhd.android.utils;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.Channel;
import com.jtv.android.models.Movie;

public class Analytics {

    private static final String EVENT_CHANNEL_ERROR = "channel_error";

    public static void logChannelError(FirebaseAnalytics analytics, Channel channel, String selectedServer, String code) {
        if (channel == null) return;
        Bundle bundle = new Bundle();
        bundle.putString("channel_id", String.valueOf(channel.getRemoteId()));
        if (selectedServer != null) {
            bundle.putString("server", selectedServer);
        }
        if (code != null) {
            bundle.putString("error_code", code);
        }
        analytics.logEvent(EVENT_CHANNEL_ERROR, bundle);
    }

    public static void logChannel(FirebaseAnalytics analytics, Channel channel) {
        if (channel == null) return;
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(channel.getRemoteId()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, channel.getName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "channel");
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logMovie(FirebaseAnalytics analytics, Movie movie) {
        if (movie == null) return;
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(movie.getImdbID()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, movie.getTitle());
        bundle.putString(FirebaseAnalytics.Param.QUANTITY, "Tickets: " + String.valueOf(movie.getTickets()));
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "movie");
        analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logWatchMovie(FirebaseAnalytics mFirebaseAnalytics, String imdbid, boolean isTrailer) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, imdbid);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, isTrailer ? "watchtrailer" : "watchmovie");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logTransaction(FirebaseAnalytics analytics, String paymentId, String price, String currency) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.TRANSACTION_ID, paymentId);
        try {
            bundle.putDouble(FirebaseAnalytics.Param.PRICE, Double.parseDouble(price));
            bundle.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        } catch (Exception ignored) {
        }
        analytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);
    }
}
