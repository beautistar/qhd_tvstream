package net.qhd.android.utils;

import android.content.Context;

import com.jtv.android.models.Category;
import com.jtv.android.utils.Database;

import net.qhd.android.R;

public class CustomCategories {

    public static final int CATEGORY_FAVORITE_ID = -1;
    public static final int CATEGORY_ALL_CHANNELS_ID = -2;

    public static Category getFavoriteCategory(Context context) {
        Category category = new Category();
        category.setRemoteId(CATEGORY_FAVORITE_ID);
        category.setName(context.getString(R.string.title_favorites));
        category.setImageURL("");
        category.setChannels(Database.getFavorites());
        return category;
    }

    public static Category getAllChannelsCategory(Context context) {
        Category category = new Category();
        category.setRemoteId(CATEGORY_ALL_CHANNELS_ID);
        category.setName(context.getString(R.string.category_all));
        category.setImageURL("");
        category.setChannels(Database.getChannels());
        return category;
    }

}
