package net.qhd.android;

import android.content.Context;

import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;

public class DatabaseContentProvider extends ContentProvider {

    public static final Class[] MODELS = new Class[]{com.jtv.android.models.Server.class,
            com.jtv.android.models.Category.class,
            com.jtv.android.models.Channel.class,
            com.jtv.android.models.ChannelStats.class,
            com.jtv.android.models.Notification.class,
            com.jtv.android.models.Reminder.class};

    public static final Class[] SERIALIZERS = new Class[]{
            com.jtv.android.utils.MapSerializer.class,
            com.jtv.android.utils.UriSerializer.class
    };

    public static Configuration getConfigurations(Context context) {
        Configuration.Builder builder = new Configuration.Builder(context);
        builder.setDatabaseVersion(BuildConfig.VERSION_CODE);
        builder.setTypeSerializers(SERIALIZERS);
        builder.addModelClasses(MODELS);
        return builder.create();
    }

    @Override
    protected Configuration getConfiguration() {
        return getConfigurations(getContext());
    }

}
