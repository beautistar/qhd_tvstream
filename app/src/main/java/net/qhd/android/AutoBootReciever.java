package net.qhd.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jtv.android.utils.SettingsManager;

import net.qhd.android.activities.main.InitActivity;

/**
 * Receives broadcast after device has finished booting up
 */
public class AutoBootReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            SettingsManager settingsManager = new SettingsManager(context);
            if (settingsManager.getAutoBoot()) {
                Intent startActivityIntent = new Intent(context, InitActivity.class);
                startActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(startActivityIntent);
            }
        }
    }
}
