package net.qhd.android.fragments.main.login;

public interface RegistrationCallback {

    void onRegistered(String username, String phone, String email);

}
