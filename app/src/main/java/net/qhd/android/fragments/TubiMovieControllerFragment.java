package net.qhd.android.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.R;
import net.qhd.android.activities.TubiControllerActivity;
import net.qhd.android.listeners.TubiCastContract;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TubiMovieControllerFragment extends Fragment {

    private static final String ARG_STREAM_URL = "stream_url";
    private static final String ARG_STREAM_NAME = "stream_name";

    private TubiCastContract listener;

    private String streamUrl;
    private String streamName;

    private int duration = 0;
    private int position = 0;

    Runnable seekbarUpdateRunnable = new Runnable()

    {

        @Override
        public void run() {
            listener.getDuration(new TubiControllerActivity.ControllerResult<Long>() {
                @Override
                public void result(Long result) {
                    duration = ((int) (long) result);
                }
            });
            listener.getPosition(new TubiControllerActivity.ControllerResult<Long>() {
                @Override
                public void result(Long result) {
                    position = ((int) (long) result);
                }
            });
            seekBar.setMax(duration);
            seekBar.setProgress(position);
            seekBar.postDelayed(this, 1000);
            seekBarDuration.setText(String.format(Locale.getDefault(),
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(seekBar.getMax()),
                    TimeUnit.MILLISECONDS.toSeconds(seekBar.getMax()) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekBar.getMax()))
            ));
            seekBarTime.setText(String.format(Locale.getDefault(),
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()),
                    TimeUnit.MILLISECONDS.toSeconds(seekBar.getProgress()) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()))));
        }
    };

    @BindView(R.id.seekbar) SeekBar seekBar;
    @BindView(R.id.play_button) ImageButton playButton;
    @BindView(R.id.seekbar_time) TextView seekBarTime;
    @BindView(R.id.seekbar_duration) TextView seekBarDuration;
    @BindView(R.id.cast_title) TextView castTitle;


    public TubiMovieControllerFragment() {
        // Required empty public constructor
    }

    public static TubiMovieControllerFragment newInstance(String streamUrl, String streamName) {
        TubiMovieControllerFragment fragment = new TubiMovieControllerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STREAM_URL, streamUrl);
        args.putString(ARG_STREAM_NAME, streamName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            streamUrl = getArguments().getString(ARG_STREAM_URL);
            streamName = getArguments().getString(ARG_STREAM_NAME);
        }
        MemoTVCastSDK.startSearchLiveDevice();
        listener.play(streamUrl, streamName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_tubi_movie_controller, container, false);
        ButterKnife.bind(this, root);
        castTitle.setText(streamName);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBarTime.setText(String.format(Locale.getDefault(),
                        "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()),
                        TimeUnit.MILLISECONDS.toSeconds(seekBar.getProgress()) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()))));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                listener.seek(seekBar.getProgress());
            }
        });
        return root;
    }

    @OnClick(R.id.play_button)
    public void onPlayClick() {
        if (listener.isPlaying()) {
            listener.pause();
            playButton.setImageResource(R.drawable.ic_play);
        } else {
            listener.resume();
            playButton.setImageResource(R.drawable.ic_pause);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        seekBar.post(seekbarUpdateRunnable);
    }

    @Override
    public void onPause() {
        super.onPause();
        seekBar.removeCallbacks(seekbarUpdateRunnable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TubiCastContract) {
            listener = (TubiCastContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChannelSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


}
