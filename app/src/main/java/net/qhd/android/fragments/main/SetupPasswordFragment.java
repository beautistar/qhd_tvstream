package net.qhd.android.fragments.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jtv.android.utils.SettingsManager;

import net.qhd.android.QHDApplication;
import net.qhd.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetupPasswordFragment extends SetupFragment {

    @BindView(R.id.edit_password) EditText editPassword;
    @BindView(R.id.edit_password2) EditText editPassword2;
    private SettingsManager settingsManager;

    public SetupPasswordFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setup_password, container, false);
        ButterKnife.bind(this, view);
        settingsManager = ((QHDApplication) getActivity().getApplication()).getSettingsManager();
        return view;
    }


    @OnClick(R.id.button_next)
    public void onClick() {
        if (editPassword.getText().length() < 4) {
            editPassword.setError(getString(R.string.validation_password_too_short));
            editPassword.requestFocus();
            return;
        }
        if (!editPassword.getText().toString().equals(editPassword2.getText().toString())) {
            editPassword2.setError(getString(R.string.validation_password_mismatch));
            editPassword2.requestFocus();
            return;
        }
        settingsManager.setPassword(editPassword.getText().toString());
        next();
    }
}
