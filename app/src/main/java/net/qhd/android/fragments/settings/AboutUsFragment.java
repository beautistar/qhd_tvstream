package net.qhd.android.fragments.settings;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.network.NetworkManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

public class AboutUsFragment extends Fragment {


    public AboutUsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about_us, container, false);
        ImageView agentLogo = root.findViewById(R.id.agent_logo);
        TextView versionNumber = root.findViewById(R.id.version_number);
        versionNumber.setText(getString(R.string.about_version_name) + BuildConfig.VERSION_NAME + "_" + BuildConfig.VERSION_CODE);
        NetworkManager networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        ImageLoader.getInstance().displayImage(
                networkManager.appendTokenToUrl(BuildConfig.BASE_URL + "/app/agent.php?logo"),
                agentLogo,
                new DisplayImageOptions.Builder().cacheInMemory(false).cacheOnDisk(false).build(),
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        view.setVisibility(View.VISIBLE);
                        view.setAlpha(0f);
                        view.animate()
                                .alpha(1f)
                                .setDuration(100)
                                .setInterpolator(new AccelerateDecelerateInterpolator())
                                .start();
                    }
                });

        return root;
    }

}
