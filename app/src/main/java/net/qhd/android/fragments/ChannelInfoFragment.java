package net.qhd.android.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.jtv.android.models.epg.EPGProgramme;
import com.jtv.android.network.NetworkManager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelInfoFragment extends Fragment implements Callback<EPGProgramme> {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
    public static final SimpleDateFormat DATE_HM = new SimpleDateFormat("HH:mm", Locale.getDefault());

    @BindView(R.id.programme_name) TextView name;
    @BindView(R.id.programme_start) TextView timeStart;
    @BindView(R.id.programme_stop) TextView timeStop;
    @BindView(R.id.programme_description) TextView description;
    @BindView(R.id.programme_time) ProgressBar programmeProgress;

    private Channel currentChannel;
    private EPGProgramme currentProgramme;

    private NetworkManager networkManager;

    private Timer timer;
    private Calendar stop = Calendar.getInstance();
    private Calendar start = Calendar.getInstance();

    public ChannelInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_channel_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new InfoUpdateTask(), 0, 60000);
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
    }

    public void channelStarted(Channel channel) {
        currentChannel = channel;
        networkManager.getCurrentProgramme(channel.getCategory().getRemoteId(), channel.getRemoteId(), this);
    }

    public void channelStopped() {
        resetInformation();
    }

    @Override
    public void onResponse(Call<EPGProgramme> call, Response<EPGProgramme> response) {
        if (!response.isSuccessful()) {
            updateInformation(new EPGProgramme());
            return;
        }
        currentProgramme = response.body();
        updateInformation(currentProgramme);
    }

    @Override
    public void onFailure(Call<EPGProgramme> call, Throwable t) {
        updateInformation(new EPGProgramme());
    }

    private void updateInformation(EPGProgramme programme) {
        if (programme != null) {
            if (programme.getTitle() == null) {
                resetInformation();
                name.setText(R.string.no_information);
                return;
            }
            name.setText(programme.getTitle());
            try {
                Calendar current = Calendar.getInstance();
                start.setTime(DATE_FORMAT.parse(programme.getStart()));
                stop.setTime(DATE_FORMAT.parse(programme.getStop()));
                timeStart.setText(DATE_HM.format(start.getTime()));
                timeStop.setText(DATE_HM.format(stop.getTime()));
                programmeProgress.setVisibility(View.VISIBLE);
                programmeProgress.setMax((int) (stop.getTimeInMillis() - start.getTimeInMillis()));
                programmeProgress.setProgress((int) (current.getTimeInMillis() - start.getTimeInMillis()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            description.setText(programme.getDescription());
        } else {
            resetInformation();
        }
    }

    private void resetInformation() {
        name.setText("");
        timeStop.setText("");
        timeStart.setText("");
        description.setText("");
        programmeProgress.setMax(100);
        programmeProgress.setProgress(0);
        programmeProgress.setVisibility(View.INVISIBLE);
    }

    private class InfoUpdateTask extends TimerTask {

        @Override
        public void run() {
            if (currentProgramme == null) {
                return;
            }
            Calendar currentTime = Calendar.getInstance();
            programmeProgress.setProgress((int) (currentTime.getTimeInMillis() - start.getTimeInMillis()));
            if (currentTime.compareTo(stop) > 0) {
                networkManager.getCurrentProgramme(currentChannel.getCategory().getRemoteId(), currentChannel.getRemoteId(), ChannelInfoFragment.this);
            }
        }
    }
}
