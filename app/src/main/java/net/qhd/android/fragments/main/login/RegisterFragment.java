package net.qhd.android.fragments.main.login;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.jtv.android.models.RegisterResponse;
import com.jtv.android.network.NetworkManager;

import net.qhd.android.BuildConfig;
import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.utils.Countries;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment implements Callback<RegisterResponse> {

    static final ButterKnife.Setter<View, Boolean> ENABLED = new ButterKnife.Setter<View, Boolean>() {
        @Override
        public void set(View view, Boolean value, int index) {
            view.setEnabled(value);
        }
    };

    private static final int REQUEST_ACCESS_REGISTRATION_PERMISSIONS = 258;
    public static final String TAG = "Location";
    public static final String VERIFICATION_ID = "verificationId";
    public static final String RESEND_TOKEN = "resendToken";
    private RegistrationCallback mListener;

    @BindView(R.id.edit_username) EditText editMail;
    @BindView(R.id.edit_truename) EditText editFullName;
    @BindView(R.id.edit_password) EditText editPassword;
    @BindView(R.id.edit_password2) EditText editPassword1;
    @BindView(R.id.edit_phone) EditText editPhone;

    @BindViews({R.id.edit_username,
            R.id.edit_truename,
            R.id.edit_password,
            R.id.edit_password2,
            R.id.edit_phone,
            R.id.country,
            R.id.button_register})
    List<View> viewsToDisable;

    @BindView(R.id.progress_bar) ProgressBar progressBar;

    @BindView(R.id.text_info) TextView textInfo;
    @BindView(R.id.country) Spinner country;

    @BindView(R.id.button_register) Button buttonRegister;

    private NetworkManager networkManager;
    private FirebaseAnalytics mFirebaseAnalytics;

    private List<Countries.Country> countryList;
    private CountryListAdapter countryAdapter;

    private PhoneNumberUtil phoneUtils;
    private String verificationId;
    private PhoneAuthProvider.ForceResendingToken forceResendingToken;
    private AlertDialog phoneVerificationDialog;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        countryList = Countries.getCountryList(getContext());
        phoneUtils = PhoneNumberUtil.getInstance();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, root);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();


        String[] array = new String[countryList.size() + 1];
        array[0] = getString(R.string.title_select_country);
        for (int i = 0; i < countryList.size(); i++) {
            array[i + 1] = countryList.get(i).getName();
        }
        countryAdapter = new CountryListAdapter(getContext(), array);
        country.setAdapter(countryAdapter);
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    FirebaseAuth.getInstance().signOut();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        getDeviceLocation();

        if (savedInstanceState != null) {
            verificationId = savedInstanceState.getString(VERIFICATION_ID, null);
            forceResendingToken = savedInstanceState.getParcelable(RESEND_TOKEN);
        }

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (verificationId != null) {
            if (phoneVerificationDialog != null && phoneVerificationDialog.isShowing()) {
                return;
            }
            showCodeInputDialog();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(VERIFICATION_ID, verificationId);
        outState.putParcelable(RESEND_TOKEN, forceResendingToken);
    }

    private void getDeviceLocation() {

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getDeviceLocationPermissions();
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location == null) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) return;
        }
        Log.d(TAG, "onSuccess: latitude=" + location.getLatitude() + "  longitude=" + location.getLongitude());
        Countries.Country closest = null;
        double closestDistance = Double.MAX_VALUE;
        int closestIndex = -1;
        for (int i = 0; i < countryList.size(); i++) {
            double thisCountryDistance = Math.abs(countryList.get(i).getLattitude() - location.getLatitude()) + Math.abs(countryList.get(i).getLongitude() - location.getLongitude());
            if (closest == null ||
                    thisCountryDistance < closestDistance) {
                closest = countryList.get(i);
                closestDistance = thisCountryDistance;
                closestIndex = i;
            }
        }
        if (closestIndex >= 0) {
            country.setSelection(closestIndex + 1);
            countryAdapter.notifyDataSetChanged();

        }
    }

    private void getDeviceLocationPermissions() {
        // Here, thisActivity is the current activity
        Log.d(TAG, "getDeviceLocationPermissions");
        if (dontHaveLocationPermissions() || dontHaveSmsPermission()) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
                    || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                    || shouldShowRequestPermissionRationale("android.permission.RECEIVE_SMS")) {
            } else {
                Log.d(TAG, "getDeviceLocationPermissions: requesting now");
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, "android.permission.RECEIVE_SMS"},
                        REQUEST_ACCESS_REGISTRATION_PERMISSIONS);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        editMail.requestFocus();
    }

    private boolean isValid() {
        String email = editMail.getText().toString();
        if (email.length() == 0) {//empty email
            editMail.setError(getString(R.string.error_email_empty));
            editMail.requestFocus();
            return false;
        }
        if (email.length() < 4) {//short email
            editMail.setError(getString(R.string.error_email_too_short));
            editMail.requestFocus();
            return false;
        }
        if (email.length() > 128) {//long email
            editMail.setError(getString(R.string.error_email_too_long));
            editMail.requestFocus();
            return false;
        }
        if (!(email.contains(".") && email.contains("@"))) {//invalid email
            editMail.setError(getString(R.string.error_email_format));
            editMail.requestFocus();
            return false;
        }
        if (email.indexOf('@') < 3 || email.indexOf('.') < 4) {//invalid email
            editMail.setError(getString(R.string.error_email_format));
            editMail.requestFocus();
            return false;
        }

        if (country.getSelectedItemPosition() <= 0) {
            TextView selectedView = (TextView) country.getSelectedView();
            selectedView.setError("Not selected");
            selectedView.setTextColor(Color.RED);
            return false;
        }

        try {
            Phonenumber.PhoneNumber parse = phoneUtils.parse(
                    editPhone.getText().toString(),
                    countryList.get(country.getSelectedItemPosition() - 1).getCode()
            );
            if (!phoneUtils.isValidNumber(parse)) {
                editPhone.setError("Number is invalid");
                editPhone.requestFocus();
                return false;
            }
        } catch (NumberParseException e) {
            return false;
        }

        String password = editPassword.getText().toString();
        if (password.length() < 5) {//password too short
            editPassword.setError(getString(R.string.validation_password_too_short));
            editPassword.requestFocus();
            return false;
        }
        String password2 = editPassword1.getText().toString();
        if (!password2.equals(password)) {//password doesnt match
            editPassword1.setError(getString(R.string.validation_password_mismatch));
            editPassword1.requestFocus();
            return false;
        }
        return true;
    }

    @OnClick(R.id.button_register)
    public void onClickRegister() {
        Log.d(TAG, "onClickRegister");
        if (isValid()) {
            progressBar.setVisibility(View.VISIBLE);
            Log.d(TAG, "onClickRegister: input is valid");
            ButterKnife.apply(viewsToDisable, ENABLED, false);
            Log.d(TAG, "onClickRegister: trying to verify phone number");
            if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                PhoneAuthProvider.getInstance().verifyPhoneNumber(editPhone.getText().toString(), 60, TimeUnit.SECONDS, getActivity(), new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        Log.d(TAG, "onVerificationCompleted: phone verified");
                        signInWithPhone(phoneAuthCredential);
                        if (phoneVerificationDialog != null) {
                            if (phoneVerificationDialog.isShowing()) {
                                phoneVerificationDialog.dismiss();
                                phoneVerificationDialog = null;
                            }
                        }
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        Log.d(TAG, "onVerificationFailed: phone failed to verify");
                        verificationId = null;
                        forceResendingToken = null;
                        ButterKnife.apply(viewsToDisable, ENABLED, true);
                        Toast.makeText(getActivity(), "Failed to verify", Toast.LENGTH_SHORT).show();
                        if (phoneVerificationDialog != null) {
                            if (phoneVerificationDialog.isShowing()) {
                                phoneVerificationDialog.dismiss();
                                phoneVerificationDialog = null;
                            }
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        Log.d(TAG, "onCodeSent: received verification Id: " + verificationId);
                        RegisterFragment.this.verificationId = verificationId;
                        RegisterFragment.this.forceResendingToken = forceResendingToken;
                        showCodeInputDialog();
                    }
                }, forceResendingToken);
            } else {
                Log.d(TAG, "onClickRegister: already phone verified, registering");
                networkManager.register(editMail.getText().toString(),
                        editPassword.getText().toString(),
                        editFullName.getText().toString(),
                        countryList.get(country.getSelectedItemPosition() - 1).getCode(),
                        editPhone.getText().toString(),
                        RegisterFragment.this);
            }


        }
    }

    private void showCodeInputDialog() {
        Log.d(TAG, "showCodeInputDialog");
        phoneVerificationDialog = CustomDialogs.showCodeInput(getActivity(), new CustomDialogs.OnCodeSubmitted() {
            @Override
            public void onCodeSubmitted(String code) {
                Log.d(TAG, "onCodeSubmitted: " + code);
                if (verificationId != null) {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
                    signInWithPhone(credential);
                } else {
                    CustomDialogs.showCodeInput(getActivity(), this);
                }
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel: phone verification input canceled");
                phoneVerificationDialog = null;
                ButterKnife.apply(viewsToDisable, ENABLED, true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void signInWithPhone(PhoneAuthCredential credential) {
        Log.d(TAG, "signInWithPhone");
        FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: successful");
                            networkManager.register(editMail.getText().toString(),
                                    editPassword.getText().toString(),
                                    editFullName.getText().toString(),
                                    countryList.get(country.getSelectedItemPosition() - 1).getCode(),
                                    editPhone.getText().toString(),
                                    RegisterFragment.this);
                        } else {
                            Log.d(TAG, "onComplete: failed to authenticate");
                            showCodeInputDialog();
                        }
                    }
                });
    }


    @Override
    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
        ButterKnife.apply(viewsToDisable, ENABLED, true);
        progressBar.setVisibility(View.GONE);

        if (response.isSuccessful()) {
            switch (response.body().code) {
                case 1: //OK
                    Log.d(TAG, "onResponse: sucessfull");
                    showAlert();
                    textInfo.setText(R.string.info_registered);
                    textInfo.setTextColor(0xFF00FF00);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, new Bundle());
                    FirebaseAuth.getInstance().signOut();
                    break;
                case -1: //Email address already in use
                    textInfo.setText(R.string.error_email_in_use);
                    textInfo.setTextColor(0xFFFF0000);
                    break;
                default:
                    textInfo.setText(response.body().error);
                    textInfo.setTextColor(0xFFFF0000);
                    break;
            }
        } else {
            if (BuildConfig.DEBUG) {
                textInfo.setText("Error: " + response.code());
                textInfo.setTextColor(0xFFFF0000);
            } else {
                textInfo.setText(R.string.error_unknown);
                textInfo.setTextColor(0xFFFF0000);
            }
        }
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.message_email_sent)
                .setTitle(R.string.title_registration_info)
                .setPositiveButton(R.string.ok, null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mListener.onRegistered(editMail.getText().toString(), editPhone.getText().toString(), editMail.getText().toString());
                    }
                });
        builder.show();
    }

    @Override
    public void onFailure(Call<RegisterResponse> call, Throwable t) {
        textInfo.setText(R.string.error_network_problems);
        textInfo.setTextColor(0xFFFF0000);
        ButterKnife.apply(viewsToDisable, ENABLED, true);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegistrationCallback) {
            mListener = (RegistrationCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginMethodSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: " + Arrays.toString(permissions) + "  " + Arrays.toString(grantResults));
        switch (requestCode) {
            case REQUEST_ACCESS_REGISTRATION_PERMISSIONS: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getDeviceLocation();
                }
                if (grantResults.length > 2 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult: sms permission granted");
                }
            }
        }
    }

    public boolean dontHaveLocationPermissions() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean dontHaveSmsPermission() {
        return ActivityCompat.checkSelfPermission(getActivity(), "android.permission.RECEIVE_SMS") != PackageManager.PERMISSION_GRANTED;
    }


    private class CountryListAdapter extends ArrayAdapter<String> {

        public static final int TEXT_ID = R.id.lang_name;

        public CountryListAdapter(Context context, String[] countries) {
            super(context, R.layout.layout_language, TEXT_ID, countries);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = view.findViewById(TEXT_ID);
            textView.setTextColor(position > 0 ? Color.WHITE : Color.GRAY);
            return view;
        }

        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            View view = super.getDropDownView(position, convertView, parent);
            view.setVisibility(position > 0 ? View.VISIBLE : View.GONE);
            return view;
        }

    }

}
