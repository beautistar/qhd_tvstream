package net.qhd.android.fragments.main.login;


public interface LoginCallback {

    void onLoginFailed();

    void onLoginStarted();

    void onLoggedIn();

}
