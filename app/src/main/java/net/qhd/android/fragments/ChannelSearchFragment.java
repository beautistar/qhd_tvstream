package net.qhd.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.adapters.ChannelGridAdapter;
import net.qhd.android.listeners.OnChannelSelectedListener;
import net.qhd.android.remake.RemakeEPGFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelSearchFragment extends Fragment implements SearchView.OnQueryTextListener, CompoundButton.OnCheckedChangeListener {

    public static final String ARG_CATEGORY_ID = "category_id";
    private OnChannelSelectedListener mListener;

    @BindView(R.id.search_result) GridView gridView;
    @BindView(R.id.search_view) SearchView searchView;
    @BindView(R.id.search_in_category) CheckBox searchInCategory;
    private List<Channel> channels;

    private long categoryId = -1;
    private InputMethodManager imm;
    private RemakeEPGFragment.EPGContract interfaceContract;

    public ChannelSearchFragment() {
        // Required empty public constructor
    }

    public static ChannelSearchFragment instance(Category category) {
        ChannelSearchFragment channelSearchFragment = new ChannelSearchFragment();
        Bundle bundle = new Bundle();
        if (category.getId() != null) {
            bundle.putLong(ARG_CATEGORY_ID, category.getId());
        }
        channelSearchFragment.setArguments(bundle);
        return channelSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getLong(ARG_CATEGORY_ID, -1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_channel_search, container, false);
        ButterKnife.bind(this, root);
        if (categoryId < 0) {
            searchInCategory.setVisibility(View.GONE);
        }
        searchInCategory.setOnCheckedChangeListener(this);
        searchView.setOnQueryTextListener(this);
        searchView.requestFocus();

        View closeButton = searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery("", false);
                if (mListener != null) {
                    mListener.onChannelSelected(null);
                }
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onChannelSelected(channels.get(position));
                }
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(interfaceContract != null){
            interfaceContract.setInterfaceActive(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(interfaceContract != null){
            interfaceContract.setInterfaceActive(true);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (context instanceof OnChannelSelectedListener) {
            mListener = (OnChannelSelectedListener) context;
            interfaceContract = (RemakeEPGFragment.EPGContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChannelSelectedListener");
        }
    }

    private void hideKeyboard() {
        View view = getView();
        if (imm != null && view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        interfaceContract = null;
    }


    private void search(String query) {
        if (categoryId >= 0 && searchInCategory.isChecked()) {
            channels = Database.searchChannels(query, categoryId);
        } else {
            channels = Database.searchChannels(query);
        }
        gridView.setAdapter(new ChannelGridAdapter(LayoutInflater.from(getContext()), channels));
        if (channels.size() == 0) {
            hideKeyboard();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query != null && !query.isEmpty()) {
            search(query);
            ((BaseActivity) getActivity()).hideSoftInput();
            gridView.requestFocus();
            return true;
        } else {
            hideKeyboard();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (searchView.getQuery().length() > 0) {
            search(searchView.getQuery().toString());
            ((BaseActivity) getActivity()).hideSoftInput();
            gridView.requestFocus();
        }
    }

}
