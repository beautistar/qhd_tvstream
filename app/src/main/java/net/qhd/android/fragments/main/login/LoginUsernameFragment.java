package net.qhd.android.fragments.main.login;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.Server;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnLoginListener;
import com.jtv.android.utils.QHDParser;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.services.BindFirebaseWithUserService;
import net.qhd.android.services.SelectServerService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginUsernameFragment extends Fragment implements TextView.OnEditorActionListener, OnLoginListener {

    public static final String ARG_USERNAME = "username";

    private LoginCallback mListener;

    private String username;

    @BindView(R.id.edit_name) EditText editName;
    @BindView(R.id.edit_password) EditText editPassword;

    @BindView(R.id.login_submit) Button buttonLoginUser;
    private NetworkManager networkManager;
    private FirebaseAnalytics firebaseAnalytics;

    public LoginUsernameFragment() {
        // Required empty public constructor
    }


    public static LoginUsernameFragment instance(String username) {
        LoginUsernameFragment loginUsernameFragment = new LoginUsernameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USERNAME, username);
        loginUsernameFragment.setArguments(args);
        return loginUsernameFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            username = arguments.getString(ARG_USERNAME, "");
        } else {
            username = ((BaseActivity) getActivity()).getSettingsManager().getLastLogin();
        }
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login_username, container, false);
        ButterKnife.bind(this, root);

        if (username != null && !username.isEmpty()) {
            editName.setText(username);
            editPassword.requestFocus();
        }
        editPassword.setOnEditorActionListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (username != null && !username.isEmpty()) {
            editPassword.requestFocus();
        } else {
            editName.requestFocus();
        }
    }

    private boolean isInputCorrect() {
        String name = editName.getText().toString();
        String pwd = editPassword.getText().toString();
        if (name.length() < 5) {
            editName.setError(getString(R.string.validation_name_too_short));
            editName.requestFocus();
            return false;
        } else if (name.length() > 128) {
            editName.setError(getString(R.string.validation_name_too_long));
            editName.requestFocus();
            return false;
        } else if (pwd.length() < 5) {
            editPassword.setError(getString(R.string.validation_password_too_short));
            editPassword.requestFocus();
            return false;
        } else if (pwd.length() > 128) {
            editPassword.setError(getString(R.string.validation_passwrod_too_long));
            editPassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @OnClick(R.id.login_submit)
    public void onClickLogin() {
        if (isInputCorrect()) {
            String name = editName.getText().toString();
            String pwd = editPassword.getText().toString();
            if (mListener != null) {
                mListener.onLoginStarted();
            }
            buttonLoginUser.setText(R.string.info_logging_in);
            buttonLoginUser.setTextColor(Color.YELLOW);
            networkManager.setLoginListener(this);
            networkManager.login(name, pwd);
            buttonLoginUser.setEnabled(false);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginCallback) {
            mListener = (LoginCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginMethodSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onClickLogin();
        }
        return false;
    }

    @Override
    public void onLoginSuccess() {

        buttonLoginUser.setText(R.string.info_updating_channels);
        buttonLoginUser.setTextColor(Color.YELLOW);

        BindFirebaseWithUserService.bindUserWithFirebase(getActivity());
        FirebaseAnalytics.getInstance(getActivity()).setUserProperty(FirebaseAnalytics.UserProperty.SIGN_UP_METHOD, "username");

        networkManager.getServerList(new Callback<List<Server>>() {
            @Override
            public void onResponse(Call<List<Server>> call, Response<List<Server>> response) {
                if (response.isSuccessful()) {
                    List<Server> servers = response.body();
                    ActiveAndroid.beginTransaction();
                    try {
                        new Delete().from(Server.class).execute();
                        if (servers.size() > 0) {
                            servers.get(0).setSelected(true);
                        }
                        for (Server server : servers) {
                            server.save();
                        }
                        ActiveAndroid.setTransactionSuccessful();
                    } finally {
                        ActiveAndroid.endTransaction();
                    }
                    SelectServerService.start(getActivity());
                    updateCategories();
                } else {
                    Toast.makeText(getActivity(), R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                    updateCategories();
                }
            }

            @Override
            public void onFailure(Call<List<Server>> call, Throwable t) {
                Toast.makeText(getActivity(), R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                updateCategories();
            }
        });
        networkManager.setLoginListener(null);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, new Bundle());
    }

    private void updateCategories() {
        networkManager.updateChannels(new NetworkManager.OnChannelUpdateCallback() {
            @Override
            public void onUpdated() {
                buttonLoginUser.setText(R.string.info_login_success);
                mListener.onLoggedIn();
            }

            @Override
            public void onFailed() {
                Toast.makeText(getActivity(), "Failed to update channels", Toast.LENGTH_SHORT).show();
                mListener.onLoggedIn();
            }
        });
    }


    @Override
    public void onLoginError(int errorCode, String errorMessage) {
        buttonLoginUser.setEnabled(true);
        buttonLoginUser.setText(R.string.log_in);
        switch (errorCode) {
            case QHDParser.ERROR_LOGIN:
                editName.setError(getString(R.string.error_login_failed));
                editName.requestFocus();
                break;
            case QHDParser.ERROR_INCORRECT_PASSWORD:
                editPassword.setError(getString(R.string.error_bad_name_or_password));
                editPassword.requestFocus();
                break;
            case QHDParser.ERROR_USER_NOT_ACTIVATED:
                editName.setError(getString(R.string.error_user_not_active));
                editName.requestFocus();
            case QHDParser.ERROR_BAD_CODE:
                editName.setError(getString(R.string.error_connection));
                editName.requestFocus();
                break;
            case QHDParser.ERROR_EMPTY:
                editName.setError(getString(R.string.error_request_dropped));
                editName.requestFocus();
                break;
            case QHDParser.ERROR_USER_ACTIVATED:
                editName.setError(getString(R.string.error_user_activated));
                editName.requestFocus();
                break;
            default:
                editName.setError(errorMessage);
                editName.requestFocus();
                break;
        }
        if (mListener != null) {
            mListener.onLoginFailed();
        }
    }
}
