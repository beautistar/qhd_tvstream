package net.qhd.android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Notification;

import net.qhd.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationFragment extends Fragment implements Runnable {


    private static final String ARG_MESSAGE = "message";
    public static final int DEFAULT_DURATION = 4;

    @BindView(R.id.notification_body) TextView body;
    @BindView(R.id.notification_title) TextView title;
    @BindView(R.id.notification_icon) ImageView icon;

    @BindView(R.id.notification_root) View root;


    private Notification message;
    private Handler handler = new Handler();

    public static NotificationFragment newInstance(Notification notification) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MESSAGE, notification);
        fragment.setArguments(args);
        return fragment;
    }

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            message = arguments.getParcelable(ARG_MESSAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (message.getBody() != null) {
            body.setText(message.getBody());
        }

        if (message.getTitle() != null) {
            title.setText(message.getTitle());
        }

        switch (message.getIcon()) {
            case "warning":
                icon.setImageResource(R.drawable.ic_warning);
                break;
            case "danger":
                icon.setImageResource(R.drawable.ic_danger);
                break;
            default:
                icon.setImageResource(R.drawable.ic_info);
        }

        int duration = DEFAULT_DURATION;
        if (message.getData().containsKey("duration")) {
            String stringDuration = message.getData().get("duration");
            try {
                duration = Math.max(0, Math.min(60, Integer.parseInt(stringDuration)));
            } catch (NumberFormatException ignored) {
            }
        }
        handler.postDelayed(this, duration * 1000);
    }

    @OnClick(R.id.notification_root)
    public void onClickNotification() {
        if (message.getLink() != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, message.getLink()));
            run(); // close notification
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        try {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
                    .remove(this).commit();
        } catch (Exception ignore) {
        }
    }
}
