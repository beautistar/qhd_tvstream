package net.qhd.android.fragments.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.qhd.android.R;
import net.qhd.android.listeners.OnChannelLongClickListener;

import java.util.List;

class HorizontalChannelAdapter extends RecyclerView.Adapter<HorizontalChannelAdapter.ViewHolder> {

    private final ChannelHorizontalListFragment.InteractionListener mListener;
    private final View.OnFocusChangeListener focusListener;

    public static final DisplayImageOptions DISPLAY_THUMBNAIL_OPTIONS = new DisplayImageOptions.Builder()
            .resetViewBeforeLoading(true)
            .displayer(new FadeInBitmapDisplayer(250))
            .cacheOnDisk(true)
            .build();

    private List<Channel> mChannels;
    private int focused = 0;
    private OnChannelLongClickListener onItemLongClickListener;

    HorizontalChannelAdapter(List<Channel> items, ChannelHorizontalListFragment.InteractionListener listener, View.OnFocusChangeListener focusListener) {
        this.mChannels = items;
        this.mListener = listener;
        this.focusListener = focusListener;
    }

    public void setChannels(List<Channel> channels) {
        mChannels = channels;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_channel_horizontal_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Channel channel = mChannels.get(position);
        holder.mItem = channel;
//        holder.mIdView.setText(mChannels.get(position).getImageURL());
        holder.mTitle.setText("" + (position + 1) + ". " + channel.getName());
        //Display image
        ImageLoader loader = ImageLoader.getInstance();
        loader.displayImage(mChannels.get(position).getImgURL(), holder.mThumbnail, DISPLAY_THUMBNAIL_OPTIONS);

        holder.mFavIcon.setVisibility(channel.isFavorite() ? View.VISIBLE : View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onChannelSelected(holder.mItem, holder.getAdapterPosition());
                }
            }
        });
        holder.mView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.mView.setSelected(true);
                    focused = holder.getAdapterPosition();
                    if (focusListener != null) {
                        focusListener.onFocusChange(v, true);
                    }
                } else {
                    holder.mView.setSelected(false);
                }
            }
        });
        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (onItemLongClickListener != null) {
                    onItemLongClickListener.onChannelLongClick(holder.mItem, holder.mView, holder.getAdapterPosition());
                }
                return true;
            }
        });
    }

    public int getFocused() {
        return focused;
    }

    @Override
    public int getItemCount() {
        return mChannels.size();
    }

    public void setOnItemLongClickListener(OnChannelLongClickListener listener) {
        onItemLongClickListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mThumbnail;
        final View mFavIcon;
        final TextView mTitle;
        Channel mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            mTitle = (TextView) view.findViewById(R.id.title);
            mFavIcon = view.findViewById(R.id.button_favorite_icon);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }

    }
}
