package net.qhd.android.fragments.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.SettingsManager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.listeners.OnChannelLongClickListener;
import net.qhd.android.utils.CustomCategories;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChannelHorizontalListFragment extends Fragment implements View.OnFocusChangeListener {

    public static final String ARG_CATEGORY_ID = "category_id";
    private ChannelHorizontalListFragment.InteractionListener mListener;
    private List<Channel> channels;

    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.lock_image) ImageView lockImage;
    private HorizontalChannelAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private int categoryId;
    private BroadcastReceiver channelUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadChannels();
            adapter.setChannels(channels);
            scrollTo(adapter.getFocused());
        }
    };
    private boolean auditing;

    public ChannelHorizontalListFragment() {
    }

    public static ChannelHorizontalListFragment newInstance(int categoryId) {
        ChannelHorizontalListFragment channelListFragment = new ChannelHorizontalListFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARG_CATEGORY_ID, categoryId);
        channelListFragment.setArguments(arguments);
        return channelListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            categoryId = arguments.getInt(ARG_CATEGORY_ID, -1);
            loadChannels();
        }
    }

    private void loadChannels() {
        switch (categoryId) {
            case CustomCategories.CATEGORY_FAVORITE_ID:
                channels = Database.getFavorites();
                auditing = false;
                break;
            case CustomCategories.CATEGORY_ALL_CHANNELS_ID:
                channels = Database.getChannels();
                auditing = false;
                break;
            default:
                Category category = Database.getCategory(categoryId);
                auditing = category.isAuditing();
                channels = category.getChannels();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_horizontal_list, container, false);
        ButterKnife.bind(this, view);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new HorizontalChannelAdapter(channels, mListener, this);
        adapter.setOnItemLongClickListener(new OnChannelLongClickListener() {
            @Override
            public void onChannelLongClick(final Channel channel, View view, final int position) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(getActivity(), view);
                popup.getMenu().add(0, 0, 0, R.string.buttton_play);
                if (!auditing) {
                    popup.getMenu().add(0, 1, 1, channel.isFavorite() ? R.string.favorite_remove : R.string.favorite_add);
                }
//                popup.getMenu().add(0, 3, 3, R.string.list_sort);
                popup.getMenu().add(0, 4, 4, R.string.search);
                popup.getMenu().add(0, 5, 5, R.string.recents);
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 0: //Play pressed
                                if (mListener != null) {
                                    mListener.onChannelSelected(channel, position);
                                }
                                break;
                            case 1: //Fav unfav
                                if (channel.isFavorite()) { // Remove from favorites
                                    channel.setFavorite(false);
                                    channel.save();
                                    ((BaseActivity) getActivity()).getNetworkManager().removeFavorite(channel.getRemoteId());
                                } else { //Add to favorites
                                    channel.setFavorite(true);
                                    channel.save();
                                    ((BaseActivity) getActivity()).getNetworkManager().addFavorite(channel.getRemoteId());
                                }
                                adapter.notifyItemChanged(position);
                                break;
                            case 4:
                                if (mListener != null) {
                                    mListener.onSearch(channel);
                                }
                                break;
                            case 5:
                                if (mListener != null) {
                                    mListener.onRecent();
                                }
                                break;
                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }
        });
        if (!auditing) {
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            lockImage.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @OnClick(R.id.lock_image)
    public void onLockImageClick() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(R.string.title_protected);
        alertDialog.setMessage(R.string.message_enter_password);

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.ic_lock);

        alertDialog.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String password = SettingsManager.SHA1(input.getText().toString());
                        String savedPassword = ((BaseActivity) getActivity()).getSettingsManager().getPassword();
                        if (savedPassword != null) {
                            if (savedPassword.equals(password)) {
                                recyclerView.setVisibility(View.VISIBLE);
                                lockImage.setVisibility(View.GONE);
                                recyclerView.setAdapter(adapter);
                            } else {
                                Toast.makeText(getActivity(),
                                        R.string.error_bad_password, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.error_no_password_set, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChannelHorizontalListFragment.InteractionListener) {
            mListener = (ChannelHorizontalListFragment.InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).registerChannelUpdateReciever(channelUpdateReceiver);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(channelUpdateReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        scrollTo(adapter.getFocused());
    }

    private void scrollTo(int position) {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, r.getDisplayMetrics());
        linearLayoutManager.scrollToPositionWithOffset(position, (recyclerView.getWidth() / 2) - ((int) px));
    }

    public interface InteractionListener {

        void onChannelSelected(Channel channel, int position);

        void onSearch();

        void onSearch(Channel channel);

        void onRecent();

    }

}
