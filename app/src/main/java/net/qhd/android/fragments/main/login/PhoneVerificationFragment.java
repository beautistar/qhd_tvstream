package net.qhd.android.fragments.main.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.jtv.android.network.NetworkManager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Unused
 */
@Deprecated
public class PhoneVerificationFragment extends Fragment {

    public static final String TAG = "Verification";
    public static final String ARG_PHONE_NUMBER = "phone_number";
    private String phone;
    private OnPhoneVerification listener;

    private NetworkManager networkManager;

    private String verificationId;
    private PhoneAuthProvider.ForceResendingToken forceResendingToken;

    @BindView(R.id.edit_code) EditText editCode;

    @BindView(R.id.verify_phone) Button submit;

    public static PhoneVerificationFragment instance(String phone){
        PhoneVerificationFragment fragment = new PhoneVerificationFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARG_PHONE_NUMBER, phone);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            phone = arguments.getString(ARG_PHONE_NUMBER);
        }
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_phone_verification, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(phone != null){
            PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60, TimeUnit.SECONDS, getActivity(), new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    signInWithPhone(phoneAuthCredential);
                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    if(listener != null){
                        listener.onPhoneVerificationError(e);
                    }
                }

                @Override
                public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    PhoneVerificationFragment.this.verificationId = verificationId;
                    PhoneVerificationFragment.this.forceResendingToken = forceResendingToken;
                }
            });
        }
    }

    @OnClick(R.id.verify_phone)
    void onVerifyClick(){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, editCode.getText().toString());
        signInWithPhone(credential);
    }

    private void signInWithPhone(PhoneAuthCredential credential){
        submit.setEnabled(false);
        FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                submit.setEnabled(true);
                if (task.isSuccessful()) {
                    Log.d(TAG, "onComplete: success");
                } else {
                    Log.d(TAG, "onComplete: fail");
                    Toast.makeText(getActivity(), "Incorrect code", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnPhoneVerification){
            listener = ((OnPhoneVerification) context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnPhoneVerification{
        void onPhoneVerified(String phone);
        void onPhoneVerificationError(Exception e);
    }

}
