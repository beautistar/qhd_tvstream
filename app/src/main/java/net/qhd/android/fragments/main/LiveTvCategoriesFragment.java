package net.qhd.android.fragments.main;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import net.qhd.android.R;
import net.qhd.android.utils.CustomCategories;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveTvCategoriesFragment extends Fragment {

    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.pager_title_strip) TabLayout pagerTabStrip;
    private List<Category> categories;
    private int lastCategory = 0;

    public LiveTvCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categories = Database.getCategories(false);
        categories.add(0, CustomCategories.getAllChannelsCategory(getContext()));
        categories.add(0, CustomCategories.getFavoriteCategory(getContext()));
        List<Channel> recentlyWatched = Database.getRecentlyWatched(1);
        if (recentlyWatched.size() > 0) {
            int remoteId = recentlyWatched.get(0).getCategory().getRemoteId();
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).getRemoteId() == remoteId) {
                    lastCategory = i;
                    break;
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_live_tv_categories, container, false);
        ButterKnife.bind(this, root);
        viewPager.setAdapter(new CategoryAdapter(getChildFragmentManager(), categories));
        viewPager.setCurrentItem(lastCategory + 1, false);
        pagerTabStrip.setupWithViewPager(viewPager);
        for (int i = 0; i < pagerTabStrip.getTabCount(); i++) {
            TabLayout.Tab tabAt = pagerTabStrip.getTabAt(i);
            if (tabAt != null) {
                View customView = inflater.inflate(R.layout.layout_tab_category, pagerTabStrip, false);

                ImageView categoryImage = customView.findViewById(R.id.category_image);
                ImageLoader.getInstance().displayImage(categories.get(i).getImageURL(), categoryImage, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        view.setVisibility(GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage == null) {
                            view.setVisibility(GONE);
                        } else {
                            view.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        view.setVisibility(GONE);
                    }
                });
                TextView categoryTitle = customView.findViewById(R.id.category_title);
                categoryTitle.setText(categories.get(i).getName());
                tabAt.setCustomView(customView);
            }
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    class CategoryAdapter extends FragmentStatePagerAdapter {

        private final List<Category> categories;

        CategoryAdapter(FragmentManager fm, List<Category> categories) {
            super(fm);
            this.categories = categories;
        }

        @Override
        public Fragment getItem(int position) {
            return ChannelHorizontalListFragment.newInstance(categories.get(position).getRemoteId());
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return categories.get(position).getName();
        }
    }

}
