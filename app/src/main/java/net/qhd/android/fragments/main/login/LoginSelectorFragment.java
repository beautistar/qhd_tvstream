package net.qhd.android.fragments.main.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.SettingsManager;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;

public class LoginSelectorFragment extends Fragment {

    private OnLoginMethodSelected mListener;

    public static final int METHOD_LOGIN_CODE = 0;
    public static final int METHOD_LOGIN_USERNAME = 1;
    public static final int METHOD_REGISTER = 2;
    public static final int METHOD_SETTINGS = 3;

    int focusedButton = -1;

    @BindView(R.id.button_login_code) View defaultButton;
    @BindView(R.id.debug) TextView debug;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({METHOD_LOGIN_CODE, METHOD_LOGIN_USERNAME, METHOD_REGISTER, METHOD_SETTINGS})
    public @interface MethodSelection {
    }

    public LoginSelectorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login_selector, container, false);
        ButterKnife.bind(this, root);
        if (BuildConfig.DEBUG) {
            BaseActivity activity = (BaseActivity) getActivity();
            SettingsManager settingsManager = activity.getSettingsManager();
            NetworkManager networkManager = activity.getNetworkManager();
            debug.setVisibility(BuildConfig.DEBUG ? View.VISIBLE : View.GONE);
            debug.setText(
                    "login token " + settingsManager.getLoginToken() + "\n" +
                            "login expiration " + settingsManager.getTokenExpirationTime() + "\n" +
                            "mac address " + networkManager.getMac()
            );
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (focusedButton != -1) {
            View view = getView();
            if (view != null) {
                view.findViewById(focusedButton).requestFocus();
            } else {
                defaultButton.requestFocus();
            }
        } else {
            defaultButton.requestFocus();
        }
    }

    @OnFocusChange({R.id.settings_button, R.id.button_login_code, R.id.button_login_username, R.id.button_register})
    public void onFocusChange(View view, boolean focused) {
        if (focused) {
            focusedButton = view.getId();
        }
    }

    @OnClick(R.id.settings_button)
    public void onSettingsClick() {
        mListener.onMethodSelected(METHOD_SETTINGS);
    }

    @OnClick(R.id.button_login_code)
    public void onLoginCode() {
        mListener.onMethodSelected(METHOD_LOGIN_CODE);
    }

    @OnClick(R.id.button_login_username)
    public void onLoginUsername() {
        mListener.onMethodSelected(METHOD_LOGIN_USERNAME);
    }

    @OnClick(R.id.button_register)
    public void onClickRegister() {
        mListener.onMethodSelected(METHOD_REGISTER);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginMethodSelected) {
            mListener = (OnLoginMethodSelected) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginMethodSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnLoginMethodSelected {

        void onMethodSelected(@MethodSelection int method);
    }
}
