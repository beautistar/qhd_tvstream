package net.qhd.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;

import net.qhd.android.R;
import net.qhd.android.adapters.ChannelGridAdapter;
import net.qhd.android.listeners.OnChannelSelectedListener;
import net.qhd.android.remake.RemakeEPGFragment;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;


public class ChannelRecentsFragment extends Fragment {

    private OnChannelSelectedListener mListener;

    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.pager) ViewPager pager;
    private RemakeEPGFragment.EPGContract interfaceContract;


    public ChannelRecentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_channel_recents, container, false);
        ButterKnife.bind(this, root);
        pager.setAdapter(new ChannelPages(inflater));
        tabs.setupWithViewPager(pager);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        pager.requestFocus();
        if(interfaceContract != null){
            interfaceContract.setInterfaceActive(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(interfaceContract != null){
            interfaceContract.setInterfaceActive(true);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChannelSelectedListener) {
            mListener = (OnChannelSelectedListener) context;
            interfaceContract = (RemakeEPGFragment.EPGContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChannelSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        interfaceContract = null;
    }

    private class ChannelPages extends PagerAdapter {

        public static final int CHANNEL_COUNT = 30;
        public final String[] PAGES = {getString(R.string.recents), getString(R.string.most_watched)};
        private final LayoutInflater inflater;

        public ChannelPages(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            List<Channel> channels = getChannels(position);
            if (channels.size() > 0) {

                final GridView v = (GridView) inflater.inflate(R.layout.layout_channel_grid, null);
                v.setAdapter(new ChannelGridAdapter(inflater, channels));
                v.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (mListener != null) {
                            mListener.onChannelSelected((Channel) v.getItemAtPosition(position));
                        }
                    }
                });
                collection.addView(v, 0);
                return v;
            } else {
                TextView textView = new TextView(getActivity());
                textView.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
                textView.setGravity(Gravity.CENTER);
                textView.setText(R.string.error_list_empty);
                collection.addView(textView, 0);
                return textView;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGES[position];
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((GridView) view);
        }

        private List<Channel> getChannels(int page) {
            switch (page) {
                case 0:
                    return Database.getRecentlyWatched(CHANNEL_COUNT);
                case 1:
                    return Database.getMostWatched(CHANNEL_COUNT);
            }
            return Collections.emptyList();
        }

    }


}