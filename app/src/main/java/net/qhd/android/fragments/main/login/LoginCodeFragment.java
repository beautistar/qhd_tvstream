package net.qhd.android.fragments.main.login;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.Server;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnLoginListener;
import com.jtv.android.utils.QHDParser;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.services.BindFirebaseWithUserService;
import net.qhd.android.services.SelectServerService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginCodeFragment extends Fragment implements TextView.OnEditorActionListener, OnLoginListener {

    private LoginCallback mListener;


    @BindView(R.id.edit_code) EditText editCode;

    @BindView(R.id.login_submit_code) Button buttonLoginCode;

    private NetworkManager networkManager;
    private FirebaseAnalytics firebaseAnalytics;

    public LoginCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_login_code, container, false);
        ButterKnife.bind(this, root);

        editCode.setOnEditorActionListener(this);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        editCode.requestFocus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginCallback) {
            mListener = (LoginCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginMethodSelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.login_submit_code)
    public void onClickLoginCode() {
        String code = editCode.getText().toString();
        if (code.length() >= 6) {
            buttonLoginCode.setText(R.string.info_validating);
            buttonLoginCode.setTextColor(Color.YELLOW);
            networkManager.setLoginListener(this);
            networkManager.login(code);
            mListener.onLoginStarted();
            buttonLoginCode.setEnabled(false);
        } else {
            editCode.setError(getString(R.string.info_incorrect_code));
            editCode.requestFocus();
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onClickLoginCode();
        }
        return false;
    }

    @Override
    public void onLoginSuccess() {

        BindFirebaseWithUserService.bindUserWithFirebase(getActivity());
        FirebaseAnalytics.getInstance(getActivity()).setUserProperty(FirebaseAnalytics.UserProperty.SIGN_UP_METHOD, "code");

        buttonLoginCode.setText(R.string.info_updating_channels);
        buttonLoginCode.setTextColor(Color.YELLOW);

        networkManager.getServerList(new Callback<List<Server>>() {
            @Override
            public void onResponse(Call<List<Server>> call, Response<List<Server>> response) {
                if (response.isSuccessful()) {
                    List<Server> servers = response.body();
                    ActiveAndroid.beginTransaction();
                    try {
                        new Delete().from(Server.class).execute();
                        if (servers.size() > 0) {
                            servers.get(0).setSelected(true);
                        }
                        for (Server server : servers) {
                            server.save();
                        }
                        ActiveAndroid.setTransactionSuccessful();
                    } finally {
                        ActiveAndroid.endTransaction();
                    }
                    SelectServerService.start(getActivity());
                    updateCategories();
                } else {
                    Toast.makeText(getActivity(), R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                    updateCategories();
                }
            }

            @Override
            public void onFailure(Call<List<Server>> call, Throwable t) {
                Toast.makeText(getActivity(), R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                updateCategories();
            }
        });
        networkManager.setLoginListener(null);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, new Bundle());
    }

    private void updateCategories() {
        networkManager.updateChannels(new NetworkManager.OnChannelUpdateCallback() {
            @Override
            public void onUpdated() {
                buttonLoginCode.setText(R.string.info_login_success);
                mListener.onLoggedIn();
            }

            @Override
            public void onFailed() {
                mListener.onLoggedIn();
                Toast.makeText(getActivity(), "Failed to update channels", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onLoginError(int errorCode, String errorMessage) {
        buttonLoginCode.setEnabled(true);
        buttonLoginCode.setText(R.string.log_in);
        buttonLoginCode.setTextColor(Color.WHITE);

        mListener.onLoginFailed();
        switch (errorCode) {
            case QHDParser.ERROR_LOGIN:
                editCode.setError(getString(R.string.error_login_failed));
                editCode.requestFocus();
                break;
            case QHDParser.ERROR_INCORRECT_PASSWORD:
                editCode.setError(getString(R.string.error_bad_name_or_password));
                editCode.requestFocus();
                break;
            case QHDParser.ERROR_USER_NOT_ACTIVATED:
                editCode.setError(errorMessage);
                editCode.requestFocus();
            case QHDParser.ERROR_BAD_CODE:
                editCode.setError(getString(R.string.error_bad_code));
                editCode.requestFocus();
                break;
            case QHDParser.ERROR_EMPTY:
                editCode.setError(getString(R.string.error_request_dropped));
                editCode.requestFocus();
                break;
            case QHDParser.ERROR_USER_ACTIVATED:
                editCode.setError(getString(R.string.error_user_activated));
                editCode.requestFocus();
                break;
            case QHDParser.ERROR_CODE_ACTIVE:
                editCode.setError(getString(R.string.error_code_activated));
                editCode.requestFocus();
                break;
            default:
                editCode.setError(errorMessage);
                editCode.requestFocus();
                break;
        }
    }
}
