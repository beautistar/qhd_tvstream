package net.qhd.android.fragments.player;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.jtv.android.network.listeners.OnLoginListener;
import com.jtv.android.subtitles.provider.OpenSubsProvider;
import com.jtv.player.JTVPlayer;
import com.jtv.player.exceptions.HttpException;
import com.jtv.player.subtitles.JTVTextOutput;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.TubiControllerActivity;
import net.qhd.android.utils.RetrofitResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;

public class MoviePlayerFragment extends PlayerFragment implements AdapterView.OnItemSelectedListener, OnLoginListener {

    private static final String ARG_CONTROLS_ENABLED = "controls_enabled";
    private static final String ARG_VOLUME = "volume";
    private static final String ARG_IS_TRAILER = "trailer";
    public static final String ARG_IMDBID = "imdbid";

    public static final String ARG_MOVIE_NAME = "movie_name";

    private AlertDialog.Builder resumeDialog;
    private boolean controlsEnabled;
    private float volume;
    private boolean isTrailer;
    private String imdbid;
    private long movieTime;
    private String movieName;

    private Map<String, String> subtitles;
    private ArrayAdapter<String> subtitleSelectorAdapter;
    private int subtitleAttempt = 0;


    @BindView(R.id.subtitle_view) SubtitleView subtitleView;
    @BindView(R.id.subs_spinner) Spinner subtitleSelector;
    @BindView(R.id.subs_settings) ImageButton subtitleSettings;
    private MoviePlayerContract readyListener;

    private int[] subtitleSizes;
    private int subtitleSize = 16;
    private int subtitleStyle = 0;


    public static PlayerFragment newInstance(String streamUrl, String movieName, String imdbId, boolean controlsEnabled, float volume, boolean isTrailer) {
        MoviePlayerFragment fragment = new MoviePlayerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL, streamUrl);
        args.putString(ARG_IMDBID, imdbId);
        args.putBoolean(ARG_CONTROLS_ENABLED, controlsEnabled);
        args.putFloat(ARG_VOLUME, volume);
        args.putBoolean(ARG_IS_TRAILER, isTrailer);
        args.putString(ARG_MOVIE_NAME, movieName);
        fragment.setArguments(args);
        return fragment;
    }

    public static PlayerFragment trailerInstance(String streamUrl, float volume) {
        MoviePlayerFragment fragment = new MoviePlayerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL, streamUrl);
        args.putBoolean(ARG_CONTROLS_ENABLED, false);
        args.putFloat(ARG_VOLUME, volume);
        args.putBoolean(ARG_IS_TRAILER, true);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingsManager = ((BaseActivity) getActivity()).getSettingsManager();
        String[] stringArray = getResources().getStringArray(R.array.caption_size);
        subtitleSizes = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            subtitleSizes[i] = Integer.parseInt(stringArray[i]);
        }
        subtitleSize = settingsManager.getCaptionSize();
        subtitleStyle = settingsManager.getCaptionStyle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initCaptionStyle();
        buildResumeDialog();

        if (!controlsEnabled) {
            controlRoot.setVisibility(View.GONE);
        } else {
            showController();
        }

        subtitleSelector.setOnItemSelectedListener(this);
        return view;
    }

    @Override
    protected void preparePlayer() {
        Log.d(TAG, "Preparing player: " + streamUrl);
        if (player != null) {
            player.release();
        }
        if (!networkManager.isLoggedIn()) {
            networkManager.setLoginListener(this);
            networkManager.login();
            return;
        }
        player = getPlayer();
        player.prepare();
        player.setSurface(surfaceView);
        player.setPlayWhenReady(true);
        player.setListener(this);
        player.play(networkManager.appendTokenToUrl(streamUrl));
        player.setVolume(volume);
        player.setTextOutput(new JTVTextOutput(subtitleView));
        if (!isTrailer && !imdbid.isEmpty()) {
            new OpenSubsProvider(getActivity(), networkManager, new OkHttpClient(), OpenSubsProvider.buildClient())
                    .getList(imdbid, new RetrofitResponse<Map<String, String>>() {

                        @Override
                        public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                            subtitleAttempt++;
                            super.onResponse(call, response);
                        }

                        @Override
                        public void onResult(Map<String, String> body) {
                            if (!body.isEmpty()) {
                                Log.d(TAG, "onSuccess: got subtitles: " + body.toString());
                                Log.d(TAG, "onSuccess: count: " + body.size());
                                subtitles = body;
                                Set<String> languages = body.keySet();
                                List<String> strings = new ArrayList<>();
                                Collections.addAll(strings, languages.toArray(new String[0]));
                                String noSubtitles = "No Subtitles";
                                try {
                                    noSubtitles = getString(R.string.no_subtitles);
                                } catch (IllegalStateException ignored) {
                                }
                                strings.add(0, noSubtitles);
                                FragmentActivity activity = getActivity();
                                if (activity != null) {
                                    subtitleSelectorAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, strings);
                                    subtitleSelectorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    subtitleSelector.setAdapter(subtitleSelectorAdapter);
                                    subtitleSelector.setVisibility(View.VISIBLE);
                                    subtitleSettings.setVisibility(View.VISIBLE);
                                }
                            } else if (subtitleAttempt == 0) {
                                networkManager.getSubtitles(imdbid, this);
                            }
                        }

                        @Override
                        public void onFail(Map<String, String> body, int code) {
                            if (isAdded()) {
                                Toast.makeText(getActivity(), "Failed to fetch subtitles.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Map<String, String>> call, Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                            if (isAdded()) {
                                Toast.makeText(getActivity(), "Failed to fetch subtitles.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onCast() {
        startActivity(TubiControllerActivity.castMovie(getActivity(), networkManager.appendTokenToUrl(streamUrl), movieName));
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (controlsEnabled) {
            return super.onKey(view, keyCode, keyEvent);
        }
        return false;
    }

    @Override
    public void showController() {
        if (controlsEnabled) {
            super.showController();
        }
    }

    @Override
    public void hideController() {
        if (controlsEnabled) {
            super.hideController();
        }
    }

    @Override
    public void onResume() {
        if (isTrailer && player == null) {
            preparePlayer();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (isTrailer && player != null) {
            player.release();
            player = null;
        }
        if (player != null && player.getPosition() > 0 && !isTrailer) {
            settingsManager.setMovieTime(streamUrl, player.getPosition());
        }
        super.onPause();
    }

    @Override
    public void onError(Exception exception) {
        super.onError(exception);
        if (exception instanceof HttpException) {
            HttpException httpException = (HttpException) exception;
            if (httpException.getStatus() == 403) {
                networkManager.setLoginListener(this);
                networkManager.login();
            }
        }
        if (readyListener != null) {
            readyListener.onMovieError();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MoviePlayerContract) {
            readyListener = ((MoviePlayerContract) context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        readyListener = null;
    }

    @Override
    protected void parseArguments(Bundle arg) {
        super.parseArguments(arg);
        controlsEnabled = arg.getBoolean(ARG_CONTROLS_ENABLED, true);
        volume = arg.getFloat(ARG_VOLUME, 1f);
        isTrailer = arg.getBoolean(ARG_IS_TRAILER, false);
        imdbid = arg.getString(ARG_IMDBID, "");
        movieName = arg.getString(ARG_MOVIE_NAME, "");
    }

    private void initCaptionStyle() {
        CaptionStyleCompat captionStyle = getCaptionStyle(subtitleStyle);
        subtitleView.setFixedTextSize(TypedValue.COMPLEX_UNIT_DIP, subtitleSize);
        subtitleView.setStyle(captionStyle);
    }


    @OnClick(R.id.subs_settings)
    public void subtitleSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Subtitle Settings");
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View root = layoutInflater.inflate(R.layout.layout_subtitle_settings, null);

        final SeekBar offsetSeekBar = root.findViewById(R.id.subtitle_offset);
        final TextView currentOffset = root.findViewById(R.id.subtitle_current_offset);
        final int max = offsetSeekBar.getMax();
        int offset = (int) player.getSubtitleOffset() + (max / 2);
        offsetSeekBar.setProgress(offset);
        currentOffset.setText(String.valueOf(offset / 1000));
        offsetSeekBar.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int progress;
                int offset;
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        progress = offsetSeekBar.getProgress() - 1000;
                        offsetSeekBar.setProgress(Math.max(progress, 0));
                        offset = offsetSeekBar.getProgress() - (max / 2);
                        currentOffset.setText(String.valueOf(offset / 1000));
                        player.setSubtitleOffset(offset);

                        return true;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        progress = Math.min(offsetSeekBar.getProgress() + 1000, offsetSeekBar.getMax());
                        offsetSeekBar.setProgress(progress);
                        offset = progress - (max / 2);
                        currentOffset.setText(String.valueOf(offset / 1000));
                        player.setSubtitleOffset(offset);
                        return true;
                    default:
                        return false;
                }
            }
        });
        offsetSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int offset = progress - (max / 2);
                if (fromUser) {
                    player.setSubtitleOffset(offset);
                }
                currentOffset.setText(String.valueOf(offset / 1000));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Spinner textSize = root.findViewById(R.id.subtitle_text_size);
        List<Integer> selections = new ArrayList<>();
        int currentSelection = 0;
        for (int i = 0; i < subtitleSizes.length; i++) {
            int size = subtitleSizes[i];
            selections.add(size);
            if(size == subtitleSize){
                currentSelection = i;
            }
        }
        textSize.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, selections));
        textSize.setSelection(currentSelection);
        textSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subtitleSize = subtitleSizes[position];
                subtitleView.setFixedTextSize(TypedValue.COMPLEX_UNIT_DIP, subtitleSizes[position]);
                if(settingsManager != null){
                    settingsManager.setCaptionSize(subtitleSizes[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner textStyle = root.findViewById(R.id.subtitle_text_style);
        String[] captions = getResources().getStringArray(R.array.caption_styles);
        textStyle.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, captions));
        textStyle.setSelection(subtitleStyle);
        textStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subtitleView.setStyle(getCaptionStyle(position));
                subtitleStyle = position;
                if(settingsManager != null){
                    settingsManager.setCaptionStyle(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setView(root);
        builder.setPositiveButton("Close", null);
        builder.show();
        offsetSeekBar.requestFocus();
    }

    private CaptionStyleCompat getCaptionStyle(int selected) {
        switch (selected) {
            case 1:
                return new CaptionStyleCompat(
                        Color.WHITE,
                        Color.BLACK,
                        Color.TRANSPARENT,
                        CaptionStyleCompat.EDGE_TYPE_NONE, Color.TRANSPARENT,
                        Typeface.DEFAULT
                );
            case 2:
                return new CaptionStyleCompat(
                        Color.BLACK,
                        Color.WHITE,
                        Color.TRANSPARENT,
                        CaptionStyleCompat.EDGE_TYPE_NONE, Color.TRANSPARENT,
                        Typeface.DEFAULT
                );
            default:
                return new CaptionStyleCompat(
                        Color.WHITE,
                        Color.TRANSPARENT,
                        Color.TRANSPARENT,
                        CaptionStyleCompat.EDGE_TYPE_OUTLINE, Color.BLACK,
                        Typeface.DEFAULT
                );
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0) { //No subtitles
            player.play(networkManager.appendTokenToUrl(streamUrl));
        } else {
            player.play(networkManager.appendTokenToUrl(streamUrl), subtitles.get(subtitleSelectorAdapter.getItem(i)));
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void buildResumeDialog() {
        resumeDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_movie_resume)
                .setMessage(R.string.message_movie_resume)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (player != null && prepared && movieTime > 0) {
                            player.seekTo(movieTime);
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
    }

    @Override
    public void onStateChanged(int state) {
        if (state == JTVPlayer.STATE_READY) {
            movieTime = settingsManager.getMovieTime(streamUrl);
            if (movieTime > 1 && !isTrailer && !prepared) {
                resumeDialog.show();
            }
            if (readyListener != null) {
                readyListener.onMovieReady();
            }
        }
        if (state == JTVPlayer.STATE_ENDED) {
            Log.d(TAG, "Media ended");
            if (mediaListener != null) {
                Log.d(TAG, "Sending event to listener");
                mediaListener.onMediaEnded();
            }
        }
        super.onStateChanged(state);
    }

    @Override
    public void onLoginSuccess() {
        networkManager.setLoginListener(null);
        if (player == null) {
            preparePlayer();
            return;
        }
        int selectedItemPosition = subtitleSelector.getSelectedItemPosition();
        if (player != null && streamUrl != null && subtitles != null && networkManager != null) {
            if (selectedItemPosition == 0) { //No subtitles
                player.play(networkManager.appendTokenToUrl(streamUrl));
            } else {
                player.play(networkManager.appendTokenToUrl(streamUrl), subtitles.get(subtitleSelectorAdapter.getItem(selectedItemPosition)));
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean isUser) {
        super.onProgressChanged(seekBar, progress, isUser);
        if (!isUser && prepared && player != null) {
            if (seekBar.getMax() > 0) {
                if (progress > seekBar.getMax() - 10) {
                    if (mediaListener != null) {
                        Log.d(TAG, "onProgressChanged: Media reached end");
                        mediaListener.onMediaEnded();
                    }
                }
            }
        }
    }

    @Override
    public void onLoginError(int errorMessage, String body) {
        Toast.makeText(getActivity(), getString(R.string.error_login_failed), Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    public interface MoviePlayerContract {
        void onMovieReady();

        void onMovieError();
    }
}
