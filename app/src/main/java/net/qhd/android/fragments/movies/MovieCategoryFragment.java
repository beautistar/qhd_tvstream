package net.qhd.android.fragments.movies;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jtv.android.models.MovieCategory;
import com.jtv.android.network.NetworkManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieCategoryFragment extends Fragment implements Callback<List<MovieCategory>>, AdapterView.OnItemClickListener, TextWatcher {

    public static final String ARG_CATEGORY_ID = "category_id";

    public static final DisplayImageOptions DISPLAY_THUMBNAIL_OPTIONS = new DisplayImageOptions.Builder()
            .resetViewBeforeLoading(true)
            .cacheOnDisk(true)
            .cacheInMemory(true)
            .displayer(new FadeInBitmapDisplayer(500))
            .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
            .build();

    @BindView(R.id.gridview) GridView gridView;
    @BindView(R.id.progress) ProgressBar progressBar;
    @BindView(R.id.message) TextView message;

    private List<MovieCategory> categories;
    private OnMovieCategorySelected mListener;
    private NetworkManager networkManager;
    private int categoryId;
    private boolean isFirstAttempt = true;
    private SearchViewProvider searchViewProvider;
    private CategoryAdapter adapter;

    public MovieCategoryFragment() {
        // Required empty public constructor
    }

    public static MovieCategoryFragment newInstance(int id) {
        MovieCategoryFragment fragment = new MovieCategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt(ARG_CATEGORY_ID, 0);
        } else {
            categoryId = 0;
        }
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        networkManager.getMovieCategories(categoryId, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_category, container, false);
        ButterKnife.bind(this, view);
        if (categories != null) {
            initialize();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        gridView.requestFocus();
    }

    void initialize() {
        progressBar.setVisibility(View.GONE);
        if (categoryId == 0 && categories.size() == 0) {
            CustomDialogs.showNoPackageDialog(getActivity());
        }
        adapter = new CategoryAdapter(categories);
        gridView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            private View last = null;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (view == null) return;
                View thumbnail = view.findViewById(R.id.thumbnail);
                scaleView(thumbnail, 1.15f);
                if (last != null) {
                    scaleView(last, 1f);
                }
                last = thumbnail;
            }

            private void scaleView(View thumbnail, float scale) {
                thumbnail.animate()
                        .scaleX(scale)
                        .scaleY(scale)
                        .setDuration(100)
                        .start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if (last != null) {
                    scaleView(last, 1f);
                }
            }
        });
        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);
        if (adapter.getCount() > 0) {
            gridView.setSelection(0);
        }
        if (searchViewProvider != null) {
            searchViewProvider.getSearchView().addTextChangedListener(this);
        }
        if (getView() != null) {
            getView().requestFocus();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMovieCategorySelected) {
            mListener = (OnMovieCategorySelected) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnMovieCategorySelected");
        }
        if (context instanceof SearchViewProvider) {
            searchViewProvider = (SearchViewProvider) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SearchViewProvider");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        searchViewProvider.getSearchView().removeTextChangedListener(this);
        searchViewProvider = null;
        mListener = null;
    }

    @Override
    public void onResponse(Call<List<MovieCategory>> call, Response<List<MovieCategory>> response) {
        if (response.isSuccessful()) {
            categories = response.body();
            if (gridView != null) {
                initialize();
            }
        } else {
            if (response.code() == 403) {
                if (isFirstAttempt) {
                    isFirstAttempt = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            networkManager.getMovieCategories(categoryId, MovieCategoryFragment.this);
                        }
                    }, 3000);
                    return;
                } else {
                    progressBar.setVisibility(View.GONE);
                    message.setText(R.string.error_token_expired);
                }
            }
            progressBar.setVisibility(View.GONE);
            message.setText(R.string.error_connection_problem);
        }
    }

    @Override
    public void onFailure(Call<List<MovieCategory>> call, Throwable t) {
        t.printStackTrace();
        progressBar.setVisibility(View.GONE);
        message.setText(R.string.error_unknown);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (mListener != null) {
            mListener.onItemSelected((MovieCategory) adapter.getItem(position), getTag());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int before, int count) {
        if (isVisible()) {
            adapter.filter(text.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface OnMovieCategorySelected {
        void onItemSelected(MovieCategory category, String fragmentTag);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser && adapter != null) {
            adapter.filter("");
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    class CategoryAdapter extends BaseAdapter {

        private List<MovieCategory> categories;
        private List<MovieCategory> filteredCategories;

        CategoryAdapter(List<MovieCategory> categories) {
            this.categories = categories;
            this.filteredCategories = new ArrayList<>(categories.size());
            this.filteredCategories.addAll(categories);
        }

        @Override
        public int getCount() {
            return filteredCategories.size();
        }

        @Override
        public Object getItem(int i) {
            return filteredCategories.get(i);
        }

        @Override
        public long getItemId(int i) {
            return filteredCategories.get(i).getId();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = getLayoutInflater(null);
            TextView title;
            ImageView thumbnail;
            if (view == null) {
                view = inflater.inflate(R.layout.layout_movie_category, viewGroup, false);
            }
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            title.setText(filteredCategories.get(i).getName());
            ImageLoader.getInstance()
                    .displayImage(
                            filteredCategories.get(i).getThumbnail(),
                            thumbnail,
                            DISPLAY_THUMBNAIL_OPTIONS
                    );
            return view;
        }

        public void filter(String filter) {
            if (categories == null) {
                return;
            }
            filter = filter.toLowerCase();
            filteredCategories.clear();
            if (filter.length() == 0) {
                filteredCategories.addAll(categories);
            } else {
                for (MovieCategory category : categories) {
                    if (category.getName().toLowerCase().contains(filter)) {
                        filteredCategories.add(category);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    public interface SearchViewProvider {
        EditText getSearchView();
    }

}
