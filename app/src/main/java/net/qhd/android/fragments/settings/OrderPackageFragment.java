package net.qhd.android.fragments.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jtv.android.models.Duration;
import com.jtv.android.models.OrderPackage;
import com.jtv.android.models.Package;
import com.jtv.android.models.UserResponse;
import com.jtv.android.network.NetworkManager;

import net.qhd.android.QHDApplication;
import net.qhd.android.R;
import net.qhd.android.activities.PaymentActivity;
import net.qhd.android.listeners.OnItemClickListener;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPackageFragment extends Fragment implements Callback<List<OrderPackage>> {

    public static final String TAG = "OrderPackageFragment";
    public static final int SEVEN_DAYS = 604_800_000;
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.pager_title_strip) TabLayout pagerTabStrip;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private PackagePagerAdapter adapter;
    private boolean havePackage;

    public OrderPackageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final NetworkManager networkManager = ((QHDApplication) getActivity().getApplication()).getNetworkManager();
        networkManager.getUser(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserResponse user = response.body();
                    for (Package pack : user.getPackages()) {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
                        try {
                            Date endTime = format.parse(pack.getEndTime());
                            Date current = new Date();
                            long difference = endTime.getTime() - current.getTime();
                            if (difference > SEVEN_DAYS) {
                                havePackage = true;
                                break;
                            }
                        } catch (ParseException ignored) {
                        }
                    }
                }
                networkManager.getOrderPackages(OrderPackageFragment.this);
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                networkManager.getOrderPackages(OrderPackageFragment.this);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_package, container, false);
        ButterKnife.bind(this, view);
        adapter = new PackagePagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        pagerTabStrip.setupWithViewPager(viewPager);
        return view;
    }

    @Override
    public void onResponse(Call<List<OrderPackage>> call, Response<List<OrderPackage>> response) {
        List<OrderPackage> packages = response.body();
        adapter.setOrders(packages);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(Call<List<OrderPackage>> call, Throwable t) {
        Log.e(TAG, "getOrderPackages->onFailure: ", t);
    }

    public static class PackageFragment extends Fragment implements OnItemClickListener {
        public static final String ARG_PACKAGE_NAME = "package_name";
        public static final String ARG_DURATION_ARRAY = "duration_array";
        public static final String ARG_DESCRIPTION = "description";
        public static final String ARG_HAVE_PACKAGE = "have_package";
        @BindView(R.id.listview) RecyclerView listView;
        private String packageName;
        private List<Duration> durations;
        private String description;
        private boolean havePackage;

        public static PackageFragment instance(String packageName, String description, List<Duration> durations, boolean havePackage) {
            PackageFragment fragment = new PackageFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PACKAGE_NAME, packageName);
            args.putString(ARG_DESCRIPTION, description);
            args.putSerializable(ARG_DURATION_ARRAY, (Serializable) durations);
            args.putBoolean(ARG_HAVE_PACKAGE, havePackage);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if (arguments != null) {
                packageName = arguments.getString(ARG_PACKAGE_NAME);
                description = arguments.getString(ARG_DESCRIPTION);
                Serializable serializable = arguments.getSerializable(ARG_DURATION_ARRAY);
                durations = (List<Duration>) serializable;
                havePackage = arguments.getBoolean(ARG_HAVE_PACKAGE, true);
                Collections.sort(durations, new Comparator<Duration>() {
                    @Override
                    public int compare(Duration left, Duration right) {
                        if (left.getTimePeriod() < right.getTimePeriod()) {
                            return -1;
                        }
                        if (left.getTimePeriod() > right.getTimePeriod()) {
                            return 1;
                        }
                        return 0;
                    }
                });
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_single_package, container, false);
            ButterKnife.bind(this, rootView);
            PackageAdapter adapter = new PackageAdapter(getActivity(), durations);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            listView.setLayoutManager(linearLayoutManager);
            listView.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));
            listView.setAdapter(adapter);
            listView.invalidate();
            adapter.setOnItemClickListener(this);
            return rootView;
        }

        @Override
        public void onItemClick(int position) {
            final Duration duration = durations.get(position);

            if (havePackage) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Are you sure?")
                        .setMessage("You still have a package that will not expire for more than a week. Do you wish to continue?")
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                goToPayment(duration);
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
            } else {
                goToPayment(duration);
            }
        }

        public void goToPayment(Duration duration) {
            getActivity().startActivity(PaymentActivity.createIntent(getActivity(),
                    String.valueOf(duration.getId()),
                    description,
                    Double.toString(duration.getPrice()),
                    Double.toString(duration.getTotal() - duration.getPrice()),
                    Double.toString(duration.getTotal()),
                    duration.getCurrency()));
        }

        public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.DurationViewHolder> {
            private final Context context;
            private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.getDefault());
            private List<Duration> values;
            private OnItemClickListener onItemClickListener;

            PackageAdapter(Context context, List<Duration> values) {
                this.context = context;
                this.values = values;
            }

            @Override
            public DurationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View rowView = LayoutInflater.from(context)
                        .inflate(R.layout.layout_package_duration, parent, false);
                return new DurationViewHolder(rowView);
            }

            @Override
            public void onBindViewHolder(DurationViewHolder durationHolder, int position) {
                Calendar instance = Calendar.getInstance();
                Duration duration = values.get(position);
                instance.add(Calendar.DATE, duration.getTimePeriod());
                Resources resources = getResources();
                int time = duration.getTimePeriod();
                String days = resources.getQuantityString(R.plurals.days, time);
                String text = getString(R.string.duration_full, "" + time + " " + days, dateFormat.format(instance.getTime()));
                Spanned result;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
                } else {
                    result = Html.fromHtml(text);
                }
                String basePrice;
                if (duration.getTax() == null) {
                    basePrice = String.format(Locale.getDefault(), "%.2f %s", duration.getPrice(), duration.getCurrency());
                } else {
                    basePrice = String.format(Locale.getDefault(), "%.2f %s\n%s", duration.getPrice(), duration.getCurrency(), duration.getTax());
                }
                String total = String.format(Locale.getDefault(), "Total: %.2f %s", duration.getTotal(), duration.getCurrency());
                durationHolder.update(result, basePrice, duration.getDiscount(), total);
            }

            void setOnItemClickListener(OnItemClickListener onItemClickListener) {
                this.onItemClickListener = onItemClickListener;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public int getItemCount() {
                return values != null ? values.size() : 0;
            }

            void onItemClick(int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position);
                }
            }

            class DurationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

                private TextView duration;
                private TextView price;
                private TextView discount;
                private TextView total;

                DurationViewHolder(View itemView) {
                    super(itemView);
                    duration = itemView.findViewById(R.id.duration);
                    price = itemView.findViewById(R.id.price);
                    discount = itemView.findViewById(R.id.discount);
                    total = itemView.findViewById(R.id.total);
                    this.itemView.setOnClickListener(this);
                }

                void update(CharSequence duration, CharSequence price, CharSequence discount, CharSequence total) {
                    this.duration.setText(duration);
                    this.price.setText(price);
                    this.discount.setVisibility(discount != null ? View.VISIBLE : View.GONE);
                    this.discount.setText(discount);
                    this.total.setText(total);
                }

                @Override
                public void onClick(View view) {
                    onItemClick(this.getAdapterPosition());
                }
            }

        }

    }

    public class PackagePagerAdapter extends FragmentStatePagerAdapter {

        private List<OrderPackage> orders;

        PackagePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        void setOrders(List<OrderPackage> orders) {
            this.orders = orders;
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int i) {
            OrderPackage orderPackage = orders.get(i);
            return PackageFragment.instance(orderPackage.getName(), orderPackage.getDescription(), orderPackage.getDurations(), havePackage);
        }

        @Override
        public int getCount() {
            return orders != null ? orders.size() : 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return orders.get(position).getName();
        }
    }

}
