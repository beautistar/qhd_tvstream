package net.qhd.android.fragments.main;

import android.content.Context;
import android.support.v4.app.Fragment;

public class SetupFragment extends Fragment {

    private SetupFragmentListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SetupFragmentListener) {
            listener = (SetupFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMovieCategorySelected");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    protected void next() {
        if (listener != null) {
            listener.onNext();
        }
    }

    public interface SetupFragmentListener {

        void onNext();

    }

}
