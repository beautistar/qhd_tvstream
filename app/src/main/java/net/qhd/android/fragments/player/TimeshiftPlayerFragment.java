package net.qhd.android.fragments.player;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.jtv.android.models.timeshift.Range;
import com.jtv.android.models.timeshift.StreamInfo;
import com.jtv.android.utils.SimpleCallback;
import com.jtv.player.JTVExoPlayer;
import com.jtv.player.JTVPlayer;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.views.TimeshiftProgressDrawable;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

import static android.view.KeyEvent.ACTION_DOWN;

public class TimeshiftPlayerFragment extends PlayerFragment implements TimePickerDialog.OnTimeSetListener, GestureDetector.OnGestureListener {

    public static final String ARG_TIMESTAMP_BEGIN = "timestamp_begin";
    public static final String ARG_STREAM_NAME = "stream_name";
    public static final String ARG_SEEK_TO = "seekTo";

    private long begining;
    private long seekTo;
    private String streamName;
    private StreamInfo streamInfo;
    private Range currentRange;

    private float scrollOffset = 0;
    private GestureDetectorCompat gestureDetector;
    private Handler handler;
    private Runnable scrollApplyRunnable = new OffsetRunnable();
    private TextView offsetView;

    public static TimeshiftPlayerFragment timeshiftPlayer(String name, String url, long begin) {
        TimeshiftPlayerFragment playerFragment = new TimeshiftPlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        bundle.putString(ARG_STREAM_NAME, name);
        bundle.putLong(ARG_TIMESTAMP_BEGIN, begin);
        playerFragment.setArguments(bundle);
        return playerFragment;
    }

    public static TimeshiftPlayerFragment timeshiftPlayer(String name, String url, long begin, long seekTo) {
        TimeshiftPlayerFragment playerFragment = new TimeshiftPlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        bundle.putString(ARG_STREAM_NAME, name);
        bundle.putLong(ARG_TIMESTAMP_BEGIN, begin);
        bundle.putLong(ARG_SEEK_TO, seekTo);
        playerFragment.setArguments(bundle);
        return playerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        gestureDetector = new GestureDetectorCompat(getActivity(), this);
    }

    @Override
    protected void parseArguments(Bundle arg) {
        super.parseArguments(arg);
        streamName = arg.getString(ARG_STREAM_NAME);
        begining = arg.getLong(ARG_TIMESTAMP_BEGIN, 0);
        seekTo = arg.getLong(ARG_SEEK_TO, -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        seekBarTime.setVisibility(View.GONE);
        seekBarDuration.setClickable(true);
        seekBarDuration.setFocusable(true);
        offsetView = new TextView(getActivity());
        offsetView.setBackgroundResource(R.color.black_transparent);
        offsetView.setPadding(16, 16, 16, 16);
        offsetView.setVisibility(View.GONE);
        offsetView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
        frameLayout.addView(offsetView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        surfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
        controls.setVisibility(View.GONE);
        networkManager.getAvailableTimeshift(streamName, Long.toString(begining), new SimpleCallback<List<StreamInfo>>() {
            @Override
            public void onResponse(Call<List<StreamInfo>> call, Response<List<StreamInfo>> response) {
                if (response.isSuccessful()) {
                    List<StreamInfo> body = response.body();
                    streamInfo = body.get(0);
                    try {
                        Log.d(TAG, "onResponse: first: " + streamInfo.getRanges().get(0).toString());
                        Log.d(TAG, "onResponse: last: " + streamInfo.getRanges().get(streamInfo.getRanges().size() - 1).toString());
                        Log.d(TAG, "onResponse: current time: " + Calendar.getInstance().getTimeInMillis());
                    } catch (IndexOutOfBoundsException ignored) {
                    }
                    seekbar.setProgressDrawable(new TimeshiftProgressDrawable(seekbar, streamInfo.getRanges()));
                }
            }
        });
        return view;
    }

    @Override
    protected void setResizeMode(AspectRatioFrameLayout frameLayout) {
        frameLayout.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        int aspectRatio = settingsManager.getAspectRatio();
        switch (aspectRatio) {
            case 0: //Auto
                break;
            case 1:
                frameLayout.setAspectRatio(4f / 3f);
                break;
            case 2:
                frameLayout.setAspectRatio(16f / 9f);
                break;
            case 3:
                frameLayout.setAspectRatio(21f / 9f);
        }
    }

    @NonNull
    @Override
    protected JTVPlayer getPlayer() {
        return new JTVExoPlayer(getActivity(), new Handler(), BuildConfig.DEBUG);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        showController();
        if (keyEvent.getAction() == ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                selectTimeshift();
                return true;
            }
        }
        return super.onKey(view, keyCode, keyEvent);
    }

    @OnClick(R.id.seekbar_duration)
    public void onDurationClick() {
        selectTimeshift();
    }

    private void selectTimeshift() {
        Calendar current = Calendar.getInstance();
        long timeInMillis = current.getTimeInMillis();
        long difference = timeInMillis - (begining * 1000 + seekbar.getProgress());

        int hours = (int) TimeUnit.MILLISECONDS.toHours(difference);
        int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(difference)));
        TimePickerDialog dialog = new TimePickerDialog(getActivity(), this, hours, minutes,
                DateFormat.is24HourFormat(getActivity()));
        dialog.show();
    }

    @Override
    public void onStateChanged(int state) {
        if (state == JTVPlayer.STATE_READY) {
            progressBar.setVisibility(View.GONE);
            Log.d(TAG, "Player ready: " + streamUrl);
            Range last = streamInfo.getRanges().get(streamInfo.getRanges().size() - 1);
            Range first = streamInfo.getRanges().get(0);
            seekbar.setMax((int) Math.max((last.getEnd() - first.getFrom()) * 1000, player.getDuration() + streamInfo.getMissingTimeTo(last) * 1000));
            showController();
            if (!prepared && seekTo != -1 && seekTo >= begining * 1000) {
                long target = seekTo - streamInfo.getRanges().get(0).getFrom() * 1000;

                Range rangeByTime = streamInfo.getRangeByTime(target);
                if (rangeByTime == null) {
                    rangeByTime = streamInfo.getRangeIfAtMissing(target);
                    Range range = streamInfo.getRanges().get(0);
                    player.seekTo((rangeByTime.getFrom() - range.getFrom() - streamInfo.getMissingTimeTo(rangeByTime)) * 1000);
                    return;
                }
                long missingTimeTo = streamInfo.getMissingTimeTo(rangeByTime) * 1000;
                currentRange = rangeByTime;
                player.seekTo(target - missingTimeTo);

            }
            prepared = true;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean isUser) {
        super.onProgressChanged(seekBar, progress, isUser);
        if (isUser) {
            Calendar current = Calendar.getInstance();
            long difference = current.getTimeInMillis() - (begining * 1000 + progress);
            updateDurationText(difference);
        }
    }

    private void updateDurationText(long difference) {
        if (difference < TimeUnit.MINUTES.toMillis(1)) {
            seekBarDuration.setText(" LIVE ");
        } else {
            seekBarDuration.setText(String.format(Locale.getDefault(),
                    "-%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(difference),
                    TimeUnit.MILLISECONDS.toMinutes(difference) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(difference))
            ));
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (streamInfo == null && player == null) {
            return;
        }
        if (seekBar.getProgress() > player.getDuration()) {
            player.seekTo(player.getDuration());
            return;
        }
        userSeeking = false;
        Range rangeByTime = streamInfo.getRangeByTime(seekBar.getProgress());
        if (rangeByTime == null) {
            rangeByTime = streamInfo.getRangeIfAtMissing(seekBar.getProgress());
            Range range = streamInfo.getRanges().get(0);
            seekBar.setProgress(((int) (rangeByTime.getFrom() - range.getFrom())));
            player.seekTo((rangeByTime.getFrom() - range.getFrom() - streamInfo.getMissingTimeTo(rangeByTime)) * 1000);
            return;
        }
        long missingTimeTo = streamInfo.getMissingTimeTo(rangeByTime);
        currentRange = rangeByTime;
        player.seekTo(seekBar.getProgress() - (missingTimeTo * 1000));
    }

    @Override
    protected Runnable getSeekbarUpdateRunnable() {
        return new SeekbarUpdateRunnable();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.d(TAG, "onTimeSet: " + hourOfDay + ":" + minute);
        long currentTime = Calendar.getInstance().getTimeInMillis();
        long selectedTime = currentTime
                - TimeUnit.HOURS.toMillis(hourOfDay) - TimeUnit.MINUTES.toMillis(minute)
                - (begining * 1000);
        selectedTime = Math.max(0, Math.min(seekbar.getMax(), selectedTime));
        seekbar.setProgress((int) selectedTime);
        Range rangeByTime = streamInfo.getRangeByTime(seekbar.getProgress());
        if (rangeByTime == null) {
            rangeByTime = streamInfo.getRangeIfAtMissing(seekbar.getProgress());
            Range range = streamInfo.getRanges().get(0);
            seekbar.setProgress(((int) (rangeByTime.getFrom() - range.getFrom())));
            player.seekTo((rangeByTime.getFrom() - range.getFrom() - streamInfo.getMissingTimeTo(rangeByTime)) * 1000);
            return;
        }
        long missingTimeTo = streamInfo.getMissingTimeTo(rangeByTime);
        currentRange = rangeByTime;
        player.seekTo(seekbar.getProgress() - (missingTimeTo * 1000));
        updateDurationText(currentTime - (selectedTime + (begining * 1000)));
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        offsetView.setVisibility(View.VISIBLE);
        scrollOffset -= distanceX / 10;
        int hours = (int) TimeUnit.MINUTES.toHours((long) Math.abs(scrollOffset));
        int minutes = (int) (Math.abs(scrollOffset) - TimeUnit.HOURS.toMinutes(hours));
        offsetView.setText(getString(R.string.offset_hours, scrollOffset < 0 ? "-" : "+", hours, minutes));
        handler.removeCallbacks(scrollApplyRunnable);
        handler.postDelayed(scrollApplyRunnable, 2000);
        Log.d(TAG, "onScroll: " + hours + "h" + minutes + "m");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public void onError(Exception exception) {
        super.onError(exception);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!prepared) {
                    preparePlayer();
                } else {
                    if (player != null && !player.isPlaying()) {
                        player.play(streamUrl);
                    }
                }
            }
        }, 1000);
    }

    private class SeekbarUpdateRunnable implements Runnable {

        @Override
        public void run() {
            if (streamInfo == null) {
                seekbar.postDelayed(this, 1000);
                return;
            }
            Range last = streamInfo.getRanges().get(streamInfo.getRanges().size() - 1);
            Range first = streamInfo.getRanges().get(0);
            seekbar.setMax((int) Math.max((last.getEnd() - first.getFrom()) * 1000, player.getDuration() + streamInfo.getMissingTimeTo(last) * 1000));
            if (prepared && !userSeeking && !seekbar.isFocused() && player != null) {
                if (currentRange == null || (seekbar.getProgress() / 1000f >= currentRange.getEnd() - first.getFrom())) {
                    Range range = currentRange = streamInfo.getRangeByTime(seekbar.getProgress());
                    if (range == null) {
                        range = currentRange = streamInfo.getRangeIfAtMissing(seekbar.getProgress());
                    }
                    currentRange = range;
                }
                seekbar.setProgress((int) (player.getPosition() + (streamInfo.getMissingTimeTo(currentRange) * 1000f)));
            }
            long diff = Calendar.getInstance().getTimeInMillis() - ((begining * 1000) + seekbar.getProgress());
            updateDurationText(diff);
            seekbar.postDelayed(this, 1000);
        }
    }

    private class OffsetRunnable implements Runnable {

        @Override
        public void run() {
            Log.d(TAG, "run: " + (int) scrollOffset);
            if (((int) scrollOffset) != 0) {
                seekbar.setProgress((int) (seekbar.getProgress() + TimeUnit.MINUTES.toMillis((long) scrollOffset)));
                onStopTrackingTouch(seekbar);
            }
            scrollOffset = 0;
            offsetView.setVisibility(View.GONE);
        }
    }

}
