package net.qhd.android.fragments.player;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.SettingsManager;
import com.jtv.player.JTVExoPlayer;
import com.jtv.player.JTVNativePlayer;
import com.jtv.player.JTVPlayer;
import com.jtv.player.exceptions.HttpException;
import com.memo.cable.MemoDeviceServiceHelper;
import com.memo.sdk.IMemoDeviceListener;
import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.TubiControllerActivity;

import org.cybergarage.upnp.Device;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.KeyEvent.ACTION_DOWN;
import static android.view.KeyEvent.KEYCODE_DPAD_CENTER;
import static android.view.KeyEvent.KEYCODE_DPAD_DOWN;
import static android.view.KeyEvent.KEYCODE_DPAD_LEFT;
import static android.view.KeyEvent.KEYCODE_DPAD_RIGHT;
import static android.view.KeyEvent.KEYCODE_DPAD_UP;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static android.view.KeyEvent.KEYCODE_MEDIA_PAUSE;
import static android.view.KeyEvent.KEYCODE_MEDIA_PLAY;
import static android.view.KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;
import static android.view.KeyEvent.KEYCODE_MEDIA_STOP;
import static com.jtv.player.JTVPlayer.PLAYER_EXO;
import static com.jtv.player.JTVPlayer.PLAYER_NATIVE;

public class PlayerFragment extends Fragment implements JTVPlayer.QHDListener, View.OnKeyListener, SeekBar.OnSeekBarChangeListener, IMemoDeviceListener {


    public static final String TAG = MoviePlayerFragment.class.getSimpleName();

    public static final int CONTROLLER_ANIMATION_DURATION = 250;
    protected static final String ARG_URL = "stream_url";
    private static final String TERA_TAG = "TeraCast";

    @BindView(R.id.root) AspectRatioFrameLayout frameLayout;
    @BindView(R.id.surface_view) SurfaceView surfaceView;
    @BindView(R.id.control_root) LinearLayout controlRoot;
    @BindView(R.id.seekbar) SeekBar seekbar;
    @BindView(R.id.seekbar_time) TextView seekBarTime;
    @BindView(R.id.seekbar_duration) TextView seekBarDuration;
    @BindView(R.id.control_play_pause) ImageButton buttonPlay;
    @BindView(R.id.control_backward) ImageButton buttonBackward;
    @BindView(R.id.control_rewind) ImageButton buttonRewind;
    @BindView(R.id.control_fast_forward) ImageButton buttonFastForward;
    @BindView(R.id.control_forward) ImageButton buttonForward;
    @BindView(R.id.progress) ProgressBar progressBar;
    @BindView(R.id.player_controls) View controls;

    @BindView(R.id.button_memo_cast) ImageButton buttonMemoCast;

    protected String streamUrl;
    protected JTVPlayer player;
    protected NetworkManager networkManager;

    private Handler handler = new Handler();
    private HideInterfaceRunnable hideInterfaceRunnable = new HideInterfaceRunnable();

    protected boolean prepared = false;
    protected boolean userSeeking = false;
    private boolean controlsShown = true;
    private boolean controlsFocus = false;
    protected MediaPlayerFragmentListener mediaListener;
    private int errorCount = 0;

    private View.OnKeyListener seekbarKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() == ACTION_DOWN) {
                if (i == KEYCODE_DPAD_UP) {
                    View view1 = getView();
                    if (view1 != null) {
                        view1.requestFocus();
                    }
                    controlsFocus = false;
                    return true;
                }
                if (i == KEYCODE_ENTER || i == KEYCODE_DPAD_CENTER) {
                    player.seekTo(seekbar.getProgress());
                    return true;
                }
            }
            return false;
        }
    };
    protected SettingsManager settingsManager;
    private Runnable seekbarUpdateRunnable;
    private int currentPlayerId = -1;

    public PlayerFragment() {
    }

    public static PlayerFragment videoPlayer(String url) {
        PlayerFragment playerFragment = new PlayerFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        playerFragment.setArguments(bundle);
        return playerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {
            parseArguments(arg);
        }
        BaseActivity activity = (BaseActivity) getActivity();
        networkManager = activity.getNetworkManager();
        settingsManager = activity.getSettingsManager();
    }

    protected void parseArguments(Bundle arg) {
        streamUrl = arg.getString(ARG_URL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_movie_player, container, false);
        ButterKnife.bind(this, root);
        setResizeMode(frameLayout);
        if (streamUrl != null) {
            preparePlayer();
        } else {
            Log.e(TAG, "Stream url is null.");
        }
        seekbar.setOnSeekBarChangeListener(this);
        seekbarUpdateRunnable = getSeekbarUpdateRunnable();
        seekbar.setOnKeyListener(seekbarKeyListener);
        return root;
    }

    protected Runnable getSeekbarUpdateRunnable() {
        return new SeekbarUpdateRunnable();
    }

    protected void setResizeMode(AspectRatioFrameLayout frameLayout) {
        frameLayout.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
    }


    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.setOnKeyListener(this);
        }
        if (player != null && prepared && !player.isPlaying()) {
            Log.d(TAG, "Resuming player");
            player.setPlayWhenReady(true);
        }
        seekbar.post(seekbarUpdateRunnable);

        if (((BaseActivity) getActivity()).getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.onResume(getActivity());
            MemoTVCastSDK.registerDeviceListener(this);
            MemoTVCastSDK.startSearchLiveDevice();
            buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        seekbar.removeCallbacks(seekbarUpdateRunnable);
        if (player != null && prepared && player.isPlaying()) {
            Log.d(TAG, "Pausing player");
            player.setPlayWhenReady(false);
            return;
        }
        if (player != null) {
            Log.d(TAG, "Stopping player");
            player.stop();
        }
        if (((BaseActivity) getActivity()).getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.unRegisterDeviceListener(this);
            MemoTVCastSDK.onPause(getActivity());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            Log.d(TAG, "Releasing player");
            player.release();
        }
//        if (mController != null) {
//            if (MemoDeviceServiceHelper.getInstance().getSelectedDevice() != null) {
//                threadPool.execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        mController.stop(MemoDeviceServiceHelper.getInstance().getSelectedDevice(), false);
//                    }
//                });
//            }
//        }
    }

    protected void preparePlayer() {
        Log.d(TAG, "Preparing player: " + streamUrl);
        if (player != null) player.release();
        player = getPlayer();
        player.prepare();
        player.setSurface(surfaceView);
        player.setPlayWhenReady(true);
        player.setListener(this);
        player.play(streamUrl);
    }

    @NonNull
    protected JTVPlayer getPlayer() {
        currentPlayerId = settingsManager.getPlayer();
        switch (currentPlayerId) {
            case PLAYER_NATIVE:
                return new JTVNativePlayer(getActivity(), BuildConfig.DEBUG);
            case PLAYER_EXO:
                return new JTVExoPlayer(getActivity(), new Handler(), BuildConfig.DEBUG);
            default:
                Log.e("QHDPlayer", "Player with id=" + currentPlayerId + " doesnt exist. Loading native player.");
                return new JTVNativePlayer(getActivity(), BuildConfig.DEBUG);
        }
    }

    @Override
    public void onStateChanged(int state) {
        if (state == JTVPlayer.STATE_READY) {
            progressBar.setVisibility(View.GONE);
            Log.d(TAG, "Player ready: " + streamUrl);
            seekbar.setMax((int) player.getDuration());
            seekBarDuration.setText(String.format(Locale.getDefault(),
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(seekbar.getMax()),
                    TimeUnit.MILLISECONDS.toSeconds(seekbar.getMax()) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekbar.getMax()))
            ));
            if (seekbar.getProgress() > 0) {
                player.seekTo(seekbar.getProgress());
            }
            prepared = true;
        }
    }

    @Override
    public void onError(Exception exception) {
        if (errorCount++ == 0) {
            progressBar.setVisibility(View.GONE);
        }
        if (exception instanceof HttpException) {
            HttpException httpException = (HttpException) exception;
            if (httpException.getStatus() == 500) {
                if (BuildConfig.DEBUG) {
                    Toast.makeText(getActivity(), "Internal server error.", Toast.LENGTH_SHORT).show();
                }
                getActivity().finish();
            }
        }
        Log.e(TAG, "Player fragment error", exception);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MediaPlayerFragmentListener) {
            mediaListener = ((MediaPlayerFragmentListener) context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mediaListener = null;
    }

    @Override
    public void onVideoSizeChanged(int width, int height, float pixelWidthHeightRatio) {
        frameLayout.setAspectRatio(((float) width) / ((float) height));
    }

    @OnClick(R.id.surface_view)
    public void onSurfaceClick() {
        showController();
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        showController();
        if (keyEvent.getAction() == ACTION_DOWN) {
            switch (keyCode) {
                case KEYCODE_MEDIA_PAUSE:
                    player.setPlayWhenReady(false);
                    updatePlayButton();
                    showController();
                    return true;
                case KEYCODE_MEDIA_PLAY:
                    player.setPlayWhenReady(true);
                    updatePlayButton();
                    showController();
                    return true;
                case KEYCODE_MEDIA_PLAY_PAUSE:
                    togglePlay();
                    showController();
                    return true;
                case KEYCODE_MEDIA_STOP:
                    if (player.isPlaying()) {
                        player.setPlayWhenReady(false);
                    }
                    showController();
                    return true;
                case KEYCODE_DPAD_RIGHT:
                    seekForward();
                    return true;
                case KEYCODE_DPAD_LEFT:
                    seekBack();
                    return true;
                case KEYCODE_DPAD_DOWN:
                    showController();
                    if (controlsShown) {
                        seekbar.requestFocus();
                    }
                    controlsFocus = true;
                    return true;
            }
        }
        return false;
    }

    @OnClick(R.id.control_backward)
    public void seekBack() {
        if (player != null && prepared) {
            long seekTo = Math.max(0, player.getPosition() - 10000);
            player.seekTo(seekTo);
        }
        showController();
    }

    @OnClick(R.id.control_forward)
    public void seekForward() {
        if (player != null && prepared) {
            long seekTo = Math.min(player.getDuration() - 1, player.getPosition() + 10000);
            player.seekTo(seekTo);
        }
        showController();
    }

    @OnClick(R.id.control_play_pause)
    public void togglePlay() {
        if (player != null) {
            final boolean isPlaying = player.isPlaying();
            if (isPlaying) {
                player.setPlayWhenReady(false);
            } else {
                player.setPlayWhenReady(true);
            }
        }
        updatePlayButton();
    }

    private void updatePlayButton() {
        buttonPlay.setImageResource(player != null && player.isPlaying() ? R.drawable.ic_pause : R.drawable.ic_play);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean isUser) {
        //Set time with format mm:ss/mm:ss
        seekBarTime.setText(String.format(Locale.getDefault(),
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()),
                TimeUnit.MILLISECONDS.toSeconds(seekBar.getProgress()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekBar.getProgress()))));

        seekBarDuration.setText(String.format(Locale.getDefault(),
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(seekbar.getMax()),
                TimeUnit.MILLISECONDS.toSeconds(seekbar.getMax()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seekbar.getMax()))
        ));

        if (isUser) {
            showController();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        userSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
        userSeeking = false;
        player.seekTo(seekBar.getProgress());
    }

    public void hideController() {
        if (controlsShown) {
            controlRoot.animate().cancel();
            controlRoot.animate()
                    .alpha(0f)
                    .translationY(30f)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setDuration(CONTROLLER_ANIMATION_DURATION)
                    .start();
            controlsShown = false;
            View view = getView();
            if (view != null) {
                view.requestFocus();
            }
        }
    }

    public void showController() {
        if (!controlsShown) {
            controlRoot.animate().cancel();
            controlRoot.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setDuration(CONTROLLER_ANIMATION_DURATION)
                    .start();
            controlsShown = true;
        }
        handler.removeCallbacks(hideInterfaceRunnable);
        if (!controlsFocus) {
            if (player != null) {
                if (player.isPlaying()) {
                    handler.postDelayed(hideInterfaceRunnable, 5000);
                }
            } else {
                handler.postDelayed(hideInterfaceRunnable, 5000);
            }
        }
    }

    @OnClick(R.id.button_memo_cast)
    public void onCast() {
        startActivity(TubiControllerActivity.castMovie(getActivity(), networkManager.appendTokenToUrl(streamUrl), "Title"));
    }

    @Override
    public void onDeviceAdd(Device device) {
        Log.i(TERA_TAG, "onDeviceAdd: " + device + ", device count: " + MemoDeviceServiceHelper.getInstance().getDevices().size());
        buttonMemoCast.post(new Runnable() {
            @Override
            public void run() {
                buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onDeviceRemove(Device device) {
        Log.i(TERA_TAG, "onDeviceRemove: " + device + ", device count: " + MemoDeviceServiceHelper.getInstance().getDevices().size());
        buttonMemoCast.post(new Runnable() {
            @Override
            public void run() {
                buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onDeviceCheated(String s, String s1) {

    }


    public interface MediaPlayerFragmentListener {
        void onMediaEnded();
    }

    class SeekbarUpdateRunnable implements Runnable {

        @Override
        public void run() {
            seekbar.setMax(((int) player.getDuration()));
            if (prepared && !userSeeking && !seekbar.isFocused() && player != null) {
                seekbar.setProgress((int) player.getPosition());
            }
            seekbar.postDelayed(this, 1000);
        }
    }

    class HideInterfaceRunnable implements Runnable {

        @Override
        public void run() {
            if (player == null) {
                hideController();
            } else {
                if (player.isPlaying()) {
                    hideController();
                }
            }
        }
    }

}
