package net.qhd.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.jtv.android.models.timeshift.StreamInfo;
import com.jtv.android.network.NetworkManager;
import com.jtv.player.trackselection.OnTrackChangedListener;
import com.jtv.player.trackselection.Track;
import com.memo.cable.MemoDeviceServiceHelper;
import com.memo.sdk.IMemoDeviceListener;
import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import org.cybergarage.upnp.Device;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelButtonsFragment extends Fragment implements IMemoDeviceListener, OnTrackChangedListener {

    public static final String TAG = "ChannelButtons";
    private static final String TERA_TAG = "TeraCast";
    @BindView(R.id.button_root) LinearLayout root;

    @BindView(R.id.button_favorite) View buttonFavorite;
    @BindView(R.id.button_favorite_icon) ImageView favoriteIcon;
    @BindView(R.id.button_favorite_text) TextView favoriteText;

    @BindView(R.id.button_timeshift) View buttonTimeshift;
    @BindView(R.id.buttom_timeshift_text) TextView buttonTimeshiftText;
    @BindView(R.id.button_epg) View buttonEpg;
    @BindView(R.id.button_memo_cast) View buttonMemoCast;
    @BindView(R.id.button_track) View buttonTrack;

    @BindView(R.id.button_epg_text) TextView buttonEpgText;
    @BindView(R.id.button_track_text) TextView buttonTrackText;

    private ChannelButtonPressed listener;
    private NetworkManager networkManager;
    private Channel channel;

    public ChannelButtonsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_channel_buttons, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View.OnKeyListener listener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN || keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    ChannelButtonsFragment.this.listener.onFocusLost();
                }
                return false;
            }
        };
        buttonFavorite.setOnKeyListener(listener);
        buttonEpg.setOnKeyListener(listener);
        buttonTimeshift.setOnKeyListener(listener);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((BaseActivity) getActivity()).getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.onResume(getActivity());
            MemoTVCastSDK.registerDeviceListener(this);
            MemoTVCastSDK.startSearchLiveDevice();
            buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (((BaseActivity) getActivity()).getSettingsManager().isMemoCastEnabled()) {
            MemoTVCastSDK.unRegisterDeviceListener(this);
            MemoTVCastSDK.onPause(getActivity());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChannelButtonPressed) {
            listener = (ChannelButtonPressed) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ChannelButtonPressed");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void requestFocus() {
        root.requestFocus();
    }

    public void setFavorited(boolean favorited) {
        if (favorited) {
            favoriteIcon.setImageResource(R.drawable.ic_favorite_remove);
            favoriteText.setText(R.string.favorite_remove);
        } else {
            favoriteIcon.setImageResource(R.drawable.ic_favorite);
            favoriteText.setText(R.string.favorite_add);
        }
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
        if (channel == null) {
            buttonFavorite.setVisibility(View.GONE);
            buttonTimeshift.setVisibility(View.GONE);
            buttonEpg.setVisibility(View.GONE);
            return;
        }
        setFavorited(channel.isFavorite());
        buttonFavorite.setVisibility(View.VISIBLE);
        buttonEpgText.setText(getString(R.string.catch_up));
        buttonEpg.setVisibility(View.VISIBLE);
        Calendar current = Calendar.getInstance();
        buttonTimeshift.setVisibility(channel.getAvailableTimeshift() < 0 ? View.GONE : View.VISIBLE);
        if (channel.getAvailableTimeshift() < 0) {
            buttonEpgText.setText(getString(R.string.epg));
        }
        if (channel.getAvailableTimeshift() >= 0) {
            long hoursAvailable = (current.getTimeInMillis() / 1000 - channel.getAvailableTimeshift()) / 3600;
            buttonTimeshiftText.setText(getString(R.string.timeshift) + " (" + hoursAvailable + "h)");
        }
        buttonTrack.setVisibility(View.GONE);

        if (isDetached()) {
            return;
        }
        networkManager.getAvailableTimeshift(channel.getStreamName(), new Callback<List<StreamInfo>>() {
            @Override
            public void onResponse(Call<List<StreamInfo>> call, Response<List<StreamInfo>> response) {
                if (response.isSuccessful() && isAdded()) {
                    List<StreamInfo> body = response.body();
                    if (body != null && body.size() > 0) {
                        StreamInfo streamInfo = body.get(0);
                        if (streamInfo.getError() == null) {
                            if (streamInfo.getRanges().size() > 0) {
                                Log.d(TAG, "onResponse: before" + ChannelButtonsFragment.this.channel.toString());
                                ChannelButtonsFragment.this.channel.setAvailableTimeshift(streamInfo.getRanges().get(0).getFrom());
                                Log.d(TAG, "onResponse: after" + ChannelButtonsFragment.this.channel.toString());
                                Log.d(TAG, "onResponse: stream info: " + streamInfo);
                                buttonTimeshift.setVisibility(View.VISIBLE);
                                Calendar current = Calendar.getInstance();
                                long hoursAvailable = (current.getTimeInMillis() / 1000 - ChannelButtonsFragment.this.channel.getAvailableTimeshift()) / 3600;
                                buttonTimeshiftText.setText(getString(R.string.timeshift) + " (" + hoursAvailable + "h)");
                                buttonEpgText.setText(getString(R.string.catch_up));
                            } else {
                                buttonEpgText.setText(getString(R.string.epg));
                            }
                        } else {
                            Log.e(TAG, "getAvailableTimeshift: " + streamInfo.getError());
                        }
                    }
                }
                Log.d(TAG, "onResponse: " + response.toString());
            }

            @Override
            public void onFailure(Call<List<StreamInfo>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    @OnClick(R.id.button_favorite)
    public void onFavoriteClick() {
        Log.d(TAG, "onFavoriteClick");
        if (listener != null) {
            Log.d(TAG, "onFavoriteClick listened not null");
            listener.onFavoritePressed(channel);
        }
    }

    @OnClick(R.id.button_timeshift)
    public void onTimeshiftClick() {
        if (listener != null) {
            listener.onTimeshiftPressed(channel);
        }
    }

    @OnClick(R.id.button_epg)
    public void onEpgClick() {
        if (listener != null) {
            listener.onEpgPressed(channel);
        }
    }

    @OnClick(R.id.button_memo_cast)
    public void onCast() {
        Log.i(TERA_TAG, "onCast button pressed");
        if (MemoDeviceServiceHelper.getInstance().getDevices().size() > 0) {
            Log.i(TERA_TAG, "onCast theres devices available");
            if (listener != null) {
                Log.i(TERA_TAG, "onCast callback");
                listener.onCast();
            }
        } else {
            buttonMemoCast.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.button_track)
    public void onTrackSelect() {
        if (listener != null) {
            listener.onTrackSelectPressed();
        }
    }

    @Override
    public void onDeviceAdd(Device device) {
        Log.i(TERA_TAG, "onDeviceAdd: " + device + ", device count: " + MemoDeviceServiceHelper.getInstance().getDevices().size());
        buttonMemoCast.post(new Runnable() {
            @Override
            public void run() {
                buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onDeviceRemove(Device device) {
        Log.i(TERA_TAG, "onDeviceRemove: " + device + ", device count: " + MemoDeviceServiceHelper.getInstance().getDevices().size());
        buttonMemoCast.post(new Runnable() {
            @Override
            public void run() {
                buttonMemoCast.setVisibility(MemoDeviceServiceHelper.getInstance().getDevices().size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onDeviceCheated(String s, String s1) {

    }

    @Override
    public void onTrackChanged(Track track) {
        Log.d(TAG, "onTrackChanged: " + track.getName());
        buttonTrack.setVisibility(View.VISIBLE);
        buttonTrackText.setText(track.getName());
    }

    public interface ChannelButtonPressed {

        void onFavoritePressed(Channel channel);

        void onTimeshiftPressed(Channel channel);

        void onEpgPressed(Channel channel);

        void onCast();

        void onFocusLost();

        void onTrackSelectPressed();

    }
}
