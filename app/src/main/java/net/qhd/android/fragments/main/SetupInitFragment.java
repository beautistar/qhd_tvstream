package net.qhd.android.fragments.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qhd.android.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetupInitFragment extends SetupFragment {

    public SetupInitFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_setup_init, container, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }


    @OnClick(R.id.button_next)
    public void onClick() {
        next();
    }
}
