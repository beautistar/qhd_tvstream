package net.qhd.android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.jtv.android.utils.SettingsManager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.fragments.settings.LanguageSelectionFragment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LanguageSpinnerFragment extends Fragment {

    private static final String TAG = LanguageSelectionFragment.class.getSimpleName();
    private OnLanguageChangedListener mListener;

    public static final Map<String, Integer> icons = new HashMap<>();

    static {
        icons.put("en", R.drawable.ic_flag_en);
        icons.put("lt", R.drawable.ic_flag_lt);
        icons.put("ru", R.drawable.ic_flag_ru);
        icons.put("fr", R.drawable.ic_flag_fr);
    }

    private String[] langNames;
    private String[] langValues;

    private Spinner languageSpinner;

    private SettingsManager settingsManager;


    public LanguageSpinnerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_language_spinner, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingsManager = ((BaseActivity) getActivity()).getSettingsManager();

        langNames = getResources().getStringArray(R.array.languages);
        langValues = getResources().getStringArray(R.array.languages_values);
        languageSpinner = view.findViewById(R.id.lang_spinner);
        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String current = settingsManager.getLocale();
                String selected = langValues[position];
                if (!current.equals(selected) && mListener != null) {
                    settingsManager.setLocale(selected);
                    mListener.onLocaleChanged(selected);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (langNames.length != langValues.length) {
            Log.e(TAG, "onCreate: Languages names and values array sizes not matching");
            languageSpinner.setVisibility(View.GONE);
        } else {
            LangArrayAdapter adapter = new LangArrayAdapter(getContext(),
                    R.layout.layout_lang_row,
                    R.id.lang_name,
                    R.id.lang_icon, langNames);
            languageSpinner.setAdapter(adapter);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        int position = Arrays.asList(langValues).indexOf(settingsManager.getLocale());
        if (position > 0) {
            languageSpinner.setSelection(position);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLanguageChangedListener) {
            mListener = (OnLanguageChangedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLanguageChangedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnLanguageChangedListener {
        void onLocaleChanged(String locale);
    }


    private class LangArrayAdapter extends ArrayAdapter<String> {

        private final int iconResource;

        public LangArrayAdapter(Context context, int resource, int textViewResourceId, int iconViewResourceId, String[] objects) {
            super(context, resource, textViewResourceId, objects);
            iconResource = iconViewResourceId;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            ImageView icon = view.findViewById(iconResource);
            if (icon != null) {
                icon.setImageResource(icons.get(langValues[position]));
            }
            return view;
        }

        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            View view = super.getDropDownView(position, convertView, parent);
            ImageView icon = view.findViewById(iconResource);
            if (icon != null) {
                icon.setImageResource(icons.get(langValues[position]));
            }
            return view;
        }
    }

}
