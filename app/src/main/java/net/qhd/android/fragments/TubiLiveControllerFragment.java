package net.qhd.android.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.Database;
import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.adapters.ChannelAdapter;
import net.qhd.android.listeners.TubiCastContract;

import org.cybergarage.upnp.Device;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TubiLiveControllerFragment extends Fragment {

    private static final String ARG_CHANNEL_ID = "channelId";

    @BindView(R.id.category_name) TextView categoryName;
    @BindView(R.id.device_name) TextView deviceName;
    @BindView(R.id.channel_list) ListView channelList;
    @BindView(R.id.action_button) FloatingActionButton stop;


    private Category category;
    private Channel currentChannel;
    private ChannelAdapter adapter;
    private NetworkManager networkManager;
    private TubiCastContract listener;

    public TubiLiveControllerFragment() {
        // Required empty public constructor
    }

    public static TubiLiveControllerFragment newInstance(long channelId) {
        TubiLiveControllerFragment fragment = new TubiLiveControllerFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CHANNEL_ID, channelId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkManager = ((BaseActivity) getActivity()).getNetworkManager();
        if (getArguments() != null) {
            long channelId = getArguments().getLong(ARG_CHANNEL_ID, -1);
            if (channelId != -1) {
                currentChannel = Database.getChannel(channelId);
                category = currentChannel.getCategory();
            }
        }
        MemoTVCastSDK.startSearchLiveDevice();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_tubi_controller, container, false);
        ButterKnife.bind(this, root);
        categoryName.setText(category.getName());
        adapter = new ChannelAdapter(getActivity());
        adapter.setChannels(category.getChannels(), null);
        channelList.setAdapter(adapter);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.stop();
                stop.hide();
                adapter.setSelected(-1);
                adapter.setFocused(-1);
                channelList.invalidateViews();
            }
        });
        channelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.setSelected(i);
                adapter.setFocused(i);

                currentChannel = category.getChannels().get(i);
                listener.playChannel(currentChannel);

                stop.show();
                channelList.invalidateViews();
            }
        });

        for (int i = 0; i < category.getChannels().size(); i++) {
            if (category.getChannels().get(i).getRemoteId() == currentChannel.getRemoteId()) {
                adapter.setSelected(i);
                adapter.setFocused(i);
                scrollToItem(i);
                break;
            }
        }

        if (MemoTVCastSDK.getDevices().size() > 1 || MemoTVCastSDK.getSelectedDevice() == null) {
            final List<Device> devices = MemoTVCastSDK.getDevices();
            List<String> names = new ArrayList<>();
            for (Device device : devices) {
                names.add(device.getFriendlyName());
            }
            CustomDialogs.showListSelection(getActivity(),
                    getString(R.string.select_device),
                    names.toArray(new String[0]),
                    new CustomDialogs.OnItemSelected() {
                        @Override
                        public void onItemSelected(int which) {
                            MemoTVCastSDK.setSelectedDevice(devices.get(which));
                            deviceName.setText(MemoTVCastSDK.getSelectedDevice().getFriendlyName());
                            listener.playChannel(currentChannel);
                        }
                    });
        } else {
            deviceName.setText(MemoTVCastSDK.getSelectedDevice().getFriendlyName());
            listener.playChannel(currentChannel);
        }


        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TubiCastContract) {
            listener = (TubiCastContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChannelSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void scrollToItem(int item) {
        int count = adapter.getCount();
        if (item >= count) {
            item = 0;
        }
        channelList.setSelectionFromTop(item, channelList.getHeight() / 2);
        channelList.computeScroll();
    }

}
