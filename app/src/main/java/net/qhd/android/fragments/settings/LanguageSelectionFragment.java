package net.qhd.android.fragments.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.franmontiel.localechanger.LocaleChanger;
import com.franmontiel.localechanger.utils.ActivityRecreationHelper;
import com.jtv.android.utils.SettingsManager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageSelectionFragment extends Fragment {

    public static final String TAG = LanguageSelectionFragment.class.getSimpleName();

    public static final Map<String, Integer> icons = new HashMap<>();

    //http://365icon.com/icon-styles/ethnic/classic2/
    static {
        icons.put("en", R.drawable.ic_flag_en);
        icons.put("lt", R.drawable.ic_flag_lt);
        icons.put("ru", R.drawable.ic_flag_ru);
        icons.put("fr", R.drawable.ic_flag_fr);
    }

    private String[] langNames;
    private String[] langValues;


    @BindView(R.id.language_list) ListView list;
    private String locale;
    private BaseAdapter adapter;
    private SettingsManager settingsManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_language_selection, container, false);
        ButterKnife.bind(this, root);

        settingsManager = ((BaseActivity) getActivity()).getSettingsManager();
        locale = settingsManager.getLocale();

        langNames = getResources().getStringArray(R.array.languages);
        langValues = getResources().getStringArray(R.array.languages_values);
        if (langNames.length != langValues.length) {
            Log.e(TAG, "onCreate: Languages names and values array sizes not matching");
        } else {
            adapter = new BaseAdapter() {
                @Override
                public int getCount() {
                    return langValues.length;
                }

                @Override
                public Object getItem(int position) {
                    return langValues[position];
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {
                        convertView = ViewGroup.inflate(getActivity(), R.layout.layout_language_item, null);
                    }
                    ImageView icon = convertView.findViewById(R.id.language_icon);
                    icon.setImageResource(icons.get(langValues[position]));
                    TextView name = convertView.findViewById(R.id.language_name);
                    name.setText(langNames[position]);
                    RadioButton selected = convertView.findViewById(R.id.language_selected);
                    selected.setChecked(locale.equals(langValues[position]));
                    View.OnClickListener l = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!locale.equals(langValues[position])) {
                                settingsManager.setLocale(langValues[position]);
                                LocaleChanger.setLocale(new Locale(langValues[position]));
                                ActivityRecreationHelper.recreate(getActivity(), true);
//                                locale = langValues[position];
//                                adapter.notifyDataSetChanged();
//                                LocaleChanger.setLocale(new Locale(langValues[position]));
////                                settingsManager.setLocale(langValues[position]);
//                                getActivity().recreate();
                            }
                        }
                    };
                    selected.setOnClickListener(l);
                    convertView.setOnClickListener(l);
                    return convertView;
                }
            };
            list.setAdapter(adapter);
        }
        return root;
    }
}
