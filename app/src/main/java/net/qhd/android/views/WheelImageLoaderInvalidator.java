package net.qhd.android.views;

import android.graphics.Bitmap;
import android.view.View;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import kankan.wheel.widget.WheelView;

public class WheelImageLoaderInvalidator extends SimpleImageLoadingListener {

    private final WheelView wheelView;

    public WheelImageLoaderInvalidator(WheelView wheelView) {
        this.wheelView = wheelView;
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        wheelView.invalidate();
    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        wheelView.invalidate();
    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {
        wheelView.invalidate();
    }
}
