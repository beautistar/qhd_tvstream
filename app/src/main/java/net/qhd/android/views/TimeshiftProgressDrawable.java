package net.qhd.android.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.SeekBar;

import com.jtv.android.models.timeshift.Range;

import java.util.List;

public class TimeshiftProgressDrawable extends Drawable {

    private SeekBar seekBar;

    private List<Range> ranges;
    private long begining;
    private float duration;

    private Paint green;
    private Paint red;
    private Paint gray;

    public TimeshiftProgressDrawable(SeekBar seekBar, List<Range> ranges) {
        this.seekBar = seekBar;
        if (ranges != null && ranges.size() > 0) {
            begining = ranges.get(0).getFrom();
            Range last = ranges.get(ranges.size() - 1);
            long end = last.getFrom() + last.getDuration();
            duration = end - begining;
        }
        this.ranges = ranges;
        green = new Paint();
        green.setColor(Color.GREEN);
        red = new Paint();
        red.setColor(Color.RED);
        gray = new Paint();
        gray.setColor(Color.argb(150, 100, 100, 100));
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        final int height = canvas.getHeight();
        final int innerPadding = (int) (height / 2.4f);
        Rect padding = new Rect();
        getPadding(padding);
        Rect bounds = getBounds();
        final int width = (bounds.right - bounds.left) - padding.left - padding.right;

        boolean firstPart = true;
        float progress = ((float) seekBar.getProgress()) / ((float) seekBar.getMax());
        canvas.save();
        canvas.clipRect(0, padding.top + innerPadding, width * progress, height - padding.bottom - innerPadding);
        canvas.drawRect(padding.left, innerPadding, width - padding.right, height - innerPadding, red);
        for (Range range : ranges) {
            float start = range.getFrom() - begining;
            float x = start / duration * width;
            float end = range.getEnd() - begining;
            float xend = end / duration * width;
            canvas.drawRect(x, padding.top + innerPadding, xend, height - padding.bottom - innerPadding, green);
            if (end / duration > progress && firstPart) {
                canvas.restore();
                canvas.clipRect(width * progress, padding.top + innerPadding + 2, width, height - padding.bottom - innerPadding - 2);
                firstPart = false;
                canvas.drawRect(x, padding.top + innerPadding, xend, height - padding.bottom - innerPadding, green);
            }
        }
        canvas.drawRect(width * progress, padding.top + innerPadding, width, height - padding.bottom - innerPadding, gray);
        canvas.restore();


    }

    @Override
    public void setAlpha(@IntRange(from = 0, to = 255) int alpha) {
        green.setAlpha(alpha);
        red.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        green.setColorFilter(colorFilter);
        red.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}
