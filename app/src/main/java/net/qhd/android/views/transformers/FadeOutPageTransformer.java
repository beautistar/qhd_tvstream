package net.qhd.android.views.transformers;

import android.support.v4.view.ViewPager;
import android.view.View;

public class FadeOutPageTransformer implements ViewPager.PageTransformer {
    public void transformPage(View view, float position) {
        view.setTranslationX(view.getWidth() * -position);
        if (position <= -1) { // [-Infinity,-1)
            view.setVisibility(View.INVISIBLE);
        } else if (position < 1) { // [-1,1]
            view.setVisibility(View.VISIBLE);
            float fade = 1f - Math.abs(position);
            view.setAlpha(fade);
        } else { // (1,+Infinity]
            view.setVisibility(View.INVISIBLE);
            view.setTranslationX(view.getWidth());
        }

        // Alpha property is based on the view position.
        if (position <= -1.0F || position >= 1.0F) {
            view.setAlpha(0.0F);
            view.setVisibility(View.INVISIBLE);
        } else if (position == 0.0F) {
            view.setVisibility(View.VISIBLE);
            view.setAlpha(1.0F);
        } else { // position is between -1.0F & 0.0F OR 0.0F & 1.0F
            view.setVisibility(View.VISIBLE);
            view.setAlpha(1.0F - Math.abs(position));
        }

    }
}