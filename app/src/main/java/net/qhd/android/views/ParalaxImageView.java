package net.qhd.android.views;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ScrollView;

import com.fmsirvent.ParallaxEverywhere.PEWImageView;

public class ParalaxImageView extends PEWImageView implements ViewTreeObserver.OnScrollChangedListener, ViewTreeObserver.OnGlobalLayoutListener {

    private ViewTreeObserver observer = null;
    private ScrollView scrollView = null;
    private float scrollY;
    private int childHeight;
    private int screenSize;

    public ParalaxImageView(Context context, ScrollView scrollView) {
        super(context);
        this.scrollView = scrollView;
        this.observer = scrollView.getViewTreeObserver();
        this.childHeight = scrollView.getChildAt(0).getHeight();
        observer.addOnScrollChangedListener(this);
        observer.addOnGlobalLayoutListener(this);
    }

    public ParalaxImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ParalaxImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public ViewTreeObserver getViewTreeObserver() {
        if (observer != null) {
            if (observer.isAlive()) {
                return observer;
            }
        }
        return super.getViewTreeObserver();
    }

    public void setObserver(ViewTreeObserver observer) {
        this.observer = observer;
    }

    @Override
    public void getLocationOnScreen(int[] outLocation) {
        outLocation[1] = (int) (100 * (scrollY / (float) (childHeight - screenSize))) - 50;
    }

    @Override
    public void onScrollChanged() {
        this.scrollY = scrollView.getScrollY();
    }

    @Override
    public void onGlobalLayout() {
        this.childHeight = scrollView.getChildAt(0).getHeight();
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        this.screenSize = screenSize.y;
    }
}
