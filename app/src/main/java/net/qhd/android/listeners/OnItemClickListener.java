package net.qhd.android.listeners;

public interface OnItemClickListener {
    void onItemClick(int position);
}