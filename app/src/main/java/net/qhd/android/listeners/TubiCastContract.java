package net.qhd.android.listeners;

import com.jtv.android.models.Channel;

import net.qhd.android.activities.TubiControllerActivity;

public interface TubiCastContract {
    void playChannel(Channel channel);

    void stop();

    void seek(long miliseconds);

    void getDuration(TubiControllerActivity.ControllerResult<Long> result);

    void getPosition(TubiControllerActivity.ControllerResult<Long> result);

    void pause();

    void resume();

    boolean isPlaying();

    void play(String streamUrl, String streamName);
}