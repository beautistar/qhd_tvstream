package net.qhd.android.listeners;

import android.view.View;

import com.jtv.android.models.Channel;

public interface OnChannelLongClickListener {

    void onChannelLongClick(final Channel channel, View view, int position);

}
