package net.qhd.android.listeners;

import com.jtv.android.models.Channel;

public interface OnChannelClickListener {
    void onChannelClick(Channel item, int position);
}