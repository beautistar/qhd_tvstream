package net.qhd.android.listeners;

import com.jtv.android.models.Channel;

public interface OnChannelSelectedListener {

    void onChannelSelected(Channel channel);

}
