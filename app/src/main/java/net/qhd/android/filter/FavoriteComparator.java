package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public class FavoriteComparator extends BaseSorter {

    @Override
    public int compare(Channel o1, Channel o2) {
        int i = (o2.isFavorite() ? 1 : 0) - (o1.isFavorite() ? 1 : 0);
        if (and != null && i == 0) {
            return and.compare(o1, o2);
        }
        return i;
    }

}
