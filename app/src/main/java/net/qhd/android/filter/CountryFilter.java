package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public class CountryFilter extends BaseFilter {

    public CountryFilter(String filterValue) {
        super(filterValue);
    }

    @Override
    public boolean keep(Channel channel) {
        if (channel == null) return false;
        return channel.getCountry() != null
                && channel.getCountry().equals(getFilterValue());

    }
}
