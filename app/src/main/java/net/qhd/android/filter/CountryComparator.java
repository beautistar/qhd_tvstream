package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public class CountryComparator extends BaseSorter {

    private String country;

    public CountryComparator(String country) {
        this.country = country;
    }

    @Override
    public int compare(Channel a, Channel b) {
        String c1 = a.getCountry() == null ? "" : a.getCountry();
        String c2 = b.getCountry() == null ? "" : b.getCountry();

        boolean e1 = c1.equals(country);
        boolean e2 = c2.equals(country);

        if (e1 && e2 && and != null) {
            return and.compare(a, b);
        }

        if (e1) {
            return -1;
        }
        if (e2) {
            return 1;
        }

        if (and != null) {
            return and.compare(a, b);
        }

        return 0;
    }
}
