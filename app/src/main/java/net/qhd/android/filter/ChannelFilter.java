package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public interface ChannelFilter {

    boolean keep(Channel channel);

    String getFilterValue();

}
