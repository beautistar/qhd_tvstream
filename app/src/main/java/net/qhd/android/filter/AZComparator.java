package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public class AZComparator extends BaseSorter {

    @Override
    public int compare(Channel o1, Channel o2) {
        int i = o1.getName().compareTo(o2.getName());
        if (and != null && i == 0) {
            return and.compare(o1, o2);
        }
        return i;
    }

}
