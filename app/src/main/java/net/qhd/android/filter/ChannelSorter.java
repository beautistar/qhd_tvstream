package net.qhd.android.filter;

import com.jtv.android.models.Channel;

import java.util.Comparator;

public interface ChannelSorter extends Comparator<Channel> {

    ChannelSorter and(ChannelSorter sorter);

}
