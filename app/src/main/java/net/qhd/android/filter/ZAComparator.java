package net.qhd.android.filter;

import com.jtv.android.models.Channel;

public class ZAComparator extends BaseSorter {

    @Override
    public int compare(Channel o1, Channel o2) {
        int i = o2.getName().compareTo(o1.getName());
        if (and != null && i == 0) {
            return and.compare(o1, o2);
        }
        return i;
    }

}
