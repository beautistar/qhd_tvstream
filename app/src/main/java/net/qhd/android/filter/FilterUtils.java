package net.qhd.android.filter;

import android.content.Context;

import com.jtv.android.models.Channel;

import net.qhd.android.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilterUtils {

    public static final String[] getSorters(Context context) {
        return context.getResources().getStringArray(R.array.sort_methods);
    }

    public static ChannelSorter getSorter(int sorter) {
        switch (sorter) {
            case 1:
                return new AZComparator();
            case 2:
                return new ZAComparator();
            case 3:
                return new FavoriteComparator();
        }
        return null;
    }

    public static ChannelSorter getSorter(int sorter, String priorityCountry) {
        if (priorityCountry == null) {
            return getSorter(sorter);
        }
        return new CountryComparator(priorityCountry).and(getSorter(sorter));
    }

    public static final String[] getFilters(Context context) {
        return context.getResources().getStringArray(R.array.filter_method);
    }

    public static List<Channel> applyFilter(List<Channel> channels, ChannelFilter filter) {
        if (channels == null) return Collections.emptyList();
        List<Channel> filtered = new ArrayList<>();
        if (filter == null) {
            filtered.addAll(channels);
            return filtered;
        }
        for (Channel channel : channels) {
            if (filter.keep(channel)) {
                filtered.add(channel);
            }
        }
        return filtered;
    }

}
