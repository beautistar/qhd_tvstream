package net.qhd.android.filter;

public abstract class BaseSorter implements ChannelSorter {

    protected ChannelSorter and;

    @Override
    public ChannelSorter and(ChannelSorter sorter) {
        this.and = sorter;
        return this;
    }
}
