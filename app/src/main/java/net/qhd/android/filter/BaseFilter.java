package net.qhd.android.filter;

abstract public class BaseFilter implements ChannelFilter {

    private String filterValue;

    public BaseFilter(String filterValue) {
        this.filterValue = filterValue;
    }

    @Override
    public String getFilterValue() {
        return filterValue;
    }
}
