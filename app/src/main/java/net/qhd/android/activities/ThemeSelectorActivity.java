package net.qhd.android.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.SettingsManager;

import net.qhd.android.QHDApplication;
import net.qhd.android.R;
import net.qhd.android.activities.main.InitActivity;
import net.qhd.android.activities.main.MainMenuActivity;
import net.qhd.android.remake.RemakeActivity;

import java.util.ArrayList;
import java.util.List;

public class ThemeSelectorActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getNetworkManager().isLoggedIn()) {
            Intent intent = new Intent(this, InitActivity.class);
            openActivity(intent, R.anim.fade_in, R.anim.fade_out);
            finish();
            return;
        }
        SettingsManager settingsManager = getSettingsManager();
        int theme = settingsManager.getTheme();
        Intent intent;
        if (theme == 0) {
            intent = new Intent(this, RemakeActivity.class);
        } else {
            intent = new Intent(this, MainMenuActivity.class);
        }
        if (isMyLauncherDefault()) {
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        }
        startActivity(intent);
    }

    public QHDApplication getQHDApplication() {
        return ((QHDApplication) getApplication());
    }

    public NetworkManager getNetworkManager() {
        return getQHDApplication().getNetworkManager();
    }

    public SettingsManager getSettingsManager() {
        return getQHDApplication().getSettingsManager();
    }

    public void openActivity(Intent intent, int enterAnim, int exitAnim) {
        super.startActivity(intent);
        overridePendingTransition(enterAnim, exitAnim);
    }

    boolean isMyLauncherDefault() {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<>();
        filters.add(filter);

        final String myPackageName = getPackageName();
        List<ComponentName> activities = new ArrayList<>();
        final PackageManager packageManager = getPackageManager();

        // You can use name of your package here as third argument
        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

}
