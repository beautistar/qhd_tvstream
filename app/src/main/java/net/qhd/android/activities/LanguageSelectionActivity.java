package net.qhd.android.activities;

import com.franmontiel.localechanger.LocaleChanger;
import com.franmontiel.localechanger.utils.ActivityRecreationHelper;

import net.qhd.android.fragments.LanguageSpinnerFragment;

import java.util.Locale;

public class LanguageSelectionActivity extends BaseActivity implements LanguageSpinnerFragment.OnLanguageChangedListener {
    @Override
    public void onLocaleChanged(String locale) {
        LocaleChanger.setLocale(new Locale(locale));
        ActivityRecreationHelper.recreate(this, false);
    }
}
