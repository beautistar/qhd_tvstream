package net.qhd.android.activities.main;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.Server;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnLoginListener;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.ThemeSelectorActivity;
import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.services.SelectServerService;
import net.qhd.android.utils.RetrofitResponse;
import net.qhd.android.views.ProgressBarAnimation;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InitActivity extends BaseActivity implements OnLoginListener {

    public static final String TAG = InitActivity.class.getSimpleName();
    public static final String APP = "app";

    @BindView(R.id.text_loading) TextView textLoading;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private NetworkManager networkManager;
    private int attempts;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        ButterKnife.bind(this);
        networkManager = getNetworkManager();
        networkManager.setLoginListener(this);

        checkInternet();

        FirebaseAnalytics.getInstance(this).setUserProperty(APP, BuildConfig.APPLICATION_ID);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (networkManager != null) {
            networkManager.setLoginListener(null);
        }
        handler.removeCallbacksAndMessages(null);
    }

    private void setSeekProgress(final int progress) {
        setSeekProgress(progress, 300);
    }

    private void setSeekProgress(final int progress, final int duration) {
        progressBar.post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(progress, true);
                } else {
                    ProgressBarAnimation animation = new ProgressBarAnimation(progressBar, progress);
                    animation.setDuration(duration);
                    progressBar.startAnimation(animation);
                }
            }
        });
    }

    private void checkInternet() {
        textLoading.setText(R.string.info_checking_internet);
        attempts = 3;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (attempts-- == 0) {
                    Toast.makeText(InitActivity.this, R.string.error_check_internet_connection, Toast.LENGTH_SHORT).show();
                    closeActivity();
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    openActivity(intent, R.anim.fade_in, R.anim.fade_out);
                }
                if (isNetworkAvailable()) {
                    tryUpdate();
                } else {
                    handler.postDelayed(this, 2000);
                }
            }
        }, 2000);
    }

    private void tryUpdate() {
        setSeekProgress(10);
        networkManager.getAppUpdate(BuildConfig.APPLICATION_ID, BuildConfig.VERSION_CODE, new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String body = response.body();
                if (body != null && body.length() > 0 && body.charAt(0) == '1') {
                    closeActivity();
                    Intent intent = new Intent(InitActivity.this, UpdateActivity.class);
                    intent.putExtra("url", body);
                    openActivity(intent, R.anim.fade_in, R.anim.fade_out);
                } else {
                    tryLogin();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                tryLogin();
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "onFailure: " + t.toString());
                }
            }
        });
        textLoading.setText(R.string.info_getting_update);
    }

    private void tryLogin() {
        attempts = 3;
        setSeekProgress(30);
        networkManager.login();
        textLoading.setText(R.string.info_attempting_login);
    }

    @Override
    public void onLoginSuccess() {
        selectServer();
    }

    private void goToMainActivity() {
        closeActivity();
        Intent intent = new Intent(this, ThemeSelectorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        openActivity(intent, R.anim.fade_in, R.anim.fade_out);
        if (networkManager != null) {
            networkManager.setLoginListener(null);
        }
    }

    private void selectServer() {
        setSeekProgress(40);
        textLoading.setText(R.string.info_gettings_server_info);
        final Handler errorHandler = new Handler();
        networkManager.getServerList(new RetrofitResponse<List<Server>>() {

            @Override
            public void onResult(List<Server> body) {
                ActiveAndroid.beginTransaction();
                try {
                    new Delete().from(Server.class).execute();
                    if (body.size() > 0) {
                        body.get(0).setSelected(true);
                    }
                    for (Server server : body) {
                        server.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                } finally {
                    ActiveAndroid.endTransaction();
                }
                SelectServerService.start(InitActivity.this);
                updateCategories();
            }

            @Override
            public void onFail(List<Server> body, int code) {
                errorHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(InitActivity.this, R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                    }
                });
                setSeekProgress(99);
                goToMainActivity();
            }

            @Override
            public void onFailure(Call<List<Server>> call, Throwable t) {
                errorHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(InitActivity.this, R.string.error_cant_select_server, Toast.LENGTH_SHORT).show();
                    }
                });
                setSeekProgress(99);
                goToMainActivity();
            }
        });
    }

    private void updateCategories() {
        setSeekProgress(60);
        setSeekProgress(90, 2000);
        textLoading.post(new Runnable() {
            @Override
            public void run() {
                textLoading.setText(R.string.info_updating_channels);
            }
        });
        long lastUpdatedAt = getSettingsManager().getLastUpdatedAt();
        long current = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis();
        long diffMinutes = (current - lastUpdatedAt) / (60 * 1000) % 60;
        if (diffMinutes > 20) {
            networkManager.updateChannels(new NetworkManager.OnChannelUpdateCallback() {
                @Override
                public void onUpdated() {
                    setSeekProgress(99);
                    goToMainActivity();
                }

                @Override
                public void onFailed() {
                    goToMainActivity();
                }
            });
        } else {
            ChannelUpdateService.start(this, true);
            setSeekProgress(99);
            goToMainActivity();
        }
    }

    @Override
    public void onLoginError(final int errorMessage, final String body) {
        if (errorMessage == -1000) {
            textLoading.setText(R.string.login_retry);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (--attempts <= 0) {
                        Toast.makeText(InitActivity.this, getString(R.string.error_network_problems) + " " + body, Toast.LENGTH_SHORT).show();
                        goToLogin();
                        return;
                    }
                    networkManager.setLoginListener(InitActivity.this);
                    networkManager.login();
                }
            }, 1000);
        } else {
            Log.e(TAG, "onLoginError: " + errorMessage + "  " + body);
            setSeekProgress(99);
            goToLogin();
        }
    }

    public void goToLogin() {
        closeActivity();
        Intent intent = new Intent(this, LoginActivity.class);
        openActivity(intent, R.anim.fade_in, R.anim.fade_out);
        networkManager.setLoginListener(null);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (connectivityManager.isActiveNetworkMetered()) {
            Toast.makeText(this, R.string.info_metered_connection, Toast.LENGTH_SHORT).show();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
