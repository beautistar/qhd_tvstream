package net.qhd.android.activities.main;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jtv.android.models.Channel;
import com.jtv.android.models.Server;
import com.jtv.android.utils.Database;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import net.qhd.android.BuildConfig;
import net.qhd.android.QHDApplication;
import net.qhd.android.R;
import net.qhd.android.activities.LanguageSelectionActivity;
import net.qhd.android.activities.LiveTvActivity;
import net.qhd.android.activities.LiveTvTheme2Activity;
import net.qhd.android.activities.SettingsActivity;
import net.qhd.android.activities.movies.MovieCategoryActivity;
import net.qhd.android.fragments.ChannelRecentsFragment;
import net.qhd.android.fragments.ChannelSearchFragment;
import net.qhd.android.fragments.main.ChannelHorizontalListFragment;
import net.qhd.android.fragments.main.LiveTvCategoriesFragment;
import net.qhd.android.listeners.OnChannelSelectedListener;
import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.services.SelectServerService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static net.qhd.android.activities.SettingsActivity.PROP_SUBSCRIBED;

public class MainMenuActivity extends LanguageSelectionActivity implements ChannelHorizontalListFragment.InteractionListener, OnChannelSelectedListener {

    public static final String TAG = MainMenuActivity.class.getSimpleName();

    public static final String FRAGMENT_TAG = "livetv_categories";

    @BindView(R.id.menu_agent_logo) ImageView agentLogo;
    @BindView(R.id.settings) View settings;
    @BindView(R.id.apps) View apps;
    @BindView(R.id.debug_version) TextView debugVersion;

    long backLastTimePressed = 0;
    boolean liveTvFragment = false;

    private BroadcastReceiver serverSelectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Server selectedServer = Database.getSelectedServer();
            if (selectedServer != null) {
                debugVersion.setText(QHDApplication.APP_NAME + "\n" + selectedServer.getName() + " - " + selectedServer.getIP());
            } else {
                debugVersion.setText(QHDApplication.APP_NAME + "\n" + "No server selected");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getNetworkManager().isLoggedIn()) {
            Intent intent = new Intent(this, InitActivity.class);
            openActivity(intent, R.anim.fade_in, R.anim.fade_out);
            finish();
            return;
        }

        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);

        if (!isMyLauncherDefault()) {
            settings.setVisibility(View.GONE);
            apps.setVisibility(View.GONE);
        }

        debugVersion.setVisibility(BuildConfig.DEBUG ? View.VISIBLE : View.GONE);


        displayAgentLogo();
        ChannelUpdateService.start(this, false);

        findViewById(R.id.live_tv).requestFocus();

        if (getSettingsManager().checkSubscribtion()) {
            new AlertDialog.Builder(this).setTitle(R.string.subscribe_title)
                    .setMessage(R.string.subscribe_message)
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getSettingsManager().setSubscribed(false);
                            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(MainMenuActivity.this);
                            analytics.setUserProperty(PROP_SUBSCRIBED, Boolean.toString(false));
                        }
                    })
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseMessaging.getInstance().subscribeToTopic("news");
                            getSettingsManager().setSubscribed(true);
                            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(MainMenuActivity.this);
                            analytics.setUserProperty(PROP_SUBSCRIBED, Boolean.toString(true));
                        }
                    }).show();
        }

        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, null, null);
    }

    boolean isMyLauncherDefault() {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<>();
        filters.add(filter);

        final String myPackageName = getPackageName();
        List<ComponentName> activities = new ArrayList<>();
        final PackageManager packageManager = getPackageManager();

        // You can use name of your package here as third argument
        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }


    private void displayAgentLogo() {
        if (agentLogo != null) {
            ImageLoader.getInstance().displayImage(
                    getNetworkManager().appendTokenToUrl(BuildConfig.BASE_URL + "/app/agent.php?logo"),
                    agentLogo,
                    new ImageSize(agentLogo.getMaxWidth(), agentLogo.getMaxHeight()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int theme = getSettingsManager().getTheme();
        if (theme == 1 && liveTvFragment) {
            removeFragment();
        }
        if (BuildConfig.DEBUG) {
            Server selectedServer = Database.getSelectedServer();
            if (selectedServer != null) {
                debugVersion.setText(QHDApplication.APP_NAME + "\n" + selectedServer.getName() + " - " + selectedServer.getIP());
            } else {
                debugVersion.setText(QHDApplication.APP_NAME + "\n" + "No server selected");
            }
            registerReceiver(serverSelectedReceiver, new IntentFilter(SelectServerService.ACTION_SERVER_SELECTED));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            unregisterReceiver(serverSelectedReceiver);
        }
    }

    @OnClick(R.id.live_tv)
    void onLiveTvClick() {
        int theme = getSettingsManager().getTheme();
        if (theme == 1) {
            onLiveTvLongClick();
        } else {
            if (!liveTvFragment) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_root, new LiveTvCategoriesFragment(), FRAGMENT_TAG)
                        .commit();
                liveTvFragment = true;
            }
        }
    }

    @OnLongClick(R.id.live_tv)
    @SuppressWarnings("unused")
    boolean onLiveTvLongClick() {
        startActivity(new Intent(this, LiveTvActivity.class));
        return true;
    }

    @OnClick(R.id.vod)
    void onVodClick() {
        startActivity(new Intent(this, MovieCategoryActivity.class));
    }

    @OnClick(R.id.apps)
    void onAppsClick() {
        startActivity(new Intent(this, AppListActivity.class));
    }

    @OnClick(R.id.config)
    void onConfigClick() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @OnClick(R.id.settings)
    void onSettingsClick() {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }
        if (liveTvFragment) {
            removeFragment();
            return;
        }

        if (isMyLauncherDefault()) {
            return;
        }

        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        if (timeInMillis - 2000 < backLastTimePressed) { //If back is pressed in 2 seconds
            super.onBackPressed();
        }
        backLastTimePressed = timeInMillis;
        Toast.makeText(this, R.string.message_press_back_again, Toast.LENGTH_SHORT).show();
    }

    private void removeFragment() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        getSupportFragmentManager().beginTransaction().remove(fragmentByTag).commit();
        liveTvFragment = false;
    }

    @Override
    public void onChannelSelected(Channel item, int position) {
        if (BuildConfig.DEBUG) {
            Log.d("ChannelClick", item.toString());
        }
        Intent intent = new Intent(this, LiveTvTheme2Activity.class);
        intent.putExtra(LiveTvActivity.EXTRA_CATEGORY, item.getCategory().getRemoteId());
        intent.putExtra(LiveTvActivity.EXTRA_CHANNEL, item.getRemoteId());
        startActivity(intent);
    }

    @Override
    public void onSearch() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                .add(R.id.main_menu_root, new ChannelSearchFragment(), "search")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onSearch(Channel channel) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                .add(R.id.main_menu_root, ChannelSearchFragment.instance(channel.getCategory()), "search")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRecent() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                .add(R.id.main_menu_root, new ChannelRecentsFragment(), "search")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onChannelSelected(Channel item) {
        if (item != null) {
            if (BuildConfig.DEBUG) {
                Log.d("ChannelClick", item.toString());
            }
            Intent intent = new Intent(this, LiveTvTheme2Activity.class);
            intent.putExtra(LiveTvActivity.EXTRA_CATEGORY, item.getCategory().getRemoteId());
            intent.putExtra(LiveTvActivity.EXTRA_CHANNEL, item.getRemoteId());
            startActivity(intent);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }


}
