package net.qhd.android.activities.main;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateActivity extends BaseActivity {

    public static final String TAG = "UpdateActivity";
    @BindView(R.id.progressBar) ProgressBar progressBar;

    @BindView(R.id.update_text) TextView textView;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        textView.setText(getString(R.string.message_update, BuildConfig.APP_NAME));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String url = extras.getString("url");
            if (url != null) {
                if (url.charAt(0) == '1') {
                    url = url.substring(1);
                    File file = new File(getExternalCacheDir(), "app-release.apk");
                    DownloadFileFromURL downloadFileFromURL = new DownloadFileFromURL(file, new DownloadContract() {
                        @Override
                        public void onProgress(int progress) {
                            progressBar.setProgress(progress);
                        }

                        @Override
                        public void onFinished(File file) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                                Uri uri = FileProvider.getUriForFile(UpdateActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                                intent.setDataAndType(uri, "application/vnd.android.package-archive");
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.putExtra(Intent.EXTRA_PACKAGE_NAME, BuildConfig.APPLICATION_ID);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            finish();
                        }
                    });
                    downloadFileFromURL.execute(url);
                } else {
                    Log.e(TAG, "Url has to start with '1'");
                    finish();
                }
            }
        }
    }



    static class DownloadFileFromURL extends AsyncTask<String, Integer, String> {

        private final DownloadContract contract;
        private final File file;

        private DownloadFileFromURL(File file, DownloadContract contract) {
            this.file = file;
            this.contract = contract;
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(file, false);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress((int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("DownloadingAPK", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            if (contract != null){
                contract.onProgress(progress[0]);
            }
        }

        /**
         * After completing background task
         **/
        @Override
        protected void onPostExecute(String fileUrl) {
            if (contract != null){
                contract.onFinished(file);
            }
        }

    }

    interface DownloadContract{
        void onProgress(int progress);
        void onFinished(File file);
    }
}
