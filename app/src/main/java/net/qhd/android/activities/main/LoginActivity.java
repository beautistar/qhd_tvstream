package net.qhd.android.activities.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;

import com.google.firebase.analytics.FirebaseAnalytics;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.ThemeSelectorActivity;
import net.qhd.android.fragments.main.login.LoginCallback;
import net.qhd.android.fragments.main.login.LoginCodeFragment;
import net.qhd.android.fragments.main.login.LoginSelectorFragment;
import net.qhd.android.fragments.main.login.LoginUsernameFragment;
import net.qhd.android.fragments.main.login.RegisterFragment;
import net.qhd.android.fragments.main.login.RegistrationCallback;
import net.qhd.android.utils.GoogleUtils;

import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginCallback, RegistrationCallback, LoginSelectorFragment.OnLoginMethodSelected {

//    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 210;
    private boolean isLoggingIn = false;

//    private SmsReceiver.OnCodeReceivedListener listener = new SmsReceiver.OnCodeReceivedListener() {
//        @Override
//        public void onCodeReceived(String code) {
//            Log.d("SmsReceiver", "onCodeReceived: " + code);
//        }
//    };
//    private SmsReceiver smsReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        hideSoftInput();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new LoginSelectorFragment(), "selector")
                .commit();

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, null, null);
    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.d("LoginActivity", "onResume: registering sms receiver");
//        if(hasSmsPermission() && smsReceiver == null){
//            smsReceiver = new SmsReceiver(listener);
//            registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if(smsReceiver != null){
//            unregisterReceiver(smsReceiver);
//            smsReceiver = null;
//        }
//    }

    public void onSettingsClick() {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        startActivity(intent);
    }

    public void onLoginCode() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.root, new LoginCodeFragment(), "logincode")
                .addToBackStack(null)
                .commit();
    }

    public void onLoginUsername() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.root, new LoginUsernameFragment(), "loginusername")
                .addToBackStack(null)
                .commit();
    }

    public void onClickRegister() {
        GoogleUtils.checkAvailabillity(this, new GoogleUtils.Result() {
            @Override
            public void onComplete() {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.root, new RegisterFragment(), "register")
                        .addToBackStack(null)
                        .commit();
            }

            @Override
            public void onError() {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://q-hdtv.com/my-account/"));
                startActivity(intent);
            }
        });

    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, ThemeSelectorActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRegistered(String username, String phone, String email) {
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.root, LoginUsernameFragment.instance(username), "login")
                .addToBackStack(null)
                .commit();
//        getSupportFragmentManager()
//                .beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
//                .replace(R.id.root, PhoneVerificationFragment.instance(phone), "verifyphone")
//                .addToBackStack(null)
//                .commit();
    }

//    private boolean hasSmsPermission(){
//        return ContextCompat.checkSelfPermission(this,
//                "android.permission.RECEIVE_SMS")
//                == PackageManager.PERMISSION_GRANTED;
//    }
//
//    @Override
//    public void requestSmsPermission() {
//        if (!hasSmsPermission()) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    "android.permission.RECEIVE_SMS")) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(this,
//                        new String[]{"android.permission.RECEIVE_SMS"},
//                        MY_PERMISSIONS_REQUEST_RECEIVE_SMS);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_RECEIVE_SMS: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if(smsReceiver == null){
//                        smsReceiver = new SmsReceiver(listener);
//                        registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
//                    }
//                } else {
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }

    @Override
    public void onLoginFailed() {
        isLoggingIn = false;
    }

    @Override
    public void onLoginStarted() {
        isLoggingIn = true;
    }

    @Override
    public void onLoggedIn() {
        isLoggingIn = false;
        goToMainActivity();
    }

    @Override
    public void onBackPressed() {
        if (!isLoggingIn) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME && isLoggingIn) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onMethodSelected(@LoginSelectorFragment.MethodSelection int method) {
        switch (method) {
            case LoginSelectorFragment.METHOD_LOGIN_CODE:
                onLoginCode();
                break;
            case LoginSelectorFragment.METHOD_LOGIN_USERNAME:
                onLoginUsername();
                break;
            case LoginSelectorFragment.METHOD_REGISTER:
                onClickRegister();
                break;
            case LoginSelectorFragment.METHOD_SETTINGS:
                onSettingsClick();
                break;
        }
    }
}
