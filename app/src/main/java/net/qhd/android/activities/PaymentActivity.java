package net.qhd.android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.BuildConfig;
import com.jtv.android.models.PaypalInfo;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import net.qhd.android.R;
import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.utils.Analytics;
import net.qhd.android.utils.RetrofitResponse;

import org.json.JSONException;

import java.math.BigDecimal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT;
import static com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION;
import static com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID;

public class PaymentActivity extends BaseActivity implements Callback<String> {

    private static final String TAG = "PaymentActivity";

    /**
     * Paypal predefined codes
     */
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    private static final String EXTRA_ID = "id";
    private static final String EXTRA_DESCRIPTION = "description";
    private static final String EXTRA_TOTAL = "total";
    private static final String EXTRA_CURRENCY = "currency";
    public static final String EXTRA_SUBTOTAL = "subtotal";
    public static final String EXTRA_TAXES = "taxes";

    private String id;
    private String description;
    private String total;
    private String currency;
    private String subtotal;
    private String tax;

    public static Intent createIntent(Context context, String id, String description, String subtotal, String tax, String total, String currency) {
        Intent intent = new Intent(context, PaymentActivity.class);
        Bundle extras = new Bundle();
        extras.putString(EXTRA_ID, id);
        extras.putString(EXTRA_DESCRIPTION, description);
        extras.putString(EXTRA_SUBTOTAL, subtotal);
        extras.putString(EXTRA_TAXES, tax);
        extras.putString(EXTRA_TOTAL, total);
        extras.putString(EXTRA_CURRENCY, currency);
        intent.putExtras(extras);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getString(EXTRA_ID);
            description = extras.getString(EXTRA_DESCRIPTION);
            subtotal = extras.getString(EXTRA_SUBTOTAL);
            tax = extras.getString(EXTRA_TAXES);
            total = extras.getString(EXTRA_TOTAL);
            currency = extras.getString(EXTRA_CURRENCY);
        }

        if (id == null || description == null || total == null || currency == null) {
            Log.e(TAG, "Incorrect payment details");
            Toast.makeText(this, "Incorrect payment details", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (savedInstanceState == null) {
            getNetworkManager().getPaypalClientID(new RetrofitResponse<PaypalInfo>() {
                @Override
                public void onResult(PaypalInfo body) {
                    getQHDApplication().setPaypalClientId(body.getClientID());
                    PayPalConfiguration config = getQHDApplication().getPayPalConfiguration();
                    Intent intent = new Intent(PaymentActivity.this, PayPalService.class);
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                    startService(intent);
                    openPaypalActivity(config);
                }

                @Override
                public void onFail(PaypalInfo body, int code) {
                    Toast.makeText(PaymentActivity.this, R.string.error_unknown, Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onFailure(Call<PaypalInfo> call, Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    Toast.makeText(PaymentActivity.this, R.string.error_check_internet_connection, Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putAll(new Bundle());
    }

    public void openPaypalActivity(PayPalConfiguration config) {
        PayPalPayment thingToBuy = createPayment(subtotal, tax, total, currency, description, id, null);
        Intent intent = new Intent(this, com.paypal.android.sdk.payments.PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    /**
     * Creates payment to buy an item
     *
     * @param total       item total
     * @param currency    total currency
     * @param description item description
     * @param id          item id
     * @param email       agent paypal email
     * @return paypal payment object
     */
    private PayPalPayment createPayment(String subtotal,
                                        String tax,
                                        String total,
                                        String currency,
                                        String description,
                                        String id,
                                        @Nullable String email) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, total + "  " + description + "   " + id);
        }
        PayPalPayment payment = new PayPalPayment(new BigDecimal(total),
                currency,
                description,
                PayPalPayment.PAYMENT_INTENT_SALE);
        payment.custom(id);
        payment.enablePayPalShippingAddressesRetrieval(true);
        payment.paymentDetails(new PayPalPaymentDetails(new BigDecimal(0), new BigDecimal(subtotal), new BigDecimal(tax)));
        if (email != null) {
            payment.payeeEmail(email);
        }
        return payment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        String paymentId = confirm.getProofOfPayment().getPaymentId();
                        Log.d(TAG, paymentId);

                        getNetworkManager().purchaseConfirmation(this, paymentId);
                        Analytics.logTransaction(FirebaseAnalytics.getInstance(this), paymentId, total, currency);

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                        buildDialog(R.string.title_error, R.string.error_unknown);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
                finish();
            } else if (resultCode == RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                buildDialog(R.string.title_paypal_error, R.string.error_paypal_extras_invalid);
            }
        }
    }


    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        Log.d(TAG, "Confirmed " + response.body());
        if (response.isSuccessful()) {
            buildDialog(R.string.title_paypal_confirmed, R.string.message_paypal_confirmed);
            ChannelUpdateService.start(this, true);
        } else {
            switch (response.code()) {
                case 400: //Bad Request (paypal info wrong)
                    buildDialog(R.string.title_paypal_error, R.string.error_paypal_info_wrong);
                    break;
                case 401: //Unauthorized
                    buildDialog(R.string.title_error, R.string.error_unauthorized);
                    break;
                case 406: //Not Acceptable
                    buildDialog(R.string.title_paypal_error, R.string.error_paypal_info_wrong);
                    break;
                default:
                    buildDialog(R.string.title_error, R.string.error_unknown);
                    break;
            }

        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        Log.d(TAG, "Failed");
        t.printStackTrace();
        buildDialog(R.string.title_error, R.string.error_connection);
        finish();
    }

    void buildDialog(int title, int message) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                }).create();
        dialog.show();
    }

}
