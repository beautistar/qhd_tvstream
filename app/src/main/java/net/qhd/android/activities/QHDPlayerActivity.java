package net.qhd.android.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.movies.MovieDetailActivity;
import net.qhd.android.fragments.player.MoviePlayerFragment;
import net.qhd.android.fragments.player.PlayerFragment;
import net.qhd.android.fragments.player.TimeshiftPlayerFragment;
import net.qhd.android.utils.Analytics;

public class QHDPlayerActivity extends BaseActivity implements MoviePlayerFragment.MediaPlayerFragmentListener {

    public static final String TAG = MovieDetailActivity.class.getSimpleName();

    public static final String ACTION_VIEW_MOVIE = BuildConfig.APPLICATION_ID + ".actions.view_movie";
    public static final String ACTION_VIEW_TRAILER = BuildConfig.APPLICATION_ID + ".actions.view_trailer";
    public static final String ACTION_VIEW_TIMESHIFT = BuildConfig.APPLICATION_ID + ".actions.view_timeshift";

    public static final String EXTRA_IMDBID = "imdbid";
    public static final String EXTRA_STREAM_URL = "streamUrl";
    private static final String EXTRA_TIMESHIFT_BEGIN = "timeshift_begin";
    private static final String EXTRA_STREAM_NAME = "stream_name";
    public static final String EXTRA_SEEK_TO = "seekTo";
    private static final String ACTION_VIEW_STREAM = BuildConfig.APPLICATION_ID + ".actions.view_stream";

    private String streamUrl = "";
    private String streamName = "";
    private String imdbid;
    private boolean isTrailer;

    private Handler handler = new Handler();
    private Runnable mediaEndedRunnable = new Runnable() {
        @Override
        public void run() {
            setResult(RESULT_OK);
            finish();
        }
    };

    public static Intent getIntent(Context context, String streamUrl) {
        Intent intent = new Intent(context, QHDPlayerActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(streamUrl));
        return intent;
    }

    public static Intent getIntent(Context context, String streamUrl, String streamName, boolean isTrailer) {
        Intent intent = new Intent(context, QHDPlayerActivity.class);
        intent.setAction(isTrailer ? ACTION_VIEW_TRAILER : ACTION_VIEW_MOVIE);
        intent.putExtra(EXTRA_STREAM_URL, streamUrl);
        intent.putExtra(EXTRA_STREAM_NAME, streamName);
        return intent;
    }

    public static Intent getIntent(Context context, String streamUrl, String streamName, String imdbid, boolean isTrailer) {
        Intent intent = new Intent(context, QHDPlayerActivity.class);
        intent.setAction(isTrailer ? ACTION_VIEW_TRAILER : ACTION_VIEW_MOVIE);
        intent.putExtra(EXTRA_STREAM_URL, streamUrl);
        intent.putExtra(EXTRA_STREAM_NAME, streamName);
        intent.putExtra(EXTRA_IMDBID, imdbid);
        return intent;
    }

    public static Intent getTimeshiftIntent(Context context, String streamName, String streamUrl, long begin) {
        Intent intent = new Intent(context, QHDPlayerActivity.class);
        intent.setAction(ACTION_VIEW_TIMESHIFT);
        intent.setData(Uri.parse(streamUrl));
        intent.putExtra(EXTRA_TIMESHIFT_BEGIN, begin);
        intent.putExtra(EXTRA_STREAM_NAME, streamName);
        return intent;
    }

    public static Intent getTimeshiftIntent(Context context, String streamName, String streamUrl, long begin, long seekTo) {
        Intent intent = new Intent(context, QHDPlayerActivity.class);
        intent.setAction(ACTION_VIEW_TIMESHIFT);
        intent.setData(Uri.parse(streamUrl));
        intent.putExtra(EXTRA_TIMESHIFT_BEGIN, begin);
        intent.putExtra(EXTRA_STREAM_NAME, streamName);
        intent.putExtra(EXTRA_SEEK_TO, seekTo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_player);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, null, null);
        Analytics.logWatchMovie(mFirebaseAnalytics, imdbid, isTrailer);

        Intent intent = getIntent();
        String action = intent.getAction();
        Bundle extras = getIntent().getExtras();
        if (action != null) {
            switch (action) {
                case Intent.ACTION_VIEW:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment, PlayerFragment.videoPlayer(getIntent().getData().toString()))
                            .commit();
                    break;
                case ACTION_VIEW_MOVIE:
                    if (extras != null) {
                        streamUrl = extras.getString(EXTRA_STREAM_URL, "");
                        streamName = extras.getString(EXTRA_STREAM_NAME, "");
                        imdbid = extras.getString(EXTRA_IMDBID, "");
                        isTrailer = false;
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment, MoviePlayerFragment.newInstance(streamUrl, streamName, imdbid, true, 1f, isTrailer))
                            .commit();
                    break;
                case ACTION_VIEW_TRAILER:
                    if (extras != null) {
                        streamUrl = extras.getString(EXTRA_STREAM_URL, "");
                        imdbid = extras.getString(EXTRA_IMDBID, "");
                        streamName = extras.getString(EXTRA_STREAM_NAME, "");
                        isTrailer = true;
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment, MoviePlayerFragment.newInstance(streamUrl, streamName, imdbid, true, 1f, isTrailer))
                            .commit();
                    break;
                case ACTION_VIEW_TIMESHIFT:
                    getWindow().setBackgroundDrawableResource(R.drawable.bg);
                    long begin = extras.getLong(EXTRA_TIMESHIFT_BEGIN, 0);
                    long seekTo = extras.getLong(EXTRA_SEEK_TO, -1);
                    String name = extras.getString(EXTRA_STREAM_NAME);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment, TimeshiftPlayerFragment.timeshiftPlayer(name, getIntent().getData().toString(), begin, seekTo))
                            .commit();
                    break;
                default:
                    Toast.makeText(this, "Intent must have action", Toast.LENGTH_SHORT).show();
                    finish();
                    break;
            }
        }


    }

    @Override
    public void onBackPressed() {
        String action = getIntent().getAction();
        if (!isTrailer && !(action != null && (action.equals(Intent.ACTION_VIEW) || action.equals(ACTION_VIEW_TIMESHIFT)))) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.warning_exit_movie)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            QHDPlayerActivity.super.closeActivity(R.anim.fade_in, R.anim.fade_out);
                            setResult(RESULT_CANCELED);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            hideNavigationBar();
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            hideNavigationBar();
                        }
                    })
                    .create()
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigationBar();
        keepScreenOn(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        showNavigationBar();
        keepScreenOn(false);
        if (handler != null) {
            handler.removeCallbacks(mediaEndedRunnable);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler = null;
    }

    @Override
    public void onMediaEnded() {
        Log.d(TAG, "Media ended. Closing activity");
        if (handler != null) {
            handler.postDelayed(mediaEndedRunnable, 2000);
        }
    }
}
