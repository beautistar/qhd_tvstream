package net.qhd.android.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.franmontiel.localechanger.LocaleChanger;
import com.franmontiel.localechanger.utils.ActivityRecreationHelper;
import com.jtv.android.models.Notification;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.SettingsManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.qhd.android.BuildConfig;
import net.qhd.android.QHDApplication;
import net.qhd.android.R;
import net.qhd.android.activities.main.LoginActivity;
import net.qhd.android.activities.main.UpdateActivity;
import net.qhd.android.fragments.NotificationFragment;
import net.qhd.android.services.ChannelUpdateService;
import net.qhd.android.services.QHDFirebaseMessagingService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static net.qhd.android.services.QHDFirebaseMessagingService.FUNCTION_LOGOUT;
import static net.qhd.android.services.QHDFirebaseMessagingService.FUNCTION_UPDATE;

public class BaseActivity extends AppCompatActivity {

    private int previousUiOptions;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Notification notification = Notification.parseIntent(intent);

            Log.d("BaseActivity", "onReceive: Received broadcast with message = " + notification.getBody() + ".");
            String type = notification.getData().get("type");
            if (type == null || type.equals("normal")) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
                        .add(android.R.id.content, NotificationFragment.newInstance(notification), "notification")
                        .commit();
            } else if (type.equals("alert")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this)
                        .setTitle(notification.getTitle())
                        .setMessage(notification.getBody());
                switch (notification.getIcon()) {
                    case "warning":
                        builder.setIcon(R.drawable.ic_warning);
                        break;
                    case "danger":
                        builder.setIcon(R.drawable.ic_danger);
                        break;
                    default:
                        builder.setIcon(R.drawable.ic_info);
                }
                if (notification.getLink() != null) {
                    builder.setPositiveButton("Open Link", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(notification.getLink());
                            startActivity(intent);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                } else {
                    builder.setPositiveButton(R.string.ok, null);
                }
                builder.show();
            }
        }
    };

    private BroadcastReceiver functionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String function = intent.getStringExtra("function");
            switch (function) {
                case FUNCTION_LOGOUT:
                    terminateSession();
                    break;
                case FUNCTION_UPDATE:
                    forceUpdate();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNetworkManager().restoreInstance(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View decorView = getWindow().getDecorView();
        previousUiOptions = decorView.getSystemUiVisibility();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase);
    }

    public void setLanguage(String language) {
        getSettingsManager().setLocale(language);
        recreate();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getNetworkManager().saveInstance(outState);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        registerReceiver(notificationReceiver, new IntentFilter(QHDFirebaseMessagingService.BROADCAST_ACTION));
        registerReceiver(functionReceiver, new IntentFilter(QHDFirebaseMessagingService.BROADCAST_FUNCTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(notificationReceiver);
            unregisterReceiver(functionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    public QHDApplication getQHDApplication() {
        return ((QHDApplication) getApplication());
    }

    public NetworkManager getNetworkManager() {
        return getQHDApplication().getNetworkManager();
    }

    public SettingsManager getSettingsManager() {
        return getQHDApplication().getSettingsManager();
    }

    public void showNavigationBar() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(previousUiOptions);
    }

    public void hideNavigationBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            uiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideNavigationBar();
        }
    }

    public void hideSoftInput() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void registerChannelUpdateReciever(BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter(ChannelUpdateService.ACTION_UPDATED);
        registerReceiver(receiver, filter);
    }

    public void keepScreenOn(boolean keepOn) {
        if (keepOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void openActivity(Intent intent) {
        super.startActivity(intent);
    }

    public void openActivity(Intent intent, int enterAnim, int exitAnim) {
        super.startActivity(intent);
        overridePendingTransition(enterAnim, exitAnim);
    }

    public void closeActivity() {
        super.finish();
    }

    public void closeActivity(int enterAnim, int exitAnim) {
        super.finish();
        overridePendingTransition(enterAnim, exitAnim);
    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityRecreationHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityRecreationHelper.onDestroy(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
    }

    public void terminateSession() {
        stopService(new Intent(this, ChannelUpdateService.class));
        ImageLoader.getInstance().clearMemoryCache();
        Intent intent = new Intent(this.getApplicationContext(), LoginActivity.class);
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finishAffinity();
    }

    public void forceUpdate() {
        getNetworkManager().getAppUpdate(BuildConfig.APPLICATION_ID, BuildConfig.VERSION_CODE, new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String body = response.body();
                if (body != null && body.length() > 0 && body.charAt(0) == '1') {
                    Intent intent = new Intent(BaseActivity.this, UpdateActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("url", body);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finishAffinity();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });

    }

}
