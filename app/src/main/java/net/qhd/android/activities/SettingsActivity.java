package net.qhd.android.activities;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jtv.android.models.Package;
import com.jtv.android.models.Server;
import com.jtv.android.models.User;
import com.jtv.android.models.UserResponse;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.SettingsManager;
import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.BuildConfig;
import net.qhd.android.CustomDialogs;
import net.qhd.android.QHDApplication;
import net.qhd.android.R;
import net.qhd.android.activities.main.UpdateActivity;
import net.qhd.android.fragments.settings.AboutUsFragment;
import net.qhd.android.fragments.settings.LanguageSelectionFragment;
import net.qhd.android.fragments.settings.OrderPackageFragment;
import net.qhd.android.listeners.OnItemClickListener;
import net.qhd.android.services.ChannelUpdateService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class SettingsActivity extends BaseActivity implements OnItemClickListener {

    public static final String TAG = "SettingsActivity";
    public static final String EXTRA_PAGE = "page";
    public static final String PROP_SUBSCRIBED = "subscribe";

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);
                if (preference.getKey().equals("player")) {
                    FirebaseAnalytics.getInstance(preference.getContext()).setUserProperty("player", stringValue);
                }

            } else {
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    private FragmentManager supportFragmentManager;
    private int currentFragment = -1;
    private boolean inMultiWindowMode;
    private FirebaseRemoteConfig remoteConfig;

    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    public static Intent getIntent(Context context, int page) {
        Intent intent = new Intent(context, SettingsActivity.class);
        intent.putExtra(EXTRA_PAGE, page);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inMultiWindowMode = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            inMultiWindowMode = isInMultiWindowMode();
        }
        setContentView(inMultiWindowMode ? R.layout.activity_settings_multiscreen : R.layout.activity_settings);
        supportFragmentManager = getSupportFragmentManager();


        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, null, null);
        remoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        remoteConfig.setConfigSettings(configSettings);
        long cacheTime = 600; //10 minutes;
        if (configSettings.isDeveloperModeEnabled()) {
            cacheTime = 0;
        }
        remoteConfig.setDefaults(R.xml.remote_defaults);
        remoteConfig.fetch(cacheTime).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    remoteConfig.activateFetched();
                }
            }
        });

        if (!inMultiWindowMode) {
            int intExtra = getIntent().getIntExtra(EXTRA_PAGE, 0);
            onItemClick(intExtra);
            //changeFragment(new UserPreferenceFragment());
        }
        supportFragmentManager.beginTransaction()
                .replace(inMultiWindowMode ? R.id.fragment_root : R.id.headers, new HeaderListFragment())
                .commit();

    }

    @Override
    public void onItemClick(int position) {
        if (currentFragment == position && !inMultiWindowMode) return;
        switch (position) {
            case 0://User
                changeFragment(new UserPreferenceFragment());
                break;
            case 1://Playback
                changeFragment(new PlaybackPreferenceFragment());
                break;
            case 2://Languages
                changeFragment(new LanguageSelectionFragment());
                break;
            case 3://App config
                changeFragment(new ApplicationPreferenceFragment());
                break;
            case 4://System settings
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                openActivity(intent, R.anim.fade_in, R.anim.fade_out);
                break;
            case 5://Order
                if (!remoteConfig.getBoolean("disable_payments")) {
                    changeFragment(new OrderPackageFragment());
                } else {
                    new AlertDialog.Builder(this).setTitle(R.string.title_error)
                            .setMessage(R.string.error_payments_disabled)
                            .show();
                }
                break;
            case 6://About us
                changeFragment(new AboutUsFragment());
                break;
            case 7://Report problem
                Intent send = new Intent(Intent.ACTION_SENDTO);
                send.setType("text/plain");

                String uriText = "mailto:" + Uri.encode("info@q-hdtv.com") + "?subject=" + Uri.encode("Q-HD Application Report")
                        + "&body=" + Uri.encode("Please fill the points:") + "%0A" +
                        Uri.encode("1. What channel/movie were you watching?") + "%0A%0A" +
                        Uri.encode("2. What went wrong?") + "%0A%0A" +
                        Uri.encode("3. What were you trying to do?") + "%0A%0A" +
                        Uri.encode("Application version: " + BuildConfig.APPLICATION_ID + ":" + BuildConfig.VERSION_NAME);

                send.setData(Uri.parse(uriText));
                startActivity(Intent.createChooser(send, "Send Report"));
                break;
        }
        if (position != 7) {
            currentFragment = position;
        }
    }

    private void changeFragment(Fragment fragment) {
        boolean inMultiWindowMode = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            inMultiWindowMode = isInMultiWindowMode();
        }
        FragmentTransaction replace = supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.fragment_root, fragment);
        if (inMultiWindowMode) {
            replace.addToBackStack(null);
        }
        replace.commit();
    }

    public static class HeaderListFragment extends Fragment {

        @BindView(R.id.header_list) ListView headerList;
        private OnItemClickListener listener;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater,
                                 @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.fragment_settings_headers, container, false);
            ButterKnife.bind(this, root);
            String[] headers = getResources().getStringArray(R.array.pref_headers);
            headerList.setAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1,
                    headers));
            return root;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnItemClickListener) {
                listener = ((OnItemClickListener) context);
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            listener = null;
        }

        @OnItemClick(R.id.header_list)
        void onItemClick(int position) {
            if (listener != null) {
                listener.onItemClick(position);
            }
        }

    }

    public static class QHDPreferenceFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        }

        protected void setText(EditTextPreference edit, String value) {
            edit.setText(value);
            edit.setSummary(value);
        }

    }

    public static class AsyncLoadingPreferenceFragment extends QHDPreferenceFragment {

        private ProgressBar progressBar;
        private LinearLayout preferenceRoot;
        private boolean isCreated = false;
        private boolean isLoading = false;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            preferenceRoot = (LinearLayout) super.onCreateView(inflater, container, savedInstanceState);
            FrameLayout root = new FrameLayout(getContext());
            root.addView(preferenceRoot);
            progressBar = new ProgressBar(getContext());
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
            params.gravity = Gravity.CENTER;
            root.setLayoutParams(params);
            FrameLayout.LayoutParams progressBarParams = new FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            progressBarParams.gravity = Gravity.CENTER;
            progressBar.setLayoutParams(progressBarParams);
            root.addView(progressBar);
            isCreated = true;
            setLoading(isLoading);
            return root;
        }

        protected void setLoading(boolean loading) {
            if (isCreated) {
                if (loading) {
                    progressBar.setVisibility(View.VISIBLE);
                    preferenceRoot.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                    preferenceRoot.setVisibility(View.VISIBLE);
                }
            }
            isLoading = loading;
        }

    }

    public static class UserPreferenceFragment extends AsyncLoadingPreferenceFragment
            implements Callback<UserResponse>, Preference.OnPreferenceChangeListener {

        private Preference textUsername;
        private EditTextPreference textName;
        private Preference textMail;
        private NetworkManager networkManager;
        private Preference buttonLogout;
        private PreferenceCategory packagesCategory;
        private Preference textMovieTickets;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_user);
            if (getView() != null) {
                getView().setVisibility(View.INVISIBLE);
            }
            textUsername = findPreference("text_username");
            textName = (EditTextPreference) findPreference("text_name");
            textMail = findPreference("text_mail");
            buttonLogout = findPreference("button_logout");
            packagesCategory = (PreferenceCategory) findPreference("packages_list");
            textMovieTickets = findPreference("text_movie_ticket");

            buttonLogout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ((QHDApplication) getActivity().getApplication())
                            .getNetworkManager()
                            .logout(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    if (response.isSuccessful()) {
                                        ((BaseActivity) getActivity()).terminateSession();
                                    } else {
                                        showFailMessage();
                                    }
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    showFailMessage();
                                }

                                private void showFailMessage() {
                                    Toast.makeText(getActivity(), "Failed to logout.", Toast.LENGTH_SHORT).show();
                                }
                            });
                    return true;
                }
            });

            textName.setOnPreferenceChangeListener(this);

            networkManager = ((QHDApplication) getActivity().getApplication()).getNetworkManager();
            networkManager.getUser(this);
            setLoading(true);
        }

        @Override
        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
            FragmentActivity activity = getActivity();
            if (response.isSuccessful() && activity != null && !activity.isFinishing()) {
                final UserResponse userResponse = response.body();
                if (userResponse != null) {
                    setLoading(false);
                    User user = userResponse.getUser();
                    textUsername.setSummary(user.getName());

                    FirebaseAnalytics.getInstance(getActivity()).setUserId(user.getName());
                    ((BaseActivity) getActivity()).getSettingsManager().setLogin(user.getName());
                    setText(textName, user.getTrueName());
                    textMail.setSummary(user.getMail());
                    packagesCategory.removeAll();
                    textMovieTickets.setSummary(String.valueOf(user.getMovies()));
                    for (Package pack : userResponse.getPackages()) {
                        if (activity == null) {
                            return;
                        }
                        Preference preference = new Preference(activity);
                        preference.setTitle(pack.getName());
                        CharSequence summary;
                        switch (pack.getState()) {
                            case Package.PACK_EXPIRED:
                                Spannable span = new SpannableString(pack.getEndTime());
                                span.setSpan(new ForegroundColorSpan(Color.RED), 0, span.length(), 0);
                                summary = span;
                                break;
                            case Package.PACK_NOT_ACTIVE:
                                summary = getString(R.string.package_not_activated);
                                break;
                            default: //Active
                                summary = pack.getEndTime();
                                break;
                        }
                        preference.setSummary(summary);
                        packagesCategory.addPreference(preference);
                    }
                }
            } else {
                setLoading(false);
            }
        }

        @Override
        public void onFailure(Call<UserResponse> call, Throwable t) {
            setLoading(false);
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {
            if (preference == textName) {
                String value = o.toString();
                textName.setSummary(value);
                networkManager.updateUser(value, this);
                return true;
            }
            return false;
        }
    }

    public static class PlaybackPreferenceFragment extends QHDPreferenceFragment implements Preference.OnPreferenceChangeListener {

        private ListPreference server;
        private List<Server> servers;
        private List<CharSequence> serverNames;
        private List<CharSequence> serverIds;
        private Server selectedServer;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_video);

            servers = Database.getServerList(true);
            if (servers == null) {
                servers = new ArrayList<>();
            }
            serverNames = new ArrayList<>();
            serverIds = new ArrayList<>();
            for (Server server1 : servers) {
                if (server1 == null) continue;
                if (server1.isSelected()) {
                    selectedServer = server1;
                }
                serverNames.add(server1.getName());
                serverIds.add(String.valueOf(server1.getRemoteId()));
            }
            bindPreferenceSummaryToValue(findPreference(SettingsManager.PREF_PLAYER));
            bindPreferenceSummaryToValue(findPreference(SettingsManager.PREF_PLAYER_RATIO));
            bindPreferenceSummaryToValue(findPreference(SettingsManager.PREF_CAPTION_STYLE));
            bindPreferenceSummaryToValue(findPreference(SettingsManager.PREF_CAPTION_SIZE));
            server = (ListPreference) findPreference("server");
            SwitchPreferenceCompat memo = (SwitchPreferenceCompat) findPreference("memo_cast");
            memo.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    if (((Boolean) newValue)) {
                        MemoTVCastSDK.init(getContext());
                    } else {
                        MemoTVCastSDK.exit();
                    }
                    return true;
                }
            });
            server.setOnPreferenceChangeListener(this);
            if (selectedServer != null) {
                server.setSummary(selectedServer.getName());
                server.setValue(String.valueOf(selectedServer.getRemoteId()));
            }
            if (servers.size() == 0) {
                server.setSummary("Failed to fetch servers. Restart application.");
            }
            server.setEntries(serverNames.toArray(new CharSequence[0]));
            server.setEntryValues(serverIds.toArray(new CharSequence[0]));

            Preference forceUpdate = findPreference("forceUpdate");
            forceUpdate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ((BaseActivity) getActivity()).getNetworkManager().clearCache();
                    ChannelUpdateService.start(getActivity(), true);
                    Toast.makeText(getActivity(), "Channels have been updated.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {
            String selectedId = o.toString();
            selectedServer = Database.selectServer(Integer.parseInt(selectedId));
            ((QHDApplication) getActivity().getApplication()).getNetworkManager()
                    .setServer(selectedServer.getIP());
            this.server.setSummary(selectedServer.getName());
            this.server.setValue(String.valueOf(selectedServer.getRemoteId()));
            this.server.setEntries(serverNames.toArray(new CharSequence[0]));
            this.server.setEntryValues(serverIds.toArray(new CharSequence[0]));
            return false;
        }
    }

    public static class ApplicationPreferenceFragment extends QHDPreferenceFragment {

        private SharedPreferences.OnSharedPreferenceChangeListener themeChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals("theme")) {
                    FirebaseAnalytics.getInstance(getActivity()).setUserProperty("theme", sharedPreferences.getString(key, "0"));
                    Intent intent = new Intent(getContext(), ThemeSelectorActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    getActivity().finishAffinity();
                }
            }
        };

        private SettingsManager settingsManager;

        public ApplicationPreferenceFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_application);
            settingsManager = new SettingsManager(getActivity());
            SwitchPreferenceCompat subscribeToNews = (SwitchPreferenceCompat) findPreference("subscribe_to_news");
            subscribeToNews.setOnPreferenceChangeListener(new SwitchPreferenceCompat.OnPreferenceChangeListener() {

                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(getActivity());
                    analytics.setUserProperty(PROP_SUBSCRIBED, newValue.toString());
                    if (((boolean) newValue)) {
                        FirebaseMessaging.getInstance().subscribeToTopic("news");
                    } else {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("news");
                    }
                    return true;
                }
            });

            ListPreference theme = (ListPreference) findPreference("theme");
            final Preference priority = findPreference("priority");
            priority.setSummary(settingsManager.getPriorityCountry());
            priority.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    final List<String> countryList = Database.getCountryList();
                    countryList.add(0, getString(R.string.selection_default_none));
                    CustomDialogs.showListSelection(
                            getContext(),
                            getString(R.string.title_select_country),
                            countryList.toArray(new String[0]),
                            new CustomDialogs.OnItemSelected() {
                                @Override
                                public void onItemSelected(int which) {
                                    if (which == 0) {
                                        settingsManager.setPriorityCountry(null);
                                        priority.setSummary("");
                                    } else {
                                        settingsManager.setPriorityCountry(countryList.get(which));
                                        priority.setSummary(countryList.get(which));
                                    }
                                }
                            });
                    return true;
                }
            });
            Preference parentControl = findPreference("parent_control");
            findPreference("check_update").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ((BaseActivity) getActivity()).getNetworkManager().getAppUpdate(BuildConfig.APPLICATION_ID, BuildConfig.VERSION_CODE, new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String body = response.body();
                            if (response.isSuccessful() && body != null) {
                                //If response body first element equals '1' theres update available
                                if (body.length() > 0 && body.charAt(0) == '1') {
                                    getActivity().finish();
                                    Intent intent = new Intent(getActivity(), UpdateActivity.class);
                                    intent.putExtra("url", body);
                                    ((BaseActivity) getActivity()).openActivity(intent, R.anim.fade_in, R.anim.fade_out);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                            Toast.makeText(getActivity(), getString(R.string.error_network_problems) + ": " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    return true;
                }
            });
            parentControl.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    LayoutInflater inflater = getLayoutInflater();
                    LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.layout_parent_control, null);
                    final EditText old = layout.findViewById(R.id.current_code);
                    final EditText newCode = layout.findViewById(R.id.new_code);
                    final EditText repeatCode = layout.findViewById(R.id.new_code_confirm);
                    final SettingsManager settingsManager = ((BaseActivity) getActivity()).getSettingsManager();
                    AlertDialog dialog = new AlertDialog.Builder(getActivity())
                            .setView(layout)
                            .setTitle(R.string.title_change_code)
                            .setPositiveButton(R.string.ok, null)
                            .setNegativeButton(R.string.cancel, null).create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(final DialogInterface dialog) {
                            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String code = settingsManager.getPassword();
                                    if (!SettingsManager.SHA1(old.getText().toString()).equals(code)) {
                                        old.setError(getString(R.string.error_bad_code));
                                        return;
                                    }
                                    if (newCode.getText().length() < 4) {
                                        newCode.setError(getString(R.string.validation_password_too_short));
                                        return;
                                    }
                                    if (!newCode.getText().toString().equals(repeatCode.getText().toString())) {
                                        repeatCode.setError(getString(R.string.error_code_mismatch));
                                        return;
                                    }
                                    settingsManager.setPassword(newCode.getText().toString());
                                    Toast.makeText(getActivity(), R.string.message_code_changed, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            });
                        }
                    });
                    dialog.show();

                    return true;
                }
            });
            Preference clearCache = findPreference("clear_cache");
            clearCache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (((BaseActivity) getActivity()).getNetworkManager().clearCache()) {
                        Toast.makeText(getActivity(), "Successfully cleared cache", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Failed to clear cache. Check logs.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
            bindPreferenceSummaryToValue(theme);
        }

        @Override
        public void onResume() {
            super.onResume();
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .registerOnSharedPreferenceChangeListener(themeChangeListener);
        }


        @Override
        public void onPause() {
            super.onPause();
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .unregisterOnSharedPreferenceChangeListener(themeChangeListener);
        }
    }

}
