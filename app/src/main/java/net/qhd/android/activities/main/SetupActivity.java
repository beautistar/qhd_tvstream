package net.qhd.android.activities.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.fragments.main.SetupFragment;
import net.qhd.android.fragments.main.SetupInitFragment;
import net.qhd.android.fragments.main.SetupPasswordFragment;
import net.qhd.android.views.transformers.FadeOutPageTransformer;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetupActivity extends BaseActivity implements SetupFragment.SetupFragmentListener {

    @BindView(R.id.view_pager) ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        ButterKnife.bind(this);
        initPager();
    }

    private void initPager() {
        viewPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager()));
        viewPager.setPageTransformer(true, new FadeOutPageTransformer());
    }

    @Override
    public void onNext() {
        if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
            finish();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private final List<Class<? extends SetupFragment>> fragments =
                Arrays.asList(SetupInitFragment.class,
                        SetupPasswordFragment.class);

        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            try {
                return fragments.get(position).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

}
