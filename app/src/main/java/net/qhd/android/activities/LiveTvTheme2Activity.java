package net.qhd.android.activities;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.jtv.android.models.Channel;

import net.qhd.android.R;
import net.qhd.android.fragments.main.ChannelHorizontalListFragment;
import net.qhd.android.fragments.main.LiveTvCategoriesFragment;

import butterknife.BindView;

public class LiveTvTheme2Activity extends LiveTvActivity implements ChannelHorizontalListFragment.InteractionListener {

    @BindView(R.id.horizontal_channel) LinearLayout guiRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        channelListRoot.setVisibility(View.GONE);
        toolbar.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.horizontal_channel, new LiveTvCategoriesFragment(), "horizontal_channel")
                .commit();
        guiRoot.setBackgroundResource(R.drawable.gradient90);
        guiShown = false;
        guiRoot.setVisibility(View.GONE);

    }

    @Override
    public void showList() {
        guiRoot.setVisibility(View.VISIBLE);
        guiShown = true;
        listHide = LIST_SHOW_TIME;
        guiRoot.requestFocus();
    }

    @Override
    public void hideList() {
        guiRoot.setVisibility(View.GONE);
        guiShown = false;
        listHide = 0;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (guiShown) {
            listHide = LIST_SHOW_TIME;
            if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                return false;
            } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onChannelSelected(Channel item, int position) {
        int remoteId = item.getCategory().getRemoteId();
        int catpos = 0;
        while (catpos < categories.size() && categories.get(catpos).getRemoteId() != remoteId) {
            catpos++;
        }
        if (catpos == categories.size()) {
            return;
        }
        if (guiShown) {
            listHide = 1;
        }
        changeChannel(position, catpos, item, false);
    }

    @Override
    public void onSearch() {
        openSearchFragment();
    }

    @Override
    public void onSearch(Channel channel) {
        openSearchFragment(channel.getCategory());
    }

    @Override
    public void onRecent() {
        openRecentsFragment();
    }
}
