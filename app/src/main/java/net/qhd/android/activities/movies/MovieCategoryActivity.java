package net.qhd.android.activities.movies;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.Pair;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.MovieCategory;

import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.fragments.movies.MovieCategoryFragment;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieCategoryActivity extends BaseActivity
        implements MovieCategoryFragment.OnMovieCategorySelected,
        MovieCategoryFragment.SearchViewProvider {

    public static final String SAVED_PATH = "saved_path";

    @BindView(R.id.path) TextView textView;
    @BindView(R.id.search_movie) EditText searchMovie;

    private FragmentManager fragmentManager;
    private ArrayList<IdTagPair> previousTags = new ArrayList<>(Collections.singleton(new IdTagPair(0, "Main")));

    @Override
    @SuppressLint("SetTextI18n")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            if (!savedInstanceState.isEmpty()) {
                previousTags = (ArrayList<IdTagPair>) savedInstanceState.getSerializable(SAVED_PATH);
            }
        }
        setContentView(R.layout.activity_movie_category);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        if (previousTags.size() <= 1) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, MovieCategoryFragment.newInstance(0), "Main")
                    .commit();
        }
        updatePathText();
        hideSoftInput();
        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, null, null);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_PATH, previousTags);
    }

    @Override
    public void onItemSelected(final MovieCategory category, String fragmentTag) {
        if (previousTags.size() > 0) {
            if (!fragmentTag.equals(previousTags.get(previousTags.size() - 1).second)) {
                return;
            }
        }
        if (category.isAuditing()) {
            CustomDialogs.showPasswordInput(this, getSettingsManager(), new CustomDialogs.OnPasswordAcceptedListener() {
                @Override
                public void onPasswordAccepted() {
                    openCategory(category);
                }
            });
        } else {
            openCategory(category);
        }
    }

    private void openCategory(MovieCategory category) {
        FirebaseAnalytics.getInstance(this).setCurrentScreen(this, category.getName(), null);
        if (category.getType().equals("cat")) {
            MovieCategoryFragment newFragment = MovieCategoryFragment.newInstance(category.getId());
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.fragment_container, newFragment, category.getName())
                    .addToBackStack(category.getName())
                    .commit();
            previousTags.add(new IdTagPair(category.getId(), category.getName()));
            textView.setText(textView.getText() + " > " + category.getName());
            searchMovie.setText("");
        } else {
            openMovieDetailedActivity(category);
        }
    }

    private void openMovieDetailedActivity(MovieCategory category) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        Bundle extras = new Bundle();
        extras.putInt(MovieDetailActivity.EXTRA_MOVIE_ID, category.getId());
        intent.putExtras(extras);
        openActivity(intent, R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        if (previousTags.size() == 0) {
            super.onBackPressed();
            return;
        } else {
            previousTags.remove(previousTags.size() - 1);
            updatePathText();
        }
        if (fragmentManager.getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }
        searchMovie.setText("");
        super.onBackPressed();
    }

    private void updatePathText() {
        textView.setText("");
        for (Pair<Integer, String> previousTag : previousTags) {
            textView.setText(textView.getText() + " > " + previousTag.second);
        }
    }

    @Override
    public EditText getSearchView() {
        return searchMovie;
    }

    public static class IdTagPair extends Pair<Integer, String> implements Parcelable {

        /**
         * Constructor for a Pair.
         *
         * @param first  the first object in the Pair
         * @param second the second object in the pair
         */
        public IdTagPair(Integer first, String second) {
            super(first, second);
        }

        protected IdTagPair(Parcel in) {
            super(in.readInt(), in.readString());
        }

        public static final Creator<IdTagPair> CREATOR = new Creator<IdTagPair>() {
            @Override
            public IdTagPair createFromParcel(Parcel in) {
                return new IdTagPair(in);
            }

            @Override
            public IdTagPair[] newArray(int size) {
                return new IdTagPair[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(first);
            dest.writeString(second);
        }
    }
}
