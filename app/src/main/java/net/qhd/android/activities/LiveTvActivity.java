package net.qhd.android.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.InputType;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;
import com.jtv.android.models.Category;
import com.jtv.android.models.Channel;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnTokenChangeListener;
import com.jtv.android.utils.ChannelStatisticsManager;
import com.jtv.android.utils.Database;
import com.jtv.android.utils.SettingsManager;
import com.jtv.player.BandwidthEstimateListener;
import com.jtv.player.JTVExoPlayer;
import com.jtv.player.JTVNativePlayer;
import com.jtv.player.JTVPlayer;
import com.jtv.player.exceptions.HttpException;
import com.jtv.player.trackselection.Track;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import net.qhd.android.BuildConfig;
import net.qhd.android.CustomDialogs;
import net.qhd.android.R;
import net.qhd.android.adapters.ChannelAdapter;
import net.qhd.android.filter.CountryFilter;
import net.qhd.android.filter.FilterUtils;
import net.qhd.android.fragments.ChannelButtonsFragment;
import net.qhd.android.fragments.ChannelInfoFragment;
import net.qhd.android.fragments.ChannelRecentsFragment;
import net.qhd.android.fragments.ChannelSearchFragment;
import net.qhd.android.listeners.OnChannelSelectedListener;
import net.qhd.android.remake.RemakeEPGFragment;
import net.qhd.android.utils.Analytics;
import net.qhd.android.utils.ChangeChannelByNumberManager;
import net.qhd.android.utils.CustomCategories;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LiveTvActivity extends BaseActivity
        implements JTVPlayer.QHDListener,
        OnTokenChangeListener,
        GestureDetector.OnGestureListener,
        OnChannelSelectedListener,
        ChannelButtonsFragment.ChannelButtonPressed,
        RemakeEPGFragment.EPGContract {

    public static final String TAG = LiveTvActivity.class.getSimpleName();

    public static final int ERROR_COUNT_MAX = 6;
    public static final int LIST_SHOW_TIME = 10;
    public static final int INFO_SHOW_TIME = 5;
    public static final String EXTRA_CATEGORY = "extra_category";
    public static final String EXTRA_CHANNEL = "extra_channel";

    public static final int SLEEP_IN_MILLISECONDS = 7_200_000; //2 hours
    public static final int SLEEP_AFTER_MESSAGE = 60000;    //60 seconds

    public static final DisplayImageOptions DISPLAY_THUMBNAIL_OPTIONS = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(android.R.color.transparent)
            .showImageOnFail(android.R.color.transparent)
            .resetViewBeforeLoading(false)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .displayer(new FadeInBitmapDisplayer(300))
            .build();

    public static final DisplayImageOptions DISPLAY_CHANNEL_IMAGE = new DisplayImageOptions.Builder()
            .cloneFrom(DISPLAY_THUMBNAIL_OPTIONS)
            .resetViewBeforeLoading(true)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .showImageForEmptyUri(android.R.color.transparent)
            .showImageOnFail(android.R.color.transparent)
            .build();


    //Views
    @BindView(R.id.text_category_title) TextView textCategoryTitle;
    @BindView(R.id.layoutSubMenu) View channelListRoot;
    @BindView(R.id.listView_channels) ListView channelList;
    @BindView(R.id.lock_image) ImageView lockImage;
    @BindView(R.id.channel_info_root) View channelInfoRoot;
    @BindView(R.id.text_channel_name) TextView textChannelTitle;
    @BindView(R.id.text_channel_number) TextView textChannelNumber;
    @BindView(R.id.image_channel) ImageView imageChannel;
    @BindView(R.id.report_bug) FloatingActionButton reportBug;

    @BindView(R.id.text_debug) TextView textDebug;
    private TimerTask debugInfoUpdateTask;

    //QHDPlayer
    @BindView(R.id.surface_view) SurfaceView surfaceView;
    @BindView(R.id.surface_hide) View surfaceHide;
    @BindView(R.id.surface_hide_image) ImageView surfaceHideImage;
    @BindView(R.id.loading_layout) LinearLayout loadingLayout;
    @BindView(R.id.loading_progress) ProgressBar loadingProgress;
    @BindView(R.id.loading_text) TextView loadingText;
    @BindView(R.id.toolbar) LinearLayout toolbar;
    @BindView(R.id.aspect_ratio_layout) AspectRatioFrameLayout aspectRatioFrameLayout;
    private JTVPlayer player;

    //GUI
    private int infoHide = 0;
    protected int listHide = 0;
    protected boolean guiShown = true;
    private int errorCount = 0;
    //Data
    protected List<Category> categories;
    private boolean locked;
    private Channel currentChannel;
    private int currentChannelPos = 0;
    private int currentCategory = 0;
    private int selectedCategory = 0;
    private int aspectRatio;
    private ChannelAdapter channelListAdapter;
    private int playerId;
    private NetworkManager networkManager;
    private SettingsManager settingsManager;

    private Handler mainThreadHandler = new Handler();
    private ChangeChannelRunnable changeChannelRunnable = new ChangeChannelRunnable();
    private Runnable restartChannelRunnable = new Runnable() {
        @Override
        public void run() {
            if (currentChannel != null) {
                statisticsManager.onChannelStopped(currentChannel);
                playChannel(currentChannel, false);
            }
        }
    };

    private Runnable goToSleepMode = new Runnable() {
        @Override
        public void run() {
            sleepAlertDialog.cancel();
            finish();
        }
    };

    private Runnable sleepModeRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                sleepAlertDialog = new AlertDialog.Builder(LiveTvActivity.this)
                        .setTitle(R.string.title_idle_device)
                        .setMessage(R.string.message_idle_device)
                        .setPositiveButton(R.string.button_im_here, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mainThreadHandler.removeCallbacks(goToSleepMode);
                            }
                        })
                        .show();
                mainThreadHandler.postDelayed(goToSleepMode, SLEEP_AFTER_MESSAGE);
            } catch (WindowManager.BadTokenException ignored) {
            }
        }
    };


    private ChannelStatisticsManager statisticsManager = new ChannelStatisticsManager();

    private BroadcastReceiver channelUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initCategories(false);
        }
    };
    private AlertDialog sleepAlertDialog;
    private boolean dialogShown = false;

    private FirebaseAnalytics mFirebaseAnalytics;
    private GestureDetectorCompat gestureDetector;
    private Timer timer;
    private HideTimerTask hideTask;
    private ChannelInfoFragment channelInfoFragment;
    private ChannelButtonsFragment channelButtonsFragment;
    private boolean longPress;
    private boolean infoFocused;
    private boolean interfaceActive = true;
    private ChangeChannelByNumberManager changeChannelByNumberManager;


    private long channelStart = 0;
    private long channelReady = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_tv);
        ButterKnife.bind(this);
        channelInfoFragment = (ChannelInfoFragment) getSupportFragmentManager().findFragmentById(R.id.channel_info_fragment);
        channelButtonsFragment = (ChannelButtonsFragment) getSupportFragmentManager().findFragmentById(R.id.channel_button_fragment);
        gestureDetector = new GestureDetectorCompat(this, this);

        networkManager = getNetworkManager();
        settingsManager = getSettingsManager();
        playerId = settingsManager.getPlayer();
        textDebug.setVisibility(BuildConfig.DEBUG ? VISIBLE : GONE);

        aspectRatioFrameLayout.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        changeChannelByNumberManager = new ChangeChannelByNumberManager(0);
        changeChannelByNumberManager.setOnChannelSelectedListener(new ChangeChannelByNumberManager.OnChannelSelected() {
            @Override
            public void onChannelSelected(int position) {
                if (selectedCategory < 0 || selectedCategory >= categories.size()) return;
                Category category = categories.get(selectedCategory);
                textChannelNumber.setTextColor(Color.WHITE);
                if (category != null) {
                    if (channelListAdapter != null) {
                        if (channelListAdapter.getCount() <= position) {
                            textChannelNumber.setText(String.valueOf(currentChannelPos + 1));
                            return;
                        }
                        if (!changeChannel(position, selectedCategory, channelListAdapter.getItem(position), false)) {
                            textChannelNumber.setText(String.valueOf(position + 1));
                        }
                    }
                }
            }

            @Override
            public void onNumberChanged(int number) {
                Log.d(TAG, "Number input: " + number);
                textChannelNumber.setText(String.valueOf(number));
                textChannelNumber.setTextColor(ContextCompat.getColor(LiveTvActivity.this, R.color.colorAccent));
                showInfo(false);
            }
        });

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, null, null);

        surfaceView.setWillNotDraw(false);

        initChannelList();
        preparePlayer();
        hideInfo();

        initCategories(true);
    }

    private void initChannelList() {
        channelListAdapter = new ChannelAdapter(this);
        channelListAdapter.setSort(FilterUtils.getSorter(-1, settingsManager.getPriorityCountry()));
        channelList.setAdapter(channelListAdapter);
        channelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                changeChannel(position, currentCategory, channelListAdapter.getItem(position), false);
            }
        });
        channelList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Channel channel = channelListAdapter.getItem(position);
                showPopupForView(view, position, channel);
                return true;
            }
        });

        channelList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view,
                                 int firstVisibleItem,
                                 int visibleItemCount,
                                 int totalItemCount) {
                listHide = LIST_SHOW_TIME;
            }
        });
    }

    private void showPopupForView(View view, final int position, final Channel channel) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(LiveTvActivity.this, view);
        if (channel != null) {
            popup.getMenu().add(0, 0, 0, R.string.buttton_play);
        }
        if (categories.size() >= currentCategory && currentCategory < 0) {
            return;
        }
        final Category category = categories.get(currentCategory);
        if (channel != null && !category.isAuditing()) {
            popup.getMenu().add(0, 1, 1, channel.isFavorite() ? R.string.favorite_remove : R.string.favorite_add);
        }
        popup.getMenu().add(0, 3, 3, R.string.list_sort);
        if (category.getRemoteId() == CustomCategories.CATEGORY_ALL_CHANNELS_ID) {
            popup.getMenu().add(0, 4, 4, R.string.list_filter);
        }
        popup.getMenu().add(0, 5, 5, R.string.search);
        popup.getMenu().add(0, 6, 6, R.string.recents);
        if (channel != null && channel.equals(currentChannel) && currentChannel.getAvailableTimeshift() > 0) {
            popup.getMenu().add(0, 7, 7, R.string.timeshift);
        }
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0: //Play pressed
                        changeChannel(position, currentCategory, channel, false);
                        break;
                    case 1: //Fav unfav
                        toggleFavorite(channel);
                        break;
                    case 3:
                        CustomDialogs.showListSelection(
                                LiveTvActivity.this,
                                getString(R.string.title_select_sort),
                                FilterUtils.getSorters(LiveTvActivity.this),
                                new CustomDialogs.OnItemSelected() {
                                    @Override
                                    public void onItemSelected(int which) {
                                        channelListAdapter.setSort(FilterUtils.getSorter(which, settingsManager.getPriorityCountry()));
                                    }
                                });
                        break;
                    case 4:
                        CustomDialogs.showListSelection(
                                LiveTvActivity.this,
                                getString(R.string.title_select_filter),
                                FilterUtils.getFilters(LiveTvActivity.this),
                                new CustomDialogs.OnItemSelected() {
                                    @Override
                                    public void onItemSelected(int which) {
                                        switch (which) {
                                            case 0:
                                                channelListAdapter.setFilter(null);
                                                changeChannelByNumberManager.setMax(channelListAdapter.getCount());
                                                break;
                                            case 1:
                                                final List<String> countryList = Database.getCountryList();
                                                CustomDialogs.showListSelection(
                                                        LiveTvActivity.this,
                                                        getString(R.string.title_select_country),
                                                        countryList.toArray(new String[0]),
                                                        new CustomDialogs.OnItemSelected() {
                                                            @Override
                                                            public void onItemSelected(int which) {
                                                                channelListAdapter.setFilter(new CountryFilter(countryList.get(which)));
                                                                changeChannelByNumberManager.setMax(channelListAdapter.getCount());
                                                            }
                                                        });
                                                break;
                                        }
                                    }
                                });
                        break;
                    case 5:
                        openSearchFragment(categories.get(currentCategory));
                        break;
                    case 6:
                        openRecentsFragment();
                        break;
                    case 7:
                        openTimeshiftActivity(currentChannel);
                        break;
                }
                return true;
            }
        });
        popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu menu) {
                dialogShown = false;
            }
        });

        popup.show(); //showing popup menu
        this.dialogShown = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mainThreadHandler.removeCallbacks(changeChannelRunnable);
        mainThreadHandler.removeCallbacks(sleepModeRunnable);
        if (Build.VERSION.SDK_INT <= 23) {
            releasePlayer();
        }
        if(hideTask != null){
            hideTask.cancel();
            hideTask = null;
        }
        if(debugInfoUpdateTask != null){
            debugInfoUpdateTask.cancel();
            debugInfoUpdateTask = null;
        }
        showNavigationBar();
        unregisterReceiver(channelUpdateReceiver);

        if (currentChannel != null) {
            settingsManager.setLastCategory(currentCategory);
            settingsManager.setLastChannel(currentChannelPos);
        }
        networkManager.setTokenListener(null);
        statisticsManager.onChannelStopped(currentChannel);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (currentChannel != null && Build.VERSION.SDK_INT > 23) {
            playerId = settingsManager.getPlayer();
            playChannel(currentChannel, false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
        currentChannel = null;
        currentCategory = -1;
        timer.cancel();
        timer = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAspectRatio();
        mainThreadHandler.postDelayed(sleepModeRunnable, SLEEP_IN_MILLISECONDS);
        if (currentChannel != null && Build.VERSION.SDK_INT <= 23) {
            playerId = settingsManager.getPlayer();
            playChannel(currentChannel, false);
        }
        registerChannelUpdateReciever(channelUpdateReceiver);
        hideNavigationBar();
        looper();
        networkManager.setTokenListener(this);
    }


    private void setAspectRatio() {
        aspectRatio = settingsManager.getAspectRatio();
        switch (aspectRatio) {
            case 0: //Auto
                break;
            case 1:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_4_3);
                break;
            case 2:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_16_9);
                break;
            case 3:
                aspectRatioFrameLayout.setAspectRatio(JTVPlayer.RATIO_21_9);
        }
    }

    private void updateChannelInfo(int position) {
        if (BuildConfig.DEBUG) {
            textChannelTitle.setText(currentChannel.getRemoteId() + "-" + currentChannel.getName() + " (" + currentChannel.getStreamName() + ")");
        } else {
            textChannelTitle.setText(currentChannel.getName());
        }
        textChannelNumber.setText(String.valueOf(position + 1));
        channelButtonsFragment.setChannel(currentChannel);
        ImageLoader.getInstance().displayImage(currentChannel.getImgURL(), imageChannel, DISPLAY_CHANNEL_IMAGE);
    }

    private void populateChannels(final List<Channel> channels) {
        if (channelList != null && channelListAdapter != null) {
            channelListAdapter.setChannels(channels, categories.get(currentCategory));
            changeChannelByNumberManager.setMax(channelListAdapter.getCount());
        }
    }

    private void updateCategoryTitle() {
        if (currentCategory > categories.size()) {
            return;
        }
        Category category = categories.get(currentCategory);
        textCategoryTitle.setText(category.getName());
    }

    public void hideList() {
        guiShown = false;
        listHide = 0;

        channelListRoot.animate()
                .setDuration(200)
                .translationX(-50)
                .alpha(0f)
                .setInterpolator(new AccelerateInterpolator(0.75f))
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        channelListRoot.setVisibility(View.GONE);
                        imageChannel.setVisibility(VISIBLE);
                    }
                })
                .start();

        toolbar.setVisibility(View.GONE);
        setCurrentCategory(selectedCategory);
        updateCategory(false, false);
        hideNavigationBar();
        surfaceView.invalidate();

    }

    public void showList() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (isInMultiWindowMode()) {
                return;
            }
        }
        guiShown = true;
        listHide = LIST_SHOW_TIME;
        channelListRoot.setVisibility(View.VISIBLE);
        if (channelListRoot.getTranslationX() != 0) {
            channelListRoot.animate()
                    .setDuration(200)
                    .translationX(0)
                    .alpha(1f)
                    .setInterpolator(new DecelerateInterpolator(0.75f))
                    .start();
        }
        imageChannel.setVisibility(GONE);
        toolbar.setVisibility(View.VISIBLE);
        surfaceView.invalidate();
    }

    public void hideInfo() {
        channelInfoRoot.setVisibility(View.GONE);
        hideNavigationBar();
        surfaceView.invalidate();
        infoFocused = false;
    }

    public void showInfo(boolean requestFocus) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (isInMultiWindowMode()) {
                return;
            }
        }
        channelInfoRoot.setVisibility(View.VISIBLE);
        if (requestFocus) {
            channelInfoRoot.requestFocus();
            infoFocused = true;
        }
        infoHide = INFO_SHOW_TIME;
        surfaceView.invalidate();
    }

    public void looper() {
        if (timer == null) {
            timer = new Timer();
        }
        if (hideTask != null) {
            hideTask.cancel();
        }
        hideTask = new HideTimerTask();

        if(BuildConfig.DEBUG){
            if (debugInfoUpdateTask != null) {
                debugInfoUpdateTask.cancel();
                debugInfoUpdateTask = null;
            }
            debugInfoUpdateTask = new TimerTask() {
                @Override
                public void run() {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            long time = channelReady > 0 ? channelReady : Calendar.getInstance().getTimeInMillis() - channelStart;
                            textDebug.setText(String.format(Locale.getDefault(), "Time for channel to start: %d miliseconds", time));
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(debugInfoUpdateTask, 0, 100);
        }
        timer.scheduleAtFixedRate(hideTask, 0, 1000);
    }

    public void playChannel(Channel channel, boolean delay) {
        String url = networkManager.getStreamUrl(channel.getStreamName());
        reportBug.setVisibility(GONE);
        changeChannelRunnable.setChannel(channel);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "playChannel: " + channel.getStreamName() + " - " + url);
        }
        if (delay) {
            mainThreadHandler.postDelayed(changeChannelRunnable, 1000);
        } else {
            mainThreadHandler.post(changeChannelRunnable);
        }
        updateLoadingLayout(View.INVISIBLE, View.VISIBLE, "");
        if (!categories.get(currentCategory).isAuditing()) {
            statisticsManager.onChannelStarted(channel);
        }
    }

    public void focusItem(int item) {
        channelListAdapter.setFocused(item);
        scrollToItem(item);
        channelList.invalidateViews();
    }

    public void selectItem(int item) {
        channelListAdapter.setSelected(item);
        focusItem(item);
    }

    private void scrollToItem(int item) {
        int count = channelListAdapter.getCount();
        if (item >= count) {
            item = 0;
        }
        channelList.setSelectionFromTop(item, channelList.getHeight() / 2);
        channelList.computeScroll();
    }

    @OnClick(R.id.category_next)
    public void onClickRight() {
        selectRight();
        showList();
    }

    private void selectRight() {
        if (currentCategory == categories.size() - 1) {
            setCurrentCategory(0);
        } else {
            setCurrentCategory(currentCategory + 1);
        }
        updateCategory(false, true);
    }

    @OnClick(R.id.category_prev)
    public void onClickLeft() {
        selectLeft();
        showList();
    }

    private void setCurrentCategory(int category) {
        currentCategory = category;
        channelListAdapter.setCurrentCategory(category);
    }

    private void selectLeft() {
        if (currentCategory == 0) {
            setCurrentCategory(categories.size() - 1);
        } else {
            setCurrentCategory(currentCategory - 1);
        }
        updateCategory(false, true);
    }

    private void updateCategory(boolean first, boolean lock) {
        updateCategoryTitle();
        if (categories.size() == 0) {
            channelListAdapter.setChannels(null, null);
            return;
        }
        if (currentCategory >= categories.size()) {
            currentCategory = categories.size() - 1;
        }
        Category currentCate = categories.get(currentCategory);

        boolean locked = currentCate.isAuditing();
        if (!locked || !lock) {
            lockImage.setVisibility(View.INVISIBLE);
            loadChannels(first);
            this.locked = false;
        } else {
            lockImage.setVisibility(View.VISIBLE);
            channelListAdapter.setChannels(null, null);
            this.locked = true;
        }
        scrollToItem(channelListAdapter.getFocused());
    }

    public void selectUp() {
        if (channelListAdapter.getCount() > 0) {
            if (!guiShown) {
                int selected = currentChannelPos - 1;
                selected = indexToRange(selected, channelListAdapter.getCount());
                selectItem(selected);
                Channel channel = channelListAdapter.getItem(selected);
                if (channel != null) {
                    changeChannel(selected, selectedCategory, channel, true);
                }
            } else {
                int focused = channelListAdapter.getFocused() - 1;
                focused = indexToRange(focused, channelListAdapter.getCount());
                focusItem(focused);
                scrollToItem(focused);
            }
        }
    }

    public void selectDown() {
        if (channelListAdapter.getCount() > 0) {
            if (!guiShown) {
                int selected = currentChannelPos + 1;
                selected = indexToRange(selected, channelListAdapter.getCount());
                selectItem(selected);
                Channel channel = channelListAdapter.getItem(selected);
                if (channel != null) {
                    changeChannel(selected, selectedCategory, channel, true);
                }
            } else {
                int focused = channelListAdapter.getFocused() + 1;
                focused = indexToRange(focused, channelListAdapter.getCount());
                focusItem(focused);
                scrollToItem(focused);
            }
        }
    }

    private int indexToRange(int position, int range) {
        if (position < 0) {
            position = range - 1;
        }
        if (position >= range) {
            position = 0;
        }
        return position;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mainThreadHandler.removeCallbacks(sleepModeRunnable);
        mainThreadHandler.postDelayed(sleepModeRunnable, SLEEP_IN_MILLISECONDS);

        if (super.onTouchEvent(event)) {
            return true;
        }
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        mainThreadHandler.removeCallbacks(sleepModeRunnable);
        mainThreadHandler.postDelayed(sleepModeRunnable, SLEEP_IN_MILLISECONDS);
        if (!interfaceActive) {
            return super.onKeyDown(keyCode, event);
        }
        if (infoFocused) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                hideInfo();
                return true;
            }
            showInfo(false);
            return super.onKeyDown(keyCode, event);
        }
        Log.d(TAG, "onKeyDown: " + keyCode);
        if (changeChannelByNumberManager.handleKeyEvent(event)) {
            return true;
        }

        if (keyCode == 132) { //list
            toggleFavorite();
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            if (changeChannelByNumberManager.getNumber() == 0) {
                selectRight();
            }
            showList();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            if (changeChannelByNumberManager.getNumber() == 0) {
                selectLeft();
            }
            showList();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            if (guiShown) {
                selectUp();
                showList();
            } else {
                selectDown();
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            if (guiShown) {
                selectDown();
                showList();
            } else {
                selectUp();
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {  //OK
            event.startTracking();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {   //back
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                return super.onKeyDown(keyCode, event);
            }
            if (guiShown) {
                hideList();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        } else if (keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_SETTINGS) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            onSearchClick();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_INFO) {
            showInfo(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!interfaceActive) {
            return super.onKeyUp(keyCode, event);
        }
        if (longPress) {
            longPress = false;
            return false;
        }
        if (event.isTracking() && !event.isLongPress()) {
            if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {  //OK
                if (guiShown) {
                    if (locked) {
                        protectedCategoryDialog();
                    } else {
                        int focused = channelListAdapter.getFocused();
                        Channel item = channelListAdapter.getItem(focused);
                        if (item != null) {
                            selectItem(focused);
                            changeChannel(focused, currentCategory, item, false);
                            hideList();
                        }
                    }
                    return true;
                } else {
                    showList();
                    return true;
                }
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (!interfaceActive) {
            return super.onKeyLongPress(keyCode, event);
        }
        longPress = true;
        if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {  //OK
            if (guiShown) {
                if (locked) {
                    protectedCategoryDialog();
                    return true;
                } else {
                    int focused = channelListAdapter.getFocused();
                    View view;
                    Channel channel = null;
                    if (channelListAdapter.getCount() == 0 || focused >= channelListAdapter.getCount()) {
                        view = textCategoryTitle;
                    } else {
                        view = getViewByPosition(focused, channelList);
                        channel = channelListAdapter.getItem(focused);
                    }
                    showPopupForView(view, focused, channel);
                    return true;
                }
            } else {
                showInfo(true);
                return true;
            }
        }
        return super.onKeyLongPress(keyCode, event);
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }


    private void toggleFavorite() {
        if (guiShown) {
            int position = channelList.getSelectedItemPosition();
            Category category = categories.get(currentCategory);
            if (position >= 0 && position < channelListAdapter.getCount() && !category.isAuditing()) {
                toggleFavorite(channelListAdapter.getItem(position));
            }
        } else {
            if (currentChannel != null) {
                toggleFavorite(currentChannel);
                updateChannelInfo(currentChannelPos);
            }
        }

    }

    private void toggleFavorite(Channel channel) {
        if (channel == null) {
            return;
        }
        channel.setFavorite(!channel.isFavorite());
        channel.save();
        channelButtonsFragment.setFavorited(channel.isFavorite());
        if (channel.isFavorite()) {
            networkManager.addFavorite(channel.getRemoteId());
        } else {
            networkManager.removeFavorite(channel.getRemoteId());
        }
        List<Channel> favoriteChannels = Database.getFavorites();
        Category favCategory = categories.get(categories.size() - 2);
        favCategory.setChannels(favoriteChannels);

        if (currentCategory == categories.size() - 2) {
            channelListAdapter.setChannels(favoriteChannels, favCategory);
        } else if (channel.getCategory().getRemoteId() == categories.get(currentCategory).getRemoteId()) {
            channelListAdapter.notifyDataSetChanged();
        } else {
            Category category = channel.getCategory();
            int index = categories.indexOf(category);
            if (index > -1) {
                categories.set(index, category);
            }
        }

    }

    private void protectedCategoryDialog() {
        this.dialogShown = true;
        final AlertDialog.Builder dialogBuilter = new AlertDialog.Builder(LiveTvActivity.this);
        dialogBuilter.setTitle(R.string.title_protected);
        dialogBuilter.setMessage(R.string.message_enter_password);

        final EditText input = new EditText(this);
        input.setSingleLine();
        input.setInputType(InputType.TYPE_CLASS_NUMBER
                | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
//        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        input.setImeActionLabel(getString(R.string.unluck), KeyEvent.KEYCODE_ENTER);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                return false;
            }
        });
        dialogBuilter.setView(input);
        dialogBuilter.setIcon(R.drawable.ic_lock);

        dialogBuilter.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String password = SettingsManager.SHA1(input.getText().toString());
                        String savedPassword = settingsManager.getPassword();
                        if (savedPassword != null) {
                            if (savedPassword.equals(password)) {
                                LiveTvActivity.this.locked = false;
                                LiveTvActivity.this.lockImage.setVisibility(View.INVISIBLE);
                                loadChannels(false);
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        R.string.error_bad_password, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(LiveTvActivity.this, R.string.error_no_password_set, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        dialogBuilter.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialogBuilter.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogShown = false;
            }
        });

        final AlertDialog alertDialog = dialogBuilter.create();

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
                    return true;
                }
                return false;
            }
        });
        alertDialog.show();

    }

    protected void loadChannels(final boolean first) {
        List<Channel> channels = categories.get(currentCategory).getChannels();
        populateChannels(channels);
        if (first) {
            int lastChannel = getIntent().getIntExtra(EXTRA_CHANNEL, -1);
            if (lastChannel == -1) {
                lastChannel = settingsManager.getLastChannel();
            } else {
                for (int i = 0; i < channels.size(); i++) {
                    if (channels.get(i).getRemoteId() == lastChannel) {
                        lastChannel = i;
                        break;
                    }
                }
            }
            if (lastChannel > -1 && lastChannel < channels.size()) {
                changeChannel(lastChannel, currentCategory, channels.get(lastChannel), false);
                hideList();
                showInfo(false);
            }
        }
    }

    protected void initCategories(boolean changeChannel) {
        categories = Database.getCategories(false);
        if (categories.size() == 0) {
            CustomDialogs.showNoPackageDialog(this);
        }
        categories.add(CustomCategories.getFavoriteCategory(this));
        categories.add(CustomCategories.getAllChannelsCategory(this));
        int lastCategory = getIntent().getIntExtra(EXTRA_CATEGORY, -1);
        if (lastCategory == -1) {
            lastCategory = settingsManager.getLastCategory();
        } else {
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).getRemoteId() == lastCategory) {
                    lastCategory = i;
                    break;
                }
            }
        }
        boolean loadChannel = false;
        if (changeChannel && lastCategory > -1 && lastCategory < categories.size()) {
            setCurrentCategory(lastCategory);
            loadChannel = true;
        }
        updateCategoryTitle();
        updateCategory(loadChannel, true);
    }

    private void openTimeshiftActivity(Channel channel) {
        Intent simpleInstance = QHDPlayerActivity.getTimeshiftIntent(this,
                channel.getStreamName(),
                networkManager.getStreamUrl(channel.getStreamName(), "index-" + channel.getAvailableTimeshift() + "-now.m3u8"),
                channel.getAvailableTimeshift());
        openActivity(simpleInstance, R.anim.fade_in, R.anim.fade_out);
    }

    @OnClick(R.id.lock_image)
    public void onLockClick() {
        if (locked) {
            protectedCategoryDialog();
        }
    }

    private void preparePlayer() {
        if (player == null) {
            player = getPlayer();
            player.prepare();
            player.setSurface(surfaceView);
            player.setPlayWhenReady(true);
            player.setListener(this);
            player.setBandwidthEstimateListener(new BandwidthEstimateListener() {
                @Override
                public void onBandwidthEstimate(long bitrate) {
                    Log.i(TAG, "onBandwidthEstimate: " + bitrate + "bps");
                }
            });
            player.setOnTrackChangedListener(channelButtonsFragment);
        }
    }

    @NonNull
    private JTVPlayer getPlayer() {
        switch (playerId) {
            case JTVPlayer.PLAYER_NATIVE:
                return new JTVNativePlayer(this, BuildConfig.DEBUG);
            case JTVPlayer.PLAYER_EXO:
                return new JTVExoPlayer(this, mainThreadHandler, BuildConfig.DEBUG);
            default:
                Log.e("JTVQHDPlayer", "Player with id=" + playerId + " doesnt exist. Loading native player.");
                return new JTVNativePlayer(this, BuildConfig.DEBUG);
        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    /**
     * Shows information interface
     *
     * @param layoutVisibility      root view
     * @param progressBarVisibility progress bar
     * @param text                  text to show
     */
    private void updateLoadingLayout(int layoutVisibility, int progressBarVisibility, String text) {
        loadingLayout.setVisibility(layoutVisibility);
        loadingProgress.setVisibility(progressBarVisibility);
        loadingText.setText(text);
    }

    public boolean changeChannel(int position, int category, Channel channel, boolean delay) {

        Analytics.logChannel(mFirebaseAnalytics, channel);
        reportBug.setVisibility(GONE);
        if (currentChannel == channel) {
            return false;
        }
        if (currentChannel != null) {
            statisticsManager.onChannelStopped(currentChannel);
        }

        ImageLoader.getInstance()
                .displayImage(
                        channel.getImgURL(),
                        surfaceHideImage,
                        DISPLAY_THUMBNAIL_OPTIONS
                );

        selectedCategory = category;
        currentChannel = channel;
        currentChannelPos = position;
        setCurrentCategory(category);
        updateCategory(false, false);

        channelListAdapter.setSelectedCategory(selectedCategory);
        channelInfoFragment.channelStarted(channel);
        playChannel(channel, delay);
        showInfo(false);
        errorCount = 0;
        selectItem(position);
        updateChannelInfo(position);
        updateLoadingLayout(View.INVISIBLE, View.VISIBLE, "");
        return true;
    }

    private void setSurfaceVisibility(boolean visible) {
        surfaceHide.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
//        surfaceHide.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStateChanged(int state) {
        Log.d(TAG, "Player state: " + state);
        mainThreadHandler.removeCallbacks(restartChannelRunnable);
        switch (state) {
            case JTVPlayer.STATE_BUFFERING:
                mainThreadHandler.postDelayed(restartChannelRunnable, 10000);
                break;
            case JTVPlayer.STATE_ENDED:
                Log.e(TAG, "Stream ended.");
                if (currentChannel != null) {
                    if (errorCount < ERROR_COUNT_MAX) {
                        playChannel(currentChannel, true);
                    }
                }
                channelInfoFragment.channelStopped();
                break;
            case JTVPlayer.STATE_READY:
                Log.d(TAG, "Player ready.");
                setSurfaceVisibility(true);
                errorCount = 0;
                if(channelStart > 0){
                    channelReady = Calendar.getInstance().getTimeInMillis() - channelStart;
                    channelStart = 0;
                }
                break;
        }
    }

    @Override
    public void onError(Exception exception) {
        statisticsManager.onChannelError(currentChannel);
        String selectedServer = networkManager.getSelectedServer();
        String code = null;
        if (exception instanceof HttpException) {
            HttpException httpException = (HttpException) exception;
            code = String.valueOf(httpException.getStatus());
            if (httpException.getStatus() == HttpException.STATUS_FORBIDDEN) {
                updateLoadingLayout(View.VISIBLE, View.VISIBLE, getString(R.string.error_token_expired));
                networkManager.login();
            } else {
                updateLoadingLayout(View.VISIBLE, View.GONE, getString(R.string.error_connection));
                ImageLoader.getInstance().cancelDisplayTask(surfaceHideImage);
                surfaceHideImage.setImageResource(android.R.color.transparent);
            }
        } else { //other
            if (errorCount < ERROR_COUNT_MAX) {
                mainThreadHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (currentChannel != null) {
                            playChannel(currentChannel, false);
                        }
                    }
                }, 5000);
            }
            if (errorCount == 2) {
                updateLoadingLayout(View.VISIBLE, View.VISIBLE, getString(R.string.info_loading));
            }
            if (errorCount == ERROR_COUNT_MAX) {
                FirebaseCrash.report(exception);
                updateLoadingLayout(View.VISIBLE, View.GONE, getString(R.string.error_connection_problem));
//                reportBug.setVisibility(VISIBLE);
            }
        }
        if (surfaceHide.getVisibility() == View.VISIBLE) {
            ImageLoader.getInstance().cancelDisplayTask(surfaceHideImage);
        }
        Analytics.logChannelError(mFirebaseAnalytics, currentChannel, selectedServer, code);
        errorCount++;
        Log.e(TAG, "Player rrror counter: " + errorCount);
    }

    @Override
    public void onVideoSizeChanged(int width, int height, float pixelWidthHeightRatio) {
        Log.d(TAG, "Video size changed : " + width + " " + height);
        if (aspectRatio == 0) {
            Log.d(TAG, "Aspect ratio set to auto. Changing aspect ratio of layout");
            aspectRatioFrameLayout.setAspectRatio(((float) width) / ((float) height));
        }
    }

    @Override
    public void onTokenChange(String token) {
        if (player != null && currentChannel != null) {
            playChannel(currentChannel, false);
        }
    }

    @OnClick(R.id.search_button)
    public void onSearchClick() {
        openSearchFragment();
    }

    @OnClick(R.id.recents_button)
    public void onRecentsClick() {
        openRecentsFragment();
    }

    @OnClick(R.id.report_bug)
    public void onReportBug() {

    }

    protected void openSearchFragment() {
        openSearchFragment(null);
    }

    protected void openSearchFragment(Category category) {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                    .add(R.id.live_tv_root, category == null ? new ChannelSearchFragment() : ChannelSearchFragment.instance(category), "search")
                    .addToBackStack(null)
                    .commit();
        }
    }

    protected void openRecentsFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                    .add(R.id.live_tv_root, new ChannelRecentsFragment(), "recent")
                    .addToBackStack(null)
                    .commit();
        }
    }

    @OnClick(R.id.settings_button)
    public void goToSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (guiShown) {
            hideList();
        } else {
            showList();
        }
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        showInfo(true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//        Log.d(TAG, "onFling: " + velocityX + " : " + velocityY);
        boolean isInMultiWindowMode = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            isInMultiWindowMode = isInMultiWindowMode();
        }
        if (Math.abs(velocityY) > Math.abs(velocityX) && !isInMultiWindowMode) {
            if (velocityY > 200) {
                hideInfo();
                return true;
            }
            if (velocityY < -200) {
                showInfo(false);
                return true;
            }
        } else {
            if (velocityX > 200) {
                if (isInMultiWindowMode) {
                    selectUp();
                } else {
                    showList();
                }
                return true;
            }
            if (velocityX < -200) {
                if (isInMultiWindowMode) {
                    selectDown();
                } else {
                    hideList();
                }
                return true;
            }
        }
        return false;
    }


    /**
     * Returned from {@link ChannelSearchFragment}
     *
     * @param channel selected channel, can be null
     */
    @Override
    public void onChannelSelected(Channel channel) {
        if (channel != null) {
            Category category = channel.getCategory();
            int catpos = 0;
            for (Category cat : categories) {
                if (cat.getRemoteId() == category.getRemoteId()) {
                    break;
                }
                catpos++;
            }
            List<Channel> temp;
            if (channelListAdapter.getSort() != null) {
                temp = new ArrayList<>();
                temp.addAll(category.getChannels());
                Collections.sort(temp, channelListAdapter.getSort());
            } else {
                temp = category.getChannels();
            }
            int chpos = 0;
            for (Channel cha : temp) {
                if (cha.getRemoteId() == channel.getRemoteId()) {
                    break;
                }
                chpos++;
            }
            changeChannel(chpos, catpos, channel, true);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onFavoritePressed(Channel channel) {
        toggleFavorite(currentChannel);
        updateChannelInfo(currentChannelPos);
    }

    @Override
    public void onTimeshiftPressed(Channel channel) {
        Log.d(TAG, "onTimeshiftPressed: " + channel);
        if (channel != null && channel.getAvailableTimeshift() >= 0) {
            openTimeshiftActivity(channel);
        }
    }

    @Override
    public void onEpgPressed(Channel channel) {
        if (channel == null) return;
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_top, R.anim.slide_from_top, R.anim.slide_to_top)
                .add(R.id.live_tv_root, RemakeEPGFragment.newInstance(channel.getRemoteId(), channel.getAvailableTimeshift()))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCast() {
        startActivity(TubiControllerActivity.castLiveTv(this, currentChannel));
    }

    @Override
    public void onFocusLost() {
        infoFocused = false;
        surfaceView.requestFocus();
    }

    @Override
    public void onTrackSelectPressed() {
        if (player != null) {
            final List<Track> tracksInfo = player.getTracksInfo();
            if (tracksInfo.size() <= 1) {
                Toast.makeText(this, "No tracks to select.", Toast.LENGTH_SHORT).show();
                return;
            }
            String[] names = new String[tracksInfo.size()];
            for (int i = 0; i < tracksInfo.size(); i++) {
                names[i] = tracksInfo.get(i).getName();
            }
            CustomDialogs.showListSelection(this, "Tracks", names, new CustomDialogs.OnItemSelected() {
                @Override
                public void onItemSelected(int which) {
                    player.selectTrack(tracksInfo.get(which));
                }
            });
        }
    }

    @Override
    public void setInterfaceActive(boolean active) {
        interfaceActive = active;
        if (!active) {
            hideInfo();
            hideList();
        }
    }

    @Override
    public void openTimeshift(Channel channel, long seekTo) {
        startActivity(QHDPlayerActivity.getTimeshiftIntent(this,
                channel.getStreamName(),
                networkManager.getStreamUrl(channel.getStreamName(), "index-" + channel.getAvailableTimeshift() + "-now.m3u8"),
                channel.getAvailableTimeshift(),
                seekTo));
    }

    private class ChangeChannelRunnable implements Runnable {

        private Channel channel;

        private void setChannel(Channel channel) {
            this.channel = channel;
        }

        @Override
        public void run() {
            preparePlayer();
            setSurfaceVisibility(false);

            channelStart = Calendar.getInstance().getTimeInMillis();
            channelReady = 0;

            player.play(networkManager.getStreamUrl(channel.getStreamName()));
            if (currentCategory >= 0 && !categories.get(currentCategory).isAuditing()) {
                settingsManager.setLastCategory(currentCategory);
                settingsManager.setLastChannel(currentChannelPos);
            }
        }
    }


    @SuppressWarnings("unused")


    private class HideTimerTask extends TimerTask {

        @Override
        public void run() {
            if (infoHide > 0) {
                infoHide--;
                if (infoHide == 0) {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            hideInfo();
                        }
                    });
                }
            }
            if (listHide > 0) {
                if (!dialogShown) {
                    listHide--;
                }
                if (listHide == 0) {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            hideList();
                        }
                    });
                }
            }
        }
    }

}