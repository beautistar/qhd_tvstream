package net.qhd.android.activities;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.jtv.android.models.Channel;
import com.jtv.android.utils.Database;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import net.qhd.android.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelSearchActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.search_result) GridView listView;
    @BindView(R.id.search_view) SearchView searchView;
    private List<Channel> channels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_search);
        ButterKnife.bind(this);
        handleIntent(getIntent());
        searchView.setOnQueryTextListener(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        // Verify the action and get the query
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchView.setQuery(query, false);
            searchView.setIconified(false);
            searchView.clearFocus();
            search(query);
        }
    }

    private void search(String query) {
        channels = Database.searchChannels(query);
        listView.setAdapter(new ChannelGridAdapter(channels));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query != null && !query.isEmpty()) {
            search(query);
            searchView.clearFocus();
            return true;
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    class ChannelGridAdapter extends BaseAdapter {

        private List<Channel> channels;

        public ChannelGridAdapter(List<Channel> channels) {
            this.channels = channels;
        }

        @Override
        public int getCount() {
            return channels.size();
        }

        @Override
        public Object getItem(int position) {
            return channels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.layout_channel_grid_item, null);
            }
            TextView title = (TextView) convertView.findViewById(R.id.title);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
            View favorite = convertView.findViewById(R.id.button_favorite_icon);
            Channel channel = channels.get(position);
            title.setText(channel.getName());

            ImageLoader loader = ImageLoader.getInstance();

            imageView.setVisibility(View.INVISIBLE);
            loader.cancelDisplayTask(imageView);
            loader.displayImage(channel.getImgURL(), imageView, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    view.setVisibility(View.VISIBLE);
                    view.setAlpha(0f);
                    view.animate()
                            .alpha(1f)
                            .setInterpolator(new AccelerateDecelerateInterpolator())
                            .setDuration(250).start();
                }
            });

            favorite.setVisibility(channel.isFavorite() ? View.VISIBLE : View.GONE);
            return convertView;
        }
    }
}
