package net.qhd.android.activities.movies;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.forcetech.android.ForceTV;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jtv.android.models.Movie;
import com.jtv.android.subtitles.provider.OpenSubsProvider;
import com.jtv.android.utils.Blur;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import net.qhd.android.BuildConfig;
import net.qhd.android.R;
import net.qhd.android.activities.BaseActivity;
import net.qhd.android.activities.QHDPlayerActivity;
import net.qhd.android.fragments.player.MoviePlayerFragment;
import net.qhd.android.utils.Analytics;
import net.qhd.android.views.ParalaxImageView;

import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class MovieDetailActivity extends BaseActivity implements Callback<Movie>, MoviePlayerFragment.MoviePlayerContract {

    public static final String TAG = MovieDetailActivity.class.getSimpleName();

    public static final String EXTRA_MOVIE_ID = "movieId";

    private static final int REQUEST_CODE_PLAY_MOVIE = 256;
    private static final int REQUEST_CODE_PLAY_TRAILER = 257;
    public static final String SAVED_MOVIE = "saved_movie";

    @BindView(R.id.activity_movie_detail) FrameLayout root;
    @BindView(R.id.scrollview) ScrollView scrollView;
    @BindView(R.id.play_button) Button buttonPlay;
    @BindView(R.id.play_button_p2p) Button buttonPlayP2p;
    @BindView(R.id.play_trailer_button) Button buttonPlayTrailer;
    @BindView(R.id.trailer_player) AspectRatioFrameLayout trailer;
    @BindView(R.id.movie_thumbnail) ImageView movieThumbnail;

    @BindView(R.id.movie_root) LinearLayout movieRoot;

    @BindView(R.id.movie_title) TextView movieTitle;
    @BindView(R.id.movie_year) TextView movieYear;
    @BindView(R.id.movie_genre) TextView movieGenre;
    @BindView(R.id.movie_length) TextView movieLength;
    @BindView(R.id.movie_rating) TextView movieRating;
    @BindView(R.id.movie_writers) TextView movieWriters;
    @BindView(R.id.movie_actors) TextView movieActors;
    @BindView(R.id.movie_country) TextView movieCountry;
    @BindView(R.id.movie_awards) TextView movieAwards;
    @BindView(R.id.movie_plot) TextView moviePlot;
    @BindView(R.id.movie_subtitles) TextView movieSubtitles;

    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.message) TextView message;
    @BindViews({R.id.scrollview, R.id.trailer_root}) List<View> roots;


    private int movieId;
    private Movie movie;

    private boolean failedImdbImage = false;
    private ParalaxImageView backgroundImage;
    private ImageLoadingListener imageLoadingListener = new SimpleImageLoadingListener() {
        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            Log.i(TAG, "Loading compelte. starting animation.");
            if (loadedImage != null) {
                Bitmap blurred = Blur.apply(MovieDetailActivity.this, loadedImage, 3);
                backgroundImage.setImageBitmap(blurred);
                backgroundImage.animate()
                        .alpha(1f)
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .setDuration(2000).start();
            } else {
                if (!failedImdbImage) {
                    failedImdbImage = true;
                    ImageLoader.getInstance().loadImage(movie.getPoster2(), this);
                }
            }
        }
    };

    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean inMultiWindowMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            inMultiWindowMode = isInMultiWindowMode();
        }
        if (inMultiWindowMode) {
            Resources resources = getResources();
            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, resources.getDisplayMetrics());
            movieRoot.setPadding(px, px, px, px);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            movieId = extras.getInt(EXTRA_MOVIE_ID, -1);
        }
        mFirebaseAnalytics.setCurrentScreen(this, String.valueOf(movieId), null);
        hideRoots();
        if (savedInstanceState != null) {
            movie = (Movie) savedInstanceState.getSerializable(SAVED_MOVIE);
            initialize();
            showRoots();
        } else {
            getNetworkManager().getMovie(movieId, this);
        }
        trailer.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
        trailer.setAspectRatio(16f / 9f);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_MOVIE, movie);
    }

    private void hideRoots() {
        for (View view : roots) {
            view.setVisibility(View.INVISIBLE);
        }
    }

    private void showRoots() {
        int i = 0;
        for (View view : roots) {
            view.setAlpha(0f);
            view.setVisibility(View.VISIBLE);
            view.setTranslationY(30f);
            view.animate().alpha(1f)
                    .translationY(0f)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setStartDelay(i++ * 200)
                    .setDuration(1000)
                    .start();
        }
        progressBar.setVisibility(View.GONE);
    }

    private void initialize() {
        if (movie != null) {
            Analytics.logMovie(mFirebaseAnalytics, movie);
            if (movie.canWatch()) {
                buttonPlay.setEnabled(true);
                buttonPlay.requestFocus();
            } else {
                Toast.makeText(this, R.string.out_of_tickets, Toast.LENGTH_SHORT).show();
            }
            buttonPlayTrailer.setEnabled(true);

            addBackroundImage();
            ImageLoader.getInstance().displayImage(movie.getPoster2(), movieThumbnail, new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build());

            if (movie.getP2pId() != null && !movie.getP2pId().isEmpty()) {
                buttonPlayP2p.setVisibility(View.VISIBLE);
                buttonPlayP2p.setEnabled(true);
            }

            movieTitle.setText(movie.getTitle());
            movieYear.setText(movie.getYear());
            movieGenre.setText(movie.getGenre());
            movieLength.setText(movie.getRuntime());
            movieRating.setText(getString(R.string.rating, movie.getImdbRating()));
            moviePlot.setText(movie.getPlot());
            if (movie.getWriter() != null) {
                movieWriters.setText(movie.getWriter().replace(",", ",\n"));
            }
            if (movie.getActors() != null) {
                movieActors.setText(movie.getActors().replace(",", ",\n"));
            }
            movieCountry.setText(movie.getCountries());
            movieAwards.setText(movie.getAwards());

            //Load trailer fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.trailer_player, MoviePlayerFragment.trailerInstance(movie.getTrailerUrl(), 0.25f))
                    .commit();

            showRoots();

            new OpenSubsProvider(this, getNetworkManager(), new OkHttpClient(), OpenSubsProvider.buildClient()).getList(movie.getImdbID(), new Callback<Map<String, String>>() {

                @Override
                public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                    if (response.isSuccessful()) {
                        Map<String, String> body = response.body();
                        if (body != null && !body.isEmpty()) {
                            Set<String> languages = body.keySet();
                            StringBuilder builder = new StringBuilder();
                            for (String language : languages) {
                                builder.append(language).append('\n');
                            }
                            movieSubtitles.setText(builder.toString());
                        }
                    } else {
                        Toast.makeText(MovieDetailActivity.this, "Failed to fetch subtitles.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Map<String, String>> call, Throwable t) {
                    Log.e(TAG, "Open subs error", t);
                }
            });

        }
    }

    private void addBackroundImage() {
        backgroundImage = new ParalaxImageView(this, scrollView);
        backgroundImage.setReverseX(true);
        backgroundImage.setReverseY(true);
        backgroundImage.setAlpha(0f);
        backgroundImage.setInterpolator(new AccelerateDecelerateInterpolator());
        ImageLoader.getInstance().loadImage(movie.getPoster2(), imageLoadingListener);
        backgroundImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        backgroundImage.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        root.addView(backgroundImage, 0);
    }

    @Override
    public void onResponse(Call<Movie> call, Response<Movie> response) {
        if (response.isSuccessful()) {
            movie = response.body();
            initialize();
        } else {
            progressBar.setVisibility(View.GONE);
            message.setVisibility(View.VISIBLE);
            message.setText(R.string.error_something_went_wrong);
        }
    }

    @Override
    public void onFailure(Call<Movie> call, Throwable t) {
        t.printStackTrace();
    }

    @OnClick(R.id.play_button)
    public void playStream() {
        if (movie != null) {
            new AlertDialog.Builder(this).setTitle(movie.getTitle())
                    .setMessage(getString(R.string.movie_ticket_warning, movie.getTickets()))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = QHDPlayerActivity.getIntent(MovieDetailActivity.this, movie.getStreamUrl(), movie.getTitle(), movie.getImdbID(), false);
                            startActivityForResult(intent, REQUEST_CODE_PLAY_MOVIE);
                        }
                    }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    @OnClick(R.id.play_button_p2p)
    public void playStreamP2p() {
        if (movie != null && movie.getP2pId() != null && !movie.getP2pId().isEmpty()) {
            buttonPlayP2p.setEnabled(false);
            ForceTV.initForceClient();
            getNetworkManager().forceStartCommand(movie.getP2pId(), new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        Intent intent = QHDPlayerActivity.getIntent(MovieDetailActivity.this, "http://0.0.0.0:" + ForceTV.FORCE_PORT + "/" + movie.getP2pId(), movie.getImdbID(), false);
                        startActivityForResult(intent, REQUEST_CODE_PLAY_MOVIE);
                    } else {
                        Toast.makeText(MovieDetailActivity.this, "Could not start P2P service.", Toast.LENGTH_SHORT).show();
                    }
                    buttonPlayP2p.setEnabled(true);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, t.toString());
                    }
                    Toast.makeText(MovieDetailActivity.this, "Could not start P2P service.", Toast.LENGTH_SHORT).show();
                    buttonPlayP2p.setEnabled(true);
                }
            });

        }
    }

    @OnClick(R.id.play_trailer_button)
    public void playTrailer() {
        if (movie != null) {
            Intent intent = QHDPlayerActivity.getIntent(this, movie.getTrailerUrl(), movie.getTitle(), true);
            startActivityForResult(intent, REQUEST_CODE_PLAY_TRAILER);
        }
    }

    /**
     * @param requestCode request code - was this movie or just a trailer
     * @param resultCode  RESULT_OK if stream has ended, RESULT_CANCELLED if user left before finishing
     * @param data        unused (null)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_CODE_PLAY_MOVIE) {
//            //Returned from watching movie
//        }
        if (requestCode == REQUEST_CODE_PLAY_TRAILER) {
            if (resultCode == Activity.RESULT_OK) {
                showDialogAfterTrailer();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialogAfterTrailer() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_interested)
                .setMessage(R.string.message_interested)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        playStream();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //unused
                    }
                }).show();
    }

    //trailer fragment
    @Override
    public void onMovieReady() {
        trailer.setVisibility(View.VISIBLE);
    }

    //trailer fragment
    @Override
    public void onMovieError() {
        trailer.setVisibility(View.GONE);
    }
}
