package net.qhd.android.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.jtv.android.models.Channel;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.network.listeners.OnTokenChangeListener;
import com.memo.cable.MemoDeviceServiceHelper;
import com.memo.sdk.MemoTVCastController;
import com.memo.sdk.MemoTVCastSDK;

import net.qhd.android.R;
import net.qhd.android.fragments.TubiLiveControllerFragment;
import net.qhd.android.fragments.TubiMovieControllerFragment;
import net.qhd.android.listeners.TubiCastContract;

import org.cybergarage.upnp.Device;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TubiControllerActivity extends BaseActivity implements TubiCastContract, OnTokenChangeListener {

    public static final String ACTION_LIVE = "ACTION_LIVE_TUBICAST";
    public static final String ACTION_MOVIE = "ACTION_MOVIE_TUBICAST";
    public static final String EXTRA_ID = "ID";
    public static final String STREAM_URL = "STREAM_URL";
    public static final String STREAM_NAME = "STREAM_NAME";

    public static Intent castLiveTv(Context context, Channel channel) {
        Intent intent = new Intent(context, TubiControllerActivity.class);
        intent.setAction(ACTION_LIVE);
        intent.putExtra(EXTRA_ID, channel.getRemoteId());
        return intent;
    }

    public static Intent castMovie(Context context, String streamUrl, String streamName) {
        Intent intent = new Intent(context, TubiControllerActivity.class);
        intent.setAction(ACTION_MOVIE);
        intent.putExtra(STREAM_URL, streamUrl);
        intent.putExtra(STREAM_NAME, streamName);
        return intent;
    }

    private Channel currentChannel;

    public Executor threadPool = Executors.newFixedThreadPool(5);
    private MemoTVCastController controller = new MemoTVCastController();
    private NetworkManager networkManager;

    private boolean isPlaying = false;
    private String pausePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tubi_controller);

        networkManager = getNetworkManager();

        switch (getIntent().getAction()) {
            case ACTION_LIVE:
                int id = getIntent().getIntExtra(EXTRA_ID, -1);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root, TubiLiveControllerFragment.newInstance(id))
                        .commit();
                break;
            case ACTION_MOVIE:
                String streamUrl = getIntent().getStringExtra(STREAM_URL);
                String streamName = getIntent().getStringExtra(STREAM_NAME);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root, TubiMovieControllerFragment.newInstance(streamUrl, streamName))
                        .commit();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        networkManager.setTokenListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkManager.setTokenListener(null);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                volumeDown();
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                volumeUp();
                return true;
            case KeyEvent.KEYCODE_VOLUME_MUTE:
                volumeMute();
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void volumeMute() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Device selected = MemoTVCastSDK.getSelectedDevice();
                int minVolume = controller.getMinVolumeValue(selected);
                controller.setVoice(selected, minVolume);
            }
        });
    }

    public void volumeDown() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Device selected = MemoTVCastSDK.getSelectedDevice();
                int minVolume = controller.getMinVolumeValue(selected);
                controller.setVoice(selected, Math.max(controller.getVoice(selected) - 10, minVolume));
            }
        });
    }

    public void volumeUp() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Device selected = MemoTVCastSDK.getSelectedDevice();
                int maxVolume = controller.getMaxVolumeValue(selected);
                controller.setVoice(selected, Math.min(maxVolume, controller.getVoice(selected) + 10));
            }
        });
    }

    @Override
    public void playChannel(final Channel channel) {
        currentChannel = channel;
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                boolean isSuccess = controller.stop(MemoTVCastSDK.getSelectedDevice(), true);
                if (isSuccess) {
                    controller.play(
                            MemoTVCastSDK.getSelectedDevice(),
                            networkManager.getStreamUrl(channel.getStreamName()),
                            channel.getName()
                    );
                }
            }
        });
        isPlaying = true;
    }

    @Override
    public void stop() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                controller.stop(MemoTVCastSDK.getSelectedDevice(), true);
            }
        });
        isPlaying = false;
        currentChannel = null;
    }

    @Override
    public void seek(final long miliseconds) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                controller.seek(MemoDeviceServiceHelper.getInstance().getSelectedDevice(), tubicastTimeEncode(miliseconds));
            }
        });
    }

    @Override
    public void getDuration(final ControllerResult<Long> result) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                String mediaDuration = controller.getMediaDuration(MemoTVCastSDK.getSelectedDevice());
                long duration = tubicastTimeDecode(mediaDuration);
                result.result(duration);
            }
        });
    }

    @Override
    public void getPosition(final ControllerResult<Long> result) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                String mediaDuration = controller.getPositionInfo(MemoTVCastSDK.getSelectedDevice());
                long duration = tubicastTimeDecode(mediaDuration);
                result.result(duration);
            }
        });
    }

    @Override
    public void pause() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                controller.pause(MemoTVCastSDK.getSelectedDevice());
                pausePosition = controller.getPositionInfo(MemoTVCastSDK.getSelectedDevice());
            }
        });
        isPlaying = false;
    }

    @Override
    public void resume() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                controller.goon(MemoTVCastSDK.getSelectedDevice(), pausePosition != null ? pausePosition : "00:00:00");
            }
        });
        isPlaying = true;
    }

    @Override
    public boolean isPlaying() {
        return isPlaying;
    }

    @Override
    public void play(final String streamUrl, final String streamName) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                boolean isSuccess = controller.stop(MemoTVCastSDK.getSelectedDevice(), true);
                if (isSuccess) {
                    controller.play(
                            MemoTVCastSDK.getSelectedDevice(),
                            streamUrl,
                            streamName
                    );
                }
            }
        });
        currentChannel = null;
        isPlaying = true;
    }

    public static String tubicastTimeEncode(long time) {
        time /= 1000;
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", time / 3600, (time / 60) % 60, time % 60);
    }

    public static long tubicastTimeDecode(String time) {
        long result = 0;
        String[] split = time.split(":");
        if (split.length == 3) {
            result += Long.parseLong(split[0]) * 3600;
            result += Long.parseLong(split[1]) * 60;
            result += Long.parseLong(split[2]);
        }
        if (split.length == 2) {
            result += Long.parseLong(split[0]) * 60;
            result += Long.parseLong(split[1]);
        }
        return result * 1000;
    }

    @Override
    public void onTokenChange(String token) {
        if (isPlaying && currentChannel != null) {
            playChannel(currentChannel);
        }
    }

    public interface ControllerResult<T> {

        void result(T result);

    }

}
