package net.qhd.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.jtv.android.network.NetworkManager;

import net.qhd.android.BuildConfig;
import net.qhd.android.QHDApplication;

import java.util.Timer;
import java.util.TimerTask;

public class ChannelUpdateService extends Service {

    public static final String TAG = ChannelUpdateService.class.getSimpleName();

    public static final String EXTRA_UPDATE_NOW = "update_now";

    public static final String ACTION_UPDATED = BuildConfig.APPLICATION_ID + ".actions.ACTION_CHANNEL_UPDATE";

    private NetworkManager networkManager;
    private Timer timer;
    private Looper serviceLooper;
    private ServiceHandler mServiceHandler;

    public static void start(Context context, boolean updateInstantly) {
        Intent intent = new Intent(context, ChannelUpdateService.class);
        intent.putExtra(EXTRA_UPDATE_NOW, updateInstantly);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        serviceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(serviceLooper);
        networkManager = ((QHDApplication) getApplication()).getNetworkManager();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        if (timer == null) {
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    Message msg = mServiceHandler.obtainMessage();
                    mServiceHandler.sendMessage(msg);
                }
            }, 600000, 600000); //Every 10 mins
        }

        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                boolean updateNow = extras.getBoolean(EXTRA_UPDATE_NOW, false);
                if (updateNow) {
                    Message msg = mServiceHandler.obtainMessage();
                    mServiceHandler.sendMessage(msg);
                }
            }
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        timer.cancel();
        timer = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void updateCategories() {
        if (networkManager.isLoggedIn()) {
            networkManager.updateChannels(new NetworkManager.OnChannelUpdateCallback() {
                @Override
                public void onUpdated() {
                    sendBroadcast(new Intent(ACTION_UPDATED));
                }

                @Override
                public void onFailed() {

                }
            });
        }
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            updateCategories();
        }
    }

}
