package net.qhd.android.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import net.qhd.android.QHDApplication;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QHDFirebaseIDService extends FirebaseInstanceIdService {

    public static final String TAG = QHDFirebaseIDService.class.getSimpleName();

    public QHDFirebaseIDService() {
    }

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "onTokenRefresh: " + token);
        ((QHDApplication) getApplication()).getNetworkManager().registerFirebaseToken(token, new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }


}
