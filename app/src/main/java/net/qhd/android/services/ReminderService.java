package net.qhd.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.jtv.android.models.Notification;
import com.jtv.android.models.Reminder;
import com.jtv.android.utils.Database;
import com.jtv.player.BuildConfig;

import net.qhd.android.R;
import net.qhd.android.utils.Foreground;
import net.qhd.android.utils.ReminderNotification;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ReminderService extends Service {

    public static final String ACTION_ADD_REMINDER = BuildConfig.APPLICATION_ID + ".action.ADD_REMINDER";
    public static final String ACTION_REMOVE_REMINDER = BuildConfig.APPLICATION_ID + ".action.REMOVE_REMINDER";
    public static final String EXTRA_ID = "id";

    private final Binder serviceBinder = new ReminderBinder();

    private Timer timer;
    private List<ReminderTask> reminderTasks = new ArrayList<>();

    public static void add(Context context, Reminder reminder) {
        Intent intent = new Intent(context, ReminderService.class);
        intent.setAction(ACTION_ADD_REMINDER);
        if (reminder.getId() == null) {
            reminder.save();
        }
        intent.putExtra(EXTRA_ID, reminder.getId());
        context.startService(intent);
    }

    public static void remove(Context context, Reminder reminder) {
        Intent intent = new Intent(context, ReminderService.class);
        intent.setAction(ACTION_REMOVE_REMINDER);
        intent.putExtra(EXTRA_ID, reminder.getId());
        context.startService(intent);
    }

    public ReminderService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer("ReminderTimer");
        List<Reminder> reminders = Database.getReminders();
        for (Reminder reminder : reminders) {
            addReminder(reminder);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer = null;
    }

    private void addReminder(Reminder reminder) {
        ReminderTask reminderTask = new ReminderTask(reminder);
        reminderTasks.add(reminderTask);
        Date time = new Date(reminder.getTimestamp());
        timer.schedule(reminderTask, time);
    }

    private boolean containsReminder(Reminder reminder) {
        for (ReminderTask reminderTask : reminderTasks) {
            if (((long) reminderTask.getReminder().getId()) == reminder.getId()) {
                return true;
            }
        }
        return false;
    }

    private void removeReminder(Reminder reminder) {
        for (ReminderTask reminderTask : reminderTasks) {
            if (((long) reminderTask.getReminder().getId()) == reminder.getId()) {
                reminderTask.cancel();
                reminderTasks.remove(reminderTask);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long reminderId = -1;
        if (intent != null) {
            reminderId = intent.getLongExtra(EXTRA_ID, -1);
        }
        if (reminderId == -1) {
            return super.onStartCommand(intent, flags, startId);
        }
        Reminder reminder = Database.getReminder(reminderId);
        if (reminder == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        switch (intent.getAction()) {
            case ACTION_ADD_REMINDER:
                if (!containsReminder(reminder)) {
                    addReminder(reminder);
                }
                break;
            case ACTION_REMOVE_REMINDER:
                removeReminder(reminder);
                reminder.delete();
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private class ReminderTask extends TimerTask {

        private Reminder reminder;

        ReminderTask(Reminder reminder) {
            this.reminder = reminder;
        }

        Reminder getReminder() {
            return reminder;
        }

        @Override
        public void run() {
            Log.i("ReminderNotification", "reminder: " + reminder + " app in foreground: " + Foreground.get(getApplication()).isForeground());

            if (Foreground.get(getApplication()).isForeground()) {
                Notification notification = new Notification("-1", reminder.getName(), getString(R.string.reminder_message, reminder.getChannel().getName()), null, null, null, 0, 0, new HashMap<String, String>());
                Intent intent = Notification.getIntent(notification, QHDFirebaseMessagingService.BROADCAST_ACTION);
                sendBroadcast(intent);
            } else {
                ReminderNotification.notify(getApplicationContext(), reminder);
            }

            reminder.delete();
            reminderTasks.remove(this);
        }
    }

    class ReminderBinder extends Binder {

        ReminderService getService() {
            return ReminderService.this;
        }

    }
}
