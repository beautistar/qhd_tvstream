package net.qhd.android.services;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jtv.android.models.Notification;

import net.qhd.android.BuildConfig;

import java.util.Calendar;
import java.util.Map;

public class QHDFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = QHDFirebaseMessagingService.class.getSimpleName();

    public static final String BROADCAST_ACTION = BuildConfig.APPLICATION_ID + ".action.NOTIFICATION";
    public static final String BROADCAST_FUNCTION = BuildConfig.APPLICATION_ID + ".action.FUNCTION";

    public static final String FUNCTION_LOGOUT = "logout";
    public static final String FUNCTION_UPDATE = "update";

    public QHDFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Message from: " + remoteMessage.getFrom());
        }

        if (remoteMessage.getNotification() != null) {
            Notification notification = buildNotification(remoteMessage);
            if (notification.getData().containsKey("save")) {
                if (notification.getData().get("save").equals("true")) {
                    notification.save();
                }
            } else {
                notification.save();
            }
            notification.save();
            Intent intent = Notification.getIntent(notification, BROADCAST_ACTION);

            sendBroadcast(intent);
        }


        if (remoteMessage.getData().containsKey("function")) {
            Intent intent = new Intent();
            intent.setAction(BROADCAST_FUNCTION);
            for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                intent.putExtra(entry.getKey(), entry.getValue());
            }
            sendBroadcast(intent);
        }


    }


    @NonNull
    private Notification buildNotification(RemoteMessage remoteMessage) {
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            return new Notification(remoteMessage.getMessageId(),
                    notification.getTitle(),
                    notification.getBody(),
                    notification.getClickAction(),
                    notification.getLink(),
                    notification.getIcon(),
                    remoteMessage.getSentTime(),
                    Calendar.getInstance().getTimeInMillis(),
                    remoteMessage.getData());
        }
        return new Notification(remoteMessage.getMessageId(),
                remoteMessage.getSentTime(),
                Calendar.getInstance().getTimeInMillis(),
                remoteMessage.getData());
    }

}
