package net.qhd.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.jtv.android.models.Server;
import com.jtv.android.network.NetworkManager;
import com.jtv.android.utils.Database;
import com.stealthcopter.networktools.Ping;
import com.stealthcopter.networktools.ping.PingResult;

import net.qhd.android.BuildConfig;
import net.qhd.android.QHDApplication;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;

public class SelectServerService extends Service {

    public static final String TAG = SelectServerService.class.getSimpleName();

    public static final String ACTION_SERVER_SELECTED = BuildConfig.APPLICATION_ID + ".action.SERVER_SELECTED";

    private List<Server> servers;
    private NetworkManager networkManager;

    public SelectServerService() {
    }

    public static void start(Context context) {
        Log.d(TAG, "Sending service start intent...");
        Intent intent = new Intent(context, SelectServerService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        networkManager = ((QHDApplication) getApplication()).getNetworkManager();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting server selection service...");

        List<Server> serverList = Database.getServerList(false);

        PingAsyncTask task = new PingAsyncTask();
        task.execute(serverList.toArray(new Server[0]));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "destroying service");
    }

    class PingAsyncTask extends AsyncTask<Server, PingResult, Server> {

        private static final int PING_COUNT = 3;

        @Override
        protected Server doInBackground(Server... servers) {
            float fastestTimeTaken = 0;
            Server fastestServer = null;
            for (int index = 0; index < servers.length; index++) {
                Server server = servers[index];
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "challenging server " + server.getName() + ": " + server.getIP());
                }
                float timeTaken = 0;
                try {
                    Ping pingTarget = Ping.onAddress(server.getIP()).setTimeOutMillis(500);
                    for (int i = 0; i < PING_COUNT; i++) {
                        PingResult pingResult = pingTarget.doPing();
                        timeTaken += pingResult.getTimeTaken();
                        onProgressUpdate(pingResult);
                    }
                } catch (UnknownHostException e) {
                    continue;
                }
                timeTaken = (timeTaken / PING_COUNT);
                float scaledTimeTaken = timeTaken * (1f + (index / 10f));
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, String.format(Locale.getDefault(),"Server %s (%s) results: average time taken: %f, time taken scaled by position: %f", server.getName(), server.getName(), timeTaken, scaledTimeTaken));
                }
                if (fastestServer == null || scaledTimeTaken < fastestTimeTaken) {
                    fastestTimeTaken = scaledTimeTaken;
                    fastestServer = server;
                }
            }
            if (fastestServer != null) {
                return fastestServer;
            }
            if (servers.length > 0) {
                return servers[0];
            }
            return null;
        }

        @Override
        protected void onPostExecute(Server server) {
            super.onPostExecute(server);
            Database.selectServer(server.getRemoteId());
            networkManager.setServer(server.getIP());
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onPostExecute: server selected=" + server.getIP() + ", " + server.getName());
            }
            sendBroadcast(new Intent(ACTION_SERVER_SELECTED));
            stopSelf();
        }
    }


}
