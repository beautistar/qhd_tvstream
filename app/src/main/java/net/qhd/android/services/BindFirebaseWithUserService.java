package net.qhd.android.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import net.qhd.android.QHDApplication;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BindFirebaseWithUserService extends IntentService {
    private static final String ACTION_BIND_USER = "net.qhd.android.services.action.BIND_USER";

    public static final String TAG = "FirebaseBind";

    public BindFirebaseWithUserService() {
        super("BindFirebaseWithUserService");
    }

    public static void bindUserWithFirebase(Context context) {
        Intent intent = new Intent(context, BindFirebaseWithUserService.class);
        intent.setAction(ACTION_BIND_USER);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_BIND_USER.equals(action)) {
                handleActionBindUser();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBindUser() {
        String old = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Firebase token: " + old);
        ((QHDApplication) getApplication()).getNetworkManager().replaceFirebaseToken(old, new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}
