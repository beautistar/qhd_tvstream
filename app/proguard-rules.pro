
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-dontwarn com.google.firebase.crash.FirebaseCrash
-dontwarn de.timroes.axmlrpc.XmlRpcClient
-dontwarn de.timroes.axmlrpc.XmlRpcClient$MethodCall
-dontwarn de.timroes.axmlrpc.serializer.DateTimeSerializer
-keep class com.squareup.okhttp.** { *; }


-keep public class org.apache.** {
  <fields>;
  <methods>;
}

-ignorewarnings

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}


-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

#LeakCanary
-keep class org.eclipse.mat.** { *; }
-keep class com.squareup.leakcanary.** { *; }

#ActiveAndroid
-keep class com.activeandroid.** { *; }
-keep class com.activeandroid.**.** { *; }
-keep class * extends com.activeandroid.Model
-keep class * extends com.activeandroid.serializer.TypeSerializer

#Tera cast
-keep class com.memo.**{ *;}
-keep class com.memo.**$*{ *;}
-keep public class org.cybergarage.upnp.Device { *; }
-keep public class org.cybergarage.upnp.device.DeviceChangeListener { *; }
-keep public class com.memo.sdk.MemoTVCastSDK$ISetTvWifiListener { *; }
-keep public class com.memo.connection.WifiAdmin$WifiCipherType { *;}
-keep public class com.memo.connection.MemoWifiManager$IConnectWifiListener { *; }
-keep public class org.cybergarage.xml.parser.** { *; }
-keep public class org.cybergarage.util.** { *; }

