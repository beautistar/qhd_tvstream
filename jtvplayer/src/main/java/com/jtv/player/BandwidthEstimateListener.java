package com.jtv.player;

public interface BandwidthEstimateListener {

    void onBandwidthEstimate(long bitrate);

}
