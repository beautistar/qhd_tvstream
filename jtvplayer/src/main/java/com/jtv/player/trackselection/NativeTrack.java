package com.jtv.player.trackselection;


public class NativeTrack implements Track {

    private int id;
    private String name;
    private int bitrate;

    public NativeTrack(int id, String name, int bitrate) {
        this.id = id;
        this.name = name;
        this.bitrate = bitrate;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getBitrate() {
        return bitrate;
    }
}
