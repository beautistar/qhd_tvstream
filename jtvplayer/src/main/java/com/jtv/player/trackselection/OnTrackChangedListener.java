package com.jtv.player.trackselection;

public interface OnTrackChangedListener {

    void onTrackChanged(Track track);

}
