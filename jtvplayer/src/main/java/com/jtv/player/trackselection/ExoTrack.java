package com.jtv.player.trackselection;

public class ExoTrack implements Track {

    private int id;

    private String name;

    private int bitrate;

    private int groupId;

    private int rendererIndex;

    public ExoTrack(int id, String name, int bitrate, int groupId, int rendererIndex) {
        this.id = id;
        this.name = name;
        this.bitrate = bitrate;
        this.groupId = groupId;
        this.rendererIndex = rendererIndex;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getBitrate() {
        return bitrate;
    }

    public int getGroupId(){
        return groupId;
    }

    public int getRendererIndex(){
        return rendererIndex;
    }

    @Override
    public String toString() {
        return "ExoTrack{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", bitrate=" + bitrate +
                ", groupId=" + groupId +
                ", rendererIndex=" + rendererIndex +
                '}';
    }
}
