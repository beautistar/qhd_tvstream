package com.jtv.player.trackselection;

import android.content.Context;

/**
 * Created by jgecy on 2017-02-15.
 */

public interface TrackSelectionHelper {

    void showSelectionDialog(Context context, CharSequence title);

}
