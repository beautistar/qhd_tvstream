package com.jtv.player.trackselection;

public interface Track {

    int id();

    String getName();

    int getBitrate();

}
