package com.jtv.player.subtitles;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.google.android.exoplayer2.text.Cue;
import com.jtv.android.subtitles.Caption;
import com.jtv.android.subtitles.FatalParsingException;
import com.jtv.android.subtitles.FormatSRT;
import com.jtv.android.subtitles.TimedTextObject;
import com.jtv.player.JTVPlayer;

import org.apache.commons.io.FileUtils;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class JTVSubtitleHandler {

    public static final String TAG = JTVSubtitleHandler.class.getSimpleName();
    private final Handler mDisplayHandler;
    private Context context;
    private JTVPlayer player;
    private JTVTextOutput textOutput;
    private File mSubFile;
    private Caption mLastSub;

    private String subtitleUrl;

    private TimedTextObject mSubs;
    private Timer subTimer;
    private TimerTask subTimerTask = new CheckSubsTimerTask();

    private long offset = 0;

    public JTVSubtitleHandler(Context context, JTVPlayer player) {
        this.context = context;
        this.player = player;
        mDisplayHandler = new Handler();
        subTimer = new Timer();
    }

    public void release() {
        subTimer.cancel();
        subTimer = null;
        if (textOutput != null) {
            textOutput.onCues(Collections.<Cue>emptyList());
        }
    }

    public void pause() {
        subTimerTask.cancel();
    }

    public void resume() {
        if (mSubs != null) {
            subTimerTask = new CheckSubsTimerTask();
            subTimer.scheduleAtFixedRate(subTimerTask, 0, 1000);
        }
    }

    public String getUrl() {
        return subtitleUrl;
    }

    public void setUrl(String url) {
        this.subtitleUrl = url;
        if (subtitleUrl == null) {
            mSubs = null;
        }
    }

    public void setTextOutput(JTVTextOutput textOutput) {
        this.textOutput = textOutput;
    }

    public void stop() {
        subTimerTask.cancel();
        subtitleUrl = null;
        if (textOutput != null) {
            textOutput.onCues(Collections.<Cue>emptyList());
        }
    }

    public void startSubtitles() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    mSubFile = new File(getSubtitleFile(subtitleUrl));
                    FormatSRT formatSRT = new FormatSRT();
                    String file = FileUtils.readFileToString(mSubFile, Charset.forName("UTF-8"));
                    mSubs = formatSRT.parseFile(mSubFile.toString(), file);
                    subTimerTask.cancel();
                    subTimerTask = new JTVSubtitleHandler.CheckSubsTimerTask();
                    if(subTimer != null) {
                        subTimer.scheduleAtFixedRate(subTimerTask, 0, 1000);
                    }
                } catch (FileNotFoundException e) {
                    if (e.getMessage().contains("EBUSY")) {
                        startSubtitles();
                    }
                    e.printStackTrace();
                } catch (IOException | FatalParsingException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();

    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    protected void checkSubs() {
        if (player != null && player.isPlaying() && mSubs != null) {
            Collection<Caption> subtitles = mSubs.captions.values();
            double currentTime = player.getPosition() - offset;
            if (mLastSub != null && currentTime >= mLastSub.start.getMilliseconds() && currentTime <= mLastSub.end.getMilliseconds()) {
                showTimedCaptionText(mLastSub);
            } else {
                for (Caption caption : subtitles) {
                    if (currentTime >= caption.start.getMilliseconds() && currentTime <= caption.end.getMilliseconds()) {
                        mLastSub = caption;
                        showTimedCaptionText(caption);
                        break;
                    } else if (currentTime > caption.end.getMilliseconds()) {
                        showTimedCaptionText(null);
                    }
                }
            }
        }
    }

    protected void showTimedCaptionText(final Caption text) {
        mDisplayHandler.post(new Runnable() {
            @Override
            public void run() {
                if (text == null) {
                    if (textOutput != null) {
                        textOutput.onCues(Collections.<Cue>emptyList());
                    }
                    return;
                }
                SpannableStringBuilder styledString = (SpannableStringBuilder) Html.fromHtml(text.content);

                ForegroundColorSpan[] toRemoveSpans = styledString.getSpans(0, styledString.length(), ForegroundColorSpan.class);
                for (ForegroundColorSpan remove : toRemoveSpans) {
                    styledString.removeSpan(remove);
                }

                if (styledString.length() > 0) {
                    if (styledString.charAt(styledString.length() - 1) == '\n') {
                        styledString.delete(styledString.length() - 1, styledString.length());
                    }
                }
                if (textOutput != null) {
                    List<Cue> cues = new ArrayList<>();
                    cues.add(new Cue(styledString));
                    textOutput.onCues(cues);
                }
            }
        });
    }

    private String getSubtitleFile(String subtitleUrl) throws IOException {
        File subtitleFile = context.getFileStreamPath(String.valueOf(subtitleUrl.hashCode()));
        if (subtitleFile.exists()) {
            Log.d(TAG, "Subtitle already exists");
            return subtitleFile.getAbsolutePath();
        }
        Log.d(TAG, "Subtitle does not exists, download it.");
        // Copy the file from the res/raw folder to your app folder on the
        // device
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            Response execute = new OkHttpClient().newCall(new Request.Builder().url(subtitleUrl).build()).execute();
            inputStream = execute.body().byteStream();
            outputStream = new FileOutputStream(subtitleFile, false);
            copyFile(inputStream, outputStream);
            return subtitleFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStreams(inputStream, outputStream);
        }
        return "";
    }

    private void copyFile(InputStream inputStream, OutputStream outputStream)
            throws IOException {
        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }
    }

    // A handy method I use to close all the streams
    private void closeStreams(Closeable... closeables) {
        if (closeables != null) {
            for (Closeable stream : closeables) {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class CheckSubsTimerTask extends TimerTask {
        @Override
        public void run() {
            try {
                checkSubs();
            } catch (IllegalStateException e) {
                subTimerTask.cancel();
            }
        }
    }

}
