package com.jtv.player.subtitles;

import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextRenderer;
import com.google.android.exoplayer2.ui.SubtitleView;

import java.util.List;

public class JTVTextOutput implements TextRenderer.Output {

    public static final String TAG = JTVTextOutput.class.getSimpleName();

    private SubtitleView subtitleView;

    public JTVTextOutput(SubtitleView subtitleView) {
        this.subtitleView = subtitleView;
    }

    public void setSubtitleView(SubtitleView subtitleView) {
        this.subtitleView = subtitleView;
    }

    /**
     * From ExoPlayer
     * Native player just add text to first Cue
     *
     * @param cues The {@link Cue}s.
     */
    @Override
    public void onCues(List<Cue> cues) {
        if (subtitleView != null) {
            subtitleView.onCues(cues);
        }
    }

}
