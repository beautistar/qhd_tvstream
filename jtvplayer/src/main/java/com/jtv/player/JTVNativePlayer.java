package com.jtv.player;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.exoplayer2.util.Util;
import com.jtv.player.exceptions.HttpException;
import com.jtv.player.subtitles.JTVSubtitleHandler;
import com.jtv.player.subtitles.JTVTextOutput;
import com.jtv.player.trackselection.NativeTrack;
import com.jtv.player.trackselection.OnTrackChangedListener;
import com.jtv.player.trackselection.Track;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.media.MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START;

public class JTVNativePlayer implements JTVPlayer, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnBufferingUpdateListener, SurfaceHolder.Callback, MediaPlayer.OnVideoSizeChangedListener {

    private final static String TAG = "JTVNativePlayer";

    private final Context context;
    private final boolean debug;
    private MediaPlayer player;
    private QHDListener listener;
    private boolean playWhenReady;

    private SurfaceHolder surface;
    private float volume = 1f;

    private Map<String, String> headers;
    private boolean isPrepared;
    private String url;

    private JTVSubtitleHandler subtitleHandler;
    private BandwidthEstimateListener bandwidthEstimateListener;
    private OnTrackChangedListener onTrackChangedListener;

    public JTVNativePlayer(Context context, boolean debug) {
        this.context = context;
        headers = new HashMap<>();
        headers.put("User-Agent", Util.getUserAgent(context, context.getString(R.string.app_name)));
        this.debug = debug;
    }

    @Override
    public void prepare() {
        if (player == null) {
            player = new MediaPlayer();
            player.setOnErrorListener(this);
            player.setOnCompletionListener(this);
            player.setOnPreparedListener(this);
            player.setOnBufferingUpdateListener(this);
            player.setScreenOnWhilePlaying(true);
            player.setOnVideoSizeChangedListener(this);
            player.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {
                    if (what == MEDIA_INFO_VIDEO_RENDERING_START) {
                        Log.d(TAG, "onInfo: Video start rendering: " + extra);
                    }
                    if (what == 703) {
                        Log.d(TAG, "onInfo: used bandwidth: " + extra + "kbps");
                        if (bandwidthEstimateListener != null) {
                            bandwidthEstimateListener.onBandwidthEstimate(extra * 1000);
                        }
                    }
                    return false;
                }
            });
            if (surface != null && !surface.isCreating()) {
                player.setSurface(surface.getSurface());
            }
            player.setVolume(volume, volume);
            subtitleHandler = new JTVSubtitleHandler(context, this);
            Log.d(TAG, "Player is now prepared.");
        }
    }

    @Override
    public void release() {
        if (player != null) {
            player.release();
            player = null;
        }
        if (subtitleHandler != null) {
            subtitleHandler.release();
        }
        if (surface != null) {
            this.surface.removeCallback(this);
        }
        isPrepared = false;
        Log.d(TAG, "Player released.");
    }

    @Override
    public void setSurface(SurfaceView surface) {
        this.surface = surface.getHolder();
        if (surface != null) {
            surface.getHolder().addCallback(this);
            if (!surface.getHolder().isCreating() && surface.getHolder().getSurface().isValid()) {
                player.setSurface(surface.getHolder().getSurface());
            }
        }
    }

    @Override
    public boolean isPlaying() {
        return player != null && player.isPlaying();
    }

    @Override
    public long getPosition() {
        return player != null && isPrepared ? player.getCurrentPosition() : 0;
    }

    @Override
    public long getDuration() {
        return player != null && isPrepared ? player.getDuration() : 0;
    }


    @Override
    public void play(String url) {
        play(url, null);
    }

    @Override
    public void play(String url, String subtitleUrl) {
        subtitleHandler.setUrl(subtitleUrl);
        if (this.url != null) {
            if (this.url.equals(url) && isPrepared) {
                if (subtitleUrl != null) {
                    subtitleHandler.startSubtitles();
                } else {
                    subtitleHandler.stop();
                }
                return;
            }
        }
        if (player != null && url != null) {
            try {
                if (debug) {
                    Log.d(TAG, "Playing stream: " + url);
                }
                player.reset();
                player.setDataSource(context, Uri.parse(url), headers);
                player.prepareAsync();
                this.url = url;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {
        if (player != null && player.isPlaying()) {
            player.stop();
            Log.d(TAG, "Player stopped.");
        }
    }

    @Override
    public void seekTo(long position) {
        if (player != null) {
            player.seekTo((int) position);
        }
    }

    @Override
    public void setVolume(float volume) {
        if (player != null) {
            player.setVolume(volume, volume);
        } else {
            this.volume = volume;
        }
        Log.d(TAG, "Player set volume to: " + volume);
    }

    @Override
    public void setPlayWhenReady(boolean playWhenReady) {
        this.playWhenReady = playWhenReady;
        if (player != null) {
            if (playWhenReady && !player.isPlaying() && isPrepared) {
                // TODO: 2016-12-21 resume subtitles
                player.start();
            }
            if (!playWhenReady && player.isPlaying()) {
                // TODO: 2016-12-21 pause subtitles
                player.pause();
            }
        }
    }

    @Override
    public void setListener(QHDListener listener) {
        this.listener = listener;
    }

    @Override
    public void setTextOutput(JTVTextOutput textOutput) {
        if (subtitleHandler != null) {
            subtitleHandler.setTextOutput(textOutput);
        }
    }

    @Override
    public void setSpeed(float speed) {
        if (player != null) {
            if (player.isPlaying()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    player.getPlaybackParams().setSpeed(speed);
                }
            }
        }
    }

    @Override
    public long getSubtitleOffset() {
        if (subtitleHandler != null) {
            return subtitleHandler.getOffset();
        }
        return 0;
    }

    @Override
    public void setSubtitleOffset(long offset) {
        if (subtitleHandler != null) {
            subtitleHandler.setOffset(offset);
        }
    }

    @Override
    public void setBandwidthEstimateListener(BandwidthEstimateListener listener) {
        bandwidthEstimateListener = listener;
    }

    @Override
    public int getPlayerId() {
        return PLAYER_NATIVE;
    }

    @Override
    public List<Track> getTracksInfo() {
//        return new ArrayList<>();
        List<Track> tracks = new ArrayList<>();
        if (player != null) {
            MediaPlayer.TrackInfo[] trackInfo = player.getTrackInfo();
            for (MediaPlayer.TrackInfo info : trackInfo) {
                tracks.add(new NativeTrack(info.hashCode(), info.getLanguage(), info.getTrackType()));
            }
        }
        return tracks;
    }

    @Override
    public void selectTrack(Track track) {

    }

    @Override
    public void setOnTrackChangedListener(OnTrackChangedListener listener) {
        this.onTrackChangedListener = listener;
    }


    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        if (listener != null) {
            if (-extra == HttpException.STATUS_FORBIDDEN) {
                listener.onError(new HttpException(HttpException.STATUS_FORBIDDEN, "MediaPlayer error: invalid token"));
            } else if (-extra == HttpException.STATUS_NOT_FOUND) {
                listener.onError(new HttpException(HttpException.STATUS_NOT_FOUND, "MediaPlayer error: stream not found"));
            } else {
                listener.onError(new RuntimeException(String.format(Locale.getDefault(), "Media Player error: (%d, %d)", what, extra)));
                // TODO: 2016-10-07 make better
            }
            return true;
        }
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        changeState(JTVPlayer.STATE_ENDED);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        isPrepared = true;
        player.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
        changeState(JTVPlayer.STATE_READY);
        if (!player.isPlaying()) {
            start();
        }
        if (subtitleHandler.getUrl() != null) {
            subtitleHandler.startSubtitles();
        }
    }


    private void changeState(int state) {
        if (listener != null) {
            listener.onStateChanged(state);
        }
    }


    private void start() {
        if (playWhenReady && player != null) {
            player.start();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (player != null) {
            if (surfaceHolder.getSurface().isValid()) {
                player.setSurface(surfaceHolder.getSurface());
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        if (player != null) {
            if (surfaceHolder.getSurface().isValid()) {
                player.setSurface(surfaceHolder.getSurface());
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (player != null) {
            player.setSurface(null);
        }
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int height) {
        if (listener != null) {
            listener.onVideoSizeChanged(width, height, ((float) width) / ((float) height));
        }
    }

}
