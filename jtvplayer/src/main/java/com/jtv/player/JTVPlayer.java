package com.jtv.player;

import android.support.annotation.IntDef;
import android.view.SurfaceView;

import com.jtv.player.subtitles.JTVTextOutput;
import com.jtv.player.trackselection.OnTrackChangedListener;
import com.jtv.player.trackselection.Track;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public interface JTVPlayer {


    float RATIO_4_3 = 4f / 3f;
    float RATIO_16_9 = 16f / 9f;
    float RATIO_21_9 = 21f / 9f;

    int STATE_IDLE = 1;
    int STATE_BUFFERING = 2;
    int STATE_READY = 3;
    int STATE_ENDED = 4;

    int PLAYER_NATIVE = 0;
    int PLAYER_EXO = 1;
//    int PLAYER_VLC = 2;

    void prepare();

    void release();

    void setSurface(SurfaceView surface);

    void play(String url);

    void play(String url, String subtitleUrl);

    boolean isPlaying();

    long getPosition();

    long getDuration();

    void stop();

    void seekTo(long position);

    void setVolume(float volume);

    void setPlayWhenReady(boolean playWhenReady);

    void setListener(QHDListener listener);

    void setTextOutput(JTVTextOutput textOutput);

    void setSpeed(float speed);

    long getSubtitleOffset();

    void setSubtitleOffset(long offset);

    void setBandwidthEstimateListener(BandwidthEstimateListener listener);

    @PlayerCode
    int getPlayerId();

    List<Track> getTracksInfo();

    void selectTrack(Track track);

    void setOnTrackChangedListener(OnTrackChangedListener listener);

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PLAYER_NATIVE, PLAYER_EXO})
            //, PLAYER_VLC
    @interface PlayerCode {
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATE_IDLE, STATE_BUFFERING, STATE_READY, STATE_ENDED})
    @interface PlayerState {
    }

    interface QHDListener {

        void onStateChanged(@PlayerState int state);

        void onError(Exception exception);

        void onVideoSizeChanged(int width, int height, float pixelWidthHeightRatio);

    }
}
