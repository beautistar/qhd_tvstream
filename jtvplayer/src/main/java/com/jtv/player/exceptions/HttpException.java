package com.jtv.player.exceptions;

public class HttpException extends Exception {

    public static final int STATUS_FORBIDDEN = 403;
    public static final int STATUS_NOT_FOUND = 404;

    private final int responseCode;

    public HttpException(int responseCode, String message) {
        super(message);
        this.responseCode = responseCode;
    }

    public HttpException(int responseCode, String s, Throwable cause) {
        super(s, cause);
        this.responseCode = responseCode;
    }

    public int getStatus() {
        return responseCode;
    }

}
