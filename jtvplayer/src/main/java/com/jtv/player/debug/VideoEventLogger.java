package com.jtv.player.debug;

import android.util.Log;
import android.view.Surface;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

public class VideoEventLogger implements VideoRendererEventListener {

    private final String tag;

    public VideoEventLogger(String tag) {
        this.tag = tag;
    }

    @Override
    public void onVideoEnabled(DecoderCounters counters) {
        Log.d(tag, "VideoEnabled: " + counters.toString());
    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
        Log.d(tag, "DecoderInitialized: " + decoderName);
    }

    @Override
    public void onVideoInputFormatChanged(Format format) {
        Log.d(tag, "VideoInputFormatChanged: " + format.toString());
    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {
        Log.d(tag, "DroppedFrames: count=" + count + "; elapsedMs=" + elapsedMs);
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        Log.d(tag, String.format("VideoSizeChanged: w=%s h=%s ratio=%s", width, height, pixelWidthHeightRatio));
    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {
        Log.d(tag, "Rendered first frame");
    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {
        Log.d(tag, "Video disabled: " + counters.toString());
    }
}
