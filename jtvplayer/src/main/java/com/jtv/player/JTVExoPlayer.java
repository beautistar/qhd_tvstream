package com.jtv.player;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.jtv.player.debug.VideoEventLogger;
import com.jtv.player.exceptions.HttpException;
import com.jtv.player.subtitles.JTVSubtitleHandler;
import com.jtv.player.subtitles.JTVTextOutput;
import com.jtv.player.trackselection.ExoTrack;
import com.jtv.player.trackselection.OnTrackChangedListener;
import com.jtv.player.trackselection.Track;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

public class JTVExoPlayer implements JTVPlayer,
        SimpleExoPlayer.VideoListener {

    private static String TAG = "JTVExoPlayer";

    private static final TrackSelection.Factory FIXED_FACTORY = new FixedTrackSelection.Factory();

    private final Handler handler;
    private final Context context;
    private final boolean debug;
    private SimpleExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private QHDListener listener;

    private DataSource.Factory mediaDataSourceFactory;

    private JTVSubtitleHandler subtitleHandler;
    private String url;
    private BandwidthEstimateListener bandwidthEstimateListener;
    private TrackSelection.Factory adaptiveTrackSelectionFactory;

    private OnTrackChangedListener onTrackChangedListener;

    private JTVBandwithMeter bandwithMeter;

    public JTVExoPlayer(Context context, Handler handler, boolean debug) {
        this.context = context;
        this.handler = handler;
        this.mediaDataSourceFactory = buildDataSourceFactory();
        this.debug = debug;
        this.bandwithMeter = new JTVBandwithMeter(new Handler(), new BandwidthMeter.EventListener() {
            @Override
            public void onBandwidthSample(int elapsedMs, long bytes, long bitrate) {
                if (bandwidthEstimateListener != null) {
                    bandwidthEstimateListener.onBandwidthEstimate(bitrate);
                }
                if (JTVExoPlayer.this.debug) {
                    Log.d(TAG, "onBandwidthSample: elapsedMs: " + elapsedMs + ", bitrate: " + bitrate);
                }
            }
        }, 2000);
    }

    @Override
    public void prepare() {
        if (player == null) {
            adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory(
                    bandwithMeter,
                    500_000,
                    3000,
                    2000,
                    1000,
                    0.75f
            );
            trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);

            player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

            player.addVideoListener(this);
            if (debug) {
                player.addVideoDebugListener(new VideoEventLogger(TAG));
            }
            player.addListener(new PlayerListener());
            subtitleHandler = new JTVSubtitleHandler(context, this);
            Log.i(TAG, "Player has been prepared");
        }
    }

    @Override
    public void release() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.release();
            player = null;
        }
        if (subtitleHandler != null) {
            subtitleHandler.release();
        }
        Log.i(TAG, "Player has been released");
    }

    @Override
    public void setSurface(SurfaceView surface) {
        if (player != null) {
            player.setVideoSurfaceView(surface);
        } else {
            Log.e(TAG, "Player needs to be prepared first");
        }
    }

    @Override
    public void play(String url) {
        play(url, null);
    }

    @Override
    public void play(String url, String subtitleUrl) {
        subtitleHandler.setUrl(subtitleUrl);
        if (this.url != null) {
            if (this.url.equals(url) && player.getPlaybackState() == Player.STATE_READY) {
                if (subtitleUrl != null) {
                    subtitleHandler.startSubtitles();
                } else {
                    subtitleHandler.stop();
                }
                return;
            }
        }
        if (player != null && url != null) {
            bandwithMeter.reset();
            MediaSource videoSource = buildMediaSource(url);
            player.prepare(videoSource);
            this.url = url;
            if (debug) {
                Log.i(TAG, "Playing stream: " + url);
            }
        }
    }

    @Override
    public boolean isPlaying() {
        return player != null && player.getPlayWhenReady();
    }

    @Override
    public long getPosition() {
        return player != null ? player.getCurrentPosition() : 0;
    }

    @Override
    public long getDuration() {
        return player != null ? player.getDuration() : 0;
    }

    private MediaSource buildMediaSource(String url) {
        return buildMediaSource(Uri.parse(url));
    }

    private MediaSource buildMediaSource(Uri uri) {
        int type = Util.inferContentType(uri.getLastPathSegment());
        switch (type) {
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(mediaDataSourceFactory)
                        .createMediaSource(uri, handler, new DefaultAdaptiveMediaSourceEventListener() {

                            int started = 0;

                            @Override
                            public void onLoadStarted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs) {
                                super.onLoadStarted(dataSpec, dataType, trackType, trackFormat, trackSelectionReason, trackSelectionData, mediaStartTimeMs, mediaEndTimeMs, elapsedRealtimeMs);
                                bandwithMeter.onTransferStart(this, dataSpec);
                                started ++;
                                Log.d(TAG, "onLoadStarted");
                            }

                            @Override
                            public void onLoadCompleted(DataSpec dataSpec, int dataType, int trackType, Format trackFormat, int trackSelectionReason, Object trackSelectionData, long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, long bytesLoaded) {
                                super.onLoadCompleted(dataSpec, dataType, trackType, trackFormat, trackSelectionReason, trackSelectionData, mediaStartTimeMs, mediaEndTimeMs, elapsedRealtimeMs, loadDurationMs, bytesLoaded);
                                Log.d(TAG, "onLoadCompleted: strated=" + started);
                                if(started > 0){
                                    bandwithMeter.onBytesTransferred(this, ((int) bytesLoaded));
                                    bandwithMeter.onTransferEnd(this);
                                    started--;
                                }
                            }

                            @Override
                            public void onDownstreamFormatChanged(int trackType,
                                                                  Format trackFormat,
                                                                  int trackSelectionReason,
                                                                  Object trackSelectionData,
                                                                  long mediaTimeMs) {
                                Log.i(TAG, "onDownstreamFormatChanged: " + trackFormat.toString());
                                Log.d(TAG, "onDownstreamFormatChanged: " + Utils.buildTrackName(trackFormat));
                                if (onTrackChangedListener != null) {
                                    onTrackChangedListener.onTrackChanged(new ExoTrack(0, Utils.buildTrackName(trackFormat), trackFormat.bitrate, 0, 0));
                                }
                            }
                        });
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(mediaDataSourceFactory)
                        .createMediaSource(uri, handler, null);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    private DataSource.Factory buildDataSourceFactory() {
        return new DefaultDataSourceFactory(
                context,
                bandwithMeter,
                new OkHttpDataSourceFactory(new OkHttpClient(), getUserAgent(), bandwithMeter)
        );
    }

    private String getUserAgent() {
        return Util.getUserAgent(context, context.getString(R.string.app_name));
    }


    @Override
    public void stop() {
        if (player != null) {
            player.stop();
            Log.i(TAG, "Player has been stopped.");
        }
    }

    @Override
    public void seekTo(long position) {
        if (player != null) {
            player.seekTo(position);
        }
    }

    @Override
    public void setVolume(float volume) {
        if (player != null) {
            player.setVolume(volume);
            Log.i(TAG, "Setting volume to: " + volume);
        }
    }

    @Override
    public void setPlayWhenReady(boolean playWhenReady) {
        if (player != null) {
            player.setPlayWhenReady(playWhenReady);
        }
    }

    @Override
    public void setListener(QHDListener listener) {
        this.listener = listener;
    }

    @Override
    public void setTextOutput(JTVTextOutput textOutput) {
        if (subtitleHandler != null) {
            subtitleHandler.setTextOutput(textOutput);
        }
    }

    @Override
    public void setSpeed(float speed) {
        if (player != null) {
            player.setPlaybackParameters(new PlaybackParameters(speed, 1f));
        }
    }

    @Override
    public long getSubtitleOffset() {
        if (subtitleHandler != null) {
            return subtitleHandler.getOffset();
        }
        return 0;
    }

    @Override
    public void setSubtitleOffset(long offset) {
        if (subtitleHandler != null) {
            subtitleHandler.setOffset(offset);
        }
    }

    @Override
    public void setBandwidthEstimateListener(BandwidthEstimateListener listener) {
        bandwidthEstimateListener = listener;
    }

    @Override
    public int getPlayerId() {
        return PLAYER_EXO;
    }

    @Override
    public List<Track> getTracksInfo() {
        List<Track> tracks = new ArrayList<>();
        if (player != null) {
            MappingTrackSelector.MappedTrackInfo trackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (trackInfo != null) {
                int videoRendererIndex = findVideoTrackGroup(trackInfo);
                TrackGroupArray trackGroups = trackInfo.getTrackGroups(videoRendererIndex);
                tracks.add(new ExoTrack(-1, "Auto", 0, 0, videoRendererIndex));
                for (int groupIndex = 0; groupIndex < trackGroups.length; groupIndex++) {
                    TrackGroup group = trackGroups.get(groupIndex);
                    for (int trackIndex = 0; trackIndex < group.length; trackIndex++) {
                        String trackname = Utils.buildTrackName(group.getFormat(trackIndex));
                        tracks.add(new ExoTrack(trackIndex, trackname, group.getFormat(trackIndex).bitrate, groupIndex, videoRendererIndex));
                    }
                }
            }

        }

        return tracks;
    }

    @Override
    public void selectTrack(Track track) {
        if (track.id() == -1) { // AUTO
            trackSelector.clearSelectionOverrides();
        } else {
            if (track instanceof ExoTrack) {
                ExoTrack exoTrack = ((ExoTrack) track);
                MappingTrackSelector.MappedTrackInfo trackInfo = trackSelector.getCurrentMappedTrackInfo();
                trackSelector.setSelectionOverride(
                        exoTrack.getRendererIndex(),
                        trackInfo.getTrackGroups(exoTrack.getRendererIndex()),
                        new MappingTrackSelector.SelectionOverride(FIXED_FACTORY, exoTrack.getGroupId(), exoTrack.id())
                );
            }
        }
    }

    @Override
    public void setOnTrackChangedListener(OnTrackChangedListener listener) {
        this.onTrackChangedListener = listener;
    }

    private int findVideoTrackGroup(MappingTrackSelector.MappedTrackInfo trackInfo) {
        if (player != null && trackInfo != null) {
            for (int i = 0; i < trackInfo.length; i++) {
                int rendererType = player.getRendererType(i);
                if (rendererType == C.TRACK_TYPE_VIDEO) {
                    return i;
                }
            }
        }
        return -1;
    }


    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees,
                                   float pixelWidthHeightRatio) {
        if (listener != null) {
            listener.onVideoSizeChanged(width, height, pixelWidthHeightRatio);
        }
    }

    @Override
    public void onRenderedFirstFrame() {
        Log.i(TAG, "onRenderedFirstFrame");
    }

    private class PlayerListener extends Player.DefaultEventListener {

        @Override
        public void onPositionDiscontinuity(int reason) {
            String namedReason = "";
            switch (reason) {
                case ExoPlayer.DISCONTINUITY_REASON_INTERNAL:
                    namedReason = "INTERNAL";
                    break;
                case ExoPlayer.DISCONTINUITY_REASON_PERIOD_TRANSITION:
                    namedReason = "PERIOD_TRANSITION";
                    break;
                case ExoPlayer.DISCONTINUITY_REASON_SEEK:
                    namedReason = "SEEK";
                    break;
                case ExoPlayer.DISCONTINUITY_REASON_SEEK_ADJUSTMENT:
                    namedReason = "SEEK_AJUSTMENT";
                    break;
            }
            Log.i(TAG, "onPositionDiscontinuity: " + namedReason);
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            if (listener != null) {
                if (error.getCause() instanceof HttpDataSource.InvalidResponseCodeException) {
                    HttpDataSource.InvalidResponseCodeException exception = (HttpDataSource.InvalidResponseCodeException) error.getCause();
                    listener.onError(new HttpException(exception.responseCode, "Invalid response", error.getCause()));
                } else {
                    listener.onError(error);
                }
            }
        }


        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (listener != null) {
                listener.onStateChanged(playbackState);
                if (playbackState == STATE_READY) {
                    if (subtitleHandler.getUrl() != null) {
                        subtitleHandler.startSubtitles();
                    }
                }
            }
        }


        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }
    }

}
